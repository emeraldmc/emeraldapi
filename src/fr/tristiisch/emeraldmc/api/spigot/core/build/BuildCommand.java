package fr.tristiisch.emeraldmc.api.spigot.core.build;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class BuildCommand implements CommandExecutor {

	private static int[] powers;

	public BuildCommand(final int... power) {
		powers = power;
	}

	public static void setPower(final int... power) {
		powers = power;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}

		final Player player = (Player) sender;
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final AccountProvider accountProvider = new AccountProvider(player.getUniqueId());
			final OlympaPlayer emeraldPlayer = accountProvider.getEmeraldPlayer();
			if(!emeraldPlayer.hasPower(powers)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}
			Build.toggle(player);
		});

		return true;
	}
}
