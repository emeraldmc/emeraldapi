package fr.tristiisch.emeraldmc.api.bungee.session;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public class Session {

	int emeraldPlayerId;
	int id;
	int ipId;
	long start;
	long stop;

	public Session(final int ipId, final int emeraldPlayerId) {
		this.ipId = ipId;
		this.start = Utils.getCurrentTimeinSeconds();
		this.emeraldPlayerId = emeraldPlayerId;
	}

	public Session(final int id, final int ipId, final int emeraldPlayerId, final long start, final long stop) {
		this.id = id;
		this.ipId = ipId;
		this.emeraldPlayerId = emeraldPlayerId;
		this.start = start;
		this.stop = stop;
	}

	public String getDifference() {
		return Utils.timestampToDuration(this.stop - this.start);
	}

	public int getEmeraldPlayerId() {
		return this.emeraldPlayerId;
	}

	public int getId() {
		return this.id;
	}

	public int getIpId() {
		return this.ipId;
	}

	public long getStart() {
		return this.start;
	}

	public String getStartDate() {
		return Utils.timestampToDateAndHour(this.start);
	}

	public long getStop() {
		return this.stop;
	}

	public String getStopDate() {
		return Utils.timestampToDateAndHour(this.stop);
	}

	public void setStop(final long stop) {
		this.stop = stop;
	}
}
