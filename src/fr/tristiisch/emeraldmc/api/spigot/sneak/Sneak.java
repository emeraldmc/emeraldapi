package fr.tristiisch.emeraldmc.api.spigot.sneak;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;

public class Sneak {

	private static final List<UUID> sneak = new ArrayList<>();

	public static List<UUID> getSneaks() {
		return sneak;
	}

	public static boolean isSneak(final Player player) {
		return sneak.contains(player.getUniqueId());
	}
}
