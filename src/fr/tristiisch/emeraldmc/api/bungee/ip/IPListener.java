package fr.tristiisch.emeraldmc.api.bungee.ip;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class IPListener implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPreLogin(final PreLoginEvent event) {

		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			final PendingConnection connection = event.getConnection();
			final String ip = connection.getAddress().getAddress().getHostAddress();

			if(IPManagment.isVPN(ip)) {
				System.out.println(Utils.color("&4[&nVPN&4] " + connection.getName() + ": " + ip));
				event.setCancelReason(BungeeUtils.connectScreen("&cLes VPN sont interdit."));
				event.setCancelled(true);
			}
		});
	}
}
