package fr.tristiisch.emeraldmc.api.bungee.commands;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer.EmeraldServerStatus;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class ServerCommand extends Command {

	private final int power;

	public ServerCommand(final String command, final EmeraldGroup group, final String... aliases) {
		super(command, null, aliases);
		this.power = group.getPower();
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
			return;
		}

		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			final ProxiedPlayer player = (ProxiedPlayer) sender;

			if(new AccountProvider(player.getUniqueId()).getEmeraldPlayer().hasPowerLessThan(this.power)) {
				sender.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
				return;
			}

			if(args.length < 1) {
				sender.sendMessage(Utils.color("&cUsage &7» &c/server <serveur>"));
				return;
			}

			final String serverName = args[0];
			final EmeraldServer emeraldServer = EmeraldServers.getServer(serverName);
			if(emeraldServer == null) {
				sender.sendMessage(Utils.color("&cLe serveur &4" + serverName + " &cn'existe pas."));
				return;
			}

			if(emeraldServer.getStatus().isOpen()) {
				player.sendMessage(Utils.color("&aConnexion au serveur §2" + emeraldServer.getName() + "&a..."));
				player.connect(ProxyServer.getInstance().getServerInfo(serverName));
			} else if(emeraldServer.getStatus().equals(EmeraldServerStatus.CLOSE)) {
				sender.sendMessage(Utils.color("&cLe serveur &4" + emeraldServer.getName() + " &cest indisponible (&4" + emeraldServer.getError() + "&c)."));
			} else {
				sender.sendMessage(Utils.color("&cLe serveur &4" + emeraldServer.getName() + " &cest " + emeraldServer.getStatus().getColor() + emeraldServer.getStatus().getName() + "&c."));
			}
		});

	}
}
