package fr.tristiisch.emeraldmc.api.spigot.utils;

public class EmeraldArea {

	final private int id;
	final private String name;
	final private Cuboid cuboid;
	
	private boolean pvp;
	private boolean useEnderpearlTo;
	private boolean useEnderpearlFrom;
	private boolean takeDamage;
	
	public EmeraldArea(int id, String name, Cuboid cuboid) {
		this.id = id;
		this.name = name;
		this.cuboid = cuboid;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public Cuboid getCuboid() {
		return cuboid;
	}
	
	public boolean canPvp() {
		return pvp;
	}

	public void setPvp(boolean pvp) {
		this.pvp = pvp;
	}
	
	public boolean canUseEnderpearlTo() {
		return useEnderpearlTo;
	}
	
	public void setUseEnderpearlTo(boolean useEnderpearlTo) {
		this.useEnderpearlTo = useEnderpearlTo;
	}

	public boolean canUseEnderpearlFrom() {
		return useEnderpearlFrom;
	}

	public void setUseEnderpearlFrom(boolean useEnderpearlFrom) {
		this.useEnderpearlFrom = useEnderpearlFrom;
	}

	public boolean canTakeDamage() {
		return takeDamage;
	}

	public void setTakeDamage(boolean takeDamage) {
		this.takeDamage = takeDamage;
	}
}
