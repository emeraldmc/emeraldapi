package fr.tristiisch.emeraldmc.api.spigot.customevent;

import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockEvent;

public class ArrowHitBlockEvent extends BlockEvent {

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	private final Arrow arrow;

	public ArrowHitBlockEvent(final Arrow arrow, final Block block) {
		super(block);
		this.arrow = arrow;
	}

	public Arrow getArrow() {
		return this.arrow;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

}