package fr.tristiisch.emeraldmc.api.spigot.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import net.minecraft.server.v1_8_R3.ItemArmor;

public class HatCommand implements CommandExecutor {

	private static int power;

	public static void setPower(final EmeraldGroup group) {
		power = group.getPower();
	}

	public HatCommand(final EmeraldGroup group) {
		setPower(group);
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			Player player = null;
			OlympaPlayer emeraldPlayer = null;
			if(sender instanceof Player) {
				player = (Player) sender;
				emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(emeraldPlayer.hasPowerLessThan(power)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}
			}

			final PlayerInventory playerInventory = player.getInventory();
			final ItemStack itemInHand = playerInventory.getItemInHand();

			if(CraftItemStack.asNMSCopy(itemInHand).getItem() instanceof ItemArmor) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cImpossible d'équiper une armure."));
				return;
			}

			final int itemInHandSlot = playerInventory.getHeldItemSlot();
			if(itemInHand == null) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez avoir un objet dans la main."));
				return;
			}

			final ItemStack oldItem;
			if(args.length == 0 || args[0].equalsIgnoreCase("casque")) {
				oldItem = playerInventory.getHelmet();
				playerInventory.setHelmet(itemInHand);
			} else if(args[0].equalsIgnoreCase("plastron")) {
				oldItem = playerInventory.getChestplate();
				playerInventory.setChestplate(itemInHand);
			} else if(args[0].equalsIgnoreCase("pantalon")) {
				oldItem = playerInventory.getLeggings();
				playerInventory.setLeggings(itemInHand);
			} else if(args[0].equalsIgnoreCase("chaussure")) {
				oldItem = playerInventory.getBoots();
				playerInventory.setBoots(itemInHand);
			} else {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez choisir entre casque, plastron, pantalon et chaussure."));
				return;
			}
			playerInventory.clear(itemInHandSlot);

			if(oldItem != null) {
				playerInventory.setItem(itemInHandSlot, oldItem);
			}
		});
		return true;
	}

}
