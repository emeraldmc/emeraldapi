package fr.tristiisch.emeraldmc.api.spigot.infogroup;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public class InfoGroupCommand implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}
		final Player player = (Player) sender;
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.RESPMODO)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}

			final EmeraldGroup group = EmeraldGroup.getByName(args[0]);
			final List<OlympaPlayer> players = MySQL.getPlayersByGroup(group);
			final GuiTools gui = new GuiTools("&6Group: " + group.getPrefix(), "infogroup", players.size());
			int i = gui.getFirstSlot();
			for(final OlympaPlayer emeraldTarget : players) {
				final ItemStack item = ItemTools.createPlayerSkull(emeraldTarget.getName(), 1, "&2" + emeraldTarget.getName(), Arrays.asList(
						"",
						Utils.color("&7Grade" + Utils.withOrWithoutS(emeraldTarget.getGroups().size()) + ": &e" + String.join(", ", emeraldTarget.getGroupsName())),
						Utils.color("&7Première connexion: &e" + Utils.timestampToDate(emeraldTarget.getFirstCo())),
						Utils.color("&7Dernière connexion: &e" + Utils.timestampToDuration(emeraldTarget.getLastCo())),
						Utils.color("&7Minerais: &e" + emeraldTarget.getMinerais()),
						Utils.color("&7Pierre" + Utils.withOrWithoutS(emeraldTarget.getPierres()) + ": &e" + emeraldTarget.getPierres()),
						""));
				gui.setItem(i++, item);
			}
			gui.openInventory(player);

		});
		return true;
	}
}
