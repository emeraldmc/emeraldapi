package fr.tristiisch.emeraldmc.api.spigot.items;

import org.bukkit.enchantments.Enchantment;

public class ItemEnchant {

	private Enchantment enchantment;
	private boolean unsafe;
	private int level;

	public ItemEnchant(final Enchantment enchantment, final int level, final boolean unsafe) {
		this.enchantment = enchantment;
		this.level = level;
		this.unsafe = unsafe;
	}

	public ItemEnchant(final Enchantment enchantment, final int level) {
		this.enchantment = enchantment;
		this.level = level;
		this.unsafe = false;
	}

	public ItemEnchant(final Enchantment enchantment) {
		this.enchantment = enchantment;
		this.level = 1;
		this.unsafe = false;
	}

	public Enchantment getEnchantment() {
		return this.enchantment;
	}

	public void setEnchantment(final Enchantment enchantment) {
		this.enchantment = enchantment;
	}

	public int getLevel() {
		return this.level;
	}

	public void setLevel(final int level) {
		this.level = level;
	}

	public boolean isUnsafe() {
		return this.unsafe;
	}

	public void setUnsafe(final boolean unsafe) {
		this.unsafe = unsafe;
	}
}

