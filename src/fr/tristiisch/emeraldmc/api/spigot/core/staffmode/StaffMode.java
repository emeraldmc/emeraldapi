package fr.tristiisch.emeraldmc.api.spigot.core.staffmode;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.clear.Clear;
import fr.tristiisch.emeraldmc.api.spigot.core.vanish.Vanish;
import fr.tristiisch.emeraldmc.api.spigot.utils.PlayerData;

public class StaffMode {

	public static Map<UUID, PlayerData> players = new HashMap<>();

	public static void disable(final Player player) {

		if(player.getGameMode() != GameMode.CREATIVE) {
			player.setAllowFlight(false);
		}

		if(!player.isOp()) {
			player.teleport(EmeraldSpigot.getSpawn());
		}

		PlayerData.clearInventory(player);
		final PlayerData datas = players.get(player.getUniqueId());
		if(player.isOnline()) {

			final PlayerInventory inventory = player.getInventory();
			inventory.setContents(datas.getInventoryItemContents());
			inventory.setArmorContents(datas.getInventoryArmorContents());
		} else {
			/*
			 * final FileConfiguration config =
			 * CustomConfigUtils.getConfig(EmeraldSpigot.getInstance(), "data");
			 * config.set(player.getUniqueId().toString() + ".armure",
			 * InventoryToBase64.toBase64(datas));
			 * config.set(player.getUniqueId().toString() + ".inventaire",
			 * InventoryToBase64.toBase64(datas.getInventoryItemContents()));
			 */
		}

		Vanish.disable(player, false);
		players.remove(player.getUniqueId());
		player.sendMessage(Utils.color("&2EmeraldMC &7» &cMode staff désactivé"));
	}

	public static void enable(final Player player) {
		players.put(player.getUniqueId(), new PlayerData(player.getInventory().getContents(), player.getInventory().getArmorContents()));
		Clear.player(player);
		Vanish.enable(player, false);
		player.getInventory().setItem(0, StaffModeItem.Teleportation.getItemStack());
		player.getInventory().setItem(1, StaffModeItem.MisterFreeze.getItemStack());
		player.getInventory().setItem(2, StaffModeItem.Verificator.getItemStack());
		player.getInventory().setItem(3, StaffModeItem.Navette.getItemStack());
		player.getInventory().setItem(4, StaffModeItem.KB.getItemStack());

		player.getInventory().setItem(7, StaffModeItem.BOULET.getItemStack());
		player.getInventory().setItem(8, StaffModeItem.SevenLeagueBoots.getItemStack());
		player.setCompassTarget(EmeraldSpigot.getSpawn());

		player.setAllowFlight(true);
		player.sendMessage(Utils.color("&2EmeraldMC &7» &aMode staff activé"));
	}

	public static Player getClosestPlayer(final Location loc) {
		return loc.getWorld().getPlayers().stream().min((o1, o2) -> {
			return Double.compare(o1.getLocation().distanceSquared(loc), o2.getLocation().distanceSquared(loc));
		}).orElse(null);
	}

	public static boolean isStaffMode(final Player player) {
		return players.containsKey(player.getUniqueId());
	}
}
