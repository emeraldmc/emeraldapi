package fr.tristiisch.emeraldmc.api.bungee.lobby;

import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Lobby {

	@SuppressWarnings("deprecation")
	public static void connect(final ProxiedPlayer player) {

		final EmeraldServer lobby = getLobby();

		if(lobby == null) {
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cLes serveurs lobby sont inaccessibles, merci de réessayer plus tard."));
			return;
		}
		final ServerInfo server = lobby.getServerInfo();
		player.sendMessage(Utils.color("&2EmeraldMC &7» &aConnexion au serveur &2" + Utils.capitalize(lobby.getName()) + "&a..."));
		player.connect(server);

	}

	public static EmeraldServer getLobby() {
		return EmeraldServers.getLobbysServers()
				.stream()
				.filter(lobby -> lobby.getStatus().isOpen() && lobby.getInfo().getPlayers().getOnline() < lobby.getInfo().getPlayers().getMax())
				.findFirst()
				.orElse(null);
	}
}
