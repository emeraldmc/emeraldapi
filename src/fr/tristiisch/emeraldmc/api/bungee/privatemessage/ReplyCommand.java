package fr.tristiisch.emeraldmc.api.bungee.privatemessage;

import java.util.Arrays;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class ReplyCommand extends Command {

	public ReplyCommand(final String command, final String... aliases) {
		super(command, null, aliases);
		PrivateMessage.replyCommand.add(command);
		PrivateMessage.replyCommand.addAll(Arrays.asList(aliases));
	}

	@Override
	public final void execute(final CommandSender sender, final String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
			return;
		}

		final ProxiedPlayer proxiedPlayer = (ProxiedPlayer) sender;
		if(args.length == 0) {
			sender.sendMessage(Utils.color("&cUsage &7» &c/r <message>"));
			return;
		}

		final UUID targetUUID = PrivateMessage.getReply(proxiedPlayer);
		if(targetUUID == null) {
			sender.sendMessage(Utils.color("&6EmeraldMC &7» &cVous n'avez personne à qui répondre."));
			return;
		}
		final ProxiedPlayer targetPlayer = ProxyServer.getInstance().getPlayer(targetUUID);
		if(targetPlayer == null) {
			sender.sendMessage(Utils.color("&6EmeraldMC &7» &4" + BungeeUtils.getName(targetUUID) + "&c n'est plus connecté."));
			return;
		}

		final String message = String.join(" ", Arrays.copyOfRange(args, 0, args.length));

		PrivateMessage.send(proxiedPlayer, targetPlayer, message);

	}
}
