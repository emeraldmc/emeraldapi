package fr.tristiisch.emeraldmc.api.spigot.core.commands;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class HealCommand implements CommandExecutor {

	private static int[] power;

	public HealCommand(final EmeraldGroup... groups) {
		setPower(groups);
	}

	public static void setPower(final EmeraldGroup... groups) {
		power = Arrays.stream(groups).mapToInt(EmeraldGroup::getPower).toArray();
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			Player player = null;
			OlympaPlayer emeraldPlayer = null;
			if(sender instanceof Player) {
				player = (Player) sender;
				emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(!emeraldPlayer.hasPower(power)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}
			}

			if(args.length == 0) {
				final double i = player.getMaxHealth() - player.getHealth();
				player.setHealth(player.getMaxHealth());
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &aVous vous êtes soigner de " + Math.round(i) + " &c❤."));
			} /*
				 * else { final Player target = Bukkit.getPlayer(args[0]); if(target == null) {
				 * return; } target.setHealth(player.getMaxHealth()); sender.sendMessage(Utils.
				 * color("&cUsage &7» &c/gm <survival|creative|adventure|spectator>")); }
				 */
		});
		return true;
	}

}
