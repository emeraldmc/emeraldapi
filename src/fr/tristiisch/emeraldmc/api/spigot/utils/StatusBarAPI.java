package fr.tristiisch.emeraldmc.api.spigot.utils;

import org.bukkit.scheduler.*;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import org.bukkit.entity.*;
import java.lang.reflect.*;
import org.bukkit.*;
import java.util.*;

public class StatusBarAPI
{
    private static PlayerMap<FakeBoss> FakeBossMap;
    private static BukkitTask bossBarRefresh;
    
    public static boolean hasStatusBar(final Player player) {
        return StatusBarAPI.FakeBossMap.containsKey(player) && StatusBarAPI.FakeBossMap.get(player) != null;
    }
    
    public static void removeStatusBar(final Player player) {
        if (hasStatusBar(player)) {
            sendPacket(player, StatusBarAPI.FakeBossMap.get(player).getDestroyPacket());
            StatusBarAPI.FakeBossMap.remove(player);
        }
    }
    
    public static void setStatusBar(final Player player, String text, float percent) {
        FakeBoss boss = StatusBarAPI.FakeBossMap.containsKey(player) ? StatusBarAPI.FakeBossMap.get(player) : null;
        if (text.length() > 64) {
            text = text.substring(0, 63);
        }
        if (percent > 1.0f) {
            percent = 1.0f;
        }
        if (percent < 0.05f) {
            percent = 0.05f;
        }
        if (text.isEmpty() && boss != null) {
            removeStatusBar(player);
            return;
        }
        if (boss == null) {
            boss = new FakeBoss(player, text, percent);
            sendPacket(player, boss.getSpawnPacket());
            StatusBarAPI.FakeBossMap.put(player, boss);
            sendPacket(player, boss.getMetaPacket(boss.getWatcher()));
            sendPacket(player, boss.getTeleportPacket(getBossLocation(player)));
        }
        else {
            boss.setName(text);
            boss.setHealth(percent);
            sendPacket(player, boss.getMetaPacket(boss.getWatcher()));
            sendPacket(player, boss.getTeleportPacket(getBossLocation(player)));
        }
        if (StatusBarAPI.bossBarRefresh == null) {
            StatusBarAPI.bossBarRefresh = TaskManager.scheduleSyncRepeatingTask("BossBarRefresh", new BossBarRefreshRunnable(), 0, 10);
        }
    }
    
    public static void removeAllStatusBars() {
        for (final Player player : Bukkit.getOnlinePlayers()) {
            removeStatusBar(player);
        }
    }
    
    public static void setAllStatusBars(final String text, final float percent) {
        for (final Player player : Bukkit.getOnlinePlayers()) {
            setStatusBar(player, text, percent);
        }
    }
    
    private static void sendPacket(final Player player, final Object packet) {
        try {
            final Object nmsPlayer = ReflectionUtils.getHandle((Entity)player);
            final Field connectionField = nmsPlayer.getClass().getField("playerConnection");
            final Object connection = connectionField.get(nmsPlayer);
            final Method sendPacket = ReflectionUtils.getMethod(connection.getClass(), "sendPacket");
            sendPacket.invoke(connection, packet);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static Location getBossLocation(final Player player) {
        final Location l = player.getLocation();
        l.add(l.getDirection().multiply(50));
        return l;
    }
    
    static {
        StatusBarAPI.FakeBossMap = new PlayerMap<FakeBoss>();
        StatusBarAPI.bossBarRefresh = null;
    }
    
    private static class BossBarRefreshRunnable implements Runnable
    {
        @Override
        public void run() {
            final Iterator<Map.Entry<Player, FakeBoss>> toRefresh = StatusBarAPI.FakeBossMap.entrySet().iterator();
            while (toRefresh.hasNext()) {
                final Map.Entry<Player, FakeBoss> entry = toRefresh.next();
                if (entry == null) {
                    continue;
                }
                final Player player = entry.getKey();
                final FakeBoss boss = entry.getValue();
                if (player == null || !player.isOnline()) {
                    toRefresh.remove();
                }
                else {
                    sendPacket(player, boss.getMetaPacket(boss.getWatcher()));
                    sendPacket(player, boss.getTeleportPacket(getBossLocation(player)));
                }
            }
        }
    }
    
    private static class FakeBoss
    {
        @SuppressWarnings("unused")
		private static final float MAX_HEALTH = 300.0f;
        private Location location;
        private int id;
        private int x;
        private int y;
        private int z;
        private int pitch;
        private int yaw;
        private byte xvel;
        private byte yvel;
        private byte zvel;
        private float health;
        private boolean invisible;
        private String name;
        private Object world;
        private Object boss;
        
        public FakeBoss(final Player player, final String name, final float percent) {
            this.pitch = 0;
            this.yaw = 0;
            this.xvel = 0;
            this.yvel = 0;
            this.zvel = 0;
            this.invisible = true;
            this.location = getBossLocation(player);
            this.name = name;
            this.x = this.location.getBlockX();
            this.y = this.location.getBlockY();
            this.z = this.location.getBlockZ();
            this.health = percent * 300.0f;
            this.world = ReflectionUtils.getHandle(player.getWorld());
        }
        
        public void setHealth(final float percent) {
            this.health = percent * 300.0f;
        }
        
        public void setName(final String name) {
            this.name = name;
        }
        
        public Object getSpawnPacket() {
            final Class<?> entity = ReflectionUtils.getCraftClass("Entity");
            final Class<?> entityLiving = ReflectionUtils.getCraftClass("EntityLiving");
            final Class<?> wither = ReflectionUtils.getCraftClass("EntityWither");
            try {
                this.boss = wither.getConstructor(ReflectionUtils.getCraftClass("World")).newInstance(this.world);
                ReflectionUtils.getMethod(wither, "setLocation", Double.TYPE, Double.TYPE, Double.TYPE, Float.TYPE, Float.TYPE).invoke(this.boss, this.x, this.y, this.z, this.pitch, this.yaw);
                ReflectionUtils.getMethod(wither, "setInvisible", Boolean.TYPE).invoke(this.boss, this.invisible);
                ReflectionUtils.getMethod(wither, "setCustomName", String.class).invoke(this.boss, this.name);
                ReflectionUtils.getMethod(wither, "setHealth", Float.TYPE).invoke(this.boss, this.health);
                ReflectionUtils.getField(entity, "motX").set(this.boss, this.xvel);
                ReflectionUtils.getField(entity, "motY").set(this.boss, this.yvel);
                ReflectionUtils.getField(entity, "motZ").set(this.boss, this.zvel);
                this.id = (int)ReflectionUtils.getMethod(wither, "getId").invoke(this.boss, new Object[0]);
                final Class<?> packetClass = ReflectionUtils.getCraftClass("PacketPlayOutSpawnEntityLiving");
                return packetClass.getConstructor(entityLiving).newInstance(this.boss);
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        
        public Object getDestroyPacket() {
            try {
                final Class<?> packetClass = ReflectionUtils.getCraftClass("PacketPlayOutEntityDestroy");
                return packetClass.getConstructor(int[].class).newInstance(new int[] { this.id });
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        
        public Object getMetaPacket(final Object watcher) {
            try {
                final Class<?> watcherClass = ReflectionUtils.getCraftClass("DataWatcher");
                final Class<?> packetClass = ReflectionUtils.getCraftClass("PacketPlayOutEntityMetadata");
                return packetClass.getConstructor(Integer.TYPE, watcherClass, Boolean.TYPE).newInstance(this.id, watcher, true);
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        
        public Object getTeleportPacket(final Location loc) {
            try {
                final Class<?> packetClass = ReflectionUtils.getCraftClass("PacketPlayOutEntityTeleport");
                return packetClass.getConstructor(Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Byte.TYPE, Byte.TYPE, Boolean.TYPE).newInstance(this.id, loc.getBlockX() * 32, loc.getBlockY() * 32, loc.getBlockZ() * 32, (byte)((int)loc.getYaw() * 256 / 360), (byte)((int)loc.getPitch() * 256 / 360), false);
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        
        public Object getWatcher() {
            final Class<?> Entity = ReflectionUtils.getCraftClass("Entity");
            final Class<?> DataWatcher = ReflectionUtils.getCraftClass("DataWatcher");
            try {
                final Object watcher = DataWatcher.getConstructor(Entity).newInstance(this.boss);
                final Method a = ReflectionUtils.getMethod(DataWatcher, "a", Integer.TYPE, Object.class);
                a.invoke(watcher, 0, (byte)(this.invisible ? 32 : 0));
                a.invoke(watcher, 2, this.name);
                a.invoke(watcher, 6, this.health);
                a.invoke(watcher, 7, 0);
                a.invoke(watcher, 8, (byte)0);
                a.invoke(watcher, 11, (byte)1);
                a.invoke(watcher, 20, Integer.MAX_VALUE);
                return watcher;
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    
    private static class ReflectionUtils
    {
        public static Class<?> getCraftClass(final String ClassName) {
            final String name = Bukkit.getServer().getClass().getPackage().getName();
            final String version = name.substring(name.lastIndexOf(46) + 1) + ".";
            final String className = "net.minecraft.server." + version + ClassName;
            Class<?> c = null;
            try {
                c = Class.forName(className);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return c;
        }
        
        public static Object getHandle(final Entity entity) {
            try {
                return getMethod(entity.getClass(), "getHandle").invoke(entity, new Object[0]);
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        
        public static Object getHandle(final World world) {
            try {
                return getMethod(world.getClass(), "getHandle").invoke(world, new Object[0]);
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        
        public static Field getField(final Class<?> cl, final String field_name) {
            try {
                return cl.getDeclaredField(field_name);
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        
        public static Method getMethod(final Class<?> cl, final String method, final Class<?>... args) {
            for (final Method m : cl.getMethods()) {
                if (m.getName().equals(method) && ClassListEqual(args, m.getParameterTypes())) {
                    return m;
                }
            }
            return null;
        }
        
        public static Method getMethod(final Class<?> cl, final String method) {
            for (final Method m : cl.getMethods()) {
                if (m.getName().equals(method)) {
                    return m;
                }
            }
            return null;
        }
        
        public static boolean ClassListEqual(final Class<?>[] l1, final Class<?>[] l2) {
            boolean equal = true;
            if (l1.length != l2.length) {
                return false;
            }
            for (int i = 0; i < l1.length; ++i) {
                if (l1[i] != l2[i]) {
                    equal = false;
                    break;
                }
            }
            return equal;
        }
    }
    
    private static class PlayerMap<V> implements Map<Player, V>
    {
        private final V defaultValue;
        private final Map<String, V> contents;
        
        public PlayerMap() {
            this.contents = new HashMap<String, V>();
            this.defaultValue = null;
        }
        
        @Override
        public void clear() {
            this.contents.clear();
        }
        
        @Override
        public boolean containsKey(final Object key) {
            if (key instanceof Player) {
                return this.contents.containsKey(((Player)key).getName());
            }
            return key instanceof String && this.contents.containsKey(key);
        }
        
        @Override
        public boolean containsValue(final Object value) {
            return this.contents.containsValue(value);
        }
        
        @Override
        public Set<Entry<Player, V>> entrySet() {
            final Set<Entry<Player, V>> toReturn = new HashSet<Entry<Player, V>>();
            for (final String name : this.contents.keySet()) {
                toReturn.add(new PlayerEntry(Bukkit.getPlayer(name), this.contents.get(name)));
            }
            return toReturn;
        }
        
        @Override
        public V get(final Object key) {
            V result = null;
            if (key instanceof Player) {
                result = this.contents.get(((Player)key).getName());
            }
            if (key instanceof String) {
                result = this.contents.get(key);
            }
            return (result == null) ? this.defaultValue : result;
        }
        
        @Override
        public boolean isEmpty() {
            return this.contents.isEmpty();
        }
        
        @Override
        public Set<Player> keySet() {
            final Set<Player> toReturn = new HashSet<Player>();
            for (final String name : this.contents.keySet()) {
                toReturn.add(Bukkit.getPlayer(name));
            }
            return toReturn;
        }
        
        @Override
        public V put(final Player key, final V value) {
            if (key == null) {
                return null;
            }
            return this.contents.put(key.getName(), value);
        }
        
        @Override
        public void putAll(final Map<? extends Player, ? extends V> map) {
            for (final Entry<? extends Player, ? extends V> entry : map.entrySet()) {
                this.put((Player)entry.getKey(), entry.getValue());
            }
        }
        
        @Override
        public V remove(final Object key) {
            if (key instanceof Player) {
                return this.contents.remove(((Player)key).getName());
            }
            if (key instanceof String) {
                return this.contents.remove(key);
            }
            return null;
        }
        
        @Override
        public int size() {
            return this.contents.size();
        }
        
        @Override
        public Collection<V> values() {
            return this.contents.values();
        }
        
        @Override
        public String toString() {
            return this.contents.toString();
        }
        
        public class PlayerEntry implements Entry<Player, V>
        {
            private Player key;
            private V value;
            
            public PlayerEntry(final Player key, final V value) {
                this.key = key;
                this.value = value;
            }
            
            @Override
            public Player getKey() {
                return this.key;
            }
            
            @Override
            public V getValue() {
                return this.value;
            }
            
            @Override
            public V setValue(final V value) {
                final V toReturn = this.value;
                this.value = value;
                return toReturn;
            }
        }
    }
}
