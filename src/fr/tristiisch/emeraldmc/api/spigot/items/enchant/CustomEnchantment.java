package fr.tristiisch.emeraldmc.api.spigot.items.enchant;

import java.lang.reflect.Field;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("deprecation")
public class CustomEnchantment extends EnchantmentWrapper {

	private final String name;

	public CustomEnchantment(final String name, final int id) {
		super(id);
		this.name = name;
	}

	@Override
	public boolean canEnchantItem(final ItemStack item) {
		return true;
	}

	@Override
	public boolean conflictsWith(final Enchantment other) {
		return false;
	}

	@Override
	public int getId() {
		return super.getId();
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ALL;
	}

	@Override
	public int getMaxLevel() {
		return 1;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int getStartLevel() {
		return 1;
	}

	public void registerEnchant() {
		EnchantUtils.removeIfExist(this.name, this.getId());
		try {
			final Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null, true);
			Enchantment.registerEnchantment(this);
		} catch(final Exception e) {
			e.printStackTrace();
		}
	}
}