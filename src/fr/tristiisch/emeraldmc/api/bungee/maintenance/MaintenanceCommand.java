package fr.tristiisch.emeraldmc.api.bungee.maintenance;

import java.util.Arrays;
import java.util.List;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class MaintenanceCommand extends Command {

	/*
	 * Dev: Tristiisch74 Pemet de mettre le serveur en maintenance ./maintenance
	 * [on|off] <dev|raison> Raison va s'afficher dans le motd players. TODO: -
	 * Envoyer un tweet pour signaler la maintenance - mettre en bdd pour afficher
	 * le même msg sur le site
	 */

	private final int[] power;

	public MaintenanceCommand(final String command, final int... power) {
		super(command);
		this.power = power;
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			if(sender instanceof ProxiedPlayer) {
				final ProxiedPlayer player = (ProxiedPlayer) sender;
				if(!new AccountProvider(player.getUniqueId()).getEmeraldPlayer().hasPower(this.power)) {
					sender.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
					return;
				}
			}
			if(args.length == 0) {
				sender.sendMessage(BungeeConfigUtils.getString("bungee.maintenance.messages.usage"));
				return;

			}
			switch(args[0].toLowerCase()) {

			case "off":
				this.setMaintenance((byte) 0, null, sender);
				break;

			case "on":
				if(args.length >= 2) {
					// Need to opti
					// String.join(", ", list)
					final String[] reason = Arrays.copyOfRange(args, 1, args.length);
					int i1 = 0;
					for(int i2 = 0; reason.length > i2; i2++) {
						i1 += reason[i2].length() + 1;

						if(i1 >= 25) {
							reason[i2] += "\n&c";
							i1 = 0;
						}
					}

					/*
					 * ArrayList<String> arg = new ArrayList<String>(Arrays.asList(args));
					 * arg.remove(0); String argsstring = ""; int i = 0; for(String s : arg){ i +=
					 * s.length(); if(i >= 25) { s = "\n&c" + s; i = 0; } argsstring += s + " "; }
					 */
					this.setMaintenance((byte) 1, "&c" + String.join(" ", reason), sender);
				} else {
					this.setMaintenance((byte) 1, "", sender);
				}
				break;

			case "dev":
				this.setMaintenance((byte) 2, null, sender);
				break;

			case "add":
				if(args.length >= 2) {
					final List<String> whitelist = BungeeConfigUtils.getConfig("maintenance").getStringList("whitelist");

					if(!whitelist.contains(args[1])) {
						whitelist.add(args[1]);
						sender.sendMessage(BungeeConfigUtils.getString("bungee.maintenance.messages.added").replaceAll("%player%", args[1]));
					} else {
						sender.sendMessage(BungeeConfigUtils.getString("bungee.maintenance.messages.alreadyadded").replaceAll("%player%", args[1]));
					}
					BungeeConfigUtils.getConfig("maintenance").set("whitelist", whitelist);
					BungeeConfigUtils.saveConfig("maintenance");
				} else {
					sender.sendMessage(BungeeConfigUtils.getString("bungee.maintenance.messages.usage"));
				}

				break;

			case "remove":
				if(args.length >= 2) {
					final List<String> whitelist = BungeeConfigUtils.getConfig("maintenance").getStringList("whitelist");
					if(whitelist.contains(args[1])) {
						whitelist.remove(args[1]);
						sender.sendMessage(BungeeConfigUtils.getString("bungee.maintenance.messages.removed").replaceAll("%player%", args[1]));
					} else {
						sender.sendMessage(BungeeConfigUtils.getString("bungee.maintenance.messages.alreadyremoved").replaceAll("%player%", args[1]));
					}
					BungeeConfigUtils.getConfig("maintenance").set("whitelist", whitelist);
					BungeeConfigUtils.saveConfig("maintenance");
				} else {
					sender.sendMessage(BungeeConfigUtils.getString("bungee.maintenance.messages.usage"));
				}
				break;

			case "list":
				final List<String> whitelist = BungeeConfigUtils.getConfig("maintenance").getStringList("whitelist");
				sender.sendMessage(BungeeConfigUtils.getString("bungee.maintenance.messages.whitelist")
						.replaceAll("%size%", String.valueOf(whitelist.size()))
						.replaceAll("%list%", String.join(BungeeConfigUtils.getString("bungee.maintenance.messages.whitelist_separator"), whitelist)));
				break;

			case "status":
				/*
				 * EmeraldServerStatus status =
				 * EmeraldServerStatus.getStatus(BungeeConfigUtils.getConfig("maintenance").
				 * getInt("bungee.maintenance.settings.status")); String message =
				 * BungeeConfigUtils.getConfig("maintenance").getString(
				 * "bungee.maintenance.settings.message"); String statusmsg;
				 * if(status.equals(EmeraldServerStatus.)){ if(!message.isEmpty()) { statusmsg =
				 * "&aActiver (" + message +"&a)"; } else { statusmsg = "&aActiver"; } } else if
				 * (status == 2){ statusmsg = "&aActiver (&6Dev&a)"; } else { statusmsg =
				 * "&cDésactiver"; } sender.sendMessage("&6Le mode maintenance est " +
				 * statusmsg.replaceAll("\n", ""));
				 */
				break;

			default:
				sender.sendMessage(BungeeConfigUtils.getString("bungee.maintenance.messages.usage"));
				break;
			}
		});
	}

	private void setMaintenance(final byte status, final String message, final CommandSender player) {
		BungeeConfigUtils.getConfig("maintenance").set("settings.status", status);
		BungeeConfigUtils.getConfig("maintenance").set("settings.message", message);
		BungeeConfigUtils.saveConfig("maintenance");
		String statusmsg;
		if(status == 1) {
			if(message != "") {
				statusmsg = "&aActivé (" + message + "&a)";
			} else {
				statusmsg = "&aActivé";
			}
		} else if(status == 2) {
			statusmsg = "&aActivé (&6Dev&a)";
		} else {
			statusmsg = "&cDésactivé";
		}
		player.sendMessage(Utils.color("&6Le mode maintenance est désormais " + statusmsg.replaceAll("\n", "")));
	}

}
