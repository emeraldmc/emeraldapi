package fr.tristiisch.emeraldmc.api.bungee.commands;

import java.util.Arrays;

import org.bukkit.Bukkit;

import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Plugin;

public abstract class BungeeCommand extends Command {

	protected String[] aliases;

	public boolean allowConsole = true;
	protected final String command;

	protected String description;

	public OlympaPlayer emeraldPlayer;
	protected EmeraldGroup[] groups;

	/**
	 * Don't foget to set {@link BungeeCommand#usageString}
	 */
	public Integer minArg = 0;

	protected final Plugin plugin;
	public ProxiedPlayer proxiedPlayer;
	private CommandSender sender;

	/**
	 * Format: Usage » %command% <%obligatory%|%obligatory%> [%optional%]
	 * Variable name: 'joueur' ...
	 *
	 */
	public String usageString;

	public BungeeCommand(final Plugin plugin, final String command) {
		super(command);
		this.plugin = plugin;
		this.command = command;
	}

	public BungeeCommand(final Plugin plugin, final String command, final EmeraldGroup group, final String... aliases) {
		super(command, null, aliases);
		this.plugin = plugin;
		this.command = command;
		this.groups = new EmeraldGroup[] { group };
		this.aliases = aliases;
	}

	public BungeeCommand(final Plugin plugin, final String command, final EmeraldGroup[] groups, final String... aliases) {
		super(command, null, aliases);
		this.plugin = plugin;
		this.command = command;
		this.groups = groups;
		this.aliases = aliases;
	}

	public BungeeCommand(final Plugin plugin, final String command, final EmeraldGroup[] groups, final String[] aliases, final String description, final String usageString, final boolean allowConsole,
		final Integer minArg) {

		super(command, null, aliases);
		this.plugin = plugin;
		this.command = command;
		this.groups = groups;
		this.aliases = aliases;
		this.description = description;
		this.usageString = usageString;
		this.allowConsole = allowConsole;
		this.minArg = minArg;
		this.register();
	}

	public BungeeCommand(final Plugin plugin, final String command, final String[] aliases) {
		super(command, null, aliases);
		this.plugin = plugin;
		this.command = command;
		this.aliases = aliases;
	}

	public void broadcast(final String message) {
		Bukkit.broadcastMessage(message);
	}

	public String buildText(final int min, final String[] args) {
		return String.join(" ", Arrays.copyOfRange(args, min, args.length));
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		ProxyServer.getInstance().getScheduler().runAsync(this.plugin, () -> {
			this.sender = sender;
			if(sender instanceof ProxiedPlayer) {
				this.proxiedPlayer = (ProxiedPlayer) sender;

				if(this.groups != null && this.groups.length != 0) {
					if(!this.loadEmeraldPlayer()) {
						this.sendImpossibleWithEmeraldPlayer();
						return;
					}
					if(!this.hasPermission()) {
						this.sendDoNotHavePermission();
						return;
					}
				}
			} else if(!this.allowConsole) {
				this.sendImpossibleWithConsole();
				return;
			}

			if(args.length < this.minArg) {
				this.sendUsage();
				return;
			}
			this.onCommand(sender, args);

		});

	}

	public String getCommand() {
		return this.command;
	}

	public OlympaPlayer getEmeraldPlayer() {
		return this.emeraldPlayer;
	}

	public ProxiedPlayer getProxiedPlayer() {
		return this.proxiedPlayer;
	}

	public boolean hasPermission(EmeraldGroup... groups) {
		if(this.proxiedPlayer == null) {
			return true;
		}

		if(groups.length == 0) {
			groups = this.groups;
		}

		if(this.emeraldPlayer == null) {
			this.loadEmeraldPlayer();
		}

		if(groups.length == 1) {
			return this.emeraldPlayer.hasPowerMoreThan(groups[0]);
		} else {
			return this.emeraldPlayer.hasPower(groups);
		}

	}

	public boolean loadEmeraldPlayer() {
		this.emeraldPlayer = new AccountProvider(this.proxiedPlayer.getUniqueId()).getEmeraldPlayer();
		return this.emeraldPlayer != null;
	}

	public abstract void onCommand(final CommandSender sender, final String[] args);

	public void register() {
		this.plugin.getProxy().getPluginManager().registerCommand(this.plugin, this);
	}

	public void sendDoNotHavePermission() {
		this.sendErreur("Vous n'avez pas la permission &l(◑_◑)");
	}

	public void sendErreur(final String message) {
		this.sendMessage(Prefix.DEFAULT_BAD, message);
	}

	public void sendImpossibleWithConsole() {
		this.sendErreur("Impossible avec la console.");
	}

	public void sendImpossibleWithEmeraldPlayer() {
		this.sendErreur("Une erreur est survenu avec vos donnés.");
	}

	public void sendMessage(final CommandSender sender, final Prefix prefix, final String text) {
		this.sendMessage(sender, prefix + Utils.color(text));
	}

	@SuppressWarnings("deprecation")
	public void sendMessage(final CommandSender sender, final String text) {
		sender.sendMessage(Utils.color(text));
	}

	public void sendMessage(final Prefix prefix, final String text) {
		this.sendMessage(this.sender, prefix, text);
	}

	public void sendMessage(final String text) {
		this.sendMessage(this.sender, Utils.color(text));
	}

	public void sendMessage(final TextComponent text) {
		this.proxiedPlayer.sendMessage(text);
	}

	public void sendUnknownPlayer(final String name) {
		this.sendErreur("Le joueur &4" + name + "&c est introuvable.");
		// TODO check historique player
	}

	public void sendUsage() {
		this.sendMessage(Prefix.USAGE, this.usageString);
	}
}
