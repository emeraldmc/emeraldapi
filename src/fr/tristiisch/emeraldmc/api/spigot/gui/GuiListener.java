package fr.tristiisch.emeraldmc.api.spigot.gui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.olympa.api.customevents.GuiCloseEvent;

public class GuiListener implements Listener {

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		GuiTools.removeGui(event.getPlayer());
	}

	@EventHandler
	public void InventoryCloseEvent(final InventoryCloseEvent event) {
		final Player player = (Player) event.getPlayer();
		Bukkit.getPluginManager().callEvent(new GuiCloseEvent(player, event));
		GuiTools.removeGui(player);
	}

	@EventHandler
	public void InventoryOpenEvent(final InventoryOpenEvent event) {
		if(event.getInventory().getType().equals(InventoryType.CRAFTING)) {
			GuiTools.setGuiData(new GuiData("container.crafting", event.getPlayer().getUniqueId()));
		}
	}
}
