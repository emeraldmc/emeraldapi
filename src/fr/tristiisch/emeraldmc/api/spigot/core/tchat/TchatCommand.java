package fr.tristiisch.emeraldmc.api.spigot.core.tchat;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.utils.EmeraldServerSettings;

public class TchatCommand implements CommandExecutor {

	/* Dev: Tristiisch74
	 *
	 * Commandes de gestion de tchat (sur le serveur où la commande est exécuté)
	 *
	 * ./chat slow  = Désactiver antispam (ralentit le chat)
	 * ./chat clear = Vide le tchat
	 * ./chat mute  = Met le tchat en pause (toggle on/off)
	 *
	 */

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String arg, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final Player player = (Player) sender;
			if(new AccountProvider(player.getUniqueId()).getEmeraldPlayer().hasPowerLessThan(EmeraldGroup.MODERATEUR)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}
			EmeraldServerSettings serverSettings;

			serverSettings = EmeraldSpigot.getServerSettings();

			if(args.length == 0){

				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.usage")));

			} else if(args[0].equalsIgnoreCase("slow")) {

				final int timecooldown = EmeraldSpigot.getInstance().getConfig().getInt("bungee.tchat.settings.timecooldown");
				if(serverSettings.isChatSlowed()){
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.antispamdisable")));
					serverSettings.setChatSlow(false);
				} else {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.antispamenable").replaceAll("%second%", timecooldown + "")));
					serverSettings.setChatSlow(true);
				}

			} else if(args[0].equalsIgnoreCase("clear")) {

				for(final Player players : Bukkit.getOnlinePlayers()) {
					for(int i = 0; i < 100; i++) {
						players.sendMessage("");
					}
					players.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.tchatclear")));
				}

			} else if(args[0].equalsIgnoreCase("mute")) {

				if(!serverSettings.isChatMuted()) {
					serverSettings.setChatMute(true);
					for(final Player players : Bukkit.getOnlinePlayers()) {
						players.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.tchatenabled")));
					}
				} else {
					serverSettings.setChatMute(false);
					for(final Player players : Bukkit.getOnlinePlayers()) {
						players.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.tchatdisabled")));
					}
				}

			} else {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.usage")));
			}
		});
		return true;
	}

}