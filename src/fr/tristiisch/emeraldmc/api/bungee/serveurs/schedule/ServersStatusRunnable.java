package fr.tristiisch.emeraldmc.api.bungee.serveurs.schedule;

import java.util.ArrayList;
import java.util.List;

import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeBPMC;
import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import fr.tristiisch.emeraldmc.api.commons.ping.ServerListPing;
import fr.tristiisch.emeraldmc.api.commons.ping.ServerListPing.EmeraldServerInfo;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

public class ServersStatusRunnable implements Runnable {

	@Override
	public void run() {
		final List<EmeraldServer> servers = new ArrayList<>();
		for(final ServerInfo server : ProxyServer.getInstance().getServers().values()) {
			EmeraldServer emeraldServer = null;
			try {
				final ServerListPing ping = new ServerListPing(server.getAddress());
				final EmeraldServerInfo response = ping.fetchData();
				emeraldServer = EmeraldServer.fromJson(response.getDescription());
				emeraldServer.setName(server.getName());
				emeraldServer.setInfo(response);
				servers.add(emeraldServer);
			} catch(final Exception e) {
				/*System.out.println(Utils.color("&bStatut de " + server.getName() + ": &4(" + e.getMessage() + ")"));*/
				emeraldServer = new EmeraldServer(server.getName(), e.getMessage());
				servers.add(emeraldServer);
			}
		}
		EmeraldServers.setServers(servers);

		for(final ServerInfo server : ProxyServer.getInstance().getServers().values()) {
			BungeeBPMC.sendServerStatus(server, servers);
		}
	}

}
