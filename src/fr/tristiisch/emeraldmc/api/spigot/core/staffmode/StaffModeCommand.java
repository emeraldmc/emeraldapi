package fr.tristiisch.emeraldmc.api.spigot.core.staffmode;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class StaffModeCommand implements CommandExecutor {

	private final int power;

	public StaffModeCommand(final int power) {
		this.power = power;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String arg, final String[] args) {

		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {

			final Player player = (Player) sender;
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();

			if(emeraldPlayer.hasPowerLessThan(this.power)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}

			Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {

				if(StaffMode.isStaffMode(player)) {
					StaffMode.disable(player);
					return;
				}

				if(!player.isOnGround()) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez être au sol pour utiliser cette commande."));
					return;
				}

				StaffMode.enable(player);

			});
		});
		return true;
	}

}
