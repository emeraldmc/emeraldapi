package fr.tristiisch.emeraldmc.api.bungee.commands;

import java.util.stream.Collectors;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class TabComplete implements Listener {

	@EventHandler
	public void onTabComplete(final TabCompleteEvent event)  {
		if(event.isCancelled() || event.getSuggestions().size() != 0) {
			return;
		}
		final String msg = event.getCursor();
		final String[] args = msg.split(" ");
		final String check = args[args.length - 1].toLowerCase();

		if(args[0].startsWith("/")) {
			if(!msg.endsWith(" ")) {
				event.getSuggestions()
				.addAll(ProxyServer.getInstance().getPlayers().stream().map(ProxiedPlayer::getName).filter(p -> p.toLowerCase().startsWith(check)).limit(30).collect(Collectors.toList()));
			}

		}
	}

	/*ProxiedPlayer player = (ProxiedPlayer) event.getSender();
		String msg = event.getCursor();
		String[] args = msg.split(" ");
		if(args[0].equalsIgnoreCase("/" + command) && EmeraldPlayers.getPlayer(player.getName()).hasPowerMoreThan(power)){
			String check = args[args.length - 1].toLowerCase();
			if(args.length == 1) {
				if(msg.endsWith(" ")) {
					for(String arg : arg2) {
						if(arg.toLowerCase().startsWith(check)) {
							event.getSuggestions().add(arg);
						}
					}
				}
			} else if(args.length == 2) {
				if(!msg.endsWith(" ")) {
					event.getSuggestions().addAll(arg2);
					for(String arg : arg2) {
						event.getSuggestions().add(arg);
					}
				} else {
					if(args[1].equalsIgnoreCase("remove")) {
						event.getSuggestions().addAll(BConfig.getConfig("maintenance").getStringList("bungee.maintenance.whitelist"));
						for(String whitelisted : Config.getConfig("maintenance").getStringList("bungee.maintenance.whitelist")){
							event.getSuggestions().add(whitelisted);
						}
					}
				}

			} else if(args.length == 3) {
				if(!msg.endsWith(" ")) {
					if(args[1].equalsIgnoreCase("add")) {
						event.getSuggestions().addAll(player.getServer().getInfo().getPlayers().stream().map(ProxiedPlayer::getName).filter(playerName -> playerName.toLowerCase().startsWith(check)).collect(Collectors.toList()));
						for(ProxiedPlayer players : player.getServer().getInfo().getPlayers()){
							if(players.getName().toLowerCase().startsWith(check)) {
								event.getSuggestions().add(players.getName());
							}
						}
					} else if(args[1].equalsIgnoreCase("remove")) {
						event.getSuggestions().addAll(BConfig.getConfig("maintenance").getStringList("bungee.maintenance.whitelist").stream().filter(playerName -> playerName.toLowerCase().startsWith(check)).collect(Collectors.toList()));

						for(String whitelisted : Config.getConfig("maintenance").getStringList("bungee.maintenance.whitelist")){
							if(whitelisted.toLowerCase().startsWith(check)) {
								event.getSuggestions().add(whitelisted);
							}
						}
					}
				}

			}
		}*/
}

