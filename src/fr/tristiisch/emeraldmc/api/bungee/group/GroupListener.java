package fr.tristiisch.emeraldmc.api.bungee.group;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class GroupListener implements Listener {

	private final List<String> commands = new ArrayList<>();
	private final int power;
	private final String[] arg3 = {"add","remove"};

	public GroupListener(final String command, final int power, final String... aliases) {
		if(aliases != null) {
			Collections.addAll(this.commands, aliases);
		}
		this.commands.add(command);
		this.power = power;
	}



	@EventHandler
	public void onTabComplete(final TabCompleteEvent event)  {
		final String msg = event.getCursor();
		final String[] args = msg.split(" ");
		if(!args[0].startsWith("/")) {
			return;
		}
		args[0] = args[0].substring(1);
		final ProxiedPlayer player = (ProxiedPlayer) event.getSender();

		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			if(Utils.containsIgnoreCase(args[0], this.commands) && new AccountProvider(player.getUniqueId()).getEmeraldPlayer().hasPowerMoreThan(this.power)) {
				final String check = args[args.length - 1].toLowerCase();
				switch(args.length) {

				case 2:
					if(!msg.endsWith(" ")) {
						for(final ProxiedPlayer players : ProxyServer.getInstance().getPlayers()){
							final String splayers = players.toString();
							if(splayers.toLowerCase().startsWith(check)) {
								event.getSuggestions().add(splayers);
							}
						}
					} else {
						event.getSuggestions().addAll(Arrays.stream(EmeraldGroup.values()).map(EmeraldGroup::getName).collect(Collectors.toList()));
						/*					for(EmeraldGroup group : EmeraldGroup.values()){
						event.getSuggestions().add(group.getName());
					}*/
					}
					break;
				case 3:
					if(!msg.endsWith(" ")) {
						event.getSuggestions().addAll(Arrays.stream(EmeraldGroup.values()).map(EmeraldGroup::getName).filter(groupName -> groupName.toLowerCase().startsWith(check)).collect(Collectors.toList()));
						/*					for(EmeraldGroup group : EmeraldGroup.values()){
						if(group.getName().toLowerCase().startsWith(check)) {
							event.getSuggestions().add(group.getName());
						}
					}*/
					} else {
						event.getSuggestions().addAll(Arrays.stream(this.arg3).collect(Collectors.toList()));
						/*					for(String arg : arg3) {
						event.getSuggestions().add(arg);
					}*/
					}
					break;
				case 4:
					if(!msg.endsWith(" ")) {
						event.getSuggestions().addAll(Arrays.stream(this.arg3).filter(groupName -> groupName.toLowerCase().startsWith(check)).collect(Collectors.toList()));
						/*					for(String arg : arg3) {
						if(arg.toLowerCase().startsWith(check)) {
							event.getSuggestions().add(arg);
						}
					}*/
					}
					break;
				}
			}
		});
	}
}
