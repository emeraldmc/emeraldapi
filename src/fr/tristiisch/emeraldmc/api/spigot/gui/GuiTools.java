package fr.tristiisch.emeraldmc.api.spigot.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public class GuiTools {

	public static class GuiData {

		private String data;
		final private String id;
		final private UUID playerUuid;

		public GuiData(final String id, final UUID playerUuid) {
			this.id = id;
			this.playerUuid = playerUuid;
			this.data = "";
		}

		public GuiData(final String id, final UUID playerUuid, final String data) {
			this.id = id;
			this.playerUuid = playerUuid;
			this.data = data;
		}

		public String getData() {
			return this.data;
		}

		public String getId() {
			return this.id;
		}

		public UUID getPlayerUniqueId() {
			return this.playerUuid;
		}

		public void setData(final String data) {
			this.data = data;

		}
	}

	public static ItemStack CANCEL = ItemTools.create(Material.REDSTONE_BLOCK, 1, (short) 0, "&4✖ &lImpossible");
	private final static int columns = 9;
	private static List<GuiData> players = new ArrayList<>();

	public static void cancelInDev(final InventoryClickEvent event) {
		event.setCancelled(true);
		final Player player = (Player) event.getWhoClicked();
		final ItemStack item = event.getCurrentItem();
		event.getClickedInventory().setItem(event.getSlot(), ItemTools.setLore(CANCEL, Arrays.asList("", "&cEn développement")));
		player.playSound(player.getLocation(), Sound.VILLAGER_NO, 1, 1);
		Bukkit.getScheduler().runTaskLater(EmeraldSpigot.getInstance(), () -> {

			final Inventory clickedInventory = event.getClickedInventory();
			if(clickedInventory.equals(player.getOpenInventory().getTopInventory())) {
				clickedInventory.setItem(event.getSlot(), item);
			}
		}, 30);
	}

	public static void cancelItem(final InventoryClickEvent event, final String msg) {
		event.setCancelled(true);
		final Player player = (Player) event.getWhoClicked();
		final ItemStack item = event.getCurrentItem();
		event.getClickedInventory().setItem(event.getSlot(), ItemTools.setLore(CANCEL, Arrays.asList("", "&c" + msg, "")));
		player.playSound(player.getLocation(), Sound.VILLAGER_NO, 1, 1);
		Bukkit.getScheduler().runTaskLater(EmeraldSpigot.getInstance(), () -> event.getClickedInventory().setItem(event.getSlot(), item), 30);
	}

	public static GuiData getGuiData(final Player player) {
		return getGuiData(player.getUniqueId());
	}

	public static GuiData getGuiData(final UUID uuid) {
		return players.stream().filter(data -> uuid == data.getPlayerUniqueId()).findFirst().orElse(null);
	}

	public static void removeGui(final GuiData guiData) {
		players.remove(guiData);
	}

	public static void removeGui(final Player player) {
		removeGui(getGuiData(player));

	}

	public static void setGuiData(final GuiData guiData) {
		final GuiData guiDataOld = getGuiData(guiData.getPlayerUniqueId());
		if(guiDataOld != null) {
			removeGui(guiDataOld);
		}
		players.add(guiData);
	}

	private String data;

	private final String id;

	private final Inventory inventory;

	public GuiTools(final String name, final String id, int size) {
		if(size % columns != 0) {
			size = size + columns - size % columns;
		}
		this.inventory = Bukkit.createInventory(null, size, Utils.color(name));
		this.id = id;
		this.data = "";
	}

	public GuiTools(final String name, final String id, int size, final String data) {
		if(size % columns != 0) {
			size = size + columns - size % columns;
		}
		this.inventory = Bukkit.createInventory(null, size, Utils.color(name));
		this.id = id;
		this.data = data;
	}

	public GuiTools(final String name, final String id, final InventoryType type) {
		this.inventory = Bukkit.createInventory(null, type, Utils.color(name));
		this.id = id;
		this.data = "";
	}

	public GuiTools(final String name, final String id, final long column) {
		final int size = (int) (columns * column);
		this.inventory = Bukkit.createInventory(null, size, Utils.color(name));
		this.id = id;
		this.data = "";
	}

	public void addItem(final ItemStack item) {
		final int index = IntStream.range(0, this.inventory.getContents().length).filter(i -> this.inventory.getContents()[i] == null).findFirst().orElse(-1);
		this.inventory.setItem(index, item);
	}

	public int getColumn() {
		return columns;
	}

	public int getColumn(final int i) {
		return this.getSlot(1, i);
	}

	public String getData() {
		return this.data;
	}

	public int getFirstSlot() {
		return 0;
	}

	public Inventory getInventory() {
		return this.inventory;
	}

	public ItemStack getItem(final int index) {
		return this.inventory.getItem(index);
	}

	public int getLastSlot() {
		return this.getSize() - 1;
	}

	public int getMiddleColumn() {
		return (columns - 1) * 2;
	}

	public int getMiddleLigne(final int i) {
		return this.getColumn(1) / 2 * i;
	}

	public int getMiddleSlot() {
		return this.getSize() / 2;
	}

	public int getMiddleSlotPlusColumn(final int i) {
		return this.getMiddleSlot() + i * 9;
	}

	/*
	 * public void addItem(ItemStack item) { int slot = 0;
	 * while(this.inventory.getItem(slot) != null) { slot++; if(slot >
	 * this.getSize()) { try { throw new Exception("The Inventory is full"); } catch
	 * (Exception e) { e.printStackTrace(); } } } this.inventory.setItem(slot,
	 * item); } public void addAll(ItemStack item) { int slot = 0;
	 * while(this.inventory.getItem(slot) != null) { slot++; if(slot >
	 * this.getSize()) { try { throw new Exception("The Inventory is full"); } catch
	 * (Exception e) { e.printStackTrace(); } } } }
	 */

	public String getName() {
		return this.inventory.getName();
	}

	public int getSize() {
		return this.inventory.getSize();
	}

	/**
	 * à verif
	 *
	 * @param ligne
	 * @param column
	 * @return
	 */
	public int getSlot(final int ligne, final int column) {
		return columns * (ligne - 1) + 1 + column;
	}

	public void openInventory(final Player player) {
		player.openInventory(this.inventory);
		if(this.id != null) {
			setGuiData(new GuiData(this.id, player.getUniqueId(), this.data));
		}
	}

	public void openInventory(final Player... players) {
		for(final Player player : players) {
			this.openInventory(player);
		}
	}

	public void removeItem(final int index) {
		this.inventory.setItem(index, null);
	}

	public void setData(final String data) {
		this.data = data;
	}

	public void setItem(final int index, final ItemStack item) {
		this.inventory.setItem(index, item);
	}

	public void setItem(final ItemStack[] items) {
		this.inventory.setContents(items);
	}
}
