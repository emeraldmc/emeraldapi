package fr.tristiisch.emeraldmc.api.spigot.core.boutique;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import fr.tristiisch.emeraldmc.api.spigot.command.SpigotCommand;

public class BoutiqueCommand extends SpigotCommand {

	public BoutiqueCommand(final Plugin plugin) {
		super(plugin, "boutique", "boutiques");
		this.allowConsole = false;
		this.register();
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Boutique.openMenu(this.player);
		return false;
	}

}
