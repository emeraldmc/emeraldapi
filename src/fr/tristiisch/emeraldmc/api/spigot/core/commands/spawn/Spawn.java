package fr.tristiisch.emeraldmc.api.spigot.core.commands.spawn;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;

public class Spawn {

	private static int defaultTime = 5;

	public static void setDefaultTime(final int i) {
		defaultTime = i;
	}

	public static int getDefaultTime() {
		return defaultTime;
	}

	private static List<Player> teleporters = new ArrayList<>();

	protected static void addTeleporter(final Player player) {
		teleporters.add(player);
	}

	protected static void removeTeleporter(final Player player) {
		teleporters.remove(player);
		player.removePotionEffect(PotionEffectType.CONFUSION);
	}

	protected static boolean isTeleporter(final Player player) {
		return teleporters.contains(player);
	}

	public static void telportToSpawn(final Player player) {
		if(isTeleporter(player)) {
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous êtes déjà en attente de téléportation."));
			return;
		}
		player.sendMessage(Utils.color("&2EmeraldMC &7» Téléportation au spawn ..."));
		player.teleport(EmeraldSpigot.getSpawn());
	}

	public static void telportToSpawn(final Player player, final int sec) {
		if(sec == 0) {
			telportToSpawn(player);
			return;
		}
		if(isTeleporter(player)) {
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous êtes déjà en attente de téléportation."));
			return;
		}
		addTeleporter(player);
		player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, sec / 20, 0, true, false), true);
		player.sendMessage(Utils.color("&2EmeraldMC &7» Téléportation au spawn dans " + sec + " secondes..."));
		TaskManager.runTaskLater("Teleporter_" + player.getName(), () -> {
			player.teleport(EmeraldSpigot.getSpawn());
			removeTeleporter(player);
		}, sec * 20);
	}
}
