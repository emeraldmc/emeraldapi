package fr.tristiisch.emeraldmc.api.spigot.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.utils.TPS;
import net.md_5.bungee.api.chat.TextComponent;

public class LagCommand implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		final TextComponent msg = new TextComponent(Utils.color("&3=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n"));
		msg.addExtra(Utils.color("&6TPS: &e" + "%tps%&7, 5 min: %tps5%&7, 15 min: %tps15%\n")
				.replaceAll("%tps%", TPS.getColor(TPS.getTPS()))
				.replaceAll("%tps5%", TPS.getColor(TPS.getTPS(5)))
				.replaceAll("%tps15%", TPS.getColor(TPS.getTPS(15))));

		msg.addExtra(Utils.color("&6Uptime: &7" + Utils.timestampToDuration(EmeraldSpigot.getUptime()) + "\n"));
		msg.addExtra(Utils.color("&3=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
		if(sender instanceof Player) {
			final Player player = (Player) sender;
			player.spigot().sendMessage(msg);
		} else {
			sender.sendMessage(msg.toLegacyText());
		}
		return true;
	}
}
