package fr.tristiisch.emeraldmc.api.spigot.core.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;


public class ColorCommand implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		List<String> msg = new ArrayList<>();
		for(ChatColor color : ChatColor.values()) {
			msg.add(color + "Exemple " + ChatColor.RESET + "&" + color.getChar());
		}
		sender.sendMessage(String.join("\n", msg));
		return true;
	}
}
