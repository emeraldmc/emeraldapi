package fr.tristiisch.emeraldmc.api.bungee.session;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.ip.IPManagment;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class SessionListener implements Listener {

	@EventHandler(priority = EventPriority.LOW)
	public void onPostLogin(final PostLoginEvent event) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			final ProxiedPlayer proxiedPlayer = event.getPlayer();

			final OlympaPlayer emeraldPlayer = new AccountProvider(proxiedPlayer.getUniqueId()).getEmeraldPlayer();
			final Session session = new Session(IPManagment.getId(emeraldPlayer.getIp()), emeraldPlayer.getId());
			SessionManagment.addSession(proxiedPlayer.getUniqueId(), session);
		});
	}

	@EventHandler
	public void PlayerDisconnectEvent(final PlayerDisconnectEvent event) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			final ProxiedPlayer proxiedPlayer = event.getPlayer();
			final Session session = SessionManagment.getSession(proxiedPlayer.getUniqueId());
			session.setStop(Utils.getCurrentTimeinSeconds());
			SessionMySQL.addSession(session);
			SessionManagment.removeSession(proxiedPlayer.getUniqueId());
		});
	}
}
