package fr.tristiisch.emeraldmc.api.spigot.tablist;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.core.vanish.Vanish;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection.ClassEnum;
import net.minecraft.server.v1_8_R3.ChatComponentText;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import net.minecraft.server.v1_8_R3.PlayerInteractManager;
import net.minecraft.server.v1_8_R3.WorldServer;

public class TabList {

	public static void set(final Player player) {
		set(player, Bukkit.getOnlinePlayers().size() - Vanish.getVanish().size());
	}

	public static void set(final Player player, final int size) {
		TabList.setTabList(player, "&aBienvenue sur &2EmeraldMC", "&2" + size + " &aconnecté" + Utils.withOrWithoutS(size) + "\n&awww.emeraldmc.fr &e&l| &ats.emeraldmc.fr");
	}

	/**
	 * Permet de mettre du texte en haut et en bas du tablist
	 *
	 * @param header String avec gestion des couleurs
	 * @param footer String avec gestion des couleurs
	 */
	public static void setTabList(final Player player, String header, String footer) {
		try {
			final Constructor<?> titleConstructor = Reflection.getClass(ClassEnum.NMS, "PacketPlayOutPlayerListHeaderFooter").getConstructor();
			final Object packet = titleConstructor.newInstance();

			if(header != null) {
				header = Utils.color(header);
				final Object tabHeader = Reflection.getClass(ClassEnum.NMS, "IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{'text':'" + header + "'}");

				final Field aField = Reflection.getField(packet.getClass(), "a");

				aField.set(packet, tabHeader);
			}

			if(footer != null) {
				footer = Utils.color(footer);
				final Object tabFooter = Reflection.getClass(ClassEnum.NMS, "IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{'text':'" + footer + "'}");
				final Field bField = Reflection.getField(packet.getClass(), "b");
				bField.set(packet, tabFooter);
			}

			Reflection.sendPacket(player, packet);
		} catch(final Exception e) {
			e.printStackTrace();
		}
	}

	public static void setToAll() {
		setToAll(0);
	}

	public static void setToAll(final int i) {
		final Collection<? extends Player> players = Bukkit.getOnlinePlayers();
		final int size = Bukkit.getOnlinePlayers().size() - Vanish.getVanish().size() - i;
		for(final Player player : players) {
			TabList.set(player, size);
		}
	}

	public void addPlayers(final Player receiver, final EntityPlayer... createdPlayers) {
		final PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, createdPlayers);
		((CraftPlayer) receiver).getHandle().playerConnection.sendPacket(packet);
	}

	public EntityPlayer createPlayers(final String name, final String listName, final String texture, final String signature) {
		final MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
		final WorldServer worldserver = server.getWorldServer(0);
		final PlayerInteractManager playerinteractmanager = new PlayerInteractManager(worldserver);
		final GameProfile profile = new GameProfile(UUID.randomUUID(), name);
		profile.getProperties().put("textures", new Property("textures", texture, signature));
		final EntityPlayer player = new EntityPlayer(server, worldserver, profile, playerinteractmanager);
		player.listName = new ChatComponentText(listName);
		return player;
	}

	public void removePlayers(final Player receiver) {
		final Collection<? extends Player> playersBukkit = Bukkit.getOnlinePlayers();
		final EntityPlayer[] playersNMS = new EntityPlayer[playersBukkit.size()];
		int current = 0;
		for(final Player player : playersBukkit) {
			playersNMS[current] = ((CraftPlayer) player).getHandle();
			current++;
		}
		final PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, playersNMS);
		((CraftPlayer) receiver).getHandle().playerConnection.sendPacket(packet);
	}

}
