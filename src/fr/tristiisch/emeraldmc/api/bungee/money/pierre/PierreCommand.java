package fr.tristiisch.emeraldmc.api.bungee.money.pierre;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeBPMC;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class PierreCommand extends Command {

	private final int power;

	public PierreCommand(final String command, final int power, final String... aliases) {
		super(command, null, aliases);
		this.power = power;
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			ProxiedPlayer player = null;
			OlympaPlayer emeraldPlayer = null;
			AccountProvider account = null;
			if(sender instanceof ProxiedPlayer) {
				player = (ProxiedPlayer) sender;
				account = new AccountProvider(player.getUniqueId());
				emeraldPlayer = account.getEmeraldPlayer();
				if(emeraldPlayer.hasPowerLessThan(this.power)) {
					player.sendMessage(Utils.color(BungeeConfigUtils.getString("commun.messages.noperm")));
					return;
				}
			}

			if(args.length >= 1) {
				if(!Matcher.isInt(args[0])) {
					sender.sendMessage(Utils.color("&cUsage &7» &c/pierre <nombre> [joueur] [give|remove|set]"));
					return;
				}
				if(args.length == 1) {
					if(emeraldPlayer == null || account == null) {
						sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
						return;
					}

					emeraldPlayer.addPierres(Integer.parseInt(args[0]));
					account.sendAccountToRedis(emeraldPlayer);
					BungeeBPMC.sendEmeraldPlayerChange(player);
					sender.sendMessage(Utils.color("&2EmeraldMC &7» &aVous venez de vous donner " + args[0] + " pierres."));

				} else if(args.length == 2) {
					final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);

					if(target != null) {
						final AccountProvider accountTarget = new AccountProvider(target.getUniqueId());
						emeraldPlayer = accountTarget.getAccountFromRedis();

						if(emeraldPlayer == null) {
							sender.sendMessage(Utils.color("&2EmeraldMC &7» &4" + args[2] + "&c n'a jamais rejoint le serveur."));
							return;
						}

						emeraldPlayer.addPierres(Integer.parseInt(args[0]));
						accountTarget.sendAccountToRedis(emeraldPlayer);
						BungeeBPMC.sendEmeraldPlayerChange(target);
						sender.sendMessage(Utils.color("&2EmeraldMC &7» &aVous venez de donner " + args[0] + " pierres à " + target.getName()));
						target.sendMessage(Utils.color("&2EmeraldMC &7» &aVous venez recevoir " + args[0] + " pierres."));

					} else {
						emeraldPlayer = MySQL.getPlayer(args[1]);
						if(emeraldPlayer == null) {
							sender.sendMessage(Utils.color("&2EmeraldMC &7» &4" + args[2] + "&c n'a jamais rejoint le serveur."));
							return;
						}

						emeraldPlayer.addPierres(Integer.parseInt(args[0]));
						MySQL.savePlayer(emeraldPlayer);
						sender.sendMessage(Utils.color("&2EmeraldMC &7» &aVous venez de donner " + args[0] + " pierres à " + emeraldPlayer.getName()));
					}
				}
			} else {
				sender.sendMessage(Utils.color("&cUsage &7» &c /pierre <nombre> [joueur] [give|remove|set]"));
			}

		});
	}
}
