package fr.tristiisch.emeraldmc.api.bungee.info;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import com.google.common.collect.Lists;

import fr.tristiisch.emeraldmc.api.bungee.ban.BanMySQL;
import fr.tristiisch.emeraldmc.api.bungee.ip.EmeraldIP;
import fr.tristiisch.emeraldmc.api.bungee.ip.IPManagment;
import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.commons.usernamehistory.Username;
import fr.tristiisch.emeraldmc.api.commons.usernamehistory.Usernames;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Info {

	@SuppressWarnings("deprecation")
	public static void showInfo(final CommandSender sender, final Object target, final OlympaPlayer emeraldPlayer) {
		ProxiedPlayer targetPlayer;
		UUID uuid = null;
		String name = null;
		OlympaPlayer targetEmeraldPlayer;

		if(target instanceof String) {
			name = (String) target;
			targetPlayer = ProxyServer.getInstance().getPlayer(name);
		} else if(target instanceof UUID) {
			uuid = UUID.fromString(String.valueOf(target));
			targetPlayer = ProxyServer.getInstance().getPlayer(uuid);
		} else if(target instanceof Integer) {
			targetEmeraldPlayer = MySQL.getPlayer((int) target);
			targetPlayer = ProxyServer.getInstance().getPlayer(targetEmeraldPlayer.getUniqueId());
		} else {
			return;
		}

		List<Username> targetUsernames;
		//targetUsernames = Usernames.loadHistory(targetPlayer.getUniqueId());
		final TextComponent msg = new TextComponent(Utils.color("&3=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n"));

		// Si target est connecté
		if(targetPlayer != null) {
			targetEmeraldPlayer = new AccountProvider(targetPlayer.getUniqueId()).getEmeraldPlayer();
			msg.addExtra(Utils.color("&6" + targetEmeraldPlayer.getName() + " est &aconnecté &6depuis " + Utils.timestampToDuration(targetEmeraldPlayer.getLastCo()) + "\n"));
			// Si target est déconnecté
		} else {

			if(name != null) {
				targetEmeraldPlayer = MySQL.getPlayer(name);
			} else {
				targetEmeraldPlayer = MySQL.getPlayer(uuid);
			}

			if(targetEmeraldPlayer == null) {
				final String targetName = String.valueOf(target);

				final List<OlympaPlayer> emeraldPlayers = new ArrayList<>();
				emeraldPlayers.addAll(MySQL.getPlayersByNameHistory(targetName));
				emeraldPlayers.addAll(MySQL.getPlayersBySimilarName(targetName));

				if(emeraldPlayers.isEmpty()) {
					sender.sendMessage(Utils.color(Prefix.DEFAULT_BAD + "&4%player%&c n'a jamais rejoint le serveur.".replaceAll("%player%", targetName)));
					return;
				}

				msg.setText(Utils.color(Prefix.DEFAULT_BAD + "Vous voulez dire "));

				final List<TextComponent> targetAccountsTexts = new ArrayList<>();

				for(final OlympaPlayer targetAccount : emeraldPlayers) {
					ChatColor color = ChatColor.GRAY;
					if(ProxyServer.getInstance().getPlayer(targetAccount.getUniqueId()) != null) {
						color = ChatColor.GREEN;
					} else if(BanMySQL.isBanned(targetAccount.getUniqueId())) {
						color = ChatColor.DARK_RED;
					}
					final TextComponent targetMsg = new TextComponent(Utils.color(color + targetAccount.getName()));
					targetMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Obtenir des infos sur &e" + targetAccount.getName())).create()));
					targetMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/info " + targetAccount.getUniqueId()));
					targetAccountsTexts.add(targetMsg);
				}

				Utils.toTextComponent(msg, targetAccountsTexts, Utils.color("&c, &4"), Utils.color(" &c?\n"));
				sender.sendMessage(msg);

				return;
			}
			msg.addExtra(Utils.color("&6" + targetEmeraldPlayer.getName() + " s'est &cdéconnecté &6il y a " + Utils.timestampToDuration(targetEmeraldPlayer.getLastCo()) + "\n"));
		}

		targetUsernames = Usernames.loadHistory(targetEmeraldPlayer);

		if(emeraldPlayer == null) {
			msg.addExtra(Utils.color("&6IP: &e" + targetEmeraldPlayer.getIp() + "\n"));
			final TextComponent msgUUID = new TextComponent(Utils.color("&6UUID: &e" + targetEmeraldPlayer.getUniqueId() + "\n"));
			msgUUID.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Copier l'UUID")).create()));
			msgUUID.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, String.valueOf(targetEmeraldPlayer.getUniqueId())));
			msg.addExtra(Utils.color("&6Historique d'IP: &e" + String.join("&6, &e", targetEmeraldPlayer.getIpHistory()) + "\n"));
			msg.addExtra(msgUUID);
		} else if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.ADMIN)) {
			final TextComponent msgip1 = new TextComponent(Utils.color("&6IP: "));
			final List<String> ipHistory = targetEmeraldPlayer.getIpHistory();
			if(ipHistory != null) {
				msgip1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Historique d'IP: " + String.join("&6, &e", ipHistory))).create()));
				msgip1.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, String.join(" ", targetEmeraldPlayer.getIpHistory())));
			} else {
				msgip1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Historique d'IP est vide")).create()));
			}
			msg.addExtra(msgip1);

			final TextComponent msgip2 = new TextComponent(Utils.color("&8[&7CACHEE&8]" + "\n"));
			msgip2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&e" + targetEmeraldPlayer.getIp())).create()));
			msgip2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/info " + targetEmeraldPlayer.getIp()));
			msg.addExtra(msgip2);
		} else {
			msg.addExtra(Utils.color("&6IP: "));
			final TextComponent msgip = new TextComponent(Utils.color("&8[&7CACHEE&8]" + "\n"));
			msgip.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&cSeuls les admins ont accès aux IP.")).create()));
			msg.addExtra(msgip);
		}
		final EmeraldIP emeraldIP = IPManagment.locIp(targetEmeraldPlayer.getIp());

		msg.addExtra(Utils.color("&6VPN: " + (emeraldIP.isVpn() ? "&4Oui" : "&eNon") + "\n"));
		msg.addExtra(Utils.color("&6Première connexion: &e" + Utils.timestampToDate(targetEmeraldPlayer.getFirstCo()) + "\n"));
		msg.addExtra(Utils.color("&6Groupes: &e"));

		final List<TextComponent> groupsTexts = new ArrayList<>();
		for(final Entry<EmeraldGroup, Long> entry : targetEmeraldPlayer.getGroups().entrySet()) {

			final EmeraldGroup group = entry.getKey();
			final long until = entry.getValue();

			ChatColor color;
			if(until == 0) {
				color = ChatColor.GRAY;
			} else {
				color = ChatColor.GREEN;
			}

			final TextComponent groupMsg = new TextComponent(Utils.color(color + group.getName()));
			if(until == 0) {
				groupMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&eGrade &6&npermanant")).create()));
			} else {
				groupMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&eGrade jusqu'au &6" + Utils.timestampToDateAndHour(until))).create()));
			}
			groupsTexts.add(groupMsg);
		}

		Utils.toTextComponent(msg, groupsTexts, ChatColor.YELLOW + ", ", ChatColor.YELLOW + "\n");

		msg.addExtra(Utils.color("&6Minerais: &e" + targetEmeraldPlayer.getMinerais() + "\n"));
		msg.addExtra(Utils.color("&6Pierres: &e" + targetEmeraldPlayer.getPierres() + "\n"));
		if(targetEmeraldPlayer.getTS3ID() != 0) {
			msg.addExtra(Utils.color("&6TS ID: &e" + targetEmeraldPlayer.getTS3ID() + "\n"));
		} else {
			msg.addExtra(Utils.color("&6TS ID: &eNon relié\n"));
		}

		TextComponent banMsg = new TextComponent(Utils.colorFix("&6Banni: " + (BanMySQL.isBanned(targetEmeraldPlayer.getUniqueId()) ? "&cOui" : "&eNon") + "\n"));
		banMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Historique de ban de &e" + targetEmeraldPlayer.getName())).create()));
		banMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/banhist " + targetEmeraldPlayer.getUniqueId()));
		msg.addExtra(banMsg);

		banMsg = new TextComponent(Utils.colorFix("&6BanniIP: " + (BanMySQL.isBanned(targetEmeraldPlayer.getIp()) ? "&cOui" : "&eNon") + "\n"));
		// TODO add for console and soft for modo
		if(emeraldPlayer != null && emeraldPlayer.hasPowerMoreThan(EmeraldGroup.ADMIN)) {
			banMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Histoire de banIP de&e" + targetEmeraldPlayer.getName())).create()));
			banMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/banhist " + targetEmeraldPlayer.getIp()));
		}
		msg.addExtra(banMsg);

		if(targetUsernames == null) {
			msg.addExtra(new TextComponent(Utils.colorFix("&6Historique de pseudo: &cUne erreur avec l'API Mojang est survenu.")));
		} else if(targetUsernames.size() > 1) {
			targetUsernames = Lists.reverse(targetUsernames);
			final List<TextComponent> targetUsernameTexts = new ArrayList<>();
			msg.addExtra(new TextComponent(Utils.colorFix("&6Historique de pseudo: &7")));

			for(final Username targetUsername : targetUsernames) {
				final TextComponent targetMsg = new TextComponent(Utils.color("&7" + targetUsername.getName()));
				if(targetUsername.getTime() != 0) {
					targetMsg.setHoverEvent(
							new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Changé le " + Utils.timestampToDateAndHour(targetUsername.getTime()))).create()));
				} else {
					targetMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Pseudo lors de la création du compte")).create()));
				}
				targetUsernameTexts.add(targetMsg);
			}

			Utils.toTextComponent(msg, targetUsernameTexts, ChatColor.YELLOW + ", ", ChatColor.YELLOW + "\n");
		}
		final List<OlympaPlayer> targetAccounts = MySQL.getPlayersByIp(targetEmeraldPlayer.getIp());
		if(targetAccounts.size() > 1) {
			final List<TextComponent> targetAccountsTexts = new ArrayList<>();
			msg.addExtra(new TextComponent(Utils.colorFix("&6" + targetAccounts.size() + " comptes: ")));

			for(final OlympaPlayer targetAccount : targetAccounts) {
				if(targetAccount.getUniqueId().equals(targetEmeraldPlayer.getUniqueId())) {
					continue;
				}
				ChatColor color = ChatColor.GRAY;
				if(ProxyServer.getInstance().getPlayer(targetAccount.getUniqueId()) != null) {
					color = ChatColor.GREEN;
				} else if(BanMySQL.isBanned(targetAccount.getUniqueId())) {
					color = ChatColor.DARK_RED;
				}
				final TextComponent targetMsg = new TextComponent(Utils.color(color + targetAccount.getName()));
				targetMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Obtenir des infos sur &e" + targetAccount.getName())).create()));
				targetMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/info " + targetAccount.getUniqueId()));
				targetAccountsTexts.add(targetMsg);
			}

			Utils.toTextComponent(msg, targetAccountsTexts, ChatColor.YELLOW + ", ", ChatColor.YELLOW + "\n");
		}

		msg.addExtra(Utils.color("&3=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
		sender.sendMessage(msg);
	}

	public static void showInfoIP(final CommandSender sender, final String ip, final OlympaPlayer emeraldPlayer) {
		final TextComponent msg = new TextComponent(Utils.color("&3=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n"));
		final EmeraldIP emeraldIP = IPManagment.locIp(ip);

		msg.addExtra(Utils.color("&6VPN: " + (emeraldIP.isVpn() ? "&4Oui" : "&eNon") + "\n"));
		msg.addExtra(Utils.color("&6IP: &e" + emeraldIP.getIp() + "\n"));
		msg.addExtra(Utils.color("&6Hostname: &e" + emeraldIP.getHostname() + "\n"));
		msg.addExtra(Utils.color("&6FAI: &e" + emeraldIP.getISP() + "\n"));
		msg.addExtra(Utils.color("&6Pays: &e" + emeraldIP.getCountry() + "\n"));
		msg.addExtra(Utils.color("&6Region: &e" + emeraldIP.getRegion() + "\n"));
		msg.addExtra(Utils.color("&6Ville: &e" + emeraldIP.getCity() + "\n"));
		msg.addExtra(Utils.color("&6Code postal: &e" + emeraldIP.getPostcode() + "\n"));

		final List<OlympaPlayer> targetAccountsIP = MySQL.getPlayersByIpHistory(ip);
		if(targetAccountsIP.size() > 1) {
			final List<TextComponent> targetAccountsTexts = new ArrayList<>();
			msg.addExtra(new TextComponent(Utils.colorFix("&aA déjà été utilisé par &2" + targetAccountsIP.size() + "&a comptes:")));

			for(final OlympaPlayer targetAccount : targetAccountsIP) {
				ChatColor color = ChatColor.GRAY;
				if(ProxyServer.getInstance().getPlayer(targetAccount.getUniqueId()) != null) {
					color = ChatColor.GREEN;
				} else if(BanMySQL.isBanned(targetAccount.getUniqueId())) {
					color = ChatColor.DARK_RED;
				}
				final TextComponent targetMsg = new TextComponent(Utils.color(color + targetAccount.getName()));
				targetMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Obtenir des infos sur &e" + targetAccount.getName())).create()));
				targetMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/info " + targetAccount.getUniqueId()));
				targetAccountsTexts.add(targetMsg);
			}

			Utils.toTextComponent(msg, targetAccountsTexts, ChatColor.YELLOW + ", ", ChatColor.YELLOW + ".\n");
		}

		final List<OlympaPlayer> targetAccounts = MySQL.getPlayersByIp(ip);
		if(targetAccounts.size() > 1) {
			final List<TextComponent> targetAccountsTexts = new ArrayList<>();
			msg.addExtra(new TextComponent(Utils.colorFix("&aCorrespond à &2" + targetAccounts.size() + "&a comptes:")));

			for(final OlympaPlayer targetAccount : targetAccounts) {
				ChatColor color = ChatColor.GRAY;
				if(ProxyServer.getInstance().getPlayer(targetAccount.getUniqueId()) != null) {
					color = ChatColor.GREEN;
				} else if(BanMySQL.isBanned(targetAccount.getUniqueId())) {
					color = ChatColor.DARK_RED;
				}
				final TextComponent targetMsg = new TextComponent(Utils.color(color + targetAccount.getName()));
				targetMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Obtenir des infos sur &e" + targetAccount.getName())).create()));
				targetMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/info " + targetAccount.getUniqueId()));
				targetAccountsTexts.add(targetMsg);
			}

			Utils.toTextComponent(msg, targetAccountsTexts, ChatColor.YELLOW + ", ", ChatColor.YELLOW + ".\n");
		}

		if(targetAccountsIP.size() == 0 && targetAccounts.size() == 0) {
			msg.addExtra(Utils.color("&cL'ip n'a jamais été utilisé." + "\n"));
		}
		/*
		final List<EmeraldPlayer> targets = MySQL.getIPInfo(ip);
		final TextComponent msg2 = new TextComponent(

					BungeeConfigUtils.getString("bungee.info.messages.iprecognized").replaceAll("%s%", targets.size() > 1 ? "s" : "").replaceAll("%size%", String.valueOf(targets.size())));
				if(!targets.isEmpty()) {
					int i = 1;
					String separator = Utils.color("&a, ");
					for(final EmeraldPlayer target : targets) {
						if(i == targets.size()) {
							separator = Utils.color("&a.\n");
						}
						final TextComponent targetmsg = new TextComponent(Utils.color("&b" + target.getName() + separator));
						targetmsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Obtenir des infos sur &e" + target.getName())).create()));
						targetmsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/info " + target.getUniqueId()));
						msg2.addExtra(targetmsg);
						i++;
					}
					msg.addExtra(msg2);
				} else {
					msg.addExtra(Utils.color(BungeeConfigUtils.getString("bungee.info.messages.ipnotrecognized").replaceAll("%ip%", ip) + "\n"));
				}*/

		msg.addExtra(Utils.color("&3=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
		sender.sendMessage(msg);
	}
}
