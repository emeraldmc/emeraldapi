package fr.tristiisch.emeraldmc.api.bungee.group;

import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class GroupCommand extends Command {

	public String command;
	private final int power;

	public GroupCommand(final String command, final int power, final String... aliases) {
		super(command, null, aliases);
		this.command = command;
		this.power = power;
		ProxyServer.getInstance().getPluginManager().registerListener(EmeraldBungee.getInstance(), new GroupListener(command, power, aliases));
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(final CommandSender sender, final String[] args) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			ProxiedPlayer player = null;
			if(sender instanceof ProxiedPlayer) {
				player = (ProxiedPlayer) sender;
				if(new AccountProvider(player.getUniqueId()).getEmeraldPlayer().hasPowerLessThan(this.power)) {
					player.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
					return;
				}
			}
			if(args.length >= 2) {
				final String arg1 = args[0];
				ProxiedPlayer target;
				OlympaPlayer emeraldTarget;
				AccountProvider accountProvider = null;

				// Si target est un UUID
				if(Matcher.isUUID(arg1)) {
					final UUID uuid = UUID.fromString(arg1);
					target = ProxyServer.getInstance().getPlayer(uuid);
					// Si target n'est pas connecté
					if(target == null) {
						emeraldTarget = MySQL.getPlayer(uuid);
						// Si target n'est pas dans la bdd
						if(emeraldTarget == null) {
							sender.sendMessage(BungeeConfigUtils.getString("bungee.group.messages.playerneverjoin").replaceAll("%player%", arg1));
							return;
						}
						// Si target est connecté
					} else {
						emeraldTarget = new AccountProvider(target.getUniqueId()).getEmeraldPlayer();
					}

					// Si target est un pseudo
				} else if(Matcher.isUsername(arg1)) {
					target = ProxyServer.getInstance().getPlayer(arg1);
					// Si target n'est pas connecté
					if(target == null) {
						emeraldTarget = MySQL.getPlayer(arg1);
						// Si target n'est pas dans la bdd
						if(emeraldTarget == null) {
							sender.sendMessage(BungeeConfigUtils.getString("bungee.group.messages.playerneverjoin").replaceAll("%player%", arg1));
							return;
						}
						// Si target est connecté
					} else {
						accountProvider = new AccountProvider(target.getUniqueId());
						emeraldTarget = accountProvider.getEmeraldPlayer();
					}
				} else {
					sender.sendMessage(BungeeConfigUtils.getString("bungee.group.messages.typeunknown").replaceAll("%type%", arg1));
					return;
				}

				final String targetGroupName = args[1];

				long targetGroupUntil = 0;
				if(args.length >= 5) {
					final int amount = Integer.parseInt(args[3]);
					final int field = Utils.toField(args[4]);
					if(field == 0) {
						sender.sendMessage(Utils.color("&cUsage &7» &c/group <player> <group> <set|add|remove> <nombre> <&n&lmois|jours&c>"));
						return;
					}
					targetGroupUntil = Utils.addTimeToCurrentTime(field, amount);
				} else if(args.length == 4) {
					sender.sendMessage(Utils.color("&cUsage &7» &c/group <player> <group> <set|add|remove> <nombre> <mois|jours>"));
					return;
				}

				final EmeraldGroup targetAfterGroup = EmeraldGroup.getByName(targetGroupName);
				if(targetAfterGroup == null) {
					sender.sendMessage(BungeeConfigUtils.getString("bungee.group.messages.groupsdoesnotexist").replaceAll("%group%", targetGroupName));
					return;
				}

				final EmeraldGroup targetBeforeGroup = emeraldTarget.getGroup();

				if(args.length == 3 && args[2].equalsIgnoreCase("add")) {

					if(targetAfterGroup.getID() == targetBeforeGroup.getID() && emeraldTarget.getGroupUntil() == targetGroupUntil) {
						sender.sendMessage(
								BungeeConfigUtils.getString("bungee.group.messages.alreadyingroup").replaceAll("%group%", targetAfterGroup.getName()).replaceAll("%player%", emeraldTarget.getName()));
						return;
					}

					BungeeGroup.addGroup(target, accountProvider, emeraldTarget, targetAfterGroup, targetGroupUntil);
					sender.sendMessage(BungeeConfigUtils.getString("bungee.group.messages.playeraddgroup").replace("%player%", emeraldTarget.getName()).replace("%group%", targetAfterGroup.getName()));

				} else if(args.length == 3 && args[2].equalsIgnoreCase("remove")) {

					if(targetAfterGroup.getID() != targetBeforeGroup.getID()) {
						sender.sendMessage(Utils.color(Prefix.DEFAULT_BAD + "Le joueur %player% n'est pas dans le groupe %group%.")
								.replaceAll("%group%", targetAfterGroup.getName())
								.replaceAll("%player%", emeraldTarget.getName()));
						return;
					}
					BungeeGroup.removeGroup(target, accountProvider, emeraldTarget, targetAfterGroup);
					sender.sendMessage(
							BungeeConfigUtils.getString("bungee.group.messages.playerremovegroup").replace("%player%", emeraldTarget.getName()).replace("%group%", targetAfterGroup.getName()));

				} else if(args.length == 2 || args[2].equalsIgnoreCase("set")) {
					if(targetAfterGroup.getID() == targetBeforeGroup.getID() && emeraldTarget.getGroupUntil() == targetGroupUntil) {
						sender.sendMessage(
								BungeeConfigUtils.getString("bungee.group.messages.alreadyingroup").replaceAll("%group%", targetAfterGroup.getName()).replaceAll("%player%", emeraldTarget.getName()));
						return;
					}
					BungeeGroup.setGroup(target, accountProvider, emeraldTarget, targetAfterGroup, targetGroupUntil);

					sender.sendMessage(BungeeConfigUtils.getString("bungee.group.messages.playerchangegrouptosender")
							.replace("%player%", emeraldTarget.getName())
							.replace("%oldgroup%", targetBeforeGroup.getName())
							.replace("%group%", targetAfterGroup.getName()));
				} else {
					sender.sendMessage(Utils.color("&cUsage &7» &c/group <player> <group> [set|add|remove] [nombre] [mois|jour]"));
				}

			} else {
				sender.sendMessage(Utils.color("&cUsage &7» &c/group <player> <group> [set|add|remove] [nombre] [mois|jour]"));
			}
		});
	}
}
