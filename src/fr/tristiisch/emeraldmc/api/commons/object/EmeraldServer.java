package fr.tristiisch.emeraldmc.api.commons.object;

import java.util.Arrays;

import com.google.gson.Gson;

import fr.tristiisch.emeraldmc.api.commons.ping.ServerListPing.EmeraldServerInfo;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

public class EmeraldServer {

	private String name;
	private double tps;
	private final EmeraldServerStatus status;
	private EmeraldServerInfo info;
	private String error;
	
	public EmeraldServer(final String name, final String error){
		this.name = name;
		this.error = error;
		this.status = EmeraldServerStatus.CLOSE;
	}
	
	public EmeraldServer(final String name, final double tps, final EmeraldServerStatus status){
		this.name = name;
		this.tps = tps;
		this.status = status;
	}
	
	public EmeraldServer(final double tps, final EmeraldServerStatus status) {
		this.tps = tps;
		this.status = status;
	}

	public void setName(final String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

	public double getTps() {
		return this.tps;
	}

	public EmeraldServerStatus getStatus() {
		return this.status;
	}
	
	public EmeraldServerInfo getInfo() {
		return this.info;
	}

	public void setInfo(final EmeraldServerInfo info) {
		this.info = info;
	}

	public String getError() {
		return this.error;
	}

	public ServerInfo getServerInfo() {
		return ProxyServer.getInstance().getServerInfo(this.name);
	}
	
	public String toJson(){
		return new Gson().toJson(this);
	}
	
	public static EmeraldServer fromJson(final String json){
		return new Gson().fromJson(json, EmeraldServer.class);
	}
	
	public static EmeraldServer fromJson(final String json, final String name){
		final EmeraldServer emeraldserver = fromJson(json);
		emeraldserver.setName(name);
		return emeraldserver;
	}
	
/*	public enum EmeraldServerError {
		
		CLOSE("connexion refusée"),
		UNKNOWN_HOST("host inconnu"),
		TIMEOUT("temps d'attente écoulé"),
		UNKNOWN("inconnu");
		
		final private String name;

		private EmeraldServerError(String name){
			this.name = name;
		}

		public String getName() {
			return name;
		}
		
	}*/
	
	public enum EmeraldServerStatus {
		
		OPEN(1, "ouvert", ChatColor.GREEN, true),
		CLOSE(2, "indisponible", ChatColor.DARK_RED, false),
		FULL(3, "plein", ChatColor.YELLOW, false),
		MAINTENANCE(4, "en maintenance", ChatColor.GOLD, false),
		WHITELISTED(5, "réservé", ChatColor.GOLD, false);
		
		final private int id;
		final private String name;
		final private ChatColor color;
		final private boolean open;
		
		private EmeraldServerStatus(final int id, final String name, final ChatColor color, final boolean open) {
			this.id = id;
			this.name = name;
			this.color = color;
			this.open = open;
		}
		
		public String getName() {
			return this.name;
		}
		
		public String getNameColored() {
			return this.color + this.name;
		}
		
		public ChatColor getColor() {
			return this.color;
		}
		
		public boolean isOpen() {
			return this.open;
		}
		
		public int getID() {
			return this.id;
		}

		public static EmeraldServerStatus getStatus(final int i) {
			return Arrays.stream(EmeraldServerStatus.values()).filter(serverstatus -> serverstatus.getID() == i).findFirst().orElse(null);

		}
	}
}
