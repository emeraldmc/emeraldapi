package fr.tristiisch.emeraldmc.api.bungee.ip;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;

public class IPManagment {

	static final List<String> FAIVPN = Arrays.asList("AS198605 AVAST Software s.r.o.", "AS60068 Datacamp Limited", "AS12876 ONLINE S.A.S.", "AS46562 Total Server Solutions L.L.C.");

	public static void addIp(final String ip, final boolean vpn) {
		final int ipInfo = IPMySQL.isVPN(ip);
		if(ipInfo == -1) {
			IPMySQL.addIp(ip, vpn);
		} else if(ipInfo == 0 && vpn || ipInfo == 1 && !vpn) {
			IPMySQL.changeIp(ip, vpn);
		}

	}

	public static int getId(final String ip) {
		int id = IPMySQL.getId(ip);
		if(id == -1) {
			IPMySQL.addIp(ip, isVPNFromInternet(ip));
			id = MySQL.getnextID("ip", "id") - 1;
		}
		return id;
	}

	public static String getIp(final int id) {
		final String ip = IPMySQL.getIp(id);
		if(ip != null) {
			return ip;
		}
		return null;
	}

	public static boolean isVPN(final String ip) {
		final int ipInfo = IPMySQL.isVPN(ip);
		if(ipInfo == 1) {
			return true;
		} else if(ipInfo == 0) {
			return false;
		}

		final boolean b = isVPNFromInternet(ip);
		if(ipInfo == -1) {
			IPMySQL.addIp(ip, b);
		}

		return b;
	}

	public static boolean isVPNFromInternet(final String ip) {
		try {
			final String sURL = "http://proxycheck.io/v2/" + ip + "?key=2i9y52-019793-1d7248-01e861&vpn=1&asn=1&node=1&time=1&inf=0&port=1&seen=1&days=7&tag=msg";
			final URL url = new URL(sURL);
			final HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.connect();

			final JsonParser jp = new JsonParser();
			JsonElement root;
			root = jp.parse(new InputStreamReader((InputStream) request.getContent()));

			final JsonObject rootobj = root.getAsJsonObject();

			if(rootobj.get("status").getAsString().equals("ok")) {

				final JsonObject infoJson = rootobj.getAsJsonObject(ip);
				final boolean isVPN = infoJson.get("proxy").getAsString().equals("yes");
				return isVPN;
			}

		} catch(JsonIOException | JsonSyntaxException | IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static EmeraldIP locIp(final String ip) {
		try {
			final String sURL = "https://ipinfo.io/" + ip + "/json?token=e3a4e10c49ca89";
			final URL url = new URL(sURL);
			final HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.connect();

			final JsonParser jp = new JsonParser();
			JsonElement root;
			root = jp.parse(new InputStreamReader((InputStream) request.getContent()));

			final JsonObject rootobj = root.getAsJsonObject();
			return new EmeraldIP(
				rootobj.get("ip") != null ? rootobj.get("ip").getAsString() : null,
				rootobj.get("hostname") != null ? rootobj.get("hostname").getAsString() : null,
				rootobj.get("org") != null ? rootobj.get("org").getAsString() : null,
				rootobj.get("country") != null ? rootobj.get("country").getAsString() : null,
				rootobj.get("region") != null ? rootobj.get("region").getAsString() : null,
				rootobj.get("postal") != null ? rootobj.get("postal").getAsString() : null,
				rootobj.get("city") != null ? rootobj.get("city").getAsString() : null,
				rootobj.get("loc") != null ? rootobj.get("loc").getAsString() : null);
		} catch(JsonIOException | JsonSyntaxException | IOException e) {
			e.printStackTrace();
			return null;
		}

	}
}
