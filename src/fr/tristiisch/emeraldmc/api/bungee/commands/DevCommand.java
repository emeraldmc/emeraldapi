package fr.tristiisch.emeraldmc.api.bungee.commands;

import fr.tristiisch.emeraldmc.api.commons.Prefix;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Plugin;

public class DevCommand extends BungeeCommand {

	public DevCommand(final Plugin plugin) {
		super(plugin, "dev");
		this.register();
	}

	@Override
	public void onCommand(final CommandSender sender, final String[] args) {
		this.sendMessage(Prefix.DEFAULT_GOOD + "Serveur exclusivement développé par &2Tristiisch&a.");
	}
}
