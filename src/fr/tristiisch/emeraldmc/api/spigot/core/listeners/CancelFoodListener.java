package fr.tristiisch.emeraldmc.api.spigot.core.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class CancelFoodListener implements Listener {
	
	@EventHandler
	private void FoodLevelChangeEvent(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}
}
