package fr.tristiisch.emeraldmc.api.bungee.report;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Report {

	private static Map<UUID, Long> players = new HashMap<>();

	public static void addPlayer(final ProxiedPlayer player) {
		players.put(player.getUniqueId(), Utils.getCurrentTimeinSeconds() + 60);
	}

	public static void remove(final ProxiedPlayer player) {
		players.remove(player.getUniqueId());
	}

	public static boolean canReport(final ProxiedPlayer player) {
		if(players.containsKey(player.getUniqueId())) {
			if(players.get(player.getUniqueId()) < Utils.getCurrentTimeinSeconds()) {
				remove(player);
				return true;
			}
			return false;
		}
		return true;
	}

	public static void sendToStaff(final ProxiedPlayer player, final ProxiedPlayer target, final String reason) {
		final TextComponent msg2 = new TextComponent(Utils.colorFix("&c✦ &5Report &7» &d" + target.getName() + " &5sur le " + target.getServer().getInfo().getName() + " ➤ &d" + reason));
		msg2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&7Auteur: " + player.getName() + "\n\n&2Cliquez pour rejoindre le serveur")).create()));
		msg2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/server " + target.getServer().getInfo().getName()));

		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			for(final ProxiedPlayer staff : ProxyServer.getInstance().getPlayers().stream().filter(p -> new AccountProvider(p.getUniqueId()).getEmeraldPlayer().getGroup().isStaffMember()).collect(
					Collectors.toList())) {
				staff.sendMessage(msg2);
			}
		});
	}
}
