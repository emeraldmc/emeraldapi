package fr.tristiisch.emeraldmc.api.spigot.core.boutique;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.core.boutique.menu.clef.BoutiqueClef;
import fr.tristiisch.emeraldmc.api.spigot.core.boutique.menu.vip.BoutiqueVIP;
import fr.tristiisch.emeraldmc.api.spigot.core.boutique.menu.vip.BoutiqueVIPInfo;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public class BoutiqueListener implements Listener {

	@EventHandler
	private void InventoryClickEvent(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final Inventory clickedInventory = event.getClickedInventory();

		if(clickedInventory == null) {
			return;
		}
		final ItemStack currentItem = event.getCurrentItem();

		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData != null) {
			switch(guiData.getId()) {
			case "shop":
				event.setCancelled(true);

				if(event.getClickedInventory() == null && event.getClickedInventory().getType() == InventoryType.PLAYER) {
					return;
				}

				if(currentItem.isSimilar(BoutiqueItem.VIP.getItemStack())) {

					BoutiqueVIP.openVIP(player);

				} else if(currentItem.isSimilar(BoutiqueItem.CLEF_MENU.getItemStack())) {

					BoutiqueClef.openClef(player);

				} else if(!currentItem.isSimilar(GuiTools.CANCEL) && !currentItem.isSimilar(ItemTools.create(Material.AIR, 0, (short) -1))) {

					GuiTools.cancelInDev(event);

				}
				break;
			case "shop_vip":
				event.setCancelled(true);
				if(event.getClickedInventory() == null && event.getInventory().getName() != event.getClickedInventory().getName()) {
					return;
				}

				if(!currentItem.isSimilar(GuiTools.CANCEL) && !currentItem.isSimilar(ItemTools.create(Material.AIR, 0, (short) -1))) {
					BoutiqueVIP.openVIP2(player, BoutiqueVIPInfo.get(currentItem));
					return;
				}
				break;
			case "shop_vip2":
				event.setCancelled(true);
				if(event.getClickedInventory() == null && event.getInventory().getName() != event.getClickedInventory().getName()) {
					return;
				}
				final BoutiqueVIPInfo vip = BoutiqueVIPInfo.get(guiData.getData());

				if(event.getRawSlot() == 3) {
					BoutiqueVIP.buy(player, vip, false, event);
					return;
				} else if(event.getRawSlot() == 5) {
					BoutiqueVIP.buy(player, vip, true, event);
					return;
				}

				break;

			case "clef":
				event.setCancelled(true);
				if(event.getClickedInventory() == null && event.getInventory().getName() != event.getClickedInventory().getName()) {
					return;
				}

				if(!currentItem.isSimilar(GuiTools.CANCEL) && !currentItem.isSimilar(ItemTools.create(Material.AIR, 0, (short) -1))) {
					BoutiqueClef.buy(player, event);
				}
				break;
			}

		}
	}

}
