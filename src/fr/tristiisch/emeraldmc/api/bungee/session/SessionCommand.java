package fr.tristiisch.emeraldmc.api.bungee.session;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.commands.BungeeCommand;
import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Plugin;

public class SessionCommand extends BungeeCommand {

	public SessionCommand(final Plugin plugin) {
		super(plugin, "session", EmeraldGroup.MODERATEUR);
		this.usageString = "/session <dd/MM/yyyy HH:mm:ss>";
		this.minArg = 1;
		this.register();
	}

	@Override
	public void onCommand(final CommandSender sender, final String[] args) {

		final List<Session> sessions;

		if(Matcher.isUsername(args[0])) {

			final OlympaPlayer emeraldTarget = MySQL.getPlayer(args[0]);
			sessions = SessionMySQL.getSessions(emeraldTarget.getId());

		} else if(Matcher.isUUID(args[0])) {

			final OlympaPlayer emeraldTarget = MySQL.getPlayer(UUID.fromString(args[0]));
			sessions = SessionMySQL.getSessions(emeraldTarget.getId());

		} else if(args.length == 1 && Matcher.isInt(args[0])) {

			final long timestamp = Long.parseLong(args[0]);
			sessions = SessionMySQL.getSessionsAt(timestamp);

		} else if(Matcher.isDate(args[0])) {

			Date date = null;
			if(args.length == 2 && Matcher.isHour(args[1])) {
				try {
					date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(args[0] + " " + args[1]);
				} catch(final ParseException e) {
					e.printStackTrace();
				}
			} else {
				try {
					date = new SimpleDateFormat("dd/MM/yyyy").parse(args[0]);
				} catch(final ParseException e) {
					e.printStackTrace();
				}
			}
			final long timestamp = date.getTime() / 1000L;
			sessions = SessionMySQL.getSessionsAt(timestamp);

		} else {
			this.sendUsage();
			return;
		}

		if(sessions.size() != 0) {
			SessionShow.showSessions(this, sessions);
		} else {
			this.sendMessage(Prefix.DEFAULT_BAD + "Aucune session n'a été trouvée.");
		}

		return;
	}

}
