package fr.tristiisch.emeraldmc.api.spigot.core.staffmode.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.tristiisch.emeraldmc.api.spigot.core.staffmode.StaffMode;

public class StaffModeRestrictListener implements Listener {

	@EventHandler
	public void EntityDamageEvent(final EntityDamageEvent event) {
		if(!(event.getEntity() instanceof Player)) {
			return;
		}
		final Player player = (Player) event.getEntity();
		if(StaffMode.isStaffMode(player)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void InventoryClickEvent(final InventoryClickEvent event) {
		if(!(event.getWhoClicked() instanceof Player)) {
			return;
		}
		final Player player = (Player) event.getWhoClicked();
		if(StaffMode.isStaffMode(player)) {
			event.setCancelled(true);

		}
	}
}
