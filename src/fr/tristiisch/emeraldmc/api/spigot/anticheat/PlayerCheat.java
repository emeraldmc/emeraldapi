package fr.tristiisch.emeraldmc.api.spigot.anticheat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection;

public class PlayerCheat {
    public static HashMap<UUID, PlayerCheat> players = new HashMap<>();;

	public static HashMap<Player, PlayerCheat> verifier = new HashMap<>();
	// temp
	public static String InventoryName = "&6Vérif ➤ &e";
	public static int guiSize = 5*9; 
	public static int slotArmor = 5;
	public static int slotInv = 9;
	public static int slotHotBar = 36;
    
    
    public List<Integer> clicks = new ArrayList<>();
    public Integer click ;
    public int nbAlerts;
    public int maxCPS;
    public long lastBlockInteraction;
    public long lastAlert;
    public long Connexion;
    public long lastBlockPlace;
    public String name;
    
    public PlayerCheat(final Player player) {
        this.lastBlockInteraction = 0L;
        this.lastAlert = 0L;
        this.Connexion = 0L;
        this.lastBlockPlace = 0L;
        click = 0;
        nbAlerts = 0;
        maxCPS = 0;
        players.put(player.getUniqueId(), this);
        this.name = player.getName();
        this.Connexion = System.currentTimeMillis();
    }
    
    public String getName() {
        return this.name;
    }
    
    public Player getPlayer() {
        return Bukkit.getPlayer(getName());
    }
    
    public static PlayerCheat getByPlayer(final Player player) {
        return players.get(player.getUniqueId());
    }
    
    public static PlayerCheat getByString(final String name) {
        if (Bukkit.getPlayerExact(name) == null) {
            return null;
        }
        return players.get(Bukkit.getPlayerExact(name).getUniqueId());
    }
    
    public static void removePlayer(final Player player) {
    	PlayerCheat pc = getByPlayer(player);
    	for(Player p : verifier.keySet()) {
    		if(verifier.get(p).equals(pc)) {
    			p.closeInventory();
    			p.sendMessage(Utils.color("&6Verif &7» &cJoueur &4%player% &cs'est déconnecté.".replaceAll("%player%", player.getName())));
    		}
    	}
        players.remove(player.getUniqueId());
    }
    
    public int getPing() {
    	return Reflection.ping(getPlayer());
    }
}
