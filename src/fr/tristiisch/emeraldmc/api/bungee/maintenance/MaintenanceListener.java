package fr.tristiisch.emeraldmc.api.bungee.maintenance;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class MaintenanceListener implements Listener {

	private final String command;
	private final int power;
	private final List<String> arg2 = Arrays.asList("on","off","dev","add","remove","list");


	public MaintenanceListener(final String command, final int power) {
		this.command = command;
		this.power = power;
	}

	@EventHandler
	public void onTabComplete(final TabCompleteEvent event)  {
		if(event.isCancelled()) {
			return;
		}
		final ProxiedPlayer player = (ProxiedPlayer) event.getSender();
		final String msg = event.getCursor();
		final String[] args = msg.split(" ");

		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			if(args[0].equalsIgnoreCase("/" + this.command) && new AccountProvider(player.getUniqueId()).getEmeraldPlayer().hasPowerMoreThan(this.power)) {
				final String check = args[args.length - 1].toLowerCase();
				if(args.length == 1) {
					if(msg.endsWith(" ")) {
						for(final String arg : this.arg2) {
							if(arg.toLowerCase().startsWith(check)) {
								event.getSuggestions().add(arg);
							}
						}
					}
				} else if(args.length == 2) {
					if(!msg.endsWith(" ")) {
						event.getSuggestions().addAll(this.arg2);
						/*					for(String arg : arg2) {
						event.getSuggestions().add(arg);
					}*/
					} else {
						if(args[1].equalsIgnoreCase("remove")) {
							event.getSuggestions().addAll(BungeeConfigUtils.getConfig("maintenance").getStringList("whitelist"));
							/*						for(String whitelisted : Config.getConfig("maintenance").getStringList("bungee.maintenance.whitelist")){
							event.getSuggestions().add(whitelisted);
						}*/
						}
					}

				} else if(args.length == 3) {
					if(!msg.endsWith(" ")) {
						if(args[1].equalsIgnoreCase("add")) {
							event.getSuggestions().addAll(player.getServer().getInfo().getPlayers().stream().map(ProxiedPlayer::getName).filter(playerName -> playerName.toLowerCase().startsWith(check)).collect(Collectors.toList()));
							/*						for(ProxiedPlayer players : player.getServer().getInfo().getPlayers()){
							if(players.getName().toLowerCase().startsWith(check)) {
								event.getSuggestions().add(players.getName());
							}
						}*/
						} else if(args[1].equalsIgnoreCase("remove")) {
							event.getSuggestions().addAll(BungeeConfigUtils.getConfig("maintenance").getStringList("whitelist").stream().filter(playerName -> playerName.toLowerCase().startsWith(check)).collect(Collectors.toList()));
							/*
						for(String whitelisted : Config.getConfig("maintenance").getStringList("bungee.maintenance.whitelist")){
							if(whitelisted.toLowerCase().startsWith(check)) {
								event.getSuggestions().add(whitelisted);
							}
						}*/
						}
					}

				}
			}
		});
	}
}
