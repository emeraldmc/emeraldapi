package fr.tristiisch.emeraldmc.api.bungee.commands;

import java.util.Arrays;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class BroadcastCommand extends Command {

	private final String command;
	private final int power;
	private final String prefix = "&7[&2&lINFO&7] &a";

	public BroadcastCommand(final String command, final int power, final String aliases) {
		super(command, null, aliases);
		this.command = command;
		this.power = power;
	}

	private void broadcast(final String message) {
		ProxyServer.getInstance().broadcast(Utils.color(this.prefix + message));
	}

	private void broadcast(final String message, final ServerInfo server) {
		for(final ProxiedPlayer players : server.getPlayers()) {
			players.sendMessage(Utils.color(this.prefix + message));
		}
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			ProxiedPlayer player = null;
			if(sender instanceof ProxiedPlayer) {
				player = (ProxiedPlayer) sender;
				final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(emeraldPlayer.hasPowerLessThan(this.power)) {
					player.sendMessage(Utils.color(BungeeConfigUtils.getString("commun.messages.noperm")));
					return;
				}
			}
			if(args.length >= 2) {
				final String messsage = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
				if(args[0].equalsIgnoreCase("all")) {
					this.broadcast(messsage);
				} else if(args[0].equalsIgnoreCase("here")) {
					if(player == null) {
						sender.sendMessage(Utils.color("&2EmeraldMC &7» &cImpossible d'utiliser &4here&c en tant que console."));
					}
					this.broadcast(messsage, player.getServer().getInfo());

				} else {
					final ServerInfo server = ProxyServer.getInstance().getServerInfo(args[0]);
					if(server == null) {
						sender.sendMessage(Utils.color("&2EmeraldMC &7» &cLe serveur &4" + args[0] + " &cn'existe pas."));
						return;
					}

					this.broadcast(messsage, server);
				}

			} else {
				sender.sendMessage(Utils.color("§6Usage &7» &c/" + this.command + " <serveur|all|here> <message>"));
			}
		});
	}
}
