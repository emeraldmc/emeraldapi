package fr.tristiisch.emeraldmc.api.spigot.core.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class RespawnListener implements Listener {
	
	@EventHandler
	public void PlayerRespawnEvent(PlayerRespawnEvent event) {
		event.setRespawnLocation(EmeraldSpigot.getSpawn());
	}
}
