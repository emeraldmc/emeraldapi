package fr.tristiisch.emeraldmc.api.spigot.core.verif;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class AlertCPSCommand implements CommandExecutor {

	private final HashMap<Player, Boolean> alert = new HashMap<>();

	private final int power;

	public AlertCPSCommand(final int power) {
		this.power = power;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String arg, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final Player player = (Player) sender;
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerLessThan(this.power)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}

			if(args.length == 0) {
				if(this.canAlert(player)) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cLes alertes cps sont désactivées"));
					this.setAlert(player, false);
				} else {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &aLes alertes cps sont activées"));
					this.setAlert(player, true);
				}
			} else {
				switch (args[0].toLowerCase()) {
				case "off":
					if(!this.canAlert(player)) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLes alertes sont déjà désactivées"));
					} else {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &aLes alertes ont bien été désactivées"));
						this.setAlert(player, false);
					}
					break;
				case "on":
					if(this.canAlert(player)) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLes alertes sont déjà activées"));
					} else {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &aLes alertes ont bien été activées"));
						this.setAlert(player, true);
					}
					break;
				default:
					player.sendMessage(Utils.color("&cUsage &7» &c/alertcps <on|off>"));
				}
			}
		});
		return true;
	}

	public boolean canAlert(final Player player) {
		return this.alert.containsKey(player) ? this.alert.get(player) : true;
	}

	public void setAlert(final Player player, final boolean b) {
		this.alert.put(player, b);
	}
}
