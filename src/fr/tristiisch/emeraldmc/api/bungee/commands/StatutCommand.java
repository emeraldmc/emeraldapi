package fr.tristiisch.emeraldmc.api.bungee.commands;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class StatutCommand extends Command {

	private final int[] power;

	public StatutCommand(final String command, final int[] power, final String... aliases) {
		super(command, null, aliases);
		this.power = power;
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			if(sender instanceof ProxiedPlayer) {
				final ProxiedPlayer player = (ProxiedPlayer) sender;
				if(!new AccountProvider(player.getUniqueId()).getEmeraldPlayer().hasPower(this.power)) {
					player.sendMessage(Utils.color(BungeeConfigUtils.getString("commun.messages.noperm")));
					return;
				}
			}
			if(args.length >= 1) {
				final String serverName = args[0];
				final EmeraldServer emeraldServer = EmeraldServers.getServer(serverName);
				if(emeraldServer == null) {
					sender.sendMessage("&cLe serveur &4" + serverName + " &cn'existe pas.");
					return;
				}

				this.sendInfoServerMessage(sender, emeraldServer);

			} else {
				int i = 0;
				for(final EmeraldServer emeraldServer : EmeraldServers.getServers()) {
					this.sendInfoServerMessage(sender, emeraldServer);
					i++;
				}
				if(i == 0) {
					sender.sendMessage("&cAucunes informations sur les serveurs spigots.");
				}
			}
		});
	}

	private void sendInfoServerMessage(final CommandSender sender, final EmeraldServer server) {
		if(server.getError() == null) {
			sender.sendMessage(Utils.color("&6%name%&7: %status% &a%online%&7/&2%max%")
					.replaceAll("%name%", server.getName())
					.replaceAll("%status%", server.getStatus().getColor() + server.getStatus().getName())
					.replaceAll("%online%", String.valueOf(server.getInfo().getPlayers().getOnline()))
					.replaceAll("%max%", String.valueOf(server.getInfo().getPlayers().getMax())));
		} else {
			sender.sendMessage(Utils.color("&c%name%&7: &cerreur: %erreur%").replaceAll("%name%", server.getName()).replaceAll("%erreur%", server.getError()));
		}
	}
}
