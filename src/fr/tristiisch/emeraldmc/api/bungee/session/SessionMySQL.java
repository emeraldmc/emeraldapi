package fr.tristiisch.emeraldmc.api.bungee.session;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.tristiisch.emeraldmc.api.bungee.ip.IPManagment;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.DatabaseManager;

public class SessionMySQL {

	public static void addSession(final Session session) {
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection().prepareStatement("INSERT INTO session (ip_id, player_id, start, stop) VALUES (?, ?, ?, ?)");
			int i = 1;
			pstate.setInt(i++, session.getIpId());
			pstate.setInt(i++, session.getEmeraldPlayerId());
			pstate.setLong(i++, session.getStart());
			pstate.setLong(i++, session.getStop());
			pstate.executeUpdate();
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return;
	}

	public static Session getSession(final ResultSet resultSet) {
		try {
			final int id = resultSet.getInt("id");
			final int playerId = resultSet.getInt("player_id");
			final long start = resultSet.getLong("start");
			final long stop = resultSet.getLong("stop");
			final int ipId = IPManagment.getId(resultSet.getString("ip_id"));

			return new Session(id, ipId, playerId, start, stop);
		} catch(final SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<Session> getSessions(final int playerId) {
		final List<Session> sessions = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM session WHERE player_id = " + playerId + ";");
			while(resultSet.next()) {
				sessions.add(getSession(resultSet));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		Collections.reverse(sessions);
		return sessions;
	}

	public static List<Session> getSessionsAt(final long timestamp) {
		final List<Session> sessions = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM session WHERE start > '" + timestamp + "' AND '" + timestamp + "' <= stop;");
			while(resultSet.next()) {
				sessions.add(getSession(resultSet));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return sessions;
	}
}
