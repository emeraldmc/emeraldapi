package fr.tristiisch.emeraldmc.api.spigot.tablist;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class TabListListener implements Listener {

	@EventHandler
	public void PlayerJoinEvent(final PlayerJoinEvent event) {
		TabList.setToAll();
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		TabList.setToAll(1);
	}
}
