package fr.tristiisch.emeraldmc.api.spigot.npc;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Npc {

	public void showNpc(final EmeraldNpc npc, final Player player) {
		npc.show(player);
	}

	public void showNpcToAll(final EmeraldNpc npc) {
		for(final Player player : Bukkit.getOnlinePlayers()) {
			this.showNpc(npc, player);
		}
	}
}
