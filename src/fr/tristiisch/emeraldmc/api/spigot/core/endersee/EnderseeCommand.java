package fr.tristiisch.emeraldmc.api.spigot.core.endersee;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class EnderseeCommand implements CommandExecutor {

	private static int power;

	public static void openEnder(final Player player, final Player target) {
		final OpenEnderChestCommandEvent openEnderChestCommandEvent = new OpenEnderChestCommandEvent(player, target);
		Bukkit.getPluginManager().callEvent(openEnderChestCommandEvent);
		if(openEnderChestCommandEvent.isCancelled()) {
			return;
		}
		player.openInventory(target.getEnderChest());
	}

	@SuppressWarnings("deprecation")
	public static void openEnder(final Player player, String targetName) {
		final OpenEnderChestCommandEvent openEnderChestCommandEvent = new OpenEnderChestCommandEvent(player, targetName);
		Bukkit.getPluginManager().callEvent(openEnderChestCommandEvent);
		if(openEnderChestCommandEvent.isCancelled()) {
			return;
		}
		if(!openEnderChestCommandEvent.isTargetConnected()) {

			final OfflinePlayer targetOffline = Bukkit.getOfflinePlayer(openEnderChestCommandEvent.getTargetName());

			targetName = targetOffline != null ? targetOffline.getName() : targetName;
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.".replaceAll("%player%", targetOffline.getName())));
			return;
		}
		player.openInventory(openEnderChestCommandEvent.getTarget().getEnderChest());
	}

	public static void setOwnEcGroup(final EmeraldGroup group) {
		power = group.getPower();
	}

	private final int power2;

	public EnderseeCommand(final EmeraldGroup group, final EmeraldGroup group2) {
		power = group.getPower();
		this.power2 = group2.getPower();
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final Player player = (Player) sender;
			if(args.length == 0) {
				final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(emeraldPlayer.hasPowerLessThan(power)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}
				openEnder(player, player);
			} else {

				final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(!emeraldPlayer.hasPower(EmeraldGroup.MODERATEURP) && emeraldPlayer.hasPowerLessThan(this.power2)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}

				final String targetName = args[0];
				/* final OfflinePlayer target2 = Bukkit.getOfflinePlayer(args[0]); */
				/*				if(target == null) {
									player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.".replaceAll("%player%", args[0])));
									return;
								}*/

				openEnder(player, targetName);
			}
		});
		return true;
	}
}
