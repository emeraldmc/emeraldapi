package fr.tristiisch.emeraldmc.api.bungee.utils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class BungeeBPMC implements Listener {

	public static Map<String, Long> data = new ConcurrentHashMap<>();
	public static Map<String, Long> data2 = new ConcurrentHashMap<>();

	public static String EmeraldChannel = EmeraldBungee.getName();

	@EventHandler
	public void PluginMessageEvent(final PluginMessageEvent event) {
		final String channel = event.getTag();
		if(channel.equalsIgnoreCase(EmeraldChannel)) {
			final ByteArrayDataInput in = ByteStreams.newDataInput(event.getData());
			final String subchannel = in.readUTF();
			String response;

			switch(subchannel) {

			/*
			 * case "EmeraldPlayer": response = in.readUTF(); final long time =
			 * Utils.getCurrentTimeinSeconds(); if(data2.containsKey(response)) { final long
			 * oldtime = data2.get(response); if(oldtime == time) {
			 * ProxyServer.getInstance().getLogger().log(Level.WARNING,
			 * "§dEmeraldPlayer duplicate reçu de " + response + ". " + time); return; } }
			 * data2.put(response, time); this.clearData(time); final EmeraldPlayer
			 * emeraldPlayer = EmeraldPlayer.fromJson(response);
			 * EmeraldPlayers.replacePlayer(emeraldPlayer);
			 * ProxyServer.getInstance().getLogger().log(Level.WARNING,
			 * "§6EmeraldPlayer reçu (" + emeraldPlayer.getName() + ")."); break; case
			 * "EmeraldPlayers": response = in.readUTF();
			 * EmeraldPlayers.replacePlayers(EmeraldPlayers.fromJson(response)); break;
			 */
			case "AskServersStatus":
				response = in.readUTF();
				/*
				 * ProxyServer.getInstance().getLogger().log(Level.WARNING,
				 * "§bAskServersStatus reçu (" + response + ").");
				 */
				BungeeBPMC.sendServerStatus(ProxyServer.getInstance().getServerInfo(response));
				break;

				/*
				 * case "AskEmeraldPlayer": response = in.readUTF();
				 * ProxyServer.getInstance().getScheduler().schedule(EmeraldBungee.getInstance()
				 * , () -> Utils.getCurrentTimeinSeconds(), 1l, TimeUnit.SECONDS); final long
				 * time2 = Utils.getCurrentTimeinSeconds(); if(data.containsKey(response)) {
				 * final long oldtime = data.get(response); if(oldtime == time2) {
				 * ProxyServer.getInstance().getLogger().log(Level.WARNING,
				 * "§dAskEmeraldPlayer duplicate reçu de " + response + ". " + time2); return; }
				 * } data.put(response, time2); this.clearData(time2); final String[]
				 * responseArray = response.split(","); final UUID uuid =
				 * UUID.fromString(responseArray[1]); final EmeraldPlayer askEmeraldPlayer =
				 * EmeraldPlayers.getPlayer(uuid); System.out.println("§caskEmeraldPlayer" + new
				 * GsonBuilder().setPrettyPrinting().create().toJson(askEmeraldPlayer));
				 * if(askEmeraldPlayer == null) { ProxyServer.getInstance().getPlayer(uuid).
				 * disconnect("§cUne erreur de donnés est survenu. #AskEmeraldPlayer"); return;
				 * } ProxyServer.getInstance().getLogger().log(Level.WARNING,
				 * "§cAskEmeraldPlayer < " + responseArray[0] + " (" +
				 * askEmeraldPlayer.getName() + ")");
				 * BungeeBPMC.sendEmeraldPlayer(ProxyServer.getInstance().getServerInfo(
				 * responseArray[0]), askEmeraldPlayer); break;
				 */
			default:
				response = in.readUTF();
				System.out.println("§cMessage BPMC perdu: " + subchannel + " > " + response);
			}
		} else {
			/*
			 * ProxyServer.getInstance().getLogger().log(Level.WARNING, "§7Msg reçu (" +
			 * event.getTag() + ")");
			 */
		}
	}

	public static void sendToSpigot(final ServerInfo server, final String subchannel, final String msg) {
		final ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(subchannel);
		out.writeUTF(msg);
		server.sendData(EmeraldChannel, out.toByteArray());
	}

	public static void sendToSpigot(final ProxiedPlayer player, final String subchannel, final String msg) {
		final ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(subchannel);
		out.writeUTF(msg);
		player.sendData(EmeraldChannel, out.toByteArray());
	}

	public static void changeServer(final ProxiedPlayer player, final String servername) {
		final ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Connect");
		out.writeUTF(servername);
		player.sendData("BungeeCord", out.toByteArray());
	}

	public static void sendServerStatus(final ServerInfo server, final List<EmeraldServer> emeraldServers) {
		sendToSpigot(server, "ServersStatus", new Gson().toJson(emeraldServers));
	}

	public static void sendServerStatus(final ServerInfo server) {
		sendServerStatus(server, EmeraldServers.getServers());
	}

	public static void sendEmeraldPlayerChange(final ProxiedPlayer player) {
		sendToSpigot(player.getServer().getInfo(), "EmeraldPlayerChange", player.getUniqueId().toString());

	}


	/*
	 * private void clearData(final long time) { final Iterator<Entry<String, Long>>
	 * iter = data.entrySet().iterator(); while(iter.hasNext()) {
	 * if(iter.next().getValue() < time) { iter.remove(); } } final
	 * Iterator<Entry<String, Long>> iter2 = data2.entrySet().iterator();
	 * while(iter2.hasNext()) { if(iter2.next().getValue() < time) { iter2.remove();
	 * } } }
	 */

}