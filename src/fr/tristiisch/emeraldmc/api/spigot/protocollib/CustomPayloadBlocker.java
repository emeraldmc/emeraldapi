package fr.tristiisch.emeraldmc.api.spigot.protocollib;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

import io.netty.buffer.ByteBuf;

/**
 * Processes all custom payload packets. If the packets are too big and sent on certain channels, it will
 * kick the user with a configurable message.
 *
 * @author David Cooke
 */
public class CustomPayloadBlocker extends PacketAdapter {

	/**
	 * The maximum capacity of the buffer in custom payload packets
	 */
	private final int maxCapacity;

	/**
	 * The channels that should be monitored
	 */
	private final List<String> channels;

	/**
	 * The message that should be used to kick the user.
	 */
	private final String message;

	/**
	 * There is a (very small) chance that the user has legitimately sent a very
	 * large packet on the same channel as Jigsaw to avoid kicking users unfairly,
	 * their uuid is added to a list, if they then send a second very large packet,
	 * they will be kicked. This list is cleared 4 times every second.
	 */
	public ArrayList<UUID> uuids = new ArrayList<>();

	/**
	 * Constructs the custom payload monitor. Reads the settings from the config
	 * into variables.
	 *
	 * @param plugin
	 *            instance of AntiJigsaw
	 * @param config
	 *            the configuration of AntiJigsaw
	 */
	public CustomPayloadBlocker(final Plugin plugin, final ConfigurationSection config) {
		super(plugin, PacketType.Play.Client.CUSTOM_PAYLOAD);
		this.maxCapacity = config.getInt("max-size");
		this.channels = config.getStringList("channels");
		this.message = config.getString("message");
	}

	/**
	 * Listens for custom payload packets and evaluates whether they are legitimate.
	 *
	 * @param event
	 *            the event from protocollib
	 */
	@Override
	public void onPacketReceiving(final PacketEvent event) {
		if(this.channels.contains(event.getPacket().getStrings().getValues().get(0))) {
			if(((ByteBuf) event.getPacket().getModifier().getValues().get(1)).capacity() > this.maxCapacity) {
				if(this.uuids.contains(event.getPlayer().getUniqueId())) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, () -> event.getPlayer().kickPlayer(this.message));
				} else {
					this.uuids.add(event.getPlayer().getUniqueId());
				}
				event.setCancelled(true);
			}
		}
	}

}