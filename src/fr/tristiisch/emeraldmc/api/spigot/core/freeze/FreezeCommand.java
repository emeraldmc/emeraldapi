package fr.tristiisch.emeraldmc.api.spigot.core.freeze;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class FreezeCommand implements CommandExecutor {

	private final int power;

	public FreezeCommand(final int power) {
		this.power = power;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String arg, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final Player player = (Player) sender;
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerLessThan(this.power)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}
			Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
				if(args.length == 1) {
					/*
					 * if(!Matcher.IsUsername(args[0])) {
					 * player.sendMessage("&2Freeze &7» &4%player% &cn'est pas un pseudo valide."
					 * .replaceAll("%player%", args[0])); }
					 */
					final Player target = Bukkit.getPlayer(args[0]);
					if(target == null) {
						player.sendMessage("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.".replaceAll("%player%", args[0]));
					}

					if(Freeze.isFreeze(target)) {
						Freeze.unfreeze(target);
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous avez unfreeze &4" + target.getName() + "&c."));
					} else {
						Freeze.freeze(target);
						player.sendMessage(Utils.color("&2EmeraldMC &7» &aVous avez freeze &2" + target.getName() + "&a."));
					}

				} else if(args.length >= 2) {
					final Player target = Bukkit.getPlayer(args[0]);
					if(target == null) {
						player.sendMessage("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.".replaceAll("%player%", args[0]));
					}

					if(Freeze.isFreeze(target)) {
						Freeze.unfreeze(target);
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous avez unfreeze &4" + target.getName() + "&c."));
					} else {
						final String reason = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
						Freeze.freeze(target, reason);
						player.sendMessage(Utils.color("&2EmeraldMC &7» &7Vous avez &cfreeze &8" + target.getName() + "&7 avec comme motif &c" + reason + "."));
					}
				} else {
					player.sendMessage(Utils.color("&cUsage &7» &c/freeze <joueur> [motif]"));
				}
			});
		});
		return true;
	}
}
