package fr.tristiisch.emeraldmc.api.spigot.customevent;

import java.util.List;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;

public class EmeraldServerInfoEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	final private List<EmeraldServer> emeraldServers;

	public EmeraldServerInfoEvent(final List<EmeraldServer> emeraldServers) {
		this.emeraldServers = emeraldServers;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public List<EmeraldServer> getEmeraldServers() {
		return this.emeraldServers;
	}

	public int getEmeraldServersOnline() {
		return EmeraldServers.getOnlineConnected(this.emeraldServers);
	}
}
