package fr.tristiisch.emeraldmc.api.spigot.core.build;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class Build {

	private static List<UUID> players = new ArrayList<>();

	public static boolean isBuildModeEnable(final Player player) {
		return players.contains(player.getUniqueId());
	}

	public static void toggle(final Player player) {
		if(isBuildModeEnable(player)) {
			disable(player);
		} else {
			enable(player);
		}
	}

	public static void enable(final Player player) {
		players.add(player.getUniqueId());
		player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("spigot.commands.build.enabled")));
	}

	public static void disable(final Player player) {
		players.remove(player.getUniqueId());
		player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("spigot.commands.build.disabled")));
	}
}
