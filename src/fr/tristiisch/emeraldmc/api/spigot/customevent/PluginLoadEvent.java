package fr.tristiisch.emeraldmc.api.spigot.customevent;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;

public class PluginLoadEvent extends Event {
	
	public static final HandlerList handlers = new HandlerList();

	final private Plugin plugin;
	final private List<Player> players;
	
	public PluginLoadEvent(Plugin plugin, List<Player> players) {
		this.plugin = plugin;
		this.players = players;
	}

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }
	
    public static HandlerList getHandlerList(){
    	return handlers; 
    }

	public Plugin getPlugin() {
		return plugin;
	}

	public List<Player> getPlayers() {
		return players;
	}
}
