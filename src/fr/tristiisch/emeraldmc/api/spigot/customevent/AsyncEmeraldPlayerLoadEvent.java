package fr.tristiisch.emeraldmc.api.spigot.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;

public class AsyncEmeraldPlayerLoadEvent extends PlayerEvent {
	
	public static final HandlerList handlers = new HandlerList();

	final private OlympaPlayer emeraldPlayer;
	private String joinMessage;
	
	public AsyncEmeraldPlayerLoadEvent(Player who, OlympaPlayer emeraldPlayer) {
		super(who);
		this.emeraldPlayer = emeraldPlayer;
	}

    public OlympaPlayer getEmeraldPlayer() {
		return emeraldPlayer;
	}

	public String getJoinMessage() {
		return joinMessage;
	}
	
	public void setJoinMessage(String joinMessage) {
		this.joinMessage = joinMessage;
	}

	public void setJoinMessageFormat(String joinMessage) {
		this.joinMessage = Utils.color(joinMessage
				.replaceAll("%group%", emeraldPlayer.getGroup().getName())
				.replaceAll("%prefix%", emeraldPlayer.getGroup().getPrefix())
				.replaceAll("%name%", player.getName())
			);
	}

	@Override
    public HandlerList getHandlers(){
        return handlers;
    }
	
    public static HandlerList getHandlerList(){
    	return handlers; 
    }
}
