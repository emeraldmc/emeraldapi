package fr.tristiisch.emeraldmc.api.bungee.staffchat;

import java.util.Arrays;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class StaffChatCommand extends Command {

	public StaffChatCommand(final String command, final String... aliases) {
		super(command, null, aliases);
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
			return;
		}
		final ProxiedPlayer player = (ProxiedPlayer) sender;

		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.getGroup().isStaffMember()) {
				if(args.length == 0) {
					sender.sendMessage(Utils.color("&2EmeraldMC &7» &aMerci de mettre un message"));
					return;
				}
				StaffChat.sendMessage(emeraldPlayer, String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
			}
		});
	}
}
