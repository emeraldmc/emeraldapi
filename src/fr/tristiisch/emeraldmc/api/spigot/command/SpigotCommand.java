package fr.tristiisch.emeraldmc.api.spigot.command;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.chat.TextComponent;

public abstract class SpigotCommand implements CommandExecutor, TabExecutor {

	private final class ReflectCommand extends Command {

		private SpigotCommand exe = null;

		protected ReflectCommand(final String command) {
			super(command);
		}

		@Override
		public boolean execute(final CommandSender sender, final String label, final String[] args) {
			if(this.exe != null) {
				Bukkit.getScheduler().runTaskAsynchronously(this.exe.plugin, () -> {
					this.exe.sender = sender;
					if(sender instanceof Player) {
						this.exe.player = (Player) sender;

						if(this.exe.groups != null && this.exe.groups.length != 0) {
							if(!this.exe.loadEmeraldPlayer()) {
								this.exe.sendImpossibleWithEmeraldPlayer();
								return;
							}

							if(!this.exe.hasPermission()) {
								this.exe.sendDoNotHavePermission();
								return;
							}
						}
					} else if(!this.exe.allowConsole) {
						this.exe.sendImpossibleWithConsole();
						return;
					}

					if(args.length < this.exe.minArg) {
						this.exe.sendUsage();
						return;
					}
					this.exe.onCommand(sender, this, label, args);

				});
			}
			return true;
		}

		public void setExecutor(final SpigotCommand exe) {
			this.exe = exe;
		}

		@Override
		public List<String> tabComplete(final CommandSender sender, final String alais, final String[] args) {
			if(this.exe != null) {
				return this.exe.onTabComplete(sender, this, alais, args);
			}
			return null;
		}
	}

	protected static CommandMap cmap;
	protected List<String> alias;
	public boolean allowConsole = true;
	protected final String command;

	protected String description;

	public OlympaPlayer emeraldPlayer;
	protected EmeraldGroup[] groups;

	/**
	 * Don't foget to set {@link SpigotCommand#usageString}
	 */
	public Integer minArg = 0;

	public Player player;
	protected final Plugin plugin;
	private CommandSender sender;

	/**
	 * Format: Usage » %command% <%obligatory%|%obligatory%> [%optional%]
	 * Variable name: 'joueur' ...
	 *
	 */
	public String usageString;

	public SpigotCommand(final Plugin plugin, final String command, final EmeraldGroup... groups) {
		this.plugin = plugin;
		this.command = command;
		this.groups = groups;
	}

	public SpigotCommand(final Plugin plugin, final String command, final List<String> alias, final EmeraldGroup... groups) {
		this.plugin = plugin;
		this.command = command;
		this.alias = alias;
		this.groups = groups;
	}

	public SpigotCommand(final Plugin plugin, final String command, final List<String> alias, final String description, final EmeraldGroup[] groups) {
		this.plugin = plugin;
		this.groups = groups;
		this.command = command;
		this.description = description;
		this.alias = alias;
	}

	public SpigotCommand(final Plugin plugin, final String command, final String... alias) {
		this.plugin = plugin;
		this.command = command;
		this.alias = Arrays.asList(alias);
	}

	public void broadcast(final String message) {
		Bukkit.broadcastMessage(message);
	}

	public String buildText(final int min, final String[] args) {
		return String.join(" ", Arrays.copyOfRange(args, min, args.length));
	}

	final CommandMap getCommandMap() {
		if(cmap == null) {
			try {
				final Field f = Bukkit.getServer().getClass().getDeclaredField("commandMap");
				f.setAccessible(true);
				cmap = (CommandMap) f.get(Bukkit.getServer());
				f.setAccessible(false);
			} catch(final Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return cmap;
	}

	public OlympaPlayer getEmeraldPlayer() {
		return this.emeraldPlayer;
	}

	public Player getPlayer() {
		return this.player;
	}

	public boolean hasPermission(EmeraldGroup... groups) {
		if(this.player == null) {
			return true;
		}

		if(groups.length == 0) {
			groups = this.groups;
		}

		if(this.emeraldPlayer == null) {
			this.loadEmeraldPlayer();
		}

		if(groups.length == 1) {
			return this.emeraldPlayer.hasPowerMoreThan(groups[0]);
		} else {
			return this.emeraldPlayer.hasPower(groups);
		}

	}

	public boolean loadEmeraldPlayer() {
		this.emeraldPlayer = new AccountProvider(this.player.getUniqueId()).getEmeraldPlayer();
		return this.emeraldPlayer != null;
	}

	@Override
	public abstract boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args);

	@Override
	public List<String> onTabComplete(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		return null;
	}

	public void register() {
		final ReflectCommand reflectCommand = new ReflectCommand(this.command);
		if(this.alias != null) {
			reflectCommand.setAliases(this.alias);
		}
		if(this.description != null) {
			reflectCommand.setDescription(this.description);
		}
		this.getCommandMap().register("", reflectCommand);
		reflectCommand.setExecutor(this);
	}

	public void sendDoNotHavePermission() {
		this.sendErreur("Vous n'avez pas la permission &l(◑_◑)");
	}

	public void sendErreur(final String message) {
		this.sendMessage(Prefix.DEFAULT_BAD, message);
	}

	public void sendImpossibleWithConsole() {
		this.sendErreur("Impossible avec la console.");
	}

	public void sendImpossibleWithEmeraldPlayer() {
		this.sendErreur("Une erreur est survenu avec vos donnés.");
	}

	public void sendMessage(final CommandSender sender, final Prefix prefix, final String text) {
		this.sendMessage(sender, prefix + Utils.color(text));
	}

	public void sendMessage(final CommandSender sender, final String text) {
		sender.sendMessage(Utils.color(text));
	}

	public void sendMessage(final Prefix prefix, final String text) {
		this.sendMessage(this.sender, prefix, text);
	}

	public void sendMessage(final String text) {
		this.sendMessage(this.sender, Utils.color(text));
	}

	public void sendMessage(final TextComponent text) {
		this.player.spigot().sendMessage(text);
	}

	public void sendUnknownPlayer(final String name) {
		this.sendErreur("Le joueur &4" + name + "&c est introuvable.");
		// TODO check historique player
	}

	public void sendUsage() {
		this.sendMessage(Prefix.USAGE, this.usageString);
	}
}
