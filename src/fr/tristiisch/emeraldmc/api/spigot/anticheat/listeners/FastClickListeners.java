package fr.tristiisch.emeraldmc.api.spigot.anticheat.listeners;

import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import fr.tristiisch.emeraldmc.api.spigot.anticheat.PlayerCheat;

public class FastClickListeners implements Listener {

	@EventHandler
	public void onClick(final PlayerInteractEvent event) {
		if(event.getAction() == Action.LEFT_CLICK_AIR) {
			final Player player = event.getPlayer();
			final PlayerCheat pc = PlayerCheat.getByPlayer(player);
			pc.click++;
			if(player.getTargetBlock((Set<Material>) null, 100).getLocation().distance(player.getLocation()) < 6.0 && pc.lastBlockInteraction > System.currentTimeMillis() && pc.click >= 10) {
				/*
				 * player.
				 * sendMessage("§cVerif: Action Impossible, merci de le signaler à §lTristiisch"
				 * );
				 */
				System.out.println("[DEBUG] " + player.getName() + ": Action impossible");
				event.setCancelled(true);
			}
			if(pc.click > pc.maxCPS) {
				pc.maxCPS = pc.click;
			}
			if(pc.click >= 18) {
				event.setCancelled(true);
			}
		} else if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
			final Player player = event.getPlayer();
			final PlayerCheat pc = PlayerCheat.getByPlayer(player);
			pc.lastBlockInteraction = System.currentTimeMillis() + 5000L;
		}
	}

	@EventHandler
	public void onInventoryClick(final InventoryClickEvent event) {
		if(event.getInventory().getTitle().startsWith(ChatColor.translateAlternateColorCodes('&', PlayerCheat.InventoryName))) {
			if(event.getClickedInventory() == null) {
				event.getWhoClicked().closeInventory();
				return;
			}
			/*        	HumanEntity player = event.getWhoClicked();
			if(!EmeraldPlayer.getPlayer(player.getName()).hasPowerMoreThan(75)) {
				event.setCancelled(true);
				return;
			}
			
			if(event.getRawSlot() >= PlayerClick.slotInv && event.getRawSlot() < PlayerClick.slotInv + 25) {
				// inv
				PlayerClick targetClick = PlayerClick.verifier.get(player);
				targetClick.maxCPS = 50;
				Player target =  targetClick.getPlayer();
				Inventory inventory = event.getInventory();
				Inventory newInventory = target.getInventory();
			
				for(int i = 0; 9 > i; i++) {
					newInventory.setItem(i, inventory.getItem(PlayerClick.slotHotBar + i));
				}
				for(int i = 0; 3*9 > i; i++) {
					newInventory.setItem(i + 9, inventory.getItem(PlayerClick.slotInv + i));
				}
				System.out.println("ok: " + Arrays.toString(newInventory.getContents()));
				target.getInventory().setContents(newInventory.getContents());
				targetInv.setContents(inventory.getContents());
				System.out.println("ok2: " + Arrays.toString(target.getInventory().getContents()));
			
				player.sendMessage("Inv");
			
			
			} else if(event.getRawSlot() >= PlayerClick.slotHotBar && event.getRawSlot() < PlayerClick.slotHotBar + 9) {
				// hotbar
				player.sendMessage("Hotbar");
			} else if(event.getRawSlot() >= PlayerClick.slotArmor && event.getRawSlot() < PlayerClick.slotArmor + 4) {
				// armor
				player.sendMessage("Armor");
			}*/
			event.setCancelled(true);
		}

	}

}
