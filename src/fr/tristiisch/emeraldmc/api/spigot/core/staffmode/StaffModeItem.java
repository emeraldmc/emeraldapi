package fr.tristiisch.emeraldmc.api.spigot.core.staffmode;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.items.ItemEnchant;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.utils.NBTEditor;

public enum StaffModeItem {

	BOULET(ItemTools.create(Material.FIREWORK_CHARGE, 1, (short) 0, "&2Boulet de forcat", "", "&7Marche doucement")),

	KB(ItemTools.create(Material.STICK, 1, "&2Baton de KB", Arrays.asList("", "&7Test le KB des joueurs"), new ItemEnchant(Enchantment.KNOCKBACK, 2))),
	MisterFreeze(ItemTools.create(Material.BLAZE_ROD, 1, (short) 0, "&2Mister freeze", "", "&7Freeze le joueur que tu vise")),
	Navette(ItemTools.create(Material.MINECART, 1, (short) 0, "&2Navette", "", "&7Téléporte où tu vise")),
	SevenLeagueBoots(ItemTools.create(Material.LEATHER_BOOTS, 1, (short) 0, "&2AirMax", "", "&7Cours plus vite")),

	Teleportation(ItemTools.create(Material.COMPASS, 1, (short) 0, "&2Téléportateur", "", "&7Téléporte aléatoirement aux joueurs")),
	Verificator(ItemTools.create(Material.CHEST, 1, (short) 0, "&2Vérificateur", "", "&7Vérifie les CPS des joueurs"));

	static {
		SevenLeagueBoots.setItemStack(NBTEditor.setItemTag(SevenLeagueBoots.getItemStack(), "generic.movementSpeed", "AttributeModifiers", null, "AttributeName"));
		SevenLeagueBoots.setItemStack(NBTEditor.setItemTag(SevenLeagueBoots.getItemStack(), "generic.movementSpeed", "AttributeModifiers", 0, "Name"));
		SevenLeagueBoots.setItemStack(NBTEditor.setItemTag(SevenLeagueBoots.getItemStack(), "mainhand", "AttributeModifiers", 0, "Slot"));
		SevenLeagueBoots.setItemStack(NBTEditor.setItemTag(SevenLeagueBoots.getItemStack(), 0.75, "AttributeModifiers", 0, "Amount"));
		SevenLeagueBoots.setItemStack(NBTEditor.setItemTag(SevenLeagueBoots.getItemStack(), 1, "AttributeModifiers", 0, "Operation"));
		SevenLeagueBoots.setItemStack(NBTEditor.setItemTag(SevenLeagueBoots.getItemStack(), 894654L, "AttributeModifiers", 0, "UUIDLeast"));
		SevenLeagueBoots.setItemStack(NBTEditor.setItemTag(SevenLeagueBoots.getItemStack(), 2872L, "AttributeModifiers", 0, "UUIDMost"));

		BOULET.setItemStack(NBTEditor.setItemTag(BOULET.getItemStack(), "generic.movementSpeed", "AttributeModifiers", null, "AttributeName"));
		BOULET.setItemStack(NBTEditor.setItemTag(BOULET.getItemStack(), "generic.movementSpeed", "AttributeModifiers", 0, "Name"));
		BOULET.setItemStack(NBTEditor.setItemTag(BOULET.getItemStack(), "mainhand", "AttributeModifiers", 0, "Slot"));
		BOULET.setItemStack(NBTEditor.setItemTag(BOULET.getItemStack(), -0.25, "AttributeModifiers", 0, "Amount"));
		BOULET.setItemStack(NBTEditor.setItemTag(BOULET.getItemStack(), 1, "AttributeModifiers", 0, "Operation"));
		BOULET.setItemStack(NBTEditor.setItemTag(BOULET.getItemStack(), 894654L, "AttributeModifiers", 0, "UUIDLeast"));
		BOULET.setItemStack(NBTEditor.setItemTag(BOULET.getItemStack(), 2872L, "AttributeModifiers", 0, "UUIDMost"));
	}

	private ItemStack itemStack;

	StaffModeItem(final ItemStack itemStack) {
		this.itemStack = itemStack;
	}

	public ItemStack getItemStack() {
		return this.itemStack;
	}

	public void setItemStack(final ItemStack itemStack) {
		this.itemStack = itemStack;
	}

}
