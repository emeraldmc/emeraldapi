package fr.tristiisch.emeraldmc.api.commons;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;

public class EmeraldServers {

	private static List<EmeraldServer> servers = new ArrayList<>();

	public static void addServer(final EmeraldServer serveur) {
		servers.add(serveur);
	}

	public static boolean canAccess(final OlympaPlayer emeraldPlayer, final EmeraldServer emeraldServer) {
		return !emeraldServer.getStatus().isOpen();
	}

	public static boolean contains(final String name) {
		return servers.stream().filter(Sname -> Sname.getName().equalsIgnoreCase(name)).findFirst().isPresent();
	}

	public static List<EmeraldServer> getLobbysServers() {
		return servers.stream().filter(server -> server.getName().startsWith("lobby")).collect(Collectors.toList());
	}

	public static int getOnlineConnected() {
		return getOnlineConnected(servers);
	}

	public static int getOnlineConnected(final List<EmeraldServer> servers) {
		return servers.stream().mapToInt(server -> server.getInfo() != null ? server.getInfo().getPlayers().getOnline() : 0).sum();
	}

	public static EmeraldServer getServer(final String serverName) {
		return servers.stream().filter(Sname -> Sname.getName().equalsIgnoreCase(serverName)).findFirst().orElse(null);
	}

	public static int getServerNumber(final String serverName) {
		if(!contains(serverName)) {
			return 0;
		}
		return Integer.parseInt(serverName.replaceAll("[^0-9]+", ""));
	}

	public static List<EmeraldServer> getServers() {
		return servers;
	}

	public static void removeServer(final EmeraldServer serveur) {
		servers.remove(serveur);
	}

	public static void setServers(final List<EmeraldServer> serveurs) {
		servers = serveurs;
	}
}
