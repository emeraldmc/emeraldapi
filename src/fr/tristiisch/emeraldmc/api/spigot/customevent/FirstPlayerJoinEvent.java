package fr.tristiisch.emeraldmc.api.spigot.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class FirstPlayerJoinEvent extends PlayerEvent {
	
	public static final HandlerList handlers = new HandlerList();

	private PlayerJoinEvent playerJoinEvent;
	
	public FirstPlayerJoinEvent(Player who, PlayerJoinEvent playerJoinEvent) {
		super(who);
		this.playerJoinEvent = playerJoinEvent;
	}

	public PlayerJoinEvent getPlayerJoinEvent() {
		return playerJoinEvent;
	}

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }
	
    public static HandlerList getHandlerList(){
    	return handlers; 
    }
}
