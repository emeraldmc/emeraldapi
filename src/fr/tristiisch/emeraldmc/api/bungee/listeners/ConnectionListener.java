package fr.tristiisch.emeraldmc.api.bungee.listeners;

import java.util.UUID;
import java.util.logging.Level;

import fr.tristiisch.emeraldmc.api.bungee.lobby.Lobby;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldConsole;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

@SuppressWarnings("deprecation")
public class ConnectionListener implements Listener {

	@EventHandler
	public void LoginEvent(final LoginEvent event) {
		if(event.isCancelled()) {
			return;
		}

		final UUID playerUUID = event.getConnection().getUniqueId();
		final String playername = event.getConnection().getName();
		// Vérifie si le joueur n'utilise pas le joueur console.
		if(playerUUID == EmeraldConsole.getUniqueId() || playername.equalsIgnoreCase(EmeraldConsole.getName())) {
			event.setCancelReason(BungeeUtils.connectScreen("&cImpossible de se connecter avec ce compte."));
			event.setCancelled(true);
			return;

		}
	}

	@EventHandler
	public void PreLoginEvent(final PreLoginEvent event) {
		if(event.isCancelled()) {
			return;
		}

		final String playername = event.getConnection().getName();
		final byte status = BungeeConfigUtils.getConfig("maintenance").getByte("settings.status");

		// Vérifie si le serveur n'est pas en maintenance
		if(status == 1) {
			if(!BungeeConfigUtils.getConfig("maintenance").getStringList("whitelist").contains(playername)) {
				if(BungeeConfigUtils.getConfig("maintenance").getString("settings.message") == "") {
					event.setCancelReason(BungeeUtils.connectScreen("&cLe serveur est actuellement en maintenance."));
				} else {
					event.setCancelReason(
							BungeeUtils.connectScreen("&cLe serveur est actuellement en maintenance.\n\n&c&nRaison:&c " + BungeeConfigUtils.getConfig("maintenance").getString("settings.message")));
				}
				event.setCancelled(true);
				ProxyServer.getInstance().getLogger().log(Level.INFO, Utils.color("&d" + event.getConnection().getName() + " ne peux pas se connecter (serveur en maintenance)"));
				return;
			}

		} else if(status == 2) {
			final String playerName = event.getConnection().getName();
			if(!BungeeConfigUtils.getConfig("maintenance").getStringList("whitelist").contains(playerName)) {
				event.setCancelReason(BungeeUtils.connectScreen("&cLe serveur est actuellement en développement.\n\n&aPlus d'infos sur le twitter &n@EmeraldMC_FR"));
				event.setCancelled(true);
				ProxyServer.getInstance().getLogger().log(Level.INFO, Utils.color("&d" + event.getConnection().getName() + " ne peux pas se connecter (serveur en dev)"));
				return;
			}
		}

		// Vérifie si l'ip est correct
		/*		if(!Utils.getAfterFirst(event.getConnection().getVirtualHost().getHostName(), ".").equalsIgnoreCase("emeraldmc.fr")) {
					event.setCancelReason(BungeeUtils.connectScreen("&cMerci de vous connecter avec l'adresse &nplay.emeraldmc.fr"));
					event.setCancelled(true);
					return;
				}*/

		// Vérifie si le joueur n'est pas déjà connecté
		final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(playername);
		if(target != null) {
			if(!target.getAddress().getAddress().getHostAddress().equals(event.getConnection().getAddress().getAddress().getHostAddress())) {
				event.setCancelReason(BungeeUtils.connectScreen("&cVotre compte est déjà connecté avec une IP différente de la votre."));
				event.setCancelled(true);
				return;
			}
			event.setCancelReason(BungeeUtils.connectScreen("&cVotre compte est déjà connecté."));
		}
	}

	@EventHandler
	public void ServerConnectEvent(final ServerConnectEvent event) {
		final ProxiedPlayer player = event.getPlayer();
		final ServerInfo serverTarget = event.getTarget();

		serverTarget.getPlayers().stream().filter(p -> player.getUniqueId().equals(p.getUniqueId())).forEach(p -> p.getPendingConnection().disconnect());

	}

	@EventHandler
	public void ServerKickEvent(final ServerKickEvent event) {
		final String kickReason = ChatColor.stripColor(BaseComponent.toLegacyText(event.getKickReasonComponent()));
		if(kickReason.equals("Votre serveur s'est redémarré...")) {
			final EmeraldServer lobby = Lobby.getLobby();
			if(lobby == null) {
				return;
			}
			final ServerInfo serverInfolobby = BungeeUtils.getServerInfo(lobby);
			final ServerInfo server = event.getKickedFrom();

			if(serverInfolobby.getName().equals(server.getName())) {
				event.setKickReason(Utils.color("&eLe &6" + Utils.capitalize(server.getName()) + "&e s'est redémarré, merci de vous reconnecter."));
				return;
			}
			event.setCancelServer(serverInfolobby);
			event.setCancelled(true);
			final ProxiedPlayer player = event.getPlayer();

			player.sendMessage(Utils.color("&2EmeraldMC &7» &eLe &6" + Utils.capitalize(server.getName()) + "&e s'est redémarré, vous êtes désormais au " + Utils.capitalize(lobby.getName()) + "."));
			return;
		} else if(BaseComponent.toLegacyText(event.getKickReasonComponent()).contains(Utils.color("&7[&2EmeraldMC&7]"))) {
			final EmeraldServer lobby = Lobby.getLobby();
			if(lobby == null) {
				return;
			}
			final ServerInfo serverInfolobby = BungeeUtils.getServerInfo(lobby);
			final ServerInfo server = event.getKickedFrom();

			if(serverInfolobby.getName().equals(server.getName())) {
				return;
			}
			event.setCancelServer(serverInfolobby);
			event.setCancelled(true);
		}
	}
}
