package fr.tristiisch.emeraldmc.api.commons.object;

import java.util.ArrayList;
import java.util.List;

public enum EmeraldPermission {

	COMMAND_BAN(EmeraldGroup.GUIDE);

	private final List<Integer> power = new ArrayList<>();

	private EmeraldPermission(final EmeraldGroup emeraldGroup) {
		this.power.add(emeraldGroup.getPower());
	}
}
