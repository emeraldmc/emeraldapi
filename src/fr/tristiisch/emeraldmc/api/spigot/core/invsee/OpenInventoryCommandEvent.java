package fr.tristiisch.emeraldmc.api.spigot.core.invsee;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class OpenInventoryCommandEvent extends PlayerEvent implements Cancellable {

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	private boolean cancel = false;
	private Player target;

	private final String targetName;

	public OpenInventoryCommandEvent(final Player who, final Player target) {
		super(who);
		this.target = target;
		this.targetName = target.getName();
	}

	public OpenInventoryCommandEvent(final Player who, final String targetName) {
		super(who);
		this.target = null;
		this.targetName = targetName;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public Player getTarget() {
		return this.target;
	}

	public String getTargetName() {
		return this.targetName;
	}

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	public boolean isTargetConnected() {
		if(this.target == null && this.targetName != null) {
			this.target = Bukkit.getPlayer(this.targetName);
		}
		return this.target != null;
	}

	@Override
	public void setCancelled(final boolean cancel) {
		this.cancel = cancel;
	}
}
