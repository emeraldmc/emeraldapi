package fr.tristiisch.emeraldmc.api.spigot;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.RedisAccess;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.DatabaseManager;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.spigot.anticheat.PlayerCheatListener;
import fr.tristiisch.emeraldmc.api.spigot.anticheat.listeners.FastClickListeners;
import fr.tristiisch.emeraldmc.api.spigot.anticheat.scheduler.FastClickRunnable;
import fr.tristiisch.emeraldmc.api.spigot.console.ConsoleFormat;
import fr.tristiisch.emeraldmc.api.spigot.core.boutique.BoutiqueCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.boutique.BoutiqueListener;
import fr.tristiisch.emeraldmc.api.spigot.core.build.BuildCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.build.BuildListener;
import fr.tristiisch.emeraldmc.api.spigot.core.clear.ClearCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.ColorCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.ForceKickSpigotCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.GamemodeCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.HatCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.HealCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.LagCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.SecretCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.StatutCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.spawn.SpawnCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.spawn.SpawnListener;
import fr.tristiisch.emeraldmc.api.spigot.core.endersee.EnderseeCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.fly.FlyCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.freeze.FreezeCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.freeze.FreezeListener;
import fr.tristiisch.emeraldmc.api.spigot.core.invsee.InvseeCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.listeners.CommandBlockerListener;
import fr.tristiisch.emeraldmc.api.spigot.core.listeners.RespawnListener;
import fr.tristiisch.emeraldmc.api.spigot.core.listeners.WeatherListener;
import fr.tristiisch.emeraldmc.api.spigot.core.staffmode.StaffModeCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.staffmode.listeners.StaffModeListener;
import fr.tristiisch.emeraldmc.api.spigot.core.staffmode.listeners.StaffModeRestrictListener;
import fr.tristiisch.emeraldmc.api.spigot.core.tchat.TchatCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.tchat.TchatListener;
import fr.tristiisch.emeraldmc.api.spigot.core.teleport.TeleportCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.vanish.VanishCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.vanish.VanishListener;
import fr.tristiisch.emeraldmc.api.spigot.core.verif.AlertCPSCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.verif.VerifCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.verif.VerifRunnable;
import fr.tristiisch.emeraldmc.api.spigot.customevent.listener.CustomEventsListener;
import fr.tristiisch.emeraldmc.api.spigot.customevent.playerchatjson.AsyncPlayerChatJsonEventListener;
import fr.tristiisch.emeraldmc.api.spigot.datamanagment.DataManagmentListener;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiListener;
import fr.tristiisch.emeraldmc.api.spigot.infogroup.InfoGroupCommand;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemGlow;
import fr.tristiisch.emeraldmc.api.spigot.motd.MotdListener;
import fr.tristiisch.emeraldmc.api.spigot.nick.NickCommand;
import fr.tristiisch.emeraldmc.api.spigot.protocollib.CancelTabComplete;
import fr.tristiisch.emeraldmc.api.spigot.protocollib.JigsawCrashFix;
import fr.tristiisch.emeraldmc.api.spigot.sneak.SneakListener;
import fr.tristiisch.emeraldmc.api.spigot.tablist.TabListListener;
import fr.tristiisch.emeraldmc.api.spigot.utils.CustomConfigUtils;
import fr.tristiisch.emeraldmc.api.spigot.utils.EmeraldServerSettings;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotBPMC;

public class EmeraldSpigot extends JavaPlugin {

	private static EmeraldSpigot instance;
	private static EmeraldServerSettings serverSettings;
	private static Location spawn;

	private final static long uptime = Utils.getCurrentTimeinSeconds();

	public static EmeraldSpigot getInstance() {
		return instance;
	}

	public static String getServerName() {
		return "EmeraldMC";
	}

	public static EmeraldServerSettings getServerSettings() {
		return serverSettings;
	}

	public static Location getSpawn() {
		return spawn;
	}

	public static long getUptime() {
		return uptime;
	}

	public static void setSpawn(final Location spawn) {
		EmeraldSpigot.spawn = spawn;
	}

	public static void setSpawnWorld(final World world) {
		EmeraldSpigot.spawn.setWorld(world);
	}

	@Override
	public void onDisable() {
		/* GhostUtils.ghostFactory.close(); */
		RedisAccess.close();
		DatabaseManager.close();
		this.getServer().getMessenger().unregisterIncomingPluginChannel(this);
		this.getServer().getMessenger().unregisterOutgoingPluginChannel(this);

		this.getServer().getScoreboardManager().getMainScoreboard().getTeams().stream().forEach(team -> team.unregister());
		ConsoleFormat.disable();
		final ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(Utils.color("&c" + this.getDescription().getName() + " (V" + this.getDescription().getVersion() + ") &cest désactivé."));
	}

	/*	«	Alt + 0171
		»	Alt + 0187*/
	@Override
	public void onEnable() { 
		try {
			Class.forName("org.spigotmc.SpigotConfig");
		} catch(final ClassNotFoundException e) {
			System.out.println("§cLe server doit utiliser Spigot pour que le plugin fonctionne. https://getbukkit.org/download/spigot");
			Bukkit.getPluginManager().disablePlugin(this);
		}
		instance = this;

		DatabaseManager.connect();
		RedisAccess.init();

		// Charge la config
		this.saveDefaultConfig();
		CustomConfigUtils.createIfDoesNotExist(this, "data");

		if(this.getConfig().getBoolean("spigot.settings.disablemobs")) {
			for(final World world : this.getServer().getWorlds()) {
				if(!world.getGameRuleValue("doMobSpawning").equalsIgnoreCase("false")) {
					world.setGameRuleValue("doMobSpawning", "false");
				}
			}
		}

		// Gestion Events
		final PluginManager pluginmanager = this.getServer().getPluginManager();
		pluginmanager.registerEvents(new DataManagmentListener(), this);
		pluginmanager.registerEvents(new GuiListener(), this);
		pluginmanager.registerEvents(new RespawnListener(), this);
		pluginmanager.registerEvents(new CommandBlockerListener(), this);
		if(this.getConfig().getBoolean("spigot.settings.protect")) {
			pluginmanager.registerEvents(new BuildListener(), this);
		}
		if(this.getConfig().getBoolean("spigot.settings.disableweather")) {
			pluginmanager.registerEvents(new WeatherListener(), this);
		}
		pluginmanager.registerEvents(new FastClickListeners(), this);
		pluginmanager.registerEvents(new MotdListener(), this);
		pluginmanager.registerEvents(new CustomEventsListener(), this);
		pluginmanager.registerEvents(new StaffModeListener(), this);
		pluginmanager.registerEvents(new StaffModeRestrictListener(), this);
		pluginmanager.registerEvents(new FreezeListener(), this);
		pluginmanager.registerEvents(new SpawnListener(), this);
		pluginmanager.registerEvents(new VanishListener(), this);
		pluginmanager.registerEvents(new PlayerCheatListener(), this);
		pluginmanager.registerEvents(new TchatListener(), this);
		pluginmanager.registerEvents(new AsyncPlayerChatJsonEventListener(), this);
		pluginmanager.registerEvents(new SneakListener(), this);
		pluginmanager.registerEvents(new TabListListener(), this);
		pluginmanager.registerEvents(new BoutiqueListener(), this);

		// Gestion Commands
		this.getCommand("build").setExecutor(new BuildCommand(45, 65, 75));
		this.getCommand("color").setExecutor(new ColorCommand());
		this.getCommand("verif").setExecutor(new VerifCommand(55));
		this.getCommand("alertcps").setExecutor(new AlertCPSCommand(55));
		this.getCommand("spawn").setExecutor(new SpawnCommand());
		this.getCommand("lag").setExecutor(new LagCommand());
		this.getCommand("vanish").setExecutor(new VanishCommand(EmeraldGroup.RESPBUILD));
		this.getCommand("staffmode").setExecutor(new StaffModeCommand(55));
		this.getCommand("freeze").setExecutor(new FreezeCommand(55));
		this.getCommand("gamemode").setExecutor(new GamemodeCommand(EmeraldGroup.ADMIN, EmeraldGroup.RESPMODO));
		this.getCommand("secret").setExecutor(new SecretCommand());
		this.getCommand("clear").setExecutor(new ClearCommand(EmeraldGroup.RESPMODO));
		this.getCommand("fly").setExecutor(new FlyCommand(EmeraldGroup.RESPMODO));
		this.getCommand("teleport").setExecutor(new TeleportCommand(EmeraldGroup.MODERATEUR));
		this.getCommand("invsee").setExecutor(new InvseeCommand(EmeraldGroup.RESPMODO));
		this.getCommand("endersee").setExecutor(new EnderseeCommand(EmeraldGroup.RESPMODO, EmeraldGroup.RESPMODO));
		this.getCommand("nick").setExecutor(new NickCommand(EmeraldGroup.RESPMODO));
		this.getCommand("statut").setExecutor(new StatutCommand(EmeraldGroup.ADMIN.getPower()));
		this.getCommand("chat").setExecutor(new TchatCommand());
		this.getCommand("forcekickspigot").setExecutor(new ForceKickSpigotCommand());
		this.getCommand("infogroup").setExecutor(new InfoGroupCommand());
		this.getCommand("heal").setExecutor(new HealCommand(EmeraldGroup.ADMIN));
		this.getCommand("hat").setExecutor(new HatCommand(EmeraldGroup.DEVELOPPEUR));
		new BoutiqueCommand(this);

		// Gestion Bungee Plugin Messaging Channel
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, SpigotBPMC.DefaultChannel);
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, SpigotBPMC.EmeraldChannel);
		this.getServer().getMessenger().registerIncomingPluginChannel(this, SpigotBPMC.EmeraldChannel, new SpigotBPMC());

		// Gestion tasks
		// 1 sec
		new FastClickRunnable().runTaskTimerAsynchronously(this, 0, 20L);
		// 0.1 sec
		new VerifRunnable().runTaskTimerAsynchronously(this, 0, 2L);

		// Fix Jigsaw Crash
		try {
			Class.forName("com.comphenix.protocol.events.PacketListener");
			JigsawCrashFix.enable(this);
			CancelTabComplete.enable();
			//new CustomPayloadBlocker(plugin, config);
		} catch(final ClassNotFoundException e) {
			Bukkit.getConsoleSender().sendMessage(Utils.color("&4ATTENTION: &cSans ProtocolLib, les joueurs peuvent faire crash le serveur avec le mod Jigsaw."));
		}

		serverSettings = new EmeraldServerSettings();

		// API
		try {
			final String[] s = this.getConfig().getString("spigot.settings.spawn").split(",");
			spawn = new Location(Bukkit.getWorld(s[0]), Double.parseDouble(s[1]), Double.parseDouble(s[2]), Double.parseDouble(s[3]), Float.parseFloat(s[4]), Float.parseFloat(s[5]));
		} catch(final Exception e) {
			e.printStackTrace();
		}
		ItemGlow.getGlowEnchant();

		final ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(Utils.color("&6=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
		console.sendMessage(Utils.color(""));
		console.sendMessage(Utils.color("&a              " + this.getDescription().getName()));
		console.sendMessage(Utils.color("&3             Version " + this.getDescription().getVersion()));
		console.sendMessage(Utils.color(""));
		console.sendMessage(Utils.color("&6=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
	}

	@Override
	public void onLoad() {
		ConsoleFormat.enable();
	}
}
