package fr.tristiisch.emeraldmc.api.spigot.items;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.utils.NBTEditor;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection.ClassEnum;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class ItemTools {

	public static void addGlow(final ItemStack item) {
		final Enchantment glow = ItemGlow.getGlowEnchant();
		if(!item.containsEnchantment(glow)) {
			item.addEnchantment(glow, 1);
		}
	}

	public static String convertItemStackToJson(final ItemStack itemStack) {
		// ItemStack methods to get a net.minecraft.server.ItemStack object for serialization
		final Class<?> craftItemStackClazz = Reflection.getClass(ClassEnum.CB, "inventory.CraftItemStack");
		final Method asNMSCopyMethod = Reflection.makeMethod(craftItemStackClazz, "asNMSCopy", ItemStack.class);

		// NMS Method to serialize a net.minecraft.server.ItemStack to a valid Json string
		final Class<?> nmsItemStackClazz = Reflection.getClass(ClassEnum.NMS, "ItemStack");
		final Class<?> nbtTagCompoundClazz = Reflection.getClass(ClassEnum.NMS, "NBTTagCompound");
		final Method saveNmsItemStackMethod = Reflection.makeMethod(nmsItemStackClazz, "save", nbtTagCompoundClazz);

		Object nmsNbtTagCompoundObj; // This will just be an empty NBTTagCompound instance to invoke the saveNms method
		Object nmsItemStackObj; // This is the net.minecraft.server.ItemStack object received from the asNMSCopy method

		Object itemAsJsonObject; // This is the net.minecraft.server.ItemStack after being put through saveNmsItem method

		try {
			nmsNbtTagCompoundObj = nbtTagCompoundClazz.newInstance();
			nmsItemStackObj = asNMSCopyMethod.invoke(null, itemStack);
			itemAsJsonObject = saveNmsItemStackMethod.invoke(nmsItemStackObj, nmsNbtTagCompoundObj);
		} catch(final Throwable t) {
			Bukkit.getLogger().log(Level.SEVERE, "failed to serialize itemstack to nms item", t);
			return null;
		}

		// Return a string representation of the serialized object
		return itemAsJsonObject.toString();
	}

	public static ItemStack create(final Material mat) {
		return create(mat, 1, (short) 0, null, (List<String>) null, (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final int size) {
		return create(mat, size, (short) 0, null, (List<String>) null, (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final int size, final short dataValue) {
		return create(mat, size, dataValue, null, (List<String>) null, (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final int size, final short dataValue, final List<String> lore) {
		return create(mat, size, dataValue, null, lore, (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final int size, final short dataValue, final String name) {
		return create(mat, size, dataValue, name, (List<String>) null, (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final int size, final short dataValue, final String name, final List<String> lore) {
		return create(mat, size, dataValue, name, lore, (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final int size, final short dataValue, final String name, final List<String> lore, final List<ItemEnchant> enchantements) {
		final ItemStack itemStack = new ItemStack(mat, size, dataValue);
		final ItemMeta itemMeta = itemStack.getItemMeta();
		if(name != null) {
			itemMeta.setDisplayName(Utils.color(name));
		}

		if(lore != null) {
			itemMeta.setLore(Utils.color(lore));
		}

		if(enchantements != null && !enchantements.isEmpty()) {
			for(final ItemEnchant enchantement : enchantements) {
				itemMeta.addEnchant(enchantement.getEnchantment(), enchantement.getLevel(), enchantement.isUnsafe());
			}
		}
		itemStack.setItemMeta(itemMeta);

		return itemStack;
	}

	public static ItemStack create(final Material mat, final int size, final short dataValue, final String name, final String... lore) {
		return create(mat, size, dataValue, name, Arrays.asList(lore), (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final int size, final String name) {
		return create(mat, size, (short) 0, name, (List<String>) null, (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final int size, final String name, final ItemEnchant... enchantement) {
		return create(mat, size, (short) 0, name, null, Arrays.asList(enchantement));
	}

	public static ItemStack create(final Material mat, final int size, final String name, final List<String> lore) {
		return create(mat, size, (short) 0, name, lore, (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final int size, final String name, final List<String> lore, final ItemEnchant... enchantement) {
		return create(mat, size, (short) 0, name, lore, Arrays.asList(enchantement));
	}

	public static ItemStack create(final Material mat, final int size, final String name, final String... lore) {
		return create(mat, size, (short) 0, name, Arrays.asList(lore), (List<ItemEnchant>) null);
	}

	public static ItemStack create(final Material mat, final ItemEnchant... enchantements) {
		return create(mat, 1, (short) 0, null, (List<String>) null, Arrays.asList(enchantements));
	}

	public static ItemStack create(final Material mat, final short dataValue) {
		return create(mat, 1, dataValue, null, (List<String>) null, (List<ItemEnchant>) null);
	}

	public static ItemStack[] createArmor(final ItemArmor itemArmor, final ItemEnchant... enchantements) {
		final List<ItemStack> itemStackarmors = itemArmor.getItemStack();
		for(final ItemStack itemStackArmor : itemStackarmors) {
			final ItemMeta itemMeta = itemStackArmor.getItemMeta();

			if(enchantements != null && enchantements.length != 0) {
				for(final ItemEnchant enchantement : enchantements) {
					itemMeta.addEnchant(enchantement.getEnchantment(), enchantement.getLevel(), enchantement.isUnsafe());
				}
			}
			itemStackArmor.setItemMeta(itemMeta);
		}
		return itemStackarmors.toArray(new ItemStack[4]);
	}

	public static ItemStack createColoredArmor(final Material material, final Color color, final String name, final List<String> lore) {
		final ItemStack itemStack = new ItemStack(material, 1);
		final LeatherArmorMeta itemMeta = (LeatherArmorMeta) itemStack.getItemMeta();
		itemMeta.setColor(color);
		if(name != null) {
			itemMeta.setDisplayName(Utils.color(name));
		}
		if(lore != null) {
			itemMeta.setLore(Utils.color(lore));
		}
		itemStack.setItemMeta(itemMeta);
		return itemStack;
	}

	public static ItemStack createPlayerSkull(final String owner) {
		return createPlayerSkull(owner, 1, null, null);
	}

	public static ItemStack createPlayerSkull(final String owner, final int size) {
		return createPlayerSkull(owner, size, null, null);
	}

	public static ItemStack createPlayerSkull(final String owner, final int size, final List<String> lore) {
		return createPlayerSkull(owner, size, null, lore);
	}

	public static ItemStack createPlayerSkull(final String owner, final int size, final String name) {
		return createPlayerSkull(owner, size, name, null);
	}

	public static ItemStack createPlayerSkull(final String owner, final int size, final String name, final List<String> lore) {
		final ItemStack itemStack = create(Material.SKULL_ITEM, size, (short) 3, name, lore, null);
		if(owner != null) {
			final SkullMeta skullmeta = (SkullMeta) itemStack.getItemMeta();
			skullmeta.setOwner(owner);
			itemStack.setItemMeta(skullmeta);
		}
		return itemStack;
	}

	public static ItemStack customPotionEffect(final PotionEffect potionEffect, final boolean overwrite, final boolean splash, final int amount, final String name, final List<String> lore) {
		final Potion potion = new Potion(PotionType.getByEffect(potionEffect.getType()), 1);
		potion.setSplash(splash);
		final ItemStack itemStack = potion.toItemStack(amount);
		final PotionMeta potionMeta = (PotionMeta) itemStack.getItemMeta();
		if(name != null) {
			potionMeta.setDisplayName(name);
		}
		if(lore != null) {
			potionMeta.setLore(lore);
		}
		potionMeta.addCustomEffect(potionEffect, overwrite);
		potionMeta.setMainEffect(potionEffect.getType());
		itemStack.setItemMeta(potionMeta);
		return itemStack;
	}

	public static Color getColorOfArmor(final ItemStack itemStack) {
		final LeatherArmorMeta meta = (LeatherArmorMeta) itemStack.getItemMeta();
		return meta.getColor();
	}

	public static String getDisplayName(final ItemStack itemStack) {
		if(hasDisplayName(itemStack)) {
			return itemStack.getItemMeta().getDisplayName();
		}
		return null;
	}

	public static HoverEvent getItemHoverEvent(final ItemStack itemStack) {
		final String itemJson = convertItemStackToJson(itemStack);

		// Prepare a BaseComponent array with the itemJson as a text component
		final BaseComponent[] hoverEventComponents = new BaseComponent[] { new TextComponent(itemJson) // The only element of the hover events basecomponents is the item json
		};
		return new HoverEvent(HoverEvent.Action.SHOW_ITEM, hoverEventComponents);
	}

	public static ItemStack getItemStackWithNoAttributes(final ItemStack itemStack) {
		return itemStack;
	}

	public static Integer getRepairCost(final ItemStack item) {
		final net.minecraft.server.v1_8_R3.ItemStack itemNms = org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack.asNMSCopy(item);
		if(itemNms.hasTag()) {
			if(itemNms.getTag().hasKeyOfType("RepairCost", 3)) {
				return itemNms.getTag().getInt("RepairCost");
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static Boolean hasDisplayName(final ItemStack itemStack) {
		if(!isValid(itemStack) || !itemStack.hasItemMeta() || !itemStack.getItemMeta().hasDisplayName()) {
			return false;
		}
		return true;
	}

	public static boolean isValid(final ItemStack itemStack) {
		return itemStack != null && itemStack.getType() != Material.AIR;
	}

	public static void removeGlow(final ItemStack item) {
		final Enchantment glow = ItemGlow.getGlowEnchant();
		if(!item.containsEnchantment(glow)) {
			item.removeEnchantment(glow);
		}
	}

	public static ItemStack replaceInLore(final ItemStack itemStack, final HashMap<String, String> replace) {
		final ItemMeta meta = itemStack.getItemMeta();
		final List<String> lore = meta.getLore();
		for(final Entry<String, String> Ereplace : replace.entrySet()) {
			int i = 0;
			for(final String Slore : lore) {
				if(Ereplace.getValue() == null) {
					lore.remove(i);
				} else {
					lore.set(i, Slore.replaceAll(Ereplace.getKey(), Ereplace.getValue()));
					i++;
				}
			}
		}
		meta.setLore(lore);
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	public static ItemStack replaceInLore(final ItemStack itemStack, final String match, final String replace) {
		final ItemMeta meta = itemStack.getItemMeta();
		final List<String> lore = meta.getLore();
		int i = 0;
		for(final String Slore : lore) {
			lore.set(i, Slore.replaceAll(match, replace));
			i++;
		}
		meta.setLore(lore);
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	public static ItemStack setColoredArmor(final ItemStack itemStack, final Color color) {
		final LeatherArmorMeta itemMeta = (LeatherArmorMeta) itemStack.getItemMeta();
		itemMeta.setColor(color);
		itemStack.setItemMeta(itemMeta);
		return itemStack;
	}

	public static ItemStack setDamage(ItemStack itemStack, final int damage) {

		itemStack = NBTEditor.setItemTag(itemStack, "generic.attackDamage", "AttributeModifiers", null, "AttributeName");
		itemStack = NBTEditor.setItemTag(itemStack, "generic.attackDamage", "AttributeModifiers", 0, "Name");
		itemStack = NBTEditor.setItemTag(itemStack, damage, "AttributeModifiers", 0, "Amount");
		itemStack = NBTEditor.setItemTag(itemStack, 0, "AttributeModifiers", 0, "Operation");
		itemStack = NBTEditor.setItemTag(itemStack, 894654L, "AttributeModifiers", 0, "UUIDLeast");
		itemStack = NBTEditor.setItemTag(itemStack, 2872L, "AttributeModifiers", 0, "UUIDMost");

		return itemStack;
	}

	public static ItemStack setDisplayName(final ItemStack itemStack, final String newName) {
		final ItemMeta meta = itemStack.getItemMeta();
		meta.setDisplayName(Utils.color(newName));
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	public static ItemStack setLore(final ItemStack itemStack, final List<String> lore) {
		final ItemMeta meta = itemStack.getItemMeta();
		meta.setLore(Utils.color(lore));
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	public static void setMaxStackSize(final int itemId, final int max) {
		try {
			final Field field = Reflection.getField(Reflection.getClass(ClassEnum.NMS, "Item"), "maxStackSize");
			field.setInt(itemId, max);
		} catch(IllegalArgumentException | IllegalAccessException | SecurityException e) {
			e.printStackTrace();
		}
	}

	public static ItemStack setOwner(final ItemStack itemStack, final String owner) {
		final SkullMeta skullmeta = (SkullMeta) itemStack.getItemMeta();
		skullmeta.setOwner(owner);
		itemStack.setItemMeta(skullmeta);
		return itemStack;
	}

	public static ItemStack setRepairCost(final ItemStack item, final int cost) {
		final net.minecraft.server.v1_8_R3.ItemStack itemNms = org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack.asNMSCopy(item);
		net.minecraft.server.v1_8_R3.NBTTagCompound tag;
		if(itemNms.hasTag()) {
			tag = itemNms.getTag();
		} else {
			tag = new net.minecraft.server.v1_8_R3.NBTTagCompound();
		}
		tag.setInt("RepairCost", cost);
		itemNms.setTag(tag);
		return org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack.asBukkitCopy(itemNms);
	}

	public static ItemStack setSplashPotion(final ItemStack itemStack, final boolean splash) {
		final Potion potion = Potion.fromItemStack(itemStack);
		potion.setSplash(splash);
		potion.apply(itemStack);
		return itemStack;
	}

	public static ItemStack setStackSize(final ItemStack itemStack, final int size) {
		final net.minecraft.server.v1_8_R3.ItemStack nmsIS = CraftItemStack.asNMSCopy(itemStack);
		nmsIS.getItem().c(size);
		return CraftItemStack.asBukkitCopy(nmsIS);
	}

}
