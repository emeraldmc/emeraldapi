package fr.tristiisch.emeraldmc.api.bungee.session;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SessionManagment {

	private static Map<UUID, Session> sessions = new HashMap<>();

	public static void addSession(final UUID uuid, final Session session) {
		SessionManagment.sessions.put(uuid, session);
	}

	public static Session getSession(final UUID uuid) {
		return sessions.get(uuid);
	}

	public static Collection<Session> getSessions() {
		return sessions.values();
	}

	public static void removeSession(final UUID uuid) {
		sessions.remove(uuid);
	}
}
