package fr.tristiisch.emeraldmc.api.spigot.core.commands.spawn;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.customevent.PlayerMoveBlockEvent;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;

public class SpawnListener implements Listener {

	@EventHandler
	public void PlayerMoveBlockEvent(final PlayerMoveBlockEvent event) {
		final Player player = event.getPlayer();
		if(Spawn.isTeleporter(player)) {
			Spawn.removeTeleporter(player);
			TaskManager.cancelTaskByName("Teleporter_" + player.getName());
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous ne devez pas bougez pendant la téléportation."));
		}
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		if(Spawn.isTeleporter(player)) {
			Spawn.removeTeleporter(player);
		}
	}

}
