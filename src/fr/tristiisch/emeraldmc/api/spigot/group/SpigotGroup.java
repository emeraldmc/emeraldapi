package fr.tristiisch.emeraldmc.api.spigot.group;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerChangeEvent;

public class SpigotGroup {

	public static void addGroup(final Player player, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer, final EmeraldGroup newGroup) {
		addGroup(player, accountProvider, emeraldPlayer, newGroup, 0L);
	}

	public static void addGroup(final Player player, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer, final EmeraldGroup newGroup, final int field, final int amount) {
		emeraldPlayer.addGroup(newGroup, Utils.addTimeToCurrentTime(field, amount));
		updateEmeraldPlayer(player, accountProvider, emeraldPlayer);
	}

	public static void addGroup(final Player player, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer, final EmeraldGroup newGroup, final long timestamp) {
		emeraldPlayer.addGroup(newGroup, timestamp);
		updateEmeraldPlayer(player, accountProvider, emeraldPlayer);
	}

	public static void removeGroup(final Player player, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer, final EmeraldGroup newGroup) {
		emeraldPlayer.removeGroup(newGroup);
		if(emeraldPlayer.getGroups().size() == 0) {
			emeraldPlayer.setGroups(EmeraldGroup.JOUEUR, 0);
		}
		updateEmeraldPlayer(player, accountProvider, emeraldPlayer);
	}

	public static void setGroup(final Player player, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer, final EmeraldGroup newGroup, final long timestamp) {
		emeraldPlayer.setGroups(newGroup, timestamp);
		updateEmeraldPlayer(player, accountProvider, emeraldPlayer);
	}

	private static void updateEmeraldPlayer(final Player player, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer) {
		final AsyncEmeraldPlayerChangeEvent event = new AsyncEmeraldPlayerChangeEvent(player, emeraldPlayer);
		Bukkit.getPluginManager().callEvent(event);
		if(player != null) {
			accountProvider.sendAccountToRedis(emeraldPlayer);
		} else {
			MySQL.savePlayer(emeraldPlayer);
		}
	}
}
