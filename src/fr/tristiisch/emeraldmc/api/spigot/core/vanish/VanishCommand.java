package fr.tristiisch.emeraldmc.api.spigot.core.vanish;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class VanishCommand implements CommandExecutor {

	private static int power;

	public VanishCommand(final EmeraldGroup group) {
		setGroup(group);
	}

	public static void setGroup(final EmeraldGroup group) {
		power = group.getPower();
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final Player player = (Player) sender;
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerLessThan(power)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}
			Player target;
			if(args.length == 1) {
				target = Bukkit.getPlayer(args[0]);
			} else {
				target = player;
			}
			Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
				if(Vanish.isVanish(target)) {
					Vanish.disable(target, true);
				} else {
					Vanish.enable(target, true);
				}
			});
		});
		return true;
	}
}