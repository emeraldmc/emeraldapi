package fr.tristiisch.emeraldmc.api.spigot.protocollib;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

public class JigsawCrashFix {

	public static void enable(final Plugin plugin) {
		final ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
		protocolManager.addPacketListener(new PacketAdapter(plugin, ListenerPriority.NORMAL, new PacketType[] { PacketType.Play.Client.POSITION }) {

			@Override
			public void onPacketReceiving(final PacketEvent event) {
				if(event.getPacketType().equals(PacketType.Play.Client.POSITION)) {
					final int before = (int) event.getPlayer().getLocation().getY();
					final double next = event.getPacket().getDoubles().read(1);
					if(before + 200 < next) {
						event.setCancelled(true);
					}

				}
			}
		});
		protocolManager.addPacketListener(new PacketAdapter(plugin, ListenerPriority.NORMAL, new PacketType[] { PacketType.Play.Client.CUSTOM_PAYLOAD }) {

			@Override
			public void onPacketReceiving(final PacketEvent event) {
				if(event.getPacketType().equals(PacketType.Play.Client.CUSTOM_PAYLOAD)) {
					final String x = event.getPacket().getStrings().read(0);
					if(x.equals("MC|BSign") || x.equals("MC|BEdit")) {
						final Player player = event.getPlayer();
						if(event.getPlayer().getItemInHand().getType() == Material.BOOK_AND_QUILL) {
							return;
						}
						event.setCancelled(true);
						Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, () -> {
							player.kickPlayer("Les cheats tel que Jigsaw sont interdit.");
						}, 0L);
					}
				}
			}
		});
	}
}
