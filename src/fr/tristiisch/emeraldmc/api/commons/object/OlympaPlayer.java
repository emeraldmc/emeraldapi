package fr.tristiisch.emeraldmc.api.commons.object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.Collectors;

import com.google.common.primitives.Ints;
import com.google.gson.GsonBuilder;

public class OlympaPlayer implements Cloneable {

	public static OlympaPlayer fromJson(final String json) {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(json, OlympaPlayer.class);
	}

	public static Map<EmeraldGroup, Long> getGroupsFromString(final String group) {
		final Map<EmeraldGroup, Long> groups2 = new HashMap<>();
		for(final String groupInfos : group.split(",")) {
			final String[] groupInfo = groupInfos.split(":");
			final EmeraldGroup emeraldGroup = EmeraldGroup.getByID(Integer.parseInt(groupInfo[0]));
			final Long until = Long.parseLong(groupInfo[1]);
			groups2.put(emeraldGroup, until);
		}
		return groups2;
	}

	public static List<String> getIpOrNameHistoryFromString(final String list) {
		return list != null ? Arrays.asList(list.split(";")) : new ArrayList<>();
	}

	private long firstco = 0;
	private final TreeMap<EmeraldGroup, Long> groups = new TreeMap<>(Comparator.comparing(EmeraldGroup::getPower).reversed());
	private int id;
	private String ip;
	private List<String> ipHistory;
	private long lastco = 0;
	private int minerais = 0;
	private String name;

	private List<String> nameHistory;
	private int pierres = 0;

	private int ts3ID = 0;

	private UUID uuid;

	public OlympaPlayer() {
	}

	// ############################################################

	public OlympaPlayer(	final int id, final String name, final List<String> nameHistory, final UUID uuid, final String ip, final List<String> ipHistory, final Map<EmeraldGroup, Long> groups,
							final long firstco, final long lastco, final int pierres, final int minerais, final int ts3ID) {
		this.id = id;
		this.name = name;
		this.nameHistory = nameHistory;
		this.uuid = uuid;
		this.uuid = uuid;
		this.ip = ip;
		this.ipHistory = ipHistory;
		this.groups.putAll(groups);
		this.firstco = firstco;
		this.lastco = lastco;
		this.pierres = pierres;
		this.minerais = minerais;
		this.ts3ID = ts3ID;
	}

	public OlympaPlayer(final String name, final UUID uuid, final String ip, final long firstco, final long lastco) {
		this.name = name;
		this.uuid = uuid;
		this.ip = ip;
		this.firstco = firstco;
		this.lastco = lastco;
		this.groups.put(EmeraldGroup.JOUEUR, 0L);
	}

	public void addGroup(final EmeraldGroup group) {
		this.groups.put(group, 0L);
		this.sortGroups();
	}

	public void addGroup(final EmeraldGroup group, final long timestamp) {
		this.groups.put(group, timestamp);
		this.sortGroups();
	}

	public void addMinerais(final int minerais) {
		this.minerais += minerais;
	}

	public int addPierres(final int i) {
		return this.pierres += i;
	}

	@Override
	public OlympaPlayer clone() {
		try {
			return (OlympaPlayer) super.clone();
		} catch(final CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

	public long getFirstCo() {
		return this.firstco;
	}

	public EmeraldGroup getGroup() {
		return this.groups.keySet().iterator().next();
	}

	public Entry<EmeraldGroup, Long> getGroupEntry() {
		return this.groups.entrySet().iterator().next();
	}

	public Map<EmeraldGroup, Long> getGroups() {
		return this.groups;
	}

	public List<String> getGroupsName() {
		return this.groups.keySet().stream().map(EmeraldGroup::getName).collect(Collectors.toList());
	}

	public List<Integer> getGroupsPower() {
		return this.groups.keySet().stream().map(EmeraldGroup::getPower).collect(Collectors.toList());
	}

	public String getGroupsToString() {
		return this.getGroups().entrySet().stream().map(entry -> entry.getKey().getID() + ":" + entry.getValue()).collect(Collectors.joining(","));
	}

	public Long getGroupUntil() {
		return this.groups.entrySet().iterator().next().getValue();
	}

	public int getId() {
		return this.id;
	}

	public String getIp() {
		return this.ip;
	}

	public List<String> getIpHistory() {
		return this.ipHistory;
	}

	public long getLastCo() {
		return this.lastco;
	}

	public int getMinerais() {
		return this.minerais;
	}

	public String getName() {
		return this.name;
	}

	public List<String> getNameHistory() {
		return this.nameHistory;
	}

	public int getPierres() {
		return this.pierres;
	}

	public List<Integer> getPowers() {
		return this.groups.keySet().stream().map(EmeraldGroup::getPower).collect(Collectors.toList());
	}

	public int getTS3ID() {
		return this.ts3ID;
	}

	public UUID getUniqueId() {
		return this.uuid;
	}

	public boolean hasMinerais(final int i) {
		return i <= this.minerais;
	}

	// Methods
	public boolean hasPermission(final EmeraldGroup... emeraldGroup) {
		if(emeraldGroup.length == 1) {
			return this.hasPowerLessThan(emeraldGroup[0]);
		}
		return this.hasPower(emeraldGroup);
	}

	public boolean hasPierre(final int i) {
		return i <= this.pierres;
	}

	public boolean hasPower(final EmeraldGroup emeraldGroup) {
		return this.getGroupsPower().stream().anyMatch(groupsPower -> emeraldGroup.getPower() == groupsPower);
	}

	public boolean hasPower(final EmeraldGroup... emeraldGroup) {
		return this.getGroupsPower().stream().anyMatch(groupsPower -> Arrays.stream(emeraldGroup).map(EmeraldGroup::getPower).anyMatch(p -> groupsPower >= p));
	}

	public boolean hasPower(final int... power) {
		return this.getGroupsPower().stream().anyMatch(groupsPower -> Ints.asList(power).contains(groupsPower));
	}

	public boolean hasPower(final int power) {
		return this.getGroupsPower().stream().anyMatch(groupsPower -> groupsPower >= groupsPower);
	}

	public boolean hasPowerLessThan(final EmeraldGroup emeraldGroup) {
		return this.getGroup().getPower() < emeraldGroup.getPower();
	}

	public boolean hasPowerLessThan(final int... power) {
		return Ints.asList(power).stream().anyMatch(p -> this.getGroup().getPower() < p);
	}

	public boolean hasPowerLessThan(final int power) {
		return this.getGroup().getPower() < power;
	}

	//
	public boolean hasPowerMoreThan(final EmeraldGroup emeraldGroup) {
		return this.getGroupsPower().stream().filter(groupsPower -> groupsPower >= emeraldGroup.getPower()).findFirst().isPresent();
	}

	public boolean hasPowerMoreThan(final int... power) {
		return this.getGroupsPower().stream().filter(groupsPower -> Ints.asList(power).stream().anyMatch(p -> groupsPower >= p)).findFirst().isPresent();
	}

	public boolean hasPowerMoreThan(final int power) {
		return this.getGroupsPower().stream().filter(groupsPower -> groupsPower >= power).findFirst().isPresent();
	}

	public void removeGroup(final EmeraldGroup group) {
		this.groups.remove(group);
		this.sortGroups();
	}

	public boolean removeMinerais(final int i) {
		if(this.hasMinerais(i)) {
			this.minerais -= i;
			return true;
		}
		return false;
	}

	public boolean removePierres(final int i) {
		if(this.hasPierre(i)) {
			this.pierres -= i;
			return true;
		}
		return false;
	}

	public void setFirstCo(final long firstco) {
		this.firstco = firstco;
	}

	public void setGroups(final EmeraldGroup targetAfterGroup, final long targetGroupUntil) {
		this.groups.clear();
		this.groups.put(targetAfterGroup, targetGroupUntil);
	}

	public void setID(final int id) {
		this.id = id;
	}

	public void setIP(final String ip) {
		this.ip = ip;
	}

	public void setIpHistory(final List<String> ipHistory) {
		this.ipHistory = ipHistory;
	}

	public void setLastCo(final long lastco) {
		this.lastco = lastco;
	}

	public void setMinerais(final int minerais) {
		this.minerais = minerais;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setPierres(final int pierres) {
		this.pierres = pierres;
	}

	public void setTS3ID(final int ts3ID) {
		this.ts3ID = ts3ID;
	}

	public void setUUID(final UUID uuid) {
		this.uuid = uuid;
	}

	private void sortGroups() {
		/* this.groups.sort(Comparator.comparing(EmeraldGroup::getPower).reversed()); */
		/*
		 * final TreeMap<EmeraldGroup, Long> groups2 = new
		 * TreeMap<>(Comparator.comparing(EmeraldGroup::getPower).reversed());
		 * groups2.putAll(this.groups);
		 */

		if(this.getGroups().size() > 1 && this.getGroups().containsKey(EmeraldGroup.JOUEUR)) {
			this.removeGroup(EmeraldGroup.JOUEUR);
		}
	}

	public String toJson() {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this);
	}
}
