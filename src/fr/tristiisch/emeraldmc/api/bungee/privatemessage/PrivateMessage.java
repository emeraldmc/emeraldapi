package fr.tristiisch.emeraldmc.api.bungee.privatemessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PrivateMessage {

	public static List<String> privateMessageCommand = new ArrayList<>();
	public static List<String> replyCommand = new ArrayList<>();
	private static Map<UUID, UUID> reply = new HashMap<>();

	public static void send(final ProxiedPlayer player, final ProxiedPlayer target, final String message) {
		// play sound mob.cat.meow

		final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
		final OlympaPlayer emeraldTarget = new AccountProvider(target.getUniqueId()).getEmeraldPlayer();

		final TextComponent msgPlayer = new TextComponent(Utils.colorFix("&6Message &c\u2B06 " + emeraldTarget.getGroup().getPrefix() + target.getName() + " &9 : "));
		msgPlayer.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&aRépondre à " + emeraldTarget.getGroup().getPrefix() + target.getName())).create()));
		msgPlayer.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/msg " + target.getName() + " "));
		msgPlayer.addExtra(Utils.colorFix("&9" + message));

		final TextComponent msgTarget = new TextComponent(Utils.colorFix("&6Message &a\u2B07 " + emeraldPlayer.getGroup().getPrefix() + player.getName() + " &9 : "));
		msgTarget.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&aRépondre à " + emeraldPlayer.getGroup().getPrefix() + player.getName())).create()));
		msgTarget.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/msg " + player.getName() + " "));
		msgTarget.addExtra(Utils.colorFix("&9" + message));


		player.sendMessage(msgPlayer);

		target.sendMessage(msgTarget);
	}

	public static void setReply(final ProxiedPlayer player, final ProxiedPlayer target) {
		if(reply.containsKey(player.getUniqueId()) && reply.get(player.getUniqueId()).equals(target.getUniqueId())) {
			return;
		}
		reply.put(player.getUniqueId(), target.getUniqueId());
	}

	public static void delReply(final ProxiedPlayer player) {
		if(reply.containsKey(player.getUniqueId())) {
			reply.remove(player.getUniqueId());
		}
	}

	public static UUID getReply(final ProxiedPlayer player) {
		if(reply.containsKey(player.getUniqueId())) {
			return reply.get(player.getUniqueId());
		}
		return null;
	}

}
