package fr.tristiisch.emeraldmc.api.spigot.core.endersee;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class OpenEnderChestCommandEvent extends PlayerEvent implements Cancellable {

	private static final HandlerList handlers = new HandlerList();
	private boolean cancel = false;
	private Player target;
	private final String targetName;

	public OpenEnderChestCommandEvent(final Player who, final Player target) {
		super(who);
		this.target = target;
		this.targetName = target.getName();
	}

	public OpenEnderChestCommandEvent(final Player who, final String targetName) {
		super(who);
		this.target = null;
		this.targetName = targetName;
	}

	@Override
	public void setCancelled(final boolean cancel) {
		this.cancel = cancel;
	}

	public String getTargetName() {
		return this.targetName;
	}

	public Player getTarget() {
		return this.target;
	}

	public boolean isTargetConnected() {
		if(this.target == null && this.targetName != null) {
			this.target = Bukkit.getPlayer(this.targetName);
		}
		return this.target != null;
	}

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
