package fr.tristiisch.emeraldmc.api.spigot.npc;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection.ClassEnum;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import net.minecraft.server.v1_8_R3.WorldServer;

@SuppressWarnings("unused")
public class EmeraldNpc {

	private Object entityPlayer;
	private GameProfile gameprofile;
	private final Location location;
	private final String name;
	private final String signature;
	private final String texture;

	public EmeraldNpc(final Location location, final String name, final String texture, final String signature) {
		this.location = location;
		this.name = Utils.color(name);
		this.texture = texture;
		this.signature = signature;
	}

	public void hide(final Player player) {
		/*
		 * try { final PlayerConnection playerconnection = PlayerConnection
		 * Reflection.getPlayerConnection(player); playerconnection.sendPacket(new
		 * PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.
		 * ADD_PLAYER, this.entityPlayer)); playerconnection.sendPacket(new
		 * PacketPlayOutNamedEntity(this.entityPlayer)); } catch(final Exception e) {
		 * e.printStackTrace(); }
		 */
	}

	public void show(final Player player) {
		try {
			final PlayerConnection playerConnection = (PlayerConnection) Reflection.getPlayerConnection(player);
			/*
			 * playerConnection.sendPacket(new
			 * PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.
			 * ADD_PLAYER, this.entityPlayer)); playerConnection.sendPacket(new
			 * PacketPlayOutNamedEntitySpawn(this.entityPlayer));
			 * playerConnection.sendPacket(new
			 * PacketPlayOutEntityHeadRotation(this.entityPlayer, (byte)
			 * (this.entityPlayer.yaw 256 / 360)));
			 */
		} catch(final Exception e) {
			e.printStackTrace();
		}
	}

	public void spawn() {
		MinecraftServer minecraftserver;
		WorldServer worldserver;
		try {
			minecraftserver = (MinecraftServer) Reflection.getClass(ClassEnum.CB, "CraftServer").getMethod("getServer").invoke(Bukkit.getServer());
			worldserver = (WorldServer) Reflection.getClass(ClassEnum.NMS, "CraftWorld").getMethod("getHandle").invoke(this.location.getWorld());

			this.gameprofile = new GameProfile(UUID.randomUUID(), this.name);
			this.gameprofile.getProperties().put("textures", new Property("textures", this.texture, this.signature));

			final Constructor<?> entityPlayerConstructor = Reflection.getClass(ClassEnum.NMS, "EntityPlayer").getDeclaredConstructors()[0];
			final Constructor<?> interactManagerConstructor = Reflection.getClass(ClassEnum.NMS, "PlayerInteractManger").getDeclaredConstructors()[0];

			/* this.entityPlayer = entityPlayerConstructor; */
			//https://youtu.be/wVyue_zXsmQ?t=10m44s
		} catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}

	}

}