package fr.tristiisch.emeraldmc.api.spigot.customevent.playerchatjson;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerEvent;

import net.md_5.bungee.api.chat.TextComponent;

public class AsyncPlayerChatJsonEvent extends PlayerEvent {

	public static final HandlerList handlers = new HandlerList();

	final private AsyncPlayerChatEvent asyncPlayerChatEvent;
	private TextComponent message;

	public AsyncPlayerChatJsonEvent(final Player player, final AsyncPlayerChatEvent asyncPlayerChatEvent) {
		super(player);
		this.asyncPlayerChatEvent = asyncPlayerChatEvent;
	}

	public TextComponent getMessage() {
		return this.message;
	}

	public void setMessage(final TextComponent message) {
		this.message = message;
	}

	@Override
	public HandlerList getHandlers(){
		return handlers;
	}

	public static HandlerList getHandlerList(){
		return handlers;
	}

	public AsyncPlayerChatEvent getAsyncPlayerChatEvent() {
		return this.asyncPlayerChatEvent;
	}
}
