package fr.tristiisch.emeraldmc.api.bungee.ban.commands.methods;

import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.bungee.ban.BanMySQL;
import fr.tristiisch.emeraldmc.api.bungee.ban.BanUtils;
import fr.tristiisch.emeraldmc.api.bungee.ban.objects.EmeraldBan;
import fr.tristiisch.emeraldmc.api.bungee.ban.objects.EmeraldBanType;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BanPlayer {

	/**
	 * Ajoute un ban
	 * targetUUID ou targetname ne doit pas être null.
	 *
	 * @param author is a UUID of author of ban or String (If the author is Console, author = "Console")
	 * @param targetname Name of player to ban. case insensitive
	 */
	@SuppressWarnings("deprecation")
	public static void addBan(final UUID author, final CommandSender sender, final String targetname, final UUID targetUUID, final String[] args, final OlympaPlayer emeraldPlayer) {
		// /ban <pseudo> <time unit> <reason>
		// args[0] = target
		// args[1] = time + unit
		// args[2] & + = reason

		ProxiedPlayer target = null;
		OlympaPlayer emeraldTarget = null;
		if(targetUUID != null) {
			target = ProxyServer.getInstance().getPlayer(targetUUID);

		} else if(targetname != null) {
			target = ProxyServer.getInstance().getPlayer(targetname);

		} else {
			throw new NullPointerException("The uuid or name must be specified");
		}

		if(target != null) {
			emeraldTarget = new AccountProvider(target.getUniqueId()).getEmeraldPlayer();

		} else {
			emeraldTarget = MySQL.getPlayer(targetUUID);
			if(emeraldTarget == null) {
				sender.sendMessage(BungeeConfigUtils.getString("commun.messages.playerneverjoin").replaceAll("%player%", args[0]));
				return;
			}
		}

		// Si le joueur n'est pas banni
		final EmeraldBan alreadyban = BanMySQL.getActiveSanction(emeraldTarget.getUniqueId(), EmeraldBanType.BAN);
		if(alreadyban != null) {
			// Sinon annuler le ban
			final TextComponent msg = BungeeUtils.formatStringToJSON(BungeeConfigUtils.getString("bungee.ban.messages.alreadyban").replaceAll("%player%", emeraldTarget.getName()));
			msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, alreadyban.toBaseComplement()));
			msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/banhist " + alreadyban.getId()));
			sender.sendMessage(msg);
			return;
		}
		final java.util.regex.Matcher matcher1 = BanUtils.matchDuration(args[1]);
		final java.util.regex.Matcher matcher2 = BanUtils.matchUnit(args[1]);
		// Si la command contient un temps et une unité valide
		if(matcher1.find() && matcher2.find()) {
			// Si la command contient un motif
			if(args.length > 2) {
				final String reason = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
				final String time = matcher1.group();
				final String unit = matcher2.group();
				final long timestamp = BanUtils.toTimeStamp(Integer.parseInt(time), unit);
				final long seconds = timestamp - Utils.getCurrentTimeinSeconds();

				if(emeraldTarget.getGroup().isStaffMember()) {
					sender.sendMessage(BungeeConfigUtils.getString("bungee.ban.messages.cantbanstaffmembers"));
					return;
				}
				if(seconds <= BungeeConfigUtils.getInt("bungee.ban.settings.minbantime")) {
					sender.sendMessage(BungeeConfigUtils.getString("bungee.ban.messages.cantbypassmaxbantime"));
					return;
				}
				if(seconds >= BungeeConfigUtils.getInt("bungee.ban.settings.maxbantime")) {
					sender.sendMessage(BungeeConfigUtils.getString("bungee.ban.messages.cantbypassmminbantime"));
					return;
				}
				final String Stimestamp = Utils.timestampToDuration(timestamp);
				final EmeraldBan ban = new EmeraldBan(EmeraldBan.getNextID(), EmeraldBanType.BAN, emeraldTarget.getUniqueId(), author, reason, Utils.getCurrentTimeinSeconds(), timestamp);
				if(!BanMySQL.addSanction(ban)) {
					sender.sendMessage(BungeeConfigUtils.getString("bungee.ban.messages.errordb"));
					return;
				}
				// Si Target est connecté
				if(target != null) {
					// Envoyer un message à tous les joueurs du même serveur spigot
					for(final ProxiedPlayer players : target.getServer().getInfo().getPlayers()) {
						players.sendMessage(BungeeConfigUtils.getString("bungee.ban.messages.tempbanannounce")
								.replaceAll("%player%", emeraldTarget.getName())
								.replaceAll("%time%", Stimestamp)
								.replaceAll("%reason%", reason));
					}
					;
					// Envoyer un message à Target lors de la déconnexion
					target.disconnect(BungeeUtils.connectScreen(BungeeConfigUtils.getString("bungee.ban.messages.tempbandisconnect")
							.replaceAll("%reason%", ban.getReason())
							.replaceAll("%time%", Stimestamp)
							.replaceAll("%id%", String.valueOf(ban.getId()))));
				}
				// Envoye un message à l'auteur
				final TextComponent msg = BungeeUtils.formatStringToJSON(BungeeConfigUtils.getString("bungee.ban.messages.tempbanannouncetoauthor")
						.replaceAll("%player%", emeraldTarget.getName())
						.replaceAll("%time%", Stimestamp)
						.replaceAll("%reason%", reason)
						.replaceAll("%author%", BungeeUtils.getName(author)));
				msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, ban.toBaseComplement()));
				msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/banhist " + ban.getId()));
				for(final ProxiedPlayer player : ProxyServer.getInstance()
						.getPlayers()
						.stream()
						.filter(p -> new AccountProvider(p.getUniqueId()).getEmeraldPlayer().getGroup().isStaffMember())
						.collect(Collectors.toList())) {
					player.sendMessage(msg);
				}
				ProxyServer.getInstance().getConsole().sendMessage(msg);
			} else {
				sender.sendMessage(BungeeConfigUtils.getString("bungee.ban.messages.usageban"));
			}
			// Sinon: ban def
		} else {
			if(emeraldPlayer != null && !emeraldPlayer.hasPowerMoreThan(BungeeConfigUtils.getInt("bungee.ban.settings.powerbandef"))) {
				sender.sendMessage(BungeeConfigUtils.getString("bungee.ban.messages.usageban"));
				return;
			}
			final String reason = String.join(" ", Arrays.copyOfRange(args, 1, args.length));

			final EmeraldBan ban = new EmeraldBan(EmeraldBan.getNextID(), EmeraldBanType.BAN, emeraldTarget.getUniqueId(), author, reason, Utils.getCurrentTimeinSeconds(), 0);
			if(!BanMySQL.addSanction(ban)) {
				sender.sendMessage(BungeeConfigUtils.getString("bungee.ban.messages.errordb"));
				return;
			}

			// Si Target est connecté
			if(target != null) {
				// Envoyer un message à tous les joueurs du même serveur spigot
				for(final ProxiedPlayer players : target.getServer().getInfo().getPlayers()) {
					players.sendMessage(BungeeConfigUtils.getString("bungee.ban.messages.banannounce").replaceAll("%player%", emeraldTarget.getName()).replaceAll("%reason%", reason));
				}
				;
				// Envoyer un message à Target lors de la déconnexion
				target.disconnect(BungeeUtils
						.connectScreen(BungeeConfigUtils.getString("bungee.ban.messages.bandisconnect").replaceAll("%reason%", ban.getReason()).replaceAll("%id%", String.valueOf(ban.getId()))));
			}
			// Envoye un message à l'auteur
			final TextComponent msg = BungeeUtils.formatStringToJSON(BungeeConfigUtils.getString("bungee.ban.messages.banannouncetoauthor")
					.replaceAll("%player%", emeraldTarget.getName())
					.replaceAll("%reason%", reason)
					.replaceAll("%author%", BungeeUtils.getName(author)));
			msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, ban.toBaseComplement()));
			msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/banhist " + ban.getId()));
			BungeeUtils.sendMessageToStaff(msg);
			ProxyServer.getInstance().getConsole().sendMessage(msg);
		}
	}
}
