package fr.tristiisch.emeraldmc.api.spigot.customevent.playerchatjson;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.md_5.bungee.api.chat.TextComponent;

public class AsyncPlayerChatJsonEventListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void AsyncPlayerChatEvent(final AsyncPlayerChatEvent event) {
		if(event.isCancelled()) {
			return;
		}
		event.setCancelled(true);

		final AsyncPlayerChatJsonEvent asyncPlayerChatJsonEvent = new AsyncPlayerChatJsonEvent(event.getPlayer(), event);
		Bukkit.getPluginManager().callEvent(asyncPlayerChatJsonEvent);

		final TextComponent message = asyncPlayerChatJsonEvent.getMessage();
		if(message != null) {
			for(final Player otherPlayer : Bukkit.getOnlinePlayers()) {
				otherPlayer.spigot().sendMessage(message);
			}
			Bukkit.getConsoleSender().sendMessage(message.toLegacyText());
		} else {
			event.setCancelled(false);
		}
	}

}
