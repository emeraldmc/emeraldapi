package fr.tristiisch.emeraldmc.api.spigot.motd;

import com.google.gson.Gson;

import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.utils.TPS;

public class EmeraldMotd {

	private String name;
	private double tps;
	private long uptime;
	private boolean maintenance;
	
	public EmeraldMotd() {
		this.name = EmeraldSpigot.getInstance().getServer().getName();
		this.uptime = EmeraldSpigot.getUptime();
		update();
	}
	
	public String getName() {
		return name;
	}

	public double getTPS() {
		return tps;
	}

	public long getUptime() {
		return uptime;
	}
	
	public boolean isMaintenance() {
		return maintenance;
	}

	public void update() {
		this.tps = TPS.getTPS();
		this.maintenance = false;
	}
	
	public String toJson() {
		update();
		return new Gson().toJson(this);
	}
	

}
