package fr.tristiisch.emeraldmc.api.commons;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.RedisAccess;

public class EmeraldPlayers {

	final static RedisAccess redisAccess = RedisAccess.INSTANCE;

	/*
	 * public static List<EmeraldPlayer> getPlayers() { final RedissonClient
	 * redissonClient = redisAccess.getRedissonClient(); final RBucket<String>
	 * accountRBucket = redissonClient.getBucket("emeraldplayers"); return
	 * EmeraldPlayers.fromJson(accountRBucket.get()); } public static void
	 * sendPlayersToRedis(final List<EmeraldPlayer> emeraldPlayers) { final
	 * RedissonClient redissonClient = redisAccess.getRedissonClient(); final
	 * RBucket<String> accountRBucket = redissonClient.getBucket("emeraldplayers");
	 * accountRBucket.set(toJson(emeraldPlayers)); } public void
	 * removeAccountFromRedis() { final RedissonClient redissonClient =
	 * redisAccess.getRedissonClient(); final RBucket<String> accountRBucket =
	 * redissonClient.getBucket("emeraldplayers"); accountRBucket.delete(); }
	 */

	/*
	 * public static EmeraldPlayer getPlayer(final UUID uuid) { return new
	 * AccountProvider(uuid).getEmeraldPlayer(); } public static void save(final
	 * EmeraldPlayer emeraldPlayer) { new
	 * AccountProvider(emeraldPlayer.getUniqueId()).sendAccountToRedis(emeraldPlayer
	 * ); }
	 */

	/*
	 * public static EmeraldPlayer getPlayer(final String playerName) { return
	 * getPlayers().stream().filter(p ->
	 * p.getName().equalsIgnoreCase(playerName)).findFirst().orElse(null); } public
	 * static EmeraldPlayer getPlayer(final int i) { return getPlayers().get(i); }
	 * public static EmeraldPlayer getPlayer(final UUID playerUUID) { return
	 * getPlayers().stream().filter(p ->
	 * p.getUniqueId().equals(playerUUID)).findFirst().orElse(null); } public static
	 * List<EmeraldPlayer> getPlayers(final UUID playerUUID) { return
	 * getPlayers().stream().filter(p ->
	 * p.getUniqueId().equals(playerUUID)).collect(Collectors.toList()); } public
	 * static EmeraldPlayer getPlayer(final ProxiedPlayer player) { return
	 * getPlayers().stream().filter(p ->
	 * p.getUniqueId().equals(player.getUniqueId())).findFirst().orElse(null); }
	 * public static EmeraldPlayer getPlayerInfo(final String playerName) { return
	 * getPlayers().stream().filter(p ->
	 * p.getName().equalsIgnoreCase(playerName)).findFirst().orElse(MySQL.getPlayer(
	 * playerName)); } public static EmeraldPlayer getPlayerInfo(final UUID
	 * playerUUID) { return getPlayers().stream().filter(p ->
	 * p.getUniqueId().equals(playerUUID)).findFirst().orElse(MySQL.getPlayer(
	 * playerUUID)); } public static EmeraldPlayer getPlayerInfo(final ProxiedPlayer
	 * player) { return getPlayers().stream().filter(p ->
	 * p.getUniqueId().equals(player.getUniqueId())).findFirst().orElse(MySQL.
	 * getPlayer(player.getUniqueId())); } public static boolean contains(final
	 * String playerName) { return getPlayers().stream().anyMatch(p ->
	 * p.getName().equalsIgnoreCase(playerName)); } public static boolean
	 * contains(final UUID uuid) { return getPlayers().stream().anyMatch(p ->
	 * p.getUniqueId() == uuid); } public static void addPlayer(final EmeraldPlayer
	 * emeraldplayer) { getPlayers().add(emeraldplayer); } public static void
	 * removePlayer(final EmeraldPlayer emeraldPlayer) {
	 * getPlayers().remove(emeraldPlayer); } public static void removePlayer(final
	 * String playerName) { getPlayers().remove(getPlayer(playerName)); } public
	 * static void removePlayer(final UUID playerUUID) {
	 * getPlayers().remove(getPlayer(playerUUID)); } public static void
	 * removePlayers(final UUID playerUUID) {
	 * getPlayers().removeAll(getPlayers(playerUUID)); } public static void
	 * removePlayer(final ProxiedPlayer player) {
	 * getPlayers().removeAll(getPlayers(player.getUniqueId())); } public static
	 * void replacePlayers(final List<EmeraldPlayer> after) { for(final
	 * EmeraldPlayer emeraldPlayer : after) { replacePlayer(emeraldPlayer); } }
	 * public static void replacePlayer(final EmeraldPlayer after) { for(final
	 * ListIterator<EmeraldPlayer> itr = EmeraldPlayers.getPlayers().listIterator();
	 * itr.hasNext();) { final EmeraldPlayer emeraldPlayer = itr.next();
	 * if(emeraldPlayer.getUniqueId().equals(after.getUniqueId())) { itr.set(after);
	 * } } } public static String toJson(final List<EmeraldPlayer> players) { return
	 * new Gson().toJson(players); }
	 * @SuppressWarnings("serial") public static List<EmeraldPlayer> fromJson(final
	 * String response) { return new Gson().fromJson(response, new
	 * TypeToken<List<EmeraldPlayer>>(){}.getType()); }
	 */
}
