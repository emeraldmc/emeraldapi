package fr.tristiisch.emeraldmc.api.bungee.info;

import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.commands.BungeeCommand;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Plugin;

public class InfoCommand extends BungeeCommand {

	public InfoCommand(final Plugin plugin) {
		super(plugin, "info", EmeraldGroup.MODERATEUR);
		this.usageString = BungeeConfigUtils.getString("bungee.info.messages.usage");
		this.minArg = 1;
		this.register();
	}

	@Override
	public void onCommand(final CommandSender sender, final String[] args) {
		if(Matcher.isUsername(args[0])) {

			Info.showInfo(sender, args[0], this.emeraldPlayer);

		} else if(Matcher.isFakeIP(args[0])) {

			if(Matcher.isIP(args[0])) {
				if(!this.hasPermission(EmeraldGroup.ADMIN)) {
					this.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
					return;
				}
				Info.showInfoIP(sender, args[0], this.emeraldPlayer);
			} else {
				this.sendMessage(BungeeConfigUtils.getString("commun.messages.ipinvalid").replaceAll("%ip%", args[0]));
				return;
			}

		} else if(Matcher.isFakeUUID(args[0])) {

			if(Matcher.isUUID(args[0])) {
				Info.showInfo(sender, UUID.fromString(args[0]), this.emeraldPlayer);
			} else {
				this.sendMessage(BungeeConfigUtils.getString("commun.messages.uuidinvalid").replaceAll("%uuid%", args[0]));
				return;
			}

		} else if(Matcher.isInt(args[0])) {

			Info.showInfo(sender, Integer.parseInt(args[0]), this.emeraldPlayer);
			return;
		} else {
			this.sendMessage(BungeeConfigUtils.getString("commun.messages.typeunknown").replaceAll("%type%", args[0]));
			return;
		}
	}

}
