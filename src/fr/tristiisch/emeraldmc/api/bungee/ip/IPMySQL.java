package fr.tristiisch.emeraldmc.api.bungee.ip;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.DatabaseManager;

public class IPMySQL {

	public static void addIp(final String ip, final boolean isVPN) {
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection().prepareStatement("INSERT INTO ip (ip, vpn) VALUES (?, ?)");
			int i = 1;
			pstate.setString(i++, ip);
			pstate.setString(i++, isVPN ? "1" : "0");
			pstate.executeUpdate();
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return;
	}

	public static void changeIp(final String ip, final boolean isVPN) {
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection().prepareStatement("UPDATE users SET vpn = ? WHERE ip = ?;");
			int i = 1;
			pstate.setString(i++, isVPN ? "1" : "0");
			pstate.setString(i++, ip);
			pstate.executeUpdate();
		} catch(final SQLException e) {
			e.printStackTrace();
		}
	}

	public static int getId(final String ip) {
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM ip WHERE `id` = '" + ip + "';");
			if(resultSet.next()) {
				return resultSet.getInt("id");
			}
			return -1;
		} catch(final SQLException e) {
			e.printStackTrace();
			return -2;
		}
	}

	public static String getIp(final int id) {
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM ip WHERE `ip` = '" + id + "';");
			if(resultSet.next()) {
				return resultSet.getString("ip");
			}
			return new String();
		} catch(final SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static int isVPN(final String ip) {
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM ip WHERE `ip` = '" + ip + "';");
			if(resultSet.next()) {
				return resultSet.getInt("vpn");
			}
			return -1;
		} catch(final SQLException e) {
			e.printStackTrace();
			return -2;
		}
	}
}
