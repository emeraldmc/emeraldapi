package fr.tristiisch.emeraldmc.api.spigot.core.tchat;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class Tchat {

	public static class EmeraldTchat {

		private String lastMsg;
		private long lastMsgTime;

		public EmeraldTchat(final String lastMsg, final long lastMsgTime) {
			this.lastMsg = lastMsg;
			this.lastMsgTime = lastMsgTime;
		}

		public String getLastMsg() {
			return this.lastMsg;
		}

		public long getLastMsgTime() {
			return this.lastMsgTime;
		}

		public boolean LastMsgIs(final String msg) {
			return this.lastMsg != null && this.lastMsg.equalsIgnoreCase(msg);
		}

		public void setLastMsg(final String lastMsg) {
			this.lastMsg = lastMsg;
		}

		public void setLastMsgTime(final long lastMsgTime) {
			this.lastMsgTime = lastMsgTime;
		}
	}

	private static final Map<UUID, EmeraldTchat> players = new HashMap<>();

	public static EmeraldTchat getPlayer(final UUID uuid) {
		if(players.containsKey(uuid)) {
			return players.get(uuid);
		} else {
			players.put(uuid, new EmeraldTchat("", 0));
			return players.get(uuid);
		}
	}

	public static void sendToStaff(final String type, final Player player, final String msg) {
		final TextComponent text = new TextComponent("\u2623 [" + type + "] " + player.getName() + " > ");
		text.setColor(ChatColor.DARK_PURPLE);

		for(final BaseComponent s : new ComponentBuilder(msg).color(ChatColor.LIGHT_PURPLE).create()) {
			text.addExtra(s);
		}
		text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&cCliquez pour mute " + player.getName())).create()));
		text.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/mute " + player.getName()));

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			for(final Player staff : Bukkit.getOnlinePlayers()) {
				if(new AccountProvider(staff.getUniqueId()).getEmeraldPlayer().getGroup().isStaffMember()) {
					staff.spigot().sendMessage(text);
				}
			}
		});
	}
}
