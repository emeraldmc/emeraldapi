package fr.tristiisch.emeraldmc.api.bungee;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.tristiisch.emeraldmc.api.bungee.announce.Announce;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.BanCommand;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.BanHistoryCommand;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.BanIpCommand;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.DelbanCommand;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.ForceKickCommand;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.KickCommand;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.MuteCommand;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.UnbanCommand;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.UnmuteCommand;
import fr.tristiisch.emeraldmc.api.bungee.ban.listeners.BanListener;
import fr.tristiisch.emeraldmc.api.bungee.ban.listeners.MuteListener;
import fr.tristiisch.emeraldmc.api.bungee.commands.BreloadCommand;
import fr.tristiisch.emeraldmc.api.bungee.commands.BroadcastCommand;
import fr.tristiisch.emeraldmc.api.bungee.commands.DevCommand;
import fr.tristiisch.emeraldmc.api.bungee.commands.PingCommand;
import fr.tristiisch.emeraldmc.api.bungee.commands.ServerCommand;
import fr.tristiisch.emeraldmc.api.bungee.commands.StatutCommand;
import fr.tristiisch.emeraldmc.api.bungee.commands.TabComplete;
import fr.tristiisch.emeraldmc.api.bungee.datamanagment.DataManagmentListener;
import fr.tristiisch.emeraldmc.api.bungee.group.GroupCommand;
import fr.tristiisch.emeraldmc.api.bungee.info.InfoCommand;
import fr.tristiisch.emeraldmc.api.bungee.ip.IPListener;
import fr.tristiisch.emeraldmc.api.bungee.listeners.ConnectionListener;
import fr.tristiisch.emeraldmc.api.bungee.lobby.LobbyCommand;
import fr.tristiisch.emeraldmc.api.bungee.maintenance.MaintenanceCommand;
import fr.tristiisch.emeraldmc.api.bungee.maintenance.MaintenanceListener;
import fr.tristiisch.emeraldmc.api.bungee.money.minerais.MineraisCommand;
import fr.tristiisch.emeraldmc.api.bungee.money.pierre.PierreCommand;
import fr.tristiisch.emeraldmc.api.bungee.motd.MotdListener;
import fr.tristiisch.emeraldmc.api.bungee.privatemessage.PrivateMessageCommand;
import fr.tristiisch.emeraldmc.api.bungee.privatemessage.PrivateMessageToggleCommand;
import fr.tristiisch.emeraldmc.api.bungee.privatemessage.ReplyCommand;
import fr.tristiisch.emeraldmc.api.bungee.report.ReportCommand;
import fr.tristiisch.emeraldmc.api.bungee.report.ReportListener;
import fr.tristiisch.emeraldmc.api.bungee.serveurs.schedule.ServersStatusRunnable;
import fr.tristiisch.emeraldmc.api.bungee.session.SessionCommand;
import fr.tristiisch.emeraldmc.api.bungee.session.SessionListener;
import fr.tristiisch.emeraldmc.api.bungee.staffchat.StaffChatCommand;
import fr.tristiisch.emeraldmc.api.bungee.staffchat.StaffChatListener;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeBPMC;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.bungee.utils.ConsoleFormat;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.RedisAccess;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.DatabaseManager;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

public class EmeraldBungee extends Plugin {

	private static Plugin instance;

	public static Plugin getInstance() {
		return instance;
	}

	public static String getName() {
		return "EmeraldMC";
	}

	@Override
	public void onDisable() {
		// Désactive la couleur dans la console
		ConsoleFormat.disable();
		DatabaseManager.close();
		RedisAccess.close();
		System.out.println(ChatColor.RED + "EmeraldAPI est désactivé.");
	}

	@Override
	public void onEnable() {
		try {
			instance = this;

			DatabaseManager.connect();
			// Init redis
			RedisAccess.init();

			// Charge les configs
			BungeeConfigUtils.loadConfigs();

			final PluginManager pluginmanager = this.getProxy().getPluginManager();

			// Gestion Bungee Plugin Messaging Channel
			this.getProxy().registerChannel(BungeeBPMC.EmeraldChannel);

			// Gestion Events
			pluginmanager.registerListener(this, new DataManagmentListener());
			pluginmanager.registerListener(this, new BungeeBPMC());
			pluginmanager.registerListener(this, new ConnectionListener());
			pluginmanager.registerListener(this, new BanListener());
			pluginmanager.registerListener(this, new MuteListener());
			pluginmanager.registerListener(this, new MaintenanceListener("maintenance", 75));
			pluginmanager.registerListener(this, new MotdListener());
			pluginmanager.registerListener(this, new TabComplete());
			pluginmanager.registerListener(this, new ReportListener());
			pluginmanager.registerListener(this, new StaffChatListener());
			pluginmanager.registerListener(this, new IPListener());
			pluginmanager.registerListener(this, new SessionListener());

			// Gestion Commandes
			pluginmanager.registerCommand(this, new MaintenanceCommand("maintenance", 75, 60));
			pluginmanager.registerCommand(this, new GroupCommand("groupe", 75, "group", "rank")); // + Group Listener
			pluginmanager.registerCommand(this, new PingCommand("ping"));
			pluginmanager.registerCommand(this, new BroadcastCommand("broadcast", 70, "bc"));
			pluginmanager.registerCommand(this, new BreloadCommand("breload", 75));

			pluginmanager.registerCommand(this, new PierreCommand("pierre", 70, "pierres", "pi"));
			pluginmanager.registerCommand(this, new MineraisCommand("minerais", 70, "minerai"));
			pluginmanager.registerCommand(this, new PrivateMessageCommand("msg", "m", "tell", "whisper", "w", "mp"));
			pluginmanager.registerCommand(this, new PrivateMessageToggleCommand("msgt", "mt", "tellt", "whispert", "wt", "mpt"));
			pluginmanager.registerCommand(this, new ReplyCommand("reply", "r", "rep"));
			pluginmanager.registerCommand(this, new LobbyCommand("lobby", "hub"));
			pluginmanager.registerCommand(this, new ServerCommand("server", EmeraldGroup.BUILDEUR, "serveur"));
			pluginmanager.registerCommand(this, new StatutCommand("statut", new int[] { 75, 60 }, "status"));
			pluginmanager.registerCommand(this, new StaffChatCommand("staffchat", "sc"));
			pluginmanager.registerCommand(this, new ReportCommand("report", "reports", "signalement", "signale"));
			new SessionCommand(this);
			new InfoCommand(this);
			new DevCommand(this);
			new BanCommand(this);
			new BanCommand(this);
			new BanHistoryCommand(this);
			new BanIpCommand(this);
			new DelbanCommand(this);
			new ForceKickCommand(this);
			new MuteCommand(this);
			new KickCommand(this);
			new UnbanCommand(this);
			new UnmuteCommand(this);

			Announce.init();

			final Logger logger = this.getProxy().getLogger();
			logger.log(Level.INFO, ChatColor.GOLD + "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
			logger.log(Level.INFO, ChatColor.GRAY + "");
			logger.log(Level.INFO, ChatColor.GRAY + "              " + this.getDescription().getName());
			logger.log(Level.INFO, ChatColor.GRAY + "             Version " + this.getDescription().getVersion());
			logger.log(Level.INFO, ChatColor.GRAY + "");
			logger.log(Level.INFO, ChatColor.GOLD + "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");

			this.getProxy().getScheduler().schedule(this, new ServersStatusRunnable(), 3, 5, TimeUnit.SECONDS);
		} catch(final Exception e) {
			this.getProxy().stop(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void onLoad() {
		// Met de la couleur dans la console
		ConsoleFormat.enable();
	}

}
