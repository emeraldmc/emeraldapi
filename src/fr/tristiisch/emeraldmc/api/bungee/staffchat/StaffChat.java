package fr.tristiisch.emeraldmc.api.bungee.staffchat;

import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class StaffChat {

	public static void sendMessage(final OlympaPlayer emeraldPlayer, final String msg) {

		final TextComponent msg2 = new TextComponent(Utils.colorFix("&c✦ &5[&dSTAFF&5] " + emeraldPlayer.getGroup().getPrefix() + emeraldPlayer.getName() + "&5 ➤" + " &d" + msg));
		msg2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&2Cliquez pour répondre au staff")).create()));
		msg2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, ";"));

		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			for(final ProxiedPlayer staff : ProxyServer.getInstance().getPlayers().stream().filter(p -> new AccountProvider(p.getUniqueId()).getEmeraldPlayer().getGroup().isStaffMember()).collect(
					Collectors.toList())) {
				staff.sendMessage(msg2);
			}
		});

	}
}
