package fr.tristiisch.emeraldmc.api.bungee.report;

import java.util.Arrays;

import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class ReportCommand extends Command {

	public ReportCommand(final String command, final String... aliases) {
		super(command, null, aliases);
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {

		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
			return;
		}
		final ProxiedPlayer player = (ProxiedPlayer) sender;

		if(args.length >= 2) {
			if(!Matcher.isUsername(args[0])) {
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &4" + args[0] + "&c n'est pas un pseudo valide."));
				return;
			}

			final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
			if(target == null || !target.getServer().getInfo().getName().equals(player.getServer().getInfo().getName())) {
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &4" + args[0] + "&c est introuvable."));
				return;
			}

			if(target.getUniqueId() == player.getUniqueId()) {
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &cVous ne pouvez pas vous autoreport."));
				return;
			}

			if(!Report.canReport(player)) {
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &cMerci de patienter une minute entre chaque signalement."));
				return;
			}

			Report.sendToStaff(player, target, String.join(" ", Arrays.copyOfRange(args, 1, args.length)));
			Report.addPlayer(player);

		} else {
			sender.sendMessage(Utils.color("&cUsage &7» &c/report <joueur> [motif]"));
		}
	}
}
