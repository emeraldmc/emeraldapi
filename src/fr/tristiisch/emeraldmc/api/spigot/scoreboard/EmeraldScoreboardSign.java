package fr.tristiisch.emeraldmc.api.spigot.scoreboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection.ClassEnum;
import net.minecraft.server.v1_8_R3.IScoreboardCriteria;
import net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardScore;

public class EmeraldScoreboardSign {

	private static HashMap<Player, EmeraldScoreboardSign> scoreboards = new HashMap<>();

	private static Class<?> packetScoreBoardObjective, packetDisplayObjective;

	static {
		packetScoreBoardObjective = Reflection.getClass(ClassEnum.NMS, "PacketPlayOutScoreboardObjective");
		packetDisplayObjective = Reflection.getClass(ClassEnum.NMS, "PacketPlayOutScoreboardDisplayObjective");

		/*
		 * packetScoreboardScore =
		 * Reflection.getClass("{nms}.PacketPlayOutScoreboardScore");
		 */
	}

	public static void animation(final Plugin plugin, final int line, final String string, final ChatColor color1, final ChatColor color2) {
		final List<String> anim = new ArrayList<>();
		for(int i = 0; i < string.length(); i++) {
			anim.add(color1 + string.substring(0, i) + color2 + string.substring(i, i + 1) + color1 + string.substring(i + 1, string.length()));
		}
		anim.add(color1 + string);
		for(int i = string.length() - 1; i > -1; i--) {
			anim.add(color1 + string.substring(0, i) + color2 + string.substring(i, i + 1) + color1 + string.substring(i + 1, string.length()));
		}
		anim.add(color1 + string);

		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {

			int i = 0;

			@Override
			public void run() {
				if(this.i >= anim.size()) {
					return;
				}
				for(final EmeraldScoreboardSign emeraldScoreboard : scoreboards.values()) {
					if(emeraldScoreboard.getLines().size() >= line + 1) {
						emeraldScoreboard.setLine(line, anim.get(this.i));
					}
					emeraldScoreboard.updateLines();
				}
				this.i++;
			}
		}, 0, (long) 1.5), 0, 100);
	}

	public static EmeraldScoreboardSign getScoreboard(final Player player) {
		return scoreboards.get(player);
	}

	public static boolean hasScoreboard(final Player player) {
		return scoreboards.containsKey(player);
	}

	public static void removeScoreboard(final Player player) {
		if(hasScoreboard(player)) {
			scoreboards.get(player).destroy();
			scoreboards.remove(player);
		}
	}

	protected String oldName;
	protected String name;

	protected String displayName;

	protected Player player;

	protected boolean toggled;

	protected List<String> scores = new ArrayList<>();

	public EmeraldScoreboardSign(final String displayName, final Player player) {
		this.name = EmeraldSpigot.getServerName();
		this.oldName = this.name;
		this.displayName = Utils.color(displayName);
		this.player = player;
	}

	public void addLine(final String value) {
		this.scores.add(this.fixDuplicate(Utils.color(value)));
	}

	public void clearLines() {
		this.scores.clear();
	}

	public void create() {
		this.destroy();
		this.sendObjective(this.player, this.name, this.displayName, 0, IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER); // Create the tmp Objective
		this.showSideBar(this.player);
		this.updateScoreObjective(this.player);
		scoreboards.put(this.player, this);
	}

	public void destroy() {
		this.sendObjective(this.player, this.oldName, "", 1, IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER);
		this.sendObjective(this.player, this.oldName + !this.toggled, "", 1, IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER);
		this.sendObjective(this.player, this.oldName + this.toggled, "", 1, IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER);
	}

	private String fixDuplicate(String value) {
		while(this.scores.contains(value)) {
			value += "§r";
		}
		return value;
	}

	public List<String> getLines() {
		return this.scores;
	}

	public void reverseLines() {
		Collections.reverse(this.scores);
	}

	/**
	 * Send Objective creation packet
	 *
	 * @param player
	 * @param displayName
	 * @param 0
	 *            = create, 1 = delete, 2 update
	 */
	private void sendObjective(final Player player, final String name, final String displayName, final int id, final IScoreboardCriteria.EnumScoreboardHealthDisplay en) {
		try {
			final Object packet = packetScoreBoardObjective.newInstance();
			Reflection.setFieldValue(packet, "a", name);
			Reflection.setFieldValue(packet, "b", displayName);
			Reflection.setFieldValue(packet, "c", en); // TODO: Remove NMS dependency
			Reflection.setFieldValue(packet, "d", id);
			Reflection.sendPacket(player, packet);
		} catch(final ReflectiveOperationException e) {
			e.printStackTrace();
		}
	}

	private void sendScore(final Player player, final String name, final int score, final PacketPlayOutScoreboardScore.EnumScoreboardAction en) {
		final Object packet = new PacketPlayOutScoreboardScore();
		Reflection.setFieldValue(packet, "a", name);
		Reflection.setFieldValue(packet, "b", this.name);
		Reflection.setFieldValue(packet, "c", score);
		Reflection.setFieldValue(packet, "d", en); // TODO: Remove NMS dependency
		Reflection.sendPacket(player, packet);
	}

	// STATIC

	public void setLine(final int index, final String value) {
		this.scores.set(index, this.fixDuplicate(Utils.color(value)));
	}

	private void showSideBar(final Player player) {
		try {
			final Object packet = packetDisplayObjective.newInstance();
			Reflection.setFieldValue(packet, "a", 1);
			Reflection.setFieldValue(packet, "b", this.name);
			Reflection.sendPacket(player, packet);
		} catch(final ReflectiveOperationException e) {
			e.printStackTrace();
		}
	}

	public void updateLines() {
		if(this.player.isOnline()) {
			this.toggled = !this.toggled;
			this.name = this.oldName + this.toggled;
			this.sendObjective(this.player, this.name, this.displayName, 0, IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER); // Create the tmp Objective
			this.updateScoreObjective(this.player);
			this.showSideBar(this.player);
			this.sendObjective(this.player, this.oldName + !this.toggled, "", 1, IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER); // Force clean up
		}
	}

	private void updateScoreObjective(final Player player) {
		if(this.scores.size() > 15) {
			try {
				throw new Exception("Le scoreboard ne peux pas dépasser 15 lignes");
			} catch(final Exception e) {
				e.printStackTrace();
			}
		}
		for(int i = 0; i < this.scores.size(); i++) {
			final String score = this.scores.get(i);
			if(score != null) {
				if(score.length() > 40) {
					try {
						throw new Exception("La ligne du scoreboard ne peux pas dépasser 40 caratères");
					} catch(final Exception e) {
						e.printStackTrace();
					}
				}
				this.sendScore(player, score, i, PacketPlayOutScoreboardScore.EnumScoreboardAction.CHANGE);
			}
		}
	}
}