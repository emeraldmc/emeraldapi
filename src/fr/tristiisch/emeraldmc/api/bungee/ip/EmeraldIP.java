package fr.tristiisch.emeraldmc.api.bungee.ip;

public class EmeraldIP {

	final private String city;
	final private String country;
	final private String hostname;
	final private String ip;
	final private String loc;
	final private String postcode;
	final private String provider;
	final private String region;
	private boolean vpn = false;

	public EmeraldIP(final String ip, final String hostname, final String provider, final String country, final String region, final String postcode, final String city, final String loc) {
		this.ip = ip;
		this.hostname = hostname;
		this.provider = provider;
		this.country = country;
		this.region = region;
		this.postcode = postcode;
		this.city = city;
		this.loc = loc;
		this.checkVpn();
	}

	public void checkVpn() {
		this.vpn = IPManagment.FAIVPN.contains(this.getISP()) || this.getPostcode() == null || this.getCity() == null || this.getCity().isEmpty() || this.getRegion() == null || this.getRegion()
				.isEmpty();
		if(!this.vpn) {
			this.vpn = IPManagment.isVPN(this.ip);
		} else {
			IPManagment.addIp(this.ip, this.vpn);
		}
	}

	public String getCity() {
		return this.city;
	}

	public String getCountry() {
		return this.country;
	}

	public String getHostname() {
		return this.hostname;
	}

	public String getIp() {
		return this.ip;
	}

	public String getISP() {
		return this.provider;
	}

	public String getLoc() {
		return this.loc;
	}

	public String getPostcode() {
		return this.postcode;
	}

	public String getRegion() {
		return this.region;
	}

	public boolean isVpn() {
		return this.vpn;
	}

	public void setVpn(final boolean vpn) {
		this.vpn = vpn;
	}
}
