package fr.tristiisch.emeraldmc.api.bungee.lobby;

import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class LobbyCommand extends Command {

	public LobbyCommand(final String command, final String... aliases) {
		super(command, null, aliases);
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
			return;
		}
		final ProxiedPlayer player = (ProxiedPlayer) sender;

		Lobby.connect(player);
	}

}
