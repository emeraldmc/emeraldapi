package fr.tristiisch.emeraldmc.api.bungee.announce;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.chat.ComponentSerializer;

public class Announce {

	private static List<BaseComponent[]> messages = new ArrayList<>();
	private static int lastMsgId;

	public static void init() {
		messages = BungeeConfigUtils.getStringList("bungee.announce").stream().filter(json -> Utils.isJSONValid(json)).map(json -> ComponentSerializer.parse(json)).collect(
				Collectors.toList());
		if(messages.isEmpty()) {
			ProxyServer.getInstance().getLogger().log(Level.SEVERE, "Les messages d'annonces n'ont pas pu être charger");
			return;
		}
		ProxyServer.getInstance().getScheduler().schedule(EmeraldBungee.getInstance(), () -> {
			int msgId;
			do {
				msgId = new Random().nextInt(messages.size());
			} while(lastMsgId == msgId);
			lastMsgId = msgId;
			final BaseComponent[] msg = messages.get(msgId);
			for(final ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
				player.sendMessage(msg);
			}
		}, 0, 5, TimeUnit.MINUTES);
	}
}
