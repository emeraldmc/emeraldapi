package fr.tristiisch.emeraldmc.api.spigot.core.staffmode.listeners;

import java.util.Arrays;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.build.Build;
import fr.tristiisch.emeraldmc.api.spigot.core.freeze.Freeze;
import fr.tristiisch.emeraldmc.api.spigot.core.staffmode.StaffMode;
import fr.tristiisch.emeraldmc.api.spigot.core.staffmode.StaffModeItem;
import fr.tristiisch.emeraldmc.api.spigot.core.vanish.Vanish;
import fr.tristiisch.emeraldmc.api.spigot.core.verif.VerifCommand;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;

public class StaffModeListener implements Listener {

	@EventHandler
	public void EntityDamageByEntityEvent(final EntityDamageByEntityEvent event) {
		if(event.isCancelled() || !(event.getDamager() instanceof Player)) {
			return;
		}
		final Player attacker = (Player) event.getDamager();
		if(StaffMode.isStaffMode(attacker)) {
			if(attacker.getItemInHand().isSimilar(StaffModeItem.KB.getItemStack())) {
				event.setCancelled(false);
				return;
			}

			event.setCancelled(true);

			if(event.getEntity() instanceof Player) {
				final Player target = (Player) event.getEntity();
				this.toggleFreezeInHand(attacker, target);
			}
		}
	}

	@EventHandler
	public void PlayerInteractAtEntityEvent(final PlayerInteractAtEntityEvent event) {
		final Player player = event.getPlayer();
		if(StaffMode.isStaffMode(player)) {
			event.setCancelled(true);
			if(event.getRightClicked() instanceof Player) {
				final Player target = (Player) event.getRightClicked();
				this.toggleFreezeInHand(player, target);
			}
		}
	}

	@EventHandler
	public void PlayerInteractEvent(final PlayerInteractEvent event) {
		final Player player = event.getPlayer();

		if(player.getItemInHand() == null) {
			return;
		}

		if(!StaffMode.isStaffMode(player)) {
			return;
		}

		if(player.getItemInHand().isSimilar(StaffModeItem.KB.getItemStack())) {
			event.setCancelled(false);

		} else if(player.getItemInHand().isSimilar(StaffModeItem.Teleportation.getItemStack())) {

			Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {

				for(final Player target : Bukkit.getOnlinePlayers().stream().collect(Utils.toShuffledList())) {

					final OlympaPlayer emeraldTarget = new AccountProvider(target.getUniqueId()).getEmeraldPlayer();

					if(!SpigotUtils.isSamePlayer(player,
						target) && (emeraldTarget.hasPowerLessThan(EmeraldGroup.MODERATEUR) || !StaffMode.isStaffMode(target) && !Build.isBuildModeEnable(target)) && !Vanish.isVanish(target)) {

						Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
							player.teleport(target);
						});
						player.sendMessage(Utils.color(Prefix.DEFAULT_GOOD + "Vous avez été téléporté à " + emeraldTarget.getGroup().getPrefix() + target.getName() + "&a."));

						return;
					}
					player.sendMessage(Utils.color(Prefix.DEFAULT_BAD + "Il n'y a aucun joueurs."));
				}
			});

		} else if(player.getItemInHand().isSimilar(StaffModeItem.Navette.getItemStack())) {

			final Block block = player.getTargetBlock(new HashSet<>(Arrays.asList(Material.AIR)), 1000);
			if(block == null) {
				player.sendMessage(Utils.color(Prefix.DEFAULT_BAD + "La distance est trop loin."));
				return;
			}
			final Location playerLocation = player.getLocation();
			final Location location = new Location(block.getWorld(), block.getX(), block.getY(), block.getZ(), playerLocation.getYaw(), playerLocation.getPitch());
			final Block bxn = location.add(-1.0, 0.0, 0.0).getBlock();
			final Block bxn2 = location.add(-0.5, 0.0, 0.0).getBlock();
			final Block bxp = location.add(1.0, 0.0, 0.0).getBlock();
			final Block bxp2 = location.add(0.5, 0.0, 0.0).getBlock();
			final Block bzn = location.add(0.0, 0.0, -1.0).getBlock();
			final Block bzn2 = location.add(0.0, 0.0, -0.5).getBlock();
			final Block bzp = location.add(0.0, 0.0, 1.0).getBlock();
			final Block bzp2 = location.add(0.0, 0.0, 0.5).getBlock();
			final Block by = location.getBlock();
			final Block by2 = location.add(0.0, 0.5, 0.0).getBlock();
			if(!by2.getType().equals(Material.AIR)) {
				for(boolean isOnLand = false; !isOnLand; isOnLand = true) {
					player.teleport(location.add(location.getDirection().multiply(-1.0)));
					if(by2.getType() != Material.AIR) {
						player.teleport(location.add(location.getDirection().multiply(-1.0)));
					}
				}
			}
			if(!by.getType().equals(Material.AIR)) {
				player.teleport(location.add(0.0, 0.5, 0.0));
			}
			if(!bxn.getType().equals(Material.AIR) || !bxn2.getType().equals(Material.AIR)) {
				player.teleport(location.add(0.5, 0.0, 0.0));
			}
			if(!bxp.getType().equals(Material.AIR) || !bxp2.getType().equals(Material.AIR)) {
				player.teleport(location.add(-0.5, 0.0, 0.0));
			}
			if(!bzn.getType().equals(Material.AIR) || !bzn2.getType().equals(Material.AIR)) {
				player.teleport(location.add(0.0, 0.0, 0.5));
			}
			if(!bzp.getType().equals(Material.AIR) || !bzp2.getType().equals(Material.AIR)) {
				player.teleport(location.add(0.0, 0.0, -0.5));
			}
		}
	}

	@EventHandler
	public void PlayerJoinEvent(final PlayerJoinEvent event) {

		/*
		 * final FileConfiguration config =
		 * CustomConfigUtils.getConfig(EmeraldSpigot.getInstance(), "data");
		 * config.getString(player.getUniqueId().toString() +
		 * ".inventaire");(player.getUniqueId().toString() + ".inventaire", datas);
		 */
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		if(StaffMode.isStaffMode(player)) {
			StaffMode.disable(player);
		}
		StaffMode.players.remove(player.getUniqueId());
	}

	private void toggleFreezeInHand(final Player attacker, final Player target) {
		if(attacker.getItemInHand().isSimilar(StaffModeItem.MisterFreeze.getItemStack())) {

			if(Freeze.isFreeze(target)) {
				Freeze.unfreeze(target);
				attacker.sendMessage(Utils.color("&2EmeraldMC &7» &cVous avez unfreeze &4" + target.getName() + "&c."));
			} else {
				Freeze.freeze(target);
				attacker.sendMessage(Utils.color("&2EmeraldMC &7» &aVous avez freeze &2" + target.getName() + "&a."));
			}
		} else if(attacker.getItemInHand().isSimilar(StaffModeItem.Verificator.getItemStack())) {
			VerifCommand.openVerifGUi(attacker, target);
		}
	}
}
