package fr.tristiisch.emeraldmc.api.spigot.core.boutique.menu.vip;

import java.util.Arrays;
import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.group.SpigotGroup;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public class BoutiqueVIP {

	public static void buy(final Player player, final BoutiqueVIPInfo vip, final boolean isLife, final InventoryClickEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final AccountProvider accountProvider = new AccountProvider(player.getUniqueId());
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();

			final EmeraldGroup playerVips = Arrays.stream(BoutiqueVIPInfo.values())
					.map(BoutiqueVIPInfo::getGroup)
					.filter(group -> emeraldPlayer.getGroups().keySet().contains(group))
					.findFirst()
					.orElse(null);

			if(playerVips != null) {
				if(playerVips.equals(vip.getGroup())) {
					GuiTools.cancelItem(event, "&cVous avez déjà le grade &4" + playerVips.getName() + "&c.");
					return;
				}
				if(emeraldPlayer.getGroup().getPower() > vip.getGroup().getPower()) {
					GuiTools.cancelItem(event, "&cVotre grade actuel est supérieur au grade &4" + playerVips.getName() + "&c.");
					return;
				}

			}

			final int price = isLife ? vip.getPriceLife() : vip.getPriceMonth();
			if(!emeraldPlayer.hasMinerais(price)) {
				GuiTools.cancelItem(event, "&cVous n'avez pas assez de minerais.");
				return;
			}

			emeraldPlayer.removeMinerais(price);

			if(isLife) {
				SpigotGroup.addGroup(player, accountProvider, emeraldPlayer, vip.getGroup());
				player.sendMessage(Utils.color("&2EmeraldMC &7» &aVous venez d'acheter le grade &2" + vip.getGroup().getName() + "&a avec " + price + " minerais !"));
			} else {
				final long timestamp = Utils.addTimeToCurrentTime(Calendar.MONTH, 1);
				SpigotGroup.addGroup(player, accountProvider, emeraldPlayer, vip.getGroup(), timestamp);
				player.sendMessage(Utils
						.color("&2EmeraldMC &7» &aVous venez d'acheter le grade &2" + vip.getGroup().getName() + "&a jusqu'au " + Utils.timestampToDate(timestamp) + " avec " + price + " minerais !"));

			}
			player.closeInventory();
		});
	}

	public static void openVIP(final Player player) {
		final GuiTools inventory = new GuiTools("&2Boutique Minerais &7- &2VIP", "shop_vip", 5);
		inventory.setItem(inventory.getMiddleSlot() - 3, BoutiqueVIPInfo.EMERAUDE.getItem());
		inventory.setItem(inventory.getMiddleSlot() - 1, BoutiqueVIPInfo.DIAMANT.getItem());
		inventory.setItem(inventory.getMiddleSlot() + 1, BoutiqueVIPInfo.SAPHIR.getItem());
		inventory.setItem(inventory.getMiddleSlot() + 3, BoutiqueVIPInfo.RUBIS.getItem());
		inventory.openInventory(player);
	}

	public static void openVIP2(final Player player, final BoutiqueVIPInfo vip) {
		final GuiTools inventory = new GuiTools("&2Boutique &7- " + vip.getNameColored(), "shop_vip2", 5, vip.toString());

		final ItemStack month = ItemTools.create(Material.STAINED_GLASS_PANE, 1, vip.getNameColored() + "&7- &21 Mois", "", "&bPrix: &3" + vip.getPriceMonth() + " minerais");
		final ItemStack life = ItemTools.create(Material.STAINED_GLASS_PANE, 1, (short) 4, vip.getNameColored() + "&7- &2A vie", "", "&bPrix: &3" + vip.getPriceLife() + " minerais");

		inventory.setItem(inventory.getMiddleSlot() - 1, month);

		inventory.setItem(inventory.getMiddleSlot() + 1, life);
		inventory.openInventory(player);
	}

	/*
	 * public static void openVIPConfirm(final Player player, final ItemStack item)
	 * { final BoutiqueVIP vip = BoutiqueVIP.get(item); final GuiTools inventory = new
	 * GuiTools("&2Boutique Minerais&7- " + vip.getNameColored(), "shop_vip2", 5);
	 * inventory.setItem(inventory.getMiddleSlot() - 1, confirmer);
	 * inventory.setItem(inventory.getMiddleSlot() + 1, annuler);
	 * inventory.openInventory(player); }
	 */

}
