package fr.tristiisch.emeraldmc.api.bungee.ban.listeners;

import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.ban.BanMySQL;
import fr.tristiisch.emeraldmc.api.bungee.ban.objects.EmeraldBan;
import fr.tristiisch.emeraldmc.api.bungee.ban.objects.EmeraldBanType;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class BanListener implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)
	public void LoginEvent(final LoginEvent event) {
		if(event.isCancelled()) {
			return;
		}

		final UUID playerUUID = event.getConnection().getUniqueId();
		// Vérifie que le joueur n'est pas banni
		EmeraldBan ban = BanMySQL.getActiveSanction(playerUUID, EmeraldBanType.BAN);

		if(ban == null) {
			ban = BanMySQL.getActiveSanction(event.getConnection().getAddress().getAddress().getHostAddress(), EmeraldBanType.BANIP);
		}

		if(ban != null) {

			if(ban.isPermanent()) {
				event.setCancelReason(BungeeUtils.connectScreen(BungeeConfigUtils.getString("bungee.ban.messages.bandisconnect")
						.replaceAll("%reason%", ban.getReason())
						.replaceAll("%id%", String.valueOf(ban.getId()))
						));
				/*			} else if(ban.getExpires() < Utils.getCurrentTimeinSeconds()){
								event.setCancelReason(BungeeUtils.connectScreen("\n&6&lVous avez été récemment banni pour &e&l" + ban.getReason() + "\n\n&6Sachez que si vous recommencez, vous serez doublement banni.\n\n&2&n&lVous pouvez maintenant vous reconnectez"));
								BanMySQL.expireBan(ban);*/
			} else {
				event.setCancelReason(BungeeUtils.connectScreen(BungeeConfigUtils.getString("bungee.ban.messages.tempbandisconnect")
						.replaceAll("%reason%", ban.getReason())
						.replaceAll("%time%", Utils.timestampToDuration(ban.getExpires()) )
						.replaceAll("%id%", String.valueOf(ban.getId()))
						));
			}
			event.setCancelled(true);
			return;
		}
	}
}
