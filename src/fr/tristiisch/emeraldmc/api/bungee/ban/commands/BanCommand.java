package fr.tristiisch.emeraldmc.api.bungee.ban.commands;

import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.ban.commands.methods.BanIp;
import fr.tristiisch.emeraldmc.api.bungee.ban.commands.methods.BanPlayer;
import fr.tristiisch.emeraldmc.api.bungee.commands.BungeeCommand;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldConsole;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

public class BanCommand extends BungeeCommand {

	public BanCommand(final Plugin plugin) {
		super(plugin, "ban", EmeraldGroup.MODERATEUR, "tempban");
		this.minArg = 2;
		this.usageString = BungeeConfigUtils.getString("bungee.ban.messages.usageban");
		this.register();
	}

	@Override
	public void onCommand(final CommandSender sender, final String[] args) {
		UUID author;
		if(sender instanceof ProxiedPlayer) {
			author = this.proxiedPlayer.getUniqueId();
		} else {
			author = EmeraldConsole.getUniqueId();
		}

		final String arg = args[0];

		if(Matcher.isUsername(arg)) {
			BanPlayer.addBan(author, sender, arg, null, args, this.emeraldPlayer);

		} else if(Matcher.isFakeIP(arg)) {

			if(Matcher.isIP(arg)) {
				BanIp.addBanIP(author, sender, arg, args, this.emeraldPlayer);
			} else {
				this.sendMessage(BungeeConfigUtils.getString("commun.messages.ipinvalid").replaceAll("%ip%", arg));
				return;
			}

		} else if(Matcher.isFakeUUID(arg)) {

			if(Matcher.isUUID(arg)) {
				BanPlayer.addBan(author, sender, null, UUID.fromString(arg), args, this.emeraldPlayer);
			} else {
				this.sendMessage(BungeeConfigUtils.getString("commun.messages.uuidinvalid").replaceAll("%uuid%", arg));
				return;
			}

		} else {
			this.sendMessage(BungeeConfigUtils.getString("commun.messages.typeunknown").replaceAll("%type%", arg));
			return;
		}

	}
}
