package fr.tristiisch.emeraldmc.api.commons.datamanagment.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseManager {

	private static DbConnection connection;

	public static void close() {
		try {
			connection.close();
		} catch(final SQLException e) {
			e.printStackTrace();
		}
	}

	public static void connect() {
		//connection = new DbConnection(new DbCredentials("localhost", "emeraldmc", "x6NWMHjUoYh41dGMigqi", "server"));
		connection = new DbConnection(new DbCredentials("localhost", "emeraldmc", "x6NWMHjUoYh41dGMigqi", "server"));
	}

	public static Connection getConnection() throws SQLException {
		return connection.getConnection();
	}
}
