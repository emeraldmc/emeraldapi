package fr.tristiisch.emeraldmc.api.spigot.core.teleport;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.vanish.Vanish;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class TeleportCommand implements CommandExecutor {

	private static int power;

	public static void setGroup(final EmeraldGroup group) {
		power = group.getPower();
	}

	public TeleportCommand(final EmeraldGroup group) {
		setGroup(group);
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final Player player = (Player) sender;
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerLessThan(power)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}

			if(args.length == 1) {
				final Player target = Bukkit.getPlayer(args[0]);
				if(target == null) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.").replaceAll("%player%", args[0]));
					return;
				}

				if(SpigotUtils.isSamePlayer(player, target)) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'allez pas vous téléporter à vous-même."));
					return;
				}

				if(Teleport.strictMode && !Vanish.isVanish(player)) {
					if(emeraldPlayer.hasPower(EmeraldGroup.MODERATEUR)) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous pouvez vous téléporté en &4/mode&c seulement."));
						return;
					} else if(!label.equalsIgnoreCase("tpv")) {
						final TextComponent msg = new TextComponent(Utils.colorFix("&2EmeraldMC &7» &cVous n'êtes pas invisible. Pour vous téléporté sans être invisible, utilisez &4/tpv&c."));
						msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Cliquez pour vous téléportez à &e" + target.getName())).create()));
						msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tpv " + target.getName()));
						player.spigot().sendMessage(msg);
						return;
					}
				}
				Teleport.teleportToPlayer(player, target);

			} else if(args.length == 2) {
				if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}

				final Player target1 = Bukkit.getPlayer(args[0]);
				if(target1 == null) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.".replaceAll("%player%", args[0])));
					return;
				}

				final Player target2 = Bukkit.getPlayer(args[1]);
				if(target2 == null) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.".replaceAll("%player%", args[1])));
					return;
				}

				if(SpigotUtils.isSamePlayer(target1, target2)) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'allez pas téléporté une personne à elle-même."));
					return;
				}
				Teleport.teleportPlayerToPlayer(player, target1, target2);
			} else if(args.length > 2) {
				if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}
				final Location location = Teleport.argsToLoc(player, args, 0);
				if(location == null) {
					player.sendMessage(Utils.color("&cUsage &7» &c/tp <x> <y> <z>"));
					return;
				}
				Teleport.teleportToPlayer(player, location);
			} else {
				player.sendMessage(Utils.color("&cUsage &7» &c/tp <joueur> [joueur]"));
			}
		});
		return true;
	}
}
