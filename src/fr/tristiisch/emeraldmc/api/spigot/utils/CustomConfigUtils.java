package fr.tristiisch.emeraldmc.api.spigot.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import com.google.common.io.ByteStreams;

public class CustomConfigUtils {


	public static void createNewConfigFile(final Plugin plugin, final String name) {
		final String fileName = name + ".yml";
		if(!plugin.getDataFolder().exists()) {
			plugin.getDataFolder().mkdir();
		}
		final File configFile = new File(plugin.getDataFolder(), fileName);
		try {
			if(!configFile.exists()) {
				configFile.createNewFile();
				if(plugin.getResource(fileName) != null) {
					ByteStreams.copy(plugin.getResource(fileName), new FileOutputStream(configFile));
				}
			}
		} catch (final Exception e) {
			Bukkit.getLogger().log(Level.SEVERE, ChatColor.RED + "Impossible de charger la config: " + fileName);
			e.printStackTrace();
		}
	}
	public static void createIfDoesNotExist(final Plugin plugin, final String... names) {
		for(final String name : names) {
			if(!configExist(plugin, name)) {
				createNewConfigFile(plugin, name);
			}
		}
	}

	public static Boolean configExist(final Plugin plugin, final String filename) {
		final File configFile = new File(plugin.getDataFolder(), filename + ".yml");
		return configFile.exists();
	}

	public static FileConfiguration getConfig(final Plugin plugin, final String filename) {
		final File configFile = new File(plugin.getDataFolder(), filename + ".yml");
		return YamlConfiguration.loadConfiguration(configFile);
	}

	public static void eraseFile(final Plugin plugin, final String filename) {
		final File configFile = new File(plugin.getDataFolder(), filename + ".yml");
		configFile.delete();
		try {
			configFile.createNewFile();
		} catch(final IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveConfig(final Plugin plugin, final FileConfiguration config, final String filename) {
		final File configFile = new File(plugin.getDataFolder(), filename + ".yml");
		try {
			config.save(configFile);
		} catch(final IOException e) {
			e.printStackTrace();
		}
	}

	public static Boolean configContains(final Plugin plugin, final String filename, final String path) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		if (data.contains(path)) {
			return true;
		}
		return false;
	}

	public static String getString(final Plugin plugin, final String filename, final String path) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		return data.getString(path);
	}

	public static Integer getInteger(final Plugin plugin, final String filename, final String path) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		return data.getInt(path);
	}

	public static Double getDouble(final Plugin plugin, final String filename, final String path) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		return data.getDouble(path);
	}

	public static Boolean getBoolean(final Plugin plugin, final String filename, final String path) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		return data.getBoolean(path);
	}

	public static Location getLocation(final Plugin plugin, final String filename, final String path) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		return LocationUtils.convertStringToLocation(data.getString(path));
	}

	public static List<String> getStringList(final Plugin plugin, final String filename, final String path) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		return data.getStringList(path);
	}

	public static List<Integer> getIntegerList(final Plugin plugin, final String filename, final String path) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		return data.getIntegerList(path);
	}

	public static List<Double> getDoubleList(final Plugin plugin, final String filename, final String path) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		return data.getDoubleList(path);
	}

	public static List<Location> getLocationList(final Plugin plugin, final String filename, final String path) {
		return getLocationList(YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), filename + ".yml")), path);
	}

	public static List<Location> getLocationList(final FileConfiguration config, final String path) {
		final List<Location> temp = new ArrayList<>();
		final List<String> tempLocs = config.getStringList(path);
		for(final String loc : tempLocs) {
			final String[] coords = loc.split(",");
			final World w = Bukkit.getWorld(coords[0]);
			final double x = Double.parseDouble(coords[1]);
			final double y = Double.parseDouble(coords[2]);
			final double z = Double.parseDouble(coords[3]);

			final Location location = new Location(w, x, y, z);
			if(coords.length == 6) {
				final int pitch = Integer.parseInt(coords[4]);
				final int yaw = Integer.parseInt(coords[5]);
				location.setPitch(pitch);
				location.setYaw(yaw);
			}
			temp.add(location);
		}
		return temp;
	}

	public static void setString(final Plugin plugin, final String filename, final String path, final String value) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		data.set(path, value);
		try {
			data.save(dfile);
		}
		catch (final IOException ex) {}
	}

	public static void setInteger(final Plugin plugin, final String filename, final String path, final int value) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		data.set(path, value);
		try {
			data.save(dfile);
		}
		catch (final IOException ex) {}
	}

	public static void setDouble(final Plugin plugin, final String filename, final String path, final double value) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		data.set(path, value);
		try {
			data.save(dfile);
		}
		catch (final IOException ex) {}
	}

	public static void setBoolean(final Plugin plugin, final String filename, final String path, final boolean value) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		data.set(path, value);
		try {
			data.save(dfile);
		}
		catch (final IOException ex) {}
	}

	public static void setLocation(final Plugin plugin, final String filename, final String path, final Location value) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		data.set(path, LocationUtils.convertLocationToString(value));
		try {
			data.save(dfile);
		}
		catch (final IOException ex) {}
	}

	public static void setStringList(final Plugin plugin, final String filename, final String path, final List<String> value) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		data.set(path, value);
		try {
			data.save(dfile);
		}
		catch (final IOException ex) {}
	}

	public static void setIntegerList(final Plugin plugin, final String filename, final String path, final List<Integer> value) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		data.set(path, value);
		try {
			data.save(dfile);
		}
		catch (final IOException ex) {}
	}

	public static void setDoubleList(final Plugin plugin, final String filename, final String path, final List<Double> value) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		data.set(path, value);
		try {
			data.save(dfile);
		}
		catch (final IOException ex) {}
	}

	public static void setLocationList(final Plugin plugin, final String filename, final String path, final List<Location> value) {
		final File dfile = new File(plugin.getDataFolder(), filename + ".yml");
		final FileConfiguration data = YamlConfiguration.loadConfiguration(dfile);
		final List<String> temp = new ArrayList<>();
		for (final Location loc : value) {
			temp.add(LocationUtils.convertLocationToString(loc));
		}
		data.set(path, temp);
		try {
			data.save(dfile);
		}
		catch (final IOException ex) {}
	}
	public static Cuboid getCuboid(final Plugin plugin, final String filename, final String path) {
		final Location pos1 = getLocation(plugin, filename, path + ".pos1");
		final Location pos2 = getLocation(plugin, filename, path + ".pos2");
		if (pos1 == null || pos2 == null) {
			return null;
		}
		return new Cuboid(pos1, pos2);
	}

	public static void setCuboid(final Plugin plugin, final String filename, final String path, final Cuboid cuboid) {
		setLocation(plugin, filename, path + ".pos1", cuboid.getPoint1());
		setLocation(plugin, filename, path + ".pos2", cuboid.getPoint2());
	}
}
