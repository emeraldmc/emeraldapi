package fr.tristiisch.emeraldmc.api.spigot.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

public class LocationUtils {
	
    public static String convertLocationToString(final Location loc) {
        final String world = loc.getWorld().getName();
        final double x = loc.getX();
        final double y = loc.getY();
        final double z = loc.getZ();
		if((int) loc.getPitch() != 0) {
	        final int pitch = (int) loc.getPitch();
	        final int yaw = (int) loc.getYaw();
	        return world + "," + x + "," + y + "," + z + "," + pitch + "," + yaw;
		}
        return world + "," + x + "," + y + "," + z;
    }
    
    public static Location convertStringToLocation(final String loc) {
    	if (loc != null) {
    		final String[] coords = loc.split(",");
    		final World w = Bukkit.getWorld(coords[0]);
    		final double x = Double.parseDouble(coords[1]);
    		final double y = Double.parseDouble(coords[2]);
    		final double z = Double.parseDouble(coords[3]);
    		if(coords.length == 6) {
    			final float pitch = Float.parseFloat(coords[4]);
    			final float yaw = Float.parseFloat(coords[5]);
    			return new Location(w, x, y, z, pitch, yaw);
    		}
    		return new Location(w, x, y, z);
    	}
    	return null;
    }
    
    public static Cuboid getCuboid(ConfigurationSection section, String key) {
        if(section != null) {
        	Location loc1 = LocationUtils.convertStringToLocation(section.getString(key + ".pos1"));
        	Location loc2 = LocationUtils.convertStringToLocation(section.getString(key + ".pos2"));
			Cuboid cuboid = new Cuboid(loc1, loc2);
            return cuboid;
        }
        return null;
    }
    
    
    
    public static boolean isIn(final Location loc, final Location playerLoc) {
    	return playerLoc.getWorld() == loc.getWorld() && loc.getBlockX() == playerLoc.getBlockX() && loc.getBlockY() == playerLoc.getBlockY() && loc.getBlockZ() == playerLoc.getBlockZ();
    }
}
