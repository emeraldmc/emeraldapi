package fr.tristiisch.emeraldmc.api.spigot.core.fly;

import org.bukkit.entity.Player;

public class Fly {

	public static boolean toggleFlight(final Player player) {
		player.setAllowFlight(!player.getAllowFlight());
		return player.getAllowFlight();
	}

	public static boolean enableFlight(final Player player) {
		if(player.getAllowFlight()) {
			return false;
		}
		player.setAllowFlight(true);
		return true;
	}

	public static boolean enableFlying(final Player player) {
		if(!player.getAllowFlight() || player.isFlying()) {
			return false;
		}
		player.setFlying(true);
		return true;
	}

	public static boolean disableFlight(final Player player) {
		if(!player.getAllowFlight()) {
			return false;
		}
		player.setAllowFlight(false);
		return true;
	}

	public static boolean disableFlying(final Player player) {
		if(!player.getAllowFlight() || !player.isFlying()) {
			return false;
		}
		player.setFlying(false);
		return true;
	}
}
