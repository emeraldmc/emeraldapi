package fr.tristiisch.emeraldmc.api.commons.datamanagment.redis;

public class RedisCredentials {

	final private String ip;;
	final private String password;
	final private int port;
	final private String clientName;

	public RedisCredentials(final String ip, final String password, final int port) {
		this.ip = ip;
		this.password = password;
		this.port = port;
		this.clientName = "EmeraldMC_Bungeecord";
	}

	public RedisCredentials(final String ip, final String password, final int port, final String clientName) {
		this.ip = ip;
		this.password = password;
		this.port = port;
		this.clientName = "EmeraldMC_" + clientName;
	}

	public String getIp() {
		return this.ip;
	}

	public String getPassword() {
		return this.password;
	}

	public int getPort() {
		return this.port;
	}

	public String getClientName() {
		return this.clientName;
	}

	public String toRedisURL() {
		return this.ip + ":" + this.port;
	}
}
