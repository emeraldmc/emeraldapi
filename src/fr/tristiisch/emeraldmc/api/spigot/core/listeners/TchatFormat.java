package fr.tristiisch.emeraldmc.api.spigot.core.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;

public class TchatFormat implements Listener {

	@EventHandler
	private void AsyncPlayerChatEvent(final AsyncPlayerChatEvent event) {
		if(event.isCancelled()) {
			return;
		}
		final Player player  = event.getPlayer();
		final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
		if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.RESPBUILD)) {
			event.setMessage(Utils.color(event.getMessage()));
		}
		event.setFormat(emeraldPlayer.getGroup().getPrefix() + "%s" + emeraldPlayer.getGroup().getTchatSuffix() + "%s");
	}
}
