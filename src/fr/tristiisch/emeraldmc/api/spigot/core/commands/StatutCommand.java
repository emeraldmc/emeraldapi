package fr.tristiisch.emeraldmc.api.spigot.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

/**
 * Commande non utilisé en spigot mais sur bungee avec une autre class
 */
public class StatutCommand implements CommandExecutor {

	private final int power;

	public StatutCommand(final int power) {
		this.power = power;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			if(sender instanceof Player) {
				final Player player = (Player) sender;
				final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(emeraldPlayer.hasPowerLessThan(this.power)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}
			}
			for(final EmeraldServer emeraldServer : EmeraldServers.getServers()) {
				this.sendInfoServerMessage(sender, emeraldServer);
			}
		});
		return true;
	}

	private void sendInfoServerMessage(final CommandSender sender, final EmeraldServer server) {
		if(server.getError() == null) {
			sender.sendMessage(Utils.color("&6%name%&7: %status% &a%online%&7/&2%max%")
					.replaceAll("%name%", server.getName())
					.replaceAll("%status%", server.getStatus().getColor() + server.getStatus().getName())
					.replaceAll("%online%", String.valueOf(server.getInfo().getPlayers().getOnline()))
					.replaceAll("%max%", String.valueOf(server.getInfo().getPlayers().getMax())));
		} else {
			sender.sendMessage(Utils.color("&c%name%&7: &cerreur: %erreur%").replaceAll("%name%", server.getName()).replaceAll("%erreur%", server.getError()));
		}
	}
}
