package fr.tristiisch.emeraldmc.api.spigot.core.listeners;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class CommandBlockerListener implements Listener {

	private final List<String> commandpl = Arrays.asList("plugins", "pl");
	private final List<String> commandblock = Arrays.asList("icanhasbukkit", "version", "ver", "about", "help", "?", "me");
	private final List<String> commandop = Arrays.asList("op", "deop", "whitelist", "stop", "reload", "restart");


	@EventHandler
	public void PlayerCommandPreprocessEvent(final PlayerCommandPreprocessEvent event) {
		if(event.isCancelled()) {
			return;
		}
		final String command = event.getMessage().substring(1).toLowerCase().split(" ")[0];
		if(this.commandpl.contains(command)) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.homeamade")));
		} else if(this.commandblock.contains(command)) {
			event.setCancelled(true);
		} else if(this.commandop.contains(command)) {
			final Player player = event.getPlayer();
			if(!player.isOp()) {
				event.setCancelled(true);
			}
		} else if(Pattern.compile("\\w*:\\w*").matcher(command).find()) {
			final Player player = event.getPlayer();
			if(!player.isOp()) {
				event.setCancelled(true);
			}
		}
	}


}
