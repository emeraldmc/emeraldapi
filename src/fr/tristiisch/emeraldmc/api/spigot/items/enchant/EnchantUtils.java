package fr.tristiisch.emeraldmc.api.spigot.items.enchant;

import java.lang.reflect.Field;
import java.util.HashMap;

import org.bukkit.enchantments.Enchantment;

public class EnchantUtils {

	public static void removeIfExist(final String name, final int id) {
		try {
			final Field byIdField = Enchantment.class.getDeclaredField("byId");
			final Field byNameField = Enchantment.class.getDeclaredField("byName");

			byIdField.setAccessible(true);
			byNameField.setAccessible(true);

			@SuppressWarnings("unchecked")
			final HashMap<Integer, Enchantment> byId = (HashMap<Integer, Enchantment>) byIdField.get(null);
			@SuppressWarnings("unchecked")
			final HashMap<String, Enchantment> byName = (HashMap<String, Enchantment>) byNameField.get(null);

			if(byId.containsKey(id)) {
				byId.remove(id);
			}

			if(byName.containsKey(name)) {
				byName.remove(name);
			}
		} catch(final Exception ignored) {
		}
	}
}
