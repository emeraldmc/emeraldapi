package fr.tristiisch.emeraldmc.api.spigot.core.commands.spawn;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class SpawnCommand implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole"));
			return true;
		}
		final Player player = (Player) sender;
		if(player.getGameMode().equals(GameMode.ADVENTURE) || player.getGameMode().equals(GameMode.SURVIVAL)) {
			Spawn.telportToSpawn(player, Spawn.getDefaultTime());
			return true;
		}
		Spawn.telportToSpawn(player);
		return true;
	}

}
