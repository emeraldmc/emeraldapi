package fr.tristiisch.emeraldmc.api.bungee.commands;

import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Matcher;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class PingCommand extends Command {

	public PingCommand(final String command) {
		super(command);
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		int ping;
		if(args.length >= 1) {
			if(!Matcher.isUsername(args[0])) {
				sender.sendMessage(BungeeConfigUtils.getString("commun.messages.invalidusername").replaceAll("%player%", args[1]));
				return;
			}
			final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage(BungeeConfigUtils.getString("commun.messages.unknownusername").replaceAll("%player%", args[1]));
				return;
			}
			ping = target.getPing();
			sender.sendMessage(
					BungeeConfigUtils.getString("bungee.ping.messages.otherping").replaceAll("%player%", target.getName()).replaceAll("%ping%", this.getPingColor(ping) + String.valueOf(ping)));
		} else {
			if(!(sender instanceof ProxiedPlayer)) {
				sender.sendMessage(BungeeConfigUtils.getString("bungee.ping.messages.consoleusage"));
				return;
			}
			ping = ((ProxiedPlayer) sender).getPing();
			sender.sendMessage(BungeeConfigUtils.getString("bungee.ping.messages.selfping").replaceAll("%ping%", this.getPingColor(ping) + String.valueOf(ping)));

		}
	}

	private ChatColor getPingColor(final int ping) {
		if(ping < 100) {
			return ChatColor.GREEN;
		} else if(ping < 200) {
			return ChatColor.GOLD;
		} else if(ping == 0) {
			return ChatColor.GRAY;
		} else {
			return ChatColor.RED;
		}
	}
}
