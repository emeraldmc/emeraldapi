package fr.tristiisch.emeraldmc.api.spigot.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerBuyKeyEvent extends PlayerEvent implements Cancellable {

	public static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	private int amount;
	private boolean cancel;

	public PlayerBuyKeyEvent(final Player who, final int amount) {
		super(who);
	}

	public int getAmount() {
		return this.amount;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	public void setAmount(final int amount) {
		this.amount = amount;
	}

	@Override
	public void setCancelled(final boolean cancel) {
		this.cancel = cancel;
	}

}
