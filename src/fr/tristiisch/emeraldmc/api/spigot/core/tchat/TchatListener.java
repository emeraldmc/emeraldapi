package fr.tristiisch.emeraldmc.api.spigot.core.tchat;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.tchat.Tchat.EmeraldTchat;
import fr.tristiisch.emeraldmc.api.spigot.utils.EmeraldServerSettings;

public class TchatListener implements Listener {

	/*
	 * Dev: Tristiisch74 Permet d'enlever les IP, les liens, les insultes, le flood,
	 * les full maj, les doubles messages...
	 */
	private final List<Pattern> regex_swear = new ArrayList<>();
	private final Pattern matchip = Pattern.compile("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(([0-9A-Fa-f]{1,4}:){0,5}:((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(::([0-9A-Fa-f]{1,4}:){0,5}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$");
	private final Pattern matchlink = Pattern.compile("[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)");
	private final Pattern flood = Pattern.compile("(.)\\1{2,}");
	//	private final Pattern symbole = Pattern.compile("(?iu)((?![a-zA-Z0-9 \\/\\\\.,;:%€^'\\\"*°#<>()[\\]{}~&!?+\\-_]).)+");

	public TchatListener() {

		// Récupère la config et convertie String => Regex
		final List<String> swear_list = EmeraldSpigot.getInstance().getConfig().getStringList("bungee.tchat.swears");
		for(String swear : swear_list) {
			String swears = "";
			String b = "\\b";
			for(int i = 0; i < swear.length(); i++) {
				if(swear.startsWith("|")) {
					b = "";
					swear = swear.substring(1);
				}
				swears += swear.charAt(i) + "+(\\W|\\d|_)*";
			}
			this.regex_swear.add(Pattern.compile("(?iu)" + b + "(" + swears + ")" + b));
		}
	}

	@EventHandler
	public void AsyncPlayerChatEvent(final AsyncPlayerChatEvent event) {
		if(event.isCancelled()) {
			return;
		}

		final Player player = event.getPlayer();
		final EmeraldServerSettings serverSettings = EmeraldSpigot.getServerSettings();

		final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();

		final EmeraldTchat emeraldTchat = Tchat.getPlayer(player.getUniqueId());

		// Si le chat est mute, cancel message
		if(serverSettings.isChatMuted()){
			if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.ADMIN)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.tchatdisablestaff")));
				return;
			}

			player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.tchatdisableplayer")));
			event.setCancelled(true);
			return;
		}

		if(emeraldPlayer.getGroup().isStaffMember()) {
			return;
		}

		final String message = event.getMessage();
		final String msgNFD = Normalizer.normalize(message, Form.NFD);
		final long currenttime = Utils.getCurrentTimeinSeconds();
		final long time = currenttime - emeraldTchat.getLastMsgTime();
		if(emeraldTchat.LastMsgIs(msgNFD) && time < 10) {
			event.setCancelled(true);
			player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.cancelspam")));
			return;
		}

		// Si le chat est slow, met un cooldown entre chaque message
		if(time < EmeraldSpigot.getInstance().getConfig().getInt("bungee.tchat.settings.timecooldown") && serverSettings.isChatSlowed()) {
			event.setCancelled(true);
			player.sendMessage(Utils.color(EmeraldSpigot.getInstance()
					.getConfig()
					.getString("bungee.tchat.messages.cancelcooldown")
					.replaceAll("%player%", player.getName())
					.replaceAll("%second%", String.valueOf(EmeraldSpigot.getInstance().getConfig().getInt("bungee.tchat.settings.timecooldown")))));
			emeraldTchat.setLastMsgTime(currenttime);
			return;
		}
		emeraldTchat.setLastMsgTime(currenttime);


		// Si le message contient des liens, cancel message
		Matcher matcher = this.matchlink.matcher(msgNFD);
		if(matcher.find()) {
			final String link = matcher.group();
			if(!EmeraldSpigot.getInstance().getConfig().getStringList("bungee.tchat.settings.linkwhitelist").stream().filter(l -> link.contains(l)).findFirst().isPresent()) {
				event.setCancelled(true);
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.cancellink")));

				Tchat.sendToStaff("Lien", player, message);
				return;
			}
		}

		// Si le message contient des ips, cancel message
		matcher = this.matchip.matcher(msgNFD);
		if(matcher.find() && Bukkit.getPlayer(matcher.group()) == null) {
			event.setCancelled(true);
			player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.cancelip")));

			Tchat.sendToStaff("IP", player, message);
			return;
		}

		// Si le message contient des insulte, cancel message
		for(final Pattern regex : this.regex_swear){
			matcher = regex.matcher(msgNFD);
			if(matcher.find() && Bukkit.getPlayer(matcher.group()) == null) {
				event.setCancelled(true);
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.cancelswear")));

				Tchat.sendToStaff("Insulte", player, message);
				return;
			}
		}

		// Si le message contient des liens, cancel message
		/*		matcher = this.symbole.matcher(msgNFD);
				if(matcher.find()) {
					event.setCancelled(true);
					final String find = matcher.group();
					player.sendMessage(Utils.color("&c➤ Merci de ne pas utiliser de caratères spéciaux tel que &r") + find + Utils.color("&c."));
					Tchat.sendToStaff("Symboles " + find, player, message);
					return;
				}*/

		// Si le message contient trop de majuscules, les enlever
		if(msgNFD.length() >= EmeraldSpigot.getInstance().getConfig().getInt("bungee.tchat.settings.blockcaps")) {
			int uppers = 0;
			for(final char c : msgNFD.toCharArray()) {
				if(Character.isUpperCase(c)) {
					++uppers;
				}
			}
			if(uppers * 1D / (msgNFD.length() * 1D) * 100D > EmeraldSpigot.getInstance().getConfig().getInt("bungee.tchat.settings.maxcaps")) {
				event.setMessage(message.toLowerCase().replaceFirst(".", (message.toLowerCase().charAt(0) + "").toUpperCase()));
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.warncaps")));
			}
		}

		// Si le message contient trop du flood, les enlever
		matcher = this.flood.matcher(message);
		if(matcher.find() && Bukkit.getPlayer(matcher.group(1)) == null) {
			/*			m = this.flood.matcher(message);
			while(m.find()) {
				String letterOrNot;
				if(m.group(1).matches("[a-zA-Z0-9]")) {
					letterOrNot = "";
				} else {
					letterOrNot = "\\";
				}
				msg = msg.replaceAll("(" + letterOrNot + m.group(1) + ")\\1{2,}", m.group(1));
				m = this.flood.matcher(message);
			}
			event.setMessage(msg);*/
			player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("bungee.tchat.messages.warnflood")));
		}
		emeraldTchat.setLastMsg(msgNFD);
	}
}
