package fr.tristiisch.emeraldmc.api.bungee.privatemessage;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public final class PrivateMessageListener implements Listener {
	@EventHandler
	public final void onTabComplete(final TabCompleteEvent event){
		final String cursor = event.getCursor();
		if(cursor.lastIndexOf("/") == -1) {
			return;
		}
		final String command = event.getCursor().substring(cursor.lastIndexOf("/"), cursor.length()).toLowerCase().replace(" ", "");

		if(!PrivateMessage.privateMessageCommand.contains(command)) {
			return;
		}

		String probabilityPlayerName = cursor.toLowerCase();

		final int lastSpaceIndex = probabilityPlayerName.lastIndexOf(' ');
		if(lastSpaceIndex >= 0) {
			probabilityPlayerName = probabilityPlayerName.substring(lastSpaceIndex + 1);
		}

		final ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayers().stream().filter(player -> player.getAddress() == event.getSender().getAddress()).findFirst().orElse(null);

		final String playerName = probabilityPlayerName;
		final ProxiedPlayer player = proxiedPlayer.getServer().getInfo().getPlayers().stream().filter(players -> players.getName().toLowerCase().startsWith(playerName)).findFirst().orElse(null);

		if(player != null) {
			event.getSuggestions().add(player.getName());
		}
	}

	@EventHandler
	public final void PlayerDisconnectEvent(final PlayerDisconnectEvent event){
		final ProxiedPlayer player = event.getPlayer();
		PrivateMessage.delReply(player);
		PrivateMessageToggleCommand.players.remove(player.getUniqueId());
	}

}
