package fr.tristiisch.emeraldmc.api.spigot.utils;

public class EmeraldServerSettings {

	private boolean chatslow;
	private boolean chatmute;

	public EmeraldServerSettings() {
		this.chatslow = true;
		this.chatmute = false;
	}

	public boolean isChatSlowed() {
		return this.chatslow;
	}

	public void setChatSlow(final boolean chatslow) {
		this.chatslow = chatslow;
	}

	public boolean isChatMuted() {
		return this.chatmute;
	}

	public void setChatMute(final boolean chatmute) {
		this.chatmute = chatmute;
	}
}
