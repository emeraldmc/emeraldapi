package fr.tristiisch.emeraldmc.api.spigot.sneak;

import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class SneakListener implements Listener {

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		final List<UUID> sneak = Sneak.getSneaks();
		sneak.remove(player.getUniqueId());
	}

	@EventHandler
	public void PlayerToggleSneakEvent(final PlayerToggleSneakEvent event) {
		final Player player = event.getPlayer();
		final List<UUID> sneak = Sneak.getSneaks();

		if(sneak.contains(player.getUniqueId())) {
			sneak.remove(player.getUniqueId());
		} else {
			sneak.add(player.getUniqueId());
		}
	}
}
