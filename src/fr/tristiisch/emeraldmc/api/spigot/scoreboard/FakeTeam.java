package fr.tristiisch.emeraldmc.api.spigot.scoreboard;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection.ClassEnum;

/**
 * Api for use teams with packet (version 1.8.X)
 * All field are verified and they works for 1.8, 1.8.8.
 * This class use Reflection from https://gist.github.com/Vinetos/6f497519f1b6465e833ac52006e9205b
 * <p>
 * To create a Team just call {@link #create()}.
 * To delete a Team just call {@link #remove()}.
 * N.B: You need to send the team of all new player. In PlayerJoinEvent for example, just add {@link #rebuildTeamPacket(Player)}.
 *
 * @author Vinetos
 */
public class FakeTeam {

	/**
	 * The NameTagVisility Parameter for the team
	 */
	public enum NameTagVisibility {
	ALWAYS("always"),
	NEVER("never"),
	HIDE_FOR_OTHER_TEAMS("hideForOtherTeams"),
	HIDE_FOR_OWN_TEAM("hideForOwnTeam");

		private final String visibility;

		NameTagVisibility(final String visibility) {
			this.visibility = visibility;
		}

		public final String getVisibility() {
			return this.visibility;
		}
	}

	/**
	 * The mode of the team (when the client receive the packet, what action it have to do)
	 */
	public enum TeamMode {

		CREATE(0),
		REMOVE(1),
		UPDATE(2),
		ADD_PLAYER(3),
		REMOVE_PLAYER(4);

		private final int mode;

		TeamMode(final int mode) {
			this.mode = mode;
		}

		public final int getMode() {
			return this.mode;
		}

	}

	// Factories
	private static final Class<?> PACKET_CLASS = Reflection.getClass(ClassEnum.NMS, "PacketPlayOutScoreboardTeam");
	// This fields haves to be updated for other version that 1.8.8 (1_8_R3)
	private static final Field TEAM_NAME = Reflection.getField(PACKET_CLASS, "a");
	private static final Field DISPLAY_NAME = Reflection.getField(PACKET_CLASS, "b");
	private static final Field PREFIX = Reflection.getField(PACKET_CLASS, "c");
	private static final Field SUFFIX = Reflection.getField(PACKET_CLASS, "d");
	private static final Field NAME_TAG_VISIBILITY = Reflection.getField(PACKET_CLASS, "e");
	private static final Field MEMBERS = Reflection.getField(PACKET_CLASS, "g");

	private static final Field TEAM_MODE = Reflection.getField(PACKET_CLASS, "h");
	private static final Field OPTIONS = Reflection.getField(PACKET_CLASS, "i");
	// Defines attributes
	private static Set<FakeTeam> teams = new HashSet<>();

	/**
	 * Get all created teams
	 *
	 * @return A {@link Set} of {@link FakeTeam}
	 */
	public static Set<FakeTeam> getRegisterTeams() {
		return Collections.unmodifiableSet(teams);
	}

	/**
	 * Get the team for a player
	 *
	 * @param player who is in a team
	 * @return The {@link FakeTeam} of the player
	 */
	public static FakeTeam getTeamOfPlayer(final Player player) {
		for(final FakeTeam team : getRegisterTeams()) {
			if(team.hasPlayer(player)) {
				return team;
			}
		}
		return null;
	}

	private final String name;
	private String displayName = "";
	private String suffix = "§r";// Remove the color for the health scoreboard for example
	private String prefix = "";

	private Set<String> players = new HashSet<>();
	private List<Player> viewers = null;
	private DyeColor teamColor;

	private boolean created = false;

	// Options of the team
	private final NameTagVisibility nameTagVisibility = NameTagVisibility.ALWAYS;

	private boolean friendlyFire = true;

	private boolean seeFriendlyInvisibles = false;

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name The name of the team (can't be changed after)
	 */
	public FakeTeam(final String name) {
		this(name, (List<Player>) null);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name      The name of the team (can't be changed after)
	 * @param teamColor The {@link DyeColor} of the team (to make a GUI for example)
	 */
	public FakeTeam(final String name, final DyeColor teamColor) {
		this(name, null, teamColor);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name    The name of the team (can't be changed after)
	 * @param viewers The players who can see the team . Set to <code>null</code> for all connected players
	 */
	public FakeTeam(final String name, final List<Player> viewers) {
		this(name, "", name, "", new HashSet<>(), viewers);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name      The name of the team (can't be changed after)
	 * @param viewers   The players who can see the team . Set to <code>null</code> for all connected players
	 * @param teamColor The {@link DyeColor} of the team (to make a GUI for example)
	 */
	public FakeTeam(final String name, final List<Player> viewers, final DyeColor teamColor) {
		this(name, "", name, "", new HashSet<>(), viewers, teamColor);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name        The name of the team (can't be changed after)
	 * @param displayName The display name of the team (on the right scoreboard for example)
	 */
	public FakeTeam(final String name, final String displayName) {
		this(name, "", displayName, "", new HashSet<>(), (List<Player>) null);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name      The name of the team (can't be changed after)
	 * @param suffix    The suffix of the team (after the player's name)
	 * @param prefix    The prefix of the team (before the player's name)
	 * @param teamColor The {@link DyeColor} of the team (to make a GUI for example)
	 */
	public FakeTeam(final String name, final String suffix, final String prefix, final DyeColor teamColor) {
		this(name, suffix, name, prefix, new HashSet<>(), null, teamColor);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name        The name of the team (can't be changed after)
	 * @param suffix      The suffix of the team (after the player's name)
	 * @param displayName The display name of the team (on the right scoreboard for example)
	 * @param prefix      The prefix of the team (before the player's name)
	 */
	public FakeTeam(final String name, final String suffix, final String displayName, final String prefix) {
		this(name, suffix, displayName, prefix, new HashSet<>(), (List<Player>) null);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name        The name of the team (can't be changed after)
	 * @param suffix      The suffix of the team (after the player's name)
	 * @param displayName The display name of the team (on the right scoreboard for example)
	 * @param prefix      The prefix of the team (before the player's name)
	 * @param teamColor   The {@link DyeColor} of the team (to make a GUI for example)
	 */
	public FakeTeam(final String name, final String suffix, final String displayName, final String prefix, final DyeColor teamColor) {
		this(name, suffix, displayName, prefix, new HashSet<>(), null, teamColor);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name        The name of the team (can't be changed after)
	 * @param suffix      The suffix of the team (after the player's name)
	 * @param displayName The display name of the team (on the right scoreboard for example)
	 * @param prefix      The prefix of the team (before the player's name)
	 * @param viewers     The players who can see the team . Set to <code>null</code> for all connected players
	 */
	public FakeTeam(final String name, final String suffix, final String displayName, final String prefix, final List<Player> viewers) {
		this(name, suffix, displayName, prefix, new HashSet<>(), viewers);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name        The name of the team (can't be changed after)
	 * @param suffix      The suffix of the team (after the player's name)
	 * @param displayName The display name of the team (on the right scoreboard for example)
	 * @param prefix      The prefix of the team (before the player's name)
	 * @param players     The players into the team
	 */
	public FakeTeam(final String name, final String suffix, final String displayName, final String prefix, final Set<String> players) {
		this(name, suffix, displayName, prefix, players, (List<Player>) null);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name        The name of the team (can't be changed after)
	 * @param suffix      The suffix of the team (after the player's name)
	 * @param displayName The display name of the team (on the right scoreboard for example)
	 * @param prefix      The prefix of the team (before the player's name)
	 * @param players     The players into the team
	 * @param teamColor   The {@link DyeColor} of the team (to make a GUI for example)
	 */
	public FakeTeam(final String name, final String suffix, final String displayName, final String prefix, final Set<String> players, final DyeColor teamColor) {
		this(name, suffix, displayName, prefix, players, null, teamColor);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name        The name of the team (can't be changed after)
	 * @param suffix      The suffix of the team (after the player's name)
	 * @param displayName The display name of the team (on the right scoreboard for example)
	 * @param prefix      The prefix of the team (before the player's name)
	 * @param players     The players into the team
	 * @param viewers     The players who can see the team . Set to <code>null</code> for all connected players
	 */
	public FakeTeam(final String name, final String suffix, final String displayName, final String prefix, final Set<String> players, final List<Player> viewers) {
		this(name, suffix, displayName, prefix, players, viewers, null);
	}

	/**
	 * Create a team with packets. Call {@link #create()} to send the to viewers players
	 *
	 * @param name        The name of the team (can't be changed after)
	 * @param suffix      The suffix of the team (after the player's name)
	 * @param displayName The display name of the team (on the right scoreboard for example)
	 * @param prefix      The prefix of the team (before the player's name)
	 * @param players     The players into the team
	 * @param viewers     The players who can see the team . Set to <code>null</code> for all connected players
	 * @param teamColor   The {@link DyeColor} of the team (to make a GUI for example)
	 */
	public FakeTeam(final String name, final String suffix, final String displayName, final String prefix, final Set<String> players, final List<Player> viewers, final DyeColor teamColor) {
		this.name = name;
		this.suffix = suffix;
		this.displayName = displayName;
		this.prefix = prefix;
		this.players = players;
		this.viewers = viewers;
		this.teamColor = teamColor;
	}

	/**
	 * Add a player to the team
	 *
	 * @param player the player who be added
	 */
	public void addPlayer(final Player player) {
		this.addPlayer(player.getName());
	}

	/**
	 * Add a player to the team
	 *
	 * @param playerName the name of player who be added
	 */
	public void addPlayer(final String playerName) {
		this.players.add(playerName);

		// Send update
		this.updatePlayer(TeamMode.ADD_PLAYER, playerName);
	}

	/**
	 * Add a player who can see the team
	 *
	 * @param player who can see the team
	 */
	public void addViewer(final Player player) {
		if(this.getViewers().contains(player)) {
			return;
		}
		this.viewers.add(player);
		// Send the team to the new viewers
		this.create(player);
	}

	/**
	 * Check if the team allow the friendly fire
	 *
	 * @return <code>true</code> if the team allow the friendly fire
	 */
	public boolean allowFriendlyFire() {
		return this.friendlyFire;
	}

	/**
	 * Construct the generic packet
	 *
	 * @param mode Mode of the team
	 * @return the packet built
	 */
	private Object constructDefaultPacket(final TeamMode mode) {
		if(mode != TeamMode.CREATE && mode != TeamMode.REMOVE && mode != TeamMode.UPDATE) {
			return null;
		}
		try {
			final Object teamPacket = PACKET_CLASS.newInstance();
			Reflection.setFieldValue(teamPacket, TEAM_NAME, this.name);
			if(mode == TeamMode.REMOVE) {
				return teamPacket;
			}

			Reflection.setFieldValue(teamPacket, DISPLAY_NAME, this.displayName);
			Reflection.setFieldValue(teamPacket, PREFIX, this.prefix);
			Reflection.setFieldValue(teamPacket, SUFFIX, this.suffix);
			Reflection.setFieldValue(teamPacket, NAME_TAG_VISIBILITY, this.nameTagVisibility.getVisibility());
			Reflection.setFieldValue(teamPacket, MEMBERS, this.players);
			Reflection.setFieldValue(teamPacket, TEAM_MODE, mode.getMode());
			Reflection.setFieldValue(teamPacket, OPTIONS, this.packOptionData());
			return teamPacket;
		} catch(final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Create a packet for adding/removing a player
	 *
	 * @param mode       mode of packet (Adding or removing)
	 * @param playerName The player who be updated
	 * @return the packet built
	 */
	private Object constructPlayerTeamPacket(final TeamMode mode, final String playerName) {
		if(mode != TeamMode.ADD_PLAYER && mode != TeamMode.REMOVE_PLAYER) {
			return null;
		}
		try {
			final Object teamPacket = PACKET_CLASS.newInstance();
			Reflection.setFieldValue(teamPacket, TEAM_NAME, this.name);
			Reflection.setFieldValue(teamPacket, MEMBERS, Collections.singleton(playerName));
			Reflection.setFieldValue(teamPacket, TEAM_MODE, mode.getMode());
			return teamPacket;
		} catch(final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Send the team packet for the targeted players
	 */
	public void create() {
		if(this.created) {
			this.update();
			return;
		}

		// Sends the team packet
		Reflection.sendPacket(this.getViewers(), this.constructDefaultPacket(TeamMode.CREATE));

		// Send players (have to be one by one)
		this.getPlayers().forEach(playerName -> Reflection.sendPacket(this.getViewers(), this.constructPlayerTeamPacket(TeamMode.ADD_PLAYER, playerName)));
		this.created = true;
	}

	/**
	 * Send the team packet for the targeted players
	 *
	 * @param player The player who will have the team created
	 */
	public void create(final Player player) {
		if(this.isViewer(player)) {
			return;
		}
		// Sends the team packet
		Reflection.sendPacket(player, this.constructDefaultPacket(TeamMode.CREATE));

		// Send players (have to be one by one)
		this.getPlayers().forEach(playerName -> Reflection.sendPacket(this.getViewers(), this.constructPlayerTeamPacket(TeamMode.ADD_PLAYER, playerName)));
		teams.add(this);
	}

	/**
	 * Delete the team for ever ! After, you can't all {@link #create()}. Be safe !
	 */
	public void delete() {
		if(this.created) {
			this.remove();
		}

		teams.remove(this);
	}

	/**
	 * Get the display name of the team
	 *
	 * @return the display name of the team
	 */
	public String getDisplayName() {
		return this.displayName;
	}

	/**
	 * Get the name of the team
	 *
	 * @return The name of the team
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Get player in the team
	 *
	 * @return A {@link Collection} of {@link String}
	 */
	public Set<String> getPlayers() {
		if(this.players == null) {
			this.players = new HashSet<>();
		}
		return Collections.unmodifiableSet(this.players);
	}

	/**
	 * Get the prefix of a team
	 *
	 * @return the prefix of the team
	 */
	public String getPrefix() {
		return this.prefix;
	}

	/**
	 * Get the suffix of a team
	 *
	 * @return the suffix of the team
	 */
	public String getSuffix() {
		return this.suffix;
	}

	/**
	 * Get the color of a team
	 *
	 * @return THe {@link DyeColor} of the team
	 */
	public DyeColor getTeamColor() {
		return this.teamColor;
	}

	/**
	 * Get players who can see the team
	 *
	 * @return A {@link List} of <code>? extends </code>{@link Player}
	 */
	public List<Player> getViewers() {
		if(this.viewers == null) {
			this.viewers = new ArrayList<>();
			Bukkit.getOnlinePlayers().forEach(player -> this.viewers.add(player));
		}
		return Collections.unmodifiableList(this.viewers);
	}

	/**
	 * Check if a player is in a team
	 *
	 * @param player the player who be checked
	 * @return <code>true</code> if the player is in the team
	 */
	public boolean hasPlayer(final Player player) {
		return this.players.contains(player.getName());
	}

	/**
	 * Check if a player is in a team
	 *
	 * @param playerName the name of player who be checked
	 * @return <code>true</code> if the player is in the team
	 */
	public boolean hasPlayer(final String playerName) {
		return this.players.contains(playerName);
	}

	/**
	 * Check if the team is created
	 *
	 * @return <code>true</code> if the team is created
	 */
	public boolean isCreated() {
		return this.created;
	}

	/**
	 * Check if a player see the team
	 *
	 * @param player the player who be testes
	 * @return <code>true</code> if the player see the team
	 */
	public boolean isViewer(final Player player) {
		return this.getViewers().contains(player);
	}

	/**
	 * Generate the int for options
	 *
	 * @return the value of options
	 */
	private int packOptionData() {
		int option = 0;

		if(this.allowFriendlyFire()) {
			option |= 1;
		}
		if(this.seeFriendlyInvisibles()) {
			option |= 2;
		}
		return option;
	}

	/**
	 * Rebuild and resend the team for a specific player
	 *
	 * @param p the player who need to get the team
	 */
	public void rebuildTeamPacket(final Player p) {
		Reflection.sendPacket(p, this.constructDefaultPacket(TeamMode.CREATE));

		// Send players (have to be one by one)
		this.getPlayers().forEach(playerName -> Reflection.sendPacket(p, this.constructPlayerTeamPacket(TeamMode.ADD_PLAYER, playerName)));

	}

	/**
	 * Remove the team for the all players. You can call {@link #create()} to re-make the team.
	 */
	public void remove() {
		if(!this.created) {
			return;
		}

		// Remove players (have to be one by one)
		this.getPlayers().forEach(playerName -> this.constructPlayerTeamPacket(TeamMode.REMOVE_PLAYER, playerName));

		// Delete the team
		Reflection.sendPacket(this.getViewers(), this.constructDefaultPacket(TeamMode.REMOVE));

		this.created = false;
	}

	/**
	 * /**
	 * Remove the team for a player. You can call {@link #create(Player)} to re-make the team.
	 *
	 * @param player The player who will have the team removed
	 */
	public void remove(final Player player) {
		if(!this.isViewer(player)) {
			return;
		}
		// Remove players (have to be one by one)
		this.getPlayers().forEach(playerName -> this.constructPlayerTeamPacket(TeamMode.REMOVE_PLAYER, playerName));

		// Delete the team
		Reflection.sendPacket(player, this.constructDefaultPacket(TeamMode.REMOVE));
	}

	/**
	 * Remove a player from a team
	 *
	 * @param player the player who be removed
	 */
	public void removePlayer(final Player player) {
		this.removePlayer(player.getName());
	}

	/**
	 * Remove a player from a team
	 *
	 * @param playerName the name player who be removed
	 */
	public void removePlayer(final String playerName) {
		this.players.remove(playerName);

		// Send update
		this.updatePlayer(TeamMode.REMOVE_PLAYER, playerName);
	}

	/**
	 * Remove a player who can see the team
	 *
	 * @param player who can see the team
	 */
	public void removeViewer(final Player player) {
		if(!this.getViewers().contains(player)) {
			return;
		}
		this.viewers.remove(player);
		// Send the team to the new viewers
		this.remove(player);
	}

	/**
	 * Check if the team can see her members invisibles
	 *
	 * @return <code>true</code> if can see her members invisibles
	 */
	public boolean seeFriendlyInvisibles() {
		return this.seeFriendlyInvisibles;
	}

	/**
	 * Set if the team can see her members invisibles
	 *
	 * @param seeFriendlyInvisibles the new value of see members invisibles
	 */
	public void setCanSeeFriendlyInvisibles(final boolean seeFriendlyInvisibles) {
		this.seeFriendlyInvisibles = seeFriendlyInvisibles;
		this.update();
	}

	/**
	 * Set the display name
	 *
	 * @param displayName The new display name
	 */
	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
		this.update();
	}

	/**
	 * Set if the team allow the friendly fire
	 *
	 * @param friendlyFire the new value of friendly fire
	 */
	public void setFriendlyFire(final boolean friendlyFire) {
		this.friendlyFire = friendlyFire;
		this.update();
	}

	/**
	 * Set the prefix of a team
	 *
	 * @param prefix the new prefix
	 */
	public void setPrefix(final String prefix) {
		this.prefix = prefix;
		this.update();
	}

	/**
	 * Set the suffix of a team
	 *
	 * @param suffix the new suffix
	 */
	public void setSuffix(final String suffix) {
		this.suffix = suffix;
		this.update();
	}

	/**
	 * Set the color for a team
	 *
	 * @param teamColor the new color of the team
	 */
	public void setTeamColor(final DyeColor teamColor) {
		this.teamColor = teamColor;
	}

	/**
	 * Update the parameters of the team
	 */
	public void update() {
		if(!this.created) {
			return;
		}

		// Send the team packet
		Reflection.sendPacket(this.getViewers(), this.constructDefaultPacket(TeamMode.UPDATE));
	}

	/**
	 * Update the parameters of the team for a player
	 *
	 * @param player The player who will have the team updated
	 */
	public void update(final Player player) {
		if(!this.isViewer(player)) {
			return;
		}
		// Send the team packet
		Reflection.sendPacket(player, this.constructDefaultPacket(TeamMode.UPDATE));
	}

	/**
	 * Add or remove a player
	 */
	public void updatePlayer(final TeamMode mode, final String playerName) {
		if(!this.created) {
			return;
		}

		// Send the new player
		Reflection.sendPacket(this.getViewers(), this.constructPlayerTeamPacket(mode, playerName));
	}

}