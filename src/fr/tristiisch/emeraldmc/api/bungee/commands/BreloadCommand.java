package fr.tristiisch.emeraldmc.api.bungee.commands;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class BreloadCommand extends Command {

	private final int power;

	public BreloadCommand(final String command, final int power) {
		super(command);
		this.power = power;
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {

		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			if(sender instanceof ProxiedPlayer) {
				final ProxiedPlayer player = (ProxiedPlayer) sender;
				if(new AccountProvider(player.getUniqueId()).getEmeraldPlayer().hasPowerLessThan(this.power)) {
					player.sendMessage(Utils.color(BungeeConfigUtils.getString("commun.messages.noperm")));
					return;
				}
			}
			BungeeConfigUtils.loadConfigs();
			sender.sendMessage(Utils.color("&6Emerald &7» &cLa config bungeecord de EmeraldAPI a été reload."));
		});
	}
}
