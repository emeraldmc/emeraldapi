package fr.tristiisch.emeraldmc.api.commons.datamanagment.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class DbConnection {

	final DbCredentials dbcredentials;
	Connection connection;

	public DbConnection(final DbCredentials dbcredentials) {
		this.dbcredentials = dbcredentials;
		this.connect();
	}

	private void connect() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection(this.dbcredentials.toURI(), this.dbcredentials.getUser(), this.dbcredentials.getPassword());
			Logger.getLogger("Minecraft").info("Connexion à la base de donnée établie");
		} catch(final SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	void close() throws SQLException {
		if(this.connection != null && !this.connection.isClosed()) {
			this.connection.close();
		}
	}

	public Connection getConnection() throws SQLException {
		if(this.connection != null && !this.connection.isClosed()) {
			return this.connection;
		}
		this.connect();
		return this.connection;
	}
}
