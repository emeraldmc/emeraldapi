package fr.tristiisch.emeraldmc.api.bungee.datamanagment;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class DataManagmentListener implements Listener {

	List<UUID> waiter = new ArrayList<>();

	@SuppressWarnings("deprecation")
	@EventHandler
	public void LoginEvent(final LoginEvent event) {
		if(event.isCancelled()) {
			return;
		}
		if(this.waiter.contains(event.getConnection().getUniqueId())) {
			event.setCancelReason(BungeeUtils.connectScreen("&cReconnection trop rapide: Merci de vous reconnecter dans quelques secondes."));
			event.setCancelled(true);
			return;
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPostLogin(final PostLoginEvent event) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {

			final ProxiedPlayer player = event.getPlayer();
			final AccountProvider accountProvider = new AccountProvider(player.getUniqueId());
			OlympaPlayer emeraldPlayer = accountProvider.getEmeraldPlayer();

			if(emeraldPlayer == null) {
				emeraldPlayer = accountProvider.createNewAccount(player.getUniqueId(), player.getName(), player.getAddress().getAddress().getHostAddress());
				System.out.println("New Account > " + player.getName() + " (" + player.getUniqueId() + ")");
			}
			if(emeraldPlayer == null) {
				player.disconnect("&cUne erreur est survenu lors du chargement de vos données. #Bungee");
				new Exception("Une erreur est survenu lors du chargement des données de " + player.getName() + " (" + player.getUniqueId() + ")").printStackTrace();
				return;
			}

			emeraldPlayer.setLastCo(Utils.getCurrentTimeinSeconds());

			final String playerName = player.getName();
			if(!emeraldPlayer.getName().equals(playerName) && !emeraldPlayer.getNameHistory().contains(playerName)) {
				MySQL.updateUsername(playerName, emeraldPlayer);
			}

			final String playerIp = player.getAddress().getAddress().getHostAddress();
			if(!emeraldPlayer.getIp().equals(playerIp)) {
				emeraldPlayer.setIpHistory(emeraldPlayer.getIpHistory().stream().distinct().collect(Collectors.toList()));
				MySQL.updateIp(playerIp, emeraldPlayer);
			}

			accountProvider.sendAccountToRedis(emeraldPlayer);
			player.addGroups(emeraldPlayer.getGroup().getName());
		});
	}

	@EventHandler
	public void PlayerDisconnectEvent(final PlayerDisconnectEvent event) {

		final ProxiedPlayer player = event.getPlayer();
		this.waiter.add(player.getUniqueId());
		ProxyServer.getInstance().getScheduler().schedule(EmeraldBungee.getInstance(), () -> {
			ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
				this.waiter.remove(player.getUniqueId());

				final AccountProvider accountProvider = new AccountProvider(player.getUniqueId());
				final OlympaPlayer emeraldPlayer = accountProvider.getEmeraldPlayer();

				emeraldPlayer.setLastCo(Utils.getCurrentTimeinSeconds());

				MySQL.savePlayer(emeraldPlayer);

				accountProvider.removeAccountFromRedis();
			});
		}, 5, TimeUnit.SECONDS);
	}
}
