package fr.tristiisch.emeraldmc.api.spigot.protocollib;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;

import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class CancelTabComplete {

	public static void enable() {
		final ProtocolManager manager = ProtocolLibrary.getProtocolManager();
		manager.addPacketListener(new PacketAdapter(EmeraldSpigot.getInstance(), new PacketType[] { PacketType.Play.Client.TAB_COMPLETE }) {

			@Override
			public void onPacketReceiving(final PacketEvent event) {
				if(event.getPacketType() == PacketType.Play.Client.TAB_COMPLETE && event.getPacket().getStrings().read(0).startsWith(
						"/") && event.getPacket().getStrings().read(0).split(" ").length == 1) {
					event.setCancelled(true);
					final List<?> list = new ArrayList<>();
					final List<?> extra = new ArrayList<>();
					final String[] tabList = new String[list.size() + extra.size()];
					for(int index = 0; index < list.size(); ++index) {
						tabList[index] = (String) list.get(index);
					}
					for(int index = 0; index < extra.size(); ++index) {
						tabList[index + list.size()] = String.valueOf('/') + (String) extra.get(index);
					}
					final PacketContainer tabComplete = manager.createPacket(PacketType.Play.Server.TAB_COMPLETE);
					tabComplete.getStringArrays().write(0, tabList);
					try {
						manager.sendServerPacket(event.getPlayer(), tabComplete);
					} catch(final InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}
}
