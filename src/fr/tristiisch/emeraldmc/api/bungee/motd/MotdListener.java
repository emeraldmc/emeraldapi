package fr.tristiisch.emeraldmc.api.bungee.motd;

import java.util.Random;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class MotdListener implements Listener {

	/* Dev: Tristiisch74
	 * Permet de mettre un motd custom
	 * Permet aussi de changer le motd si le serveur est en maintenance
	 *
	 * TODO:
	 * 	- Lier tous les textes avec la config.yml
	 *
	 */

	String motd_base = "                   §3⬣ §2§lEmeraldMC §e§l1.8 - 1.12 §3⬣\n";
	// §6Fun \u2606 Tryhard \u2606 Ranked
	String teamspeak = "§2Teamspeak: §e§nts.emeraldmc.fr";
	String site = "§2Site: §e§nhttps://www.emeraldmc.fr";
	String separator = "§6--------------------------------";

	@EventHandler
	public void onPing(final ProxyPingEvent event) {
		final ServerPing ping = event.getResponse();
		final ServerPing.Protocol version = ping.getVersion();
		version.setName("§cVersions 1.8.x à 1.12.x §l✖");
		ping.setVersion(version);
		final ServerPing.Players players = ping.getPlayers();
		final Byte status = BungeeConfigUtils.getConfig("maintenance").getByte("settings.status");
		if(status == 0) {
			players.setSample(new ServerPing.PlayerInfo[] { new ServerPing.PlayerInfo(this.separator, UUID.randomUUID()), new ServerPing.PlayerInfo("", UUID.randomUUID()), new ServerPing.PlayerInfo(
				this.teamspeak,
				UUID.randomUUID()), new ServerPing.PlayerInfo(
					this.site,
					UUID.randomUUID()), new ServerPing.PlayerInfo("", UUID.randomUUID()), new ServerPing.PlayerInfo(this.separator, UUID.randomUUID()), });

			if(new Random().nextInt(2) == 1) {
				ping.setDescriptionComponent(new TextComponent(this.motd_base + "           §b§mSkyWars§c \u00a4§b SkyFight §c\u00a4 §b§mHikaTournement"));
			} else {
				ping.setDescriptionComponent(new TextComponent(this.motd_base + "§3Twitter: §2@EmeraldMC_FR §6| §3TS: §2ts.emeraldmc.fr"));
			}

		} else if(status == 1) {
			players.setSample(new ServerPing.PlayerInfo[] { new ServerPing.PlayerInfo(this.separator, UUID.randomUUID()), new ServerPing.PlayerInfo("", UUID.randomUUID()), new ServerPing.PlayerInfo(
				"§6Raison de la maintenance :",
				UUID.randomUUID()), new ServerPing.PlayerInfo(
					Utils.color(BungeeConfigUtils.getConfig("maintenance").getString("settings.message")),
					UUID.randomUUID()), new ServerPing.PlayerInfo("", UUID.randomUUID()), new ServerPing.PlayerInfo(this.separator, UUID.randomUUID()), });
			ping.setVersion(new ServerPing.Protocol("§c§lINFOS ICI §4§l➤", ping.getVersion().getProtocol() - 1));
			ping.setDescriptionComponent(new TextComponent(this.motd_base + "               §4§l⚠ §cSERVEUR EN MAINTENANCE §4§l⚠"));
		} else {
			players.setSample(new ServerPing.PlayerInfo[] { new ServerPing.PlayerInfo(this.separator, UUID.randomUUID()), new ServerPing.PlayerInfo("", UUID.randomUUID()), new ServerPing.PlayerInfo(
				"§2Serveur en développement depuis ",
				UUID.randomUUID()), new ServerPing.PlayerInfo(
					"§2le 25 juillet 2017",
					UUID.randomUUID()), new ServerPing.PlayerInfo("", UUID.randomUUID()), new ServerPing.PlayerInfo(this.separator, UUID.randomUUID()), });
			ping.setDescriptionComponent(
				new TextComponent(this.motd_base + "                   §a§k|§c§k|§a§k|§c§k|§a§k|§c§k|§a§k|§c§k|§d Ouverture à 18h00 §a§k|§c§k|§a§k|§c§k|§a§k|§c§k|§a§k|§c§k|"));
		}
	}
}
