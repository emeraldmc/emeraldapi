package fr.tristiisch.emeraldmc.api.commons.object;

import java.util.Arrays;

import net.md_5.bungee.api.ChatColor;

public enum EmeraldGroup {

	//          id power name           prefix                                    tchatsuffix               staffmemb ? ts3 id
	ADMIN(      15, 75, "Admin",        ChatColor.DARK_RED +      "[Admin] ",      " » " + ChatColor.WHITE,    true,  21),
	RESPMODO(   14, 70, "RespModo",     ChatColor.DARK_GREEN +    "[Resp Modo] ",  " » " + ChatColor.WHITE,   true, 20),
	RESPBUILD(  13, 65, "RespBuild",    ChatColor.BLUE +          "[Resp Build] ", " » " + ChatColor.WHITE,   true,  19),
	DEVELOPPEUR(12, 60, "Développeur",  ChatColor.RED +           "[Développeur] "," » " + ChatColor.WHITE,  true,  18),
	MODERATEURP(16, 56, "Modérateur+",  ChatColor.DARK_GREEN +    "[Modérateur] ", " » " + ChatColor.WHITE, true, 7),
	MODERATEUR( 11, 55, "Modérateur",   ChatColor.DARK_GREEN +    "[Modérateur] ", " » " + ChatColor.WHITE,   true,   7),
	GUIDE(      10, 50, "Guide",        ChatColor.DARK_PURPLE +   "[Guide] ",      " » " + ChatColor.WHITE, true,   9),
	BUILDEUR(    9, 45, "Buildeur",     ChatColor.BLUE +          "[Buildeur] ",   " » " + ChatColor.WHITE,    true,  10),
	YOUTUBERP(   8, 35, "Youtuber+",    ChatColor.LIGHT_PURPLE +  "[YouTuber]+ ",  " » " + ChatColor.WHITE,  false, 11),
	YOUTUBER(    7, 30, "Youtuber",     ChatColor.LIGHT_PURPLE +  "[YouTuber] ",   " » " + ChatColor.WHITE,  false, 12),
	AMI(         6, 25, "Ami",          ChatColor.GREEN +         "[Ami] ",        " » " + ChatColor.WHITE,   false, 13),
	EMERAUDE(    5, 20, "Emeraude",     ChatColor.GOLD +          "[Emeraude] ",   " » " + ChatColor.GRAY,  false, 14),
	DIAMANT(     4, 15, "Diamant",      ChatColor.AQUA +          "[Diamant] ",    " » " + ChatColor.GRAY,   false, 15),
	SAPHIR(      3, 10, "Saphir",       ChatColor.DARK_AQUA +     "[Saphir] ",     " » " + ChatColor.GRAY,   false, 16),
	RUBIS(       2,  5, "Rubis",        ChatColor.YELLOW +        "[Rubis] ",      " » " + ChatColor.GRAY,  false, 17),
	JOUEUR(      1,  0, "Joueur",       ChatColor.GRAY +          "",              " : "                 ,    false,  8);

	final private int id;
	final private int power;
	final private String name;
	final private String prefix;
	final private String tchatsuffix;
	final private boolean staffmember;
	final private int ts3id;

	private EmeraldGroup(final int id, final int power, final String name, final String prefix, final String tchatsuffix, final boolean staffmember, final int ts3id){
		this.id = id;
		this.power = power;
		this.name = name;
		this.prefix = prefix;
		this.tchatsuffix = tchatsuffix;
		this.staffmember = staffmember;
		this.ts3id = ts3id;
	}


	public int getID(){
		return this.id;
	}

	public int getPower(){
		return this.power;
	}

	public String getName(){
		return this.name;
	}

	public ChatColor getColor() {
		return ChatColor.getByChar(this.prefix.charAt(1));
	}

	public String getPrefix(){
		return this.prefix;
	}

	public String getTchatSuffix() {
		return this.tchatsuffix;
	}

	public boolean isStaffMember() {
		return this.staffmember;
	}

	public int getTS3ID() {
		return this.ts3id;
	}

	// Methods

	public static boolean existsID(final int groupId) {
		return Arrays.stream(EmeraldGroup.values()).map(EmeraldGroup::getID).anyMatch(groupid -> groupid == groupId);
	}
	public static boolean existsName(final String groupName) {
		return Arrays.stream(EmeraldGroup.values()).map(EmeraldGroup::getName).anyMatch(groupname -> groupname.equalsIgnoreCase(groupName));
	}

	public static EmeraldGroup getByID(final int groupId){
		return Arrays.stream(EmeraldGroup.values()).filter(group -> group.getID() == groupId).findFirst().orElse(null);
	}
	public static EmeraldGroup getByName(final String groupName) {
		return Arrays.stream(EmeraldGroup.values()).filter(group -> group.getName().equalsIgnoreCase(groupName)).findFirst().orElse(null);
	}

	public static EmeraldGroup getByTSID(final int id2) {
		return Arrays.stream(EmeraldGroup.values()).filter(group -> group.getTS3ID() == id2).findFirst().orElse(null);
	}
}
