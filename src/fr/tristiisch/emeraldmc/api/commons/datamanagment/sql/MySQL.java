package fr.tristiisch.emeraldmc.api.commons.datamanagment.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;

public class MySQL {

	/**
		CREATE TABLE `server`.`players` (
				  `id` INT AUTO_INCREMENT NOT NULL,
				  `uuid` VARCHAR(36) NOT NULL,
				  `name` VARCHAR(16) NULL,
				  `group` TEXT NULL,
				  `ip` VARCHAR(16) NULL,
				  `name_history` TEXT NULL,
				  `ip_history` TEXT NULL,
				  `ts3_id` MEDIUMINT NULL,
				  `first_connection` TIMESTAMP NULL,
				  `last_connection` TIMESTAMP NULL,
				  `minerais` INT NULL,
				  `pierres` INT NULL,
				  PRIMARY KEY (`id`, `uuid`),
				  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
				  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC) VISIBLE);
	**/

	public static void createPlayer(final OlympaPlayer emeraldPlayer) {
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection()
					.prepareStatement("INSERT INTO players (uuid, name, ip, `group`, first_connection, last_connection) VALUES (?, ?, ?, ?, ?, ?);");
			pstate.setString(1, emeraldPlayer.getUniqueId().toString());
			pstate.setString(2, emeraldPlayer.getName());
			pstate.setString(3, emeraldPlayer.getIp());
			pstate.setString(4, emeraldPlayer.getGroupsToString());
			pstate.setLong(5, emeraldPlayer.getFirstCo());
			pstate.setLong(6, emeraldPlayer.getLastCo());
			pstate.executeUpdate();
		} catch(final SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Création d'une table
	 */
	public static void createTable(final String paramString) {
		if(!paramString.contains("CREATE TABLE")) {
			throw new IllegalArgumentException("\"" + paramString + "\" n'est pas le bon argument pour lire une/des valeur(s).");
		}
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			state.executeUpdate(paramString);
		} catch(final SQLException e) {
			e.printStackTrace();
		}
	}

	public static OlympaPlayer getEmeraldPlayer(final ResultSet resultSet) {
		try {
			return new OlympaPlayer(
				resultSet.getInt("id"),
				resultSet.getString("name"),
				OlympaPlayer.getIpOrNameHistoryFromString(resultSet.getString("name_history")),
				UUID.fromString(resultSet.getString("uuid")),
				resultSet.getString("ip"),
				OlympaPlayer.getIpOrNameHistoryFromString(resultSet.getString("ip_history")),
				OlympaPlayer.getGroupsFromString(resultSet.getString("group")),
				resultSet.getLong("first_connection"),
				resultSet.getLong("last_connection"),
				resultSet.getInt("pierres"),
				resultSet.getInt("minerais"),
				resultSet.getInt("ts3databaseid"));
		} catch(final SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Object[] getExactNameAndPlayerUniqueId(final String playerName) {
		final Object[] o = new Object[2];
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE name = '" + playerName + "';");
			if(resultSet.next()) {
				o[0] = resultSet.getString("name");
				o[1] = UUID.fromString(resultSet.getString("uuid"));
				return o;
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Récupère le nom exacte d'un joueur dans la base de données à l'aide de son
	 * UUID
	 */
	public static String getNameFromUUID(final UUID uuid) {
		final List<Object> list = MySQL.selectTable("SELECT name FROM players WHERE uuid = '" + uuid.toString() + "';", "name");
		if(!list.isEmpty()) {
			return list.get(0).toString();
		}
		return null;
	}

	/**
	 * Récupère la prochaine ID d'une table
	 */
	public static int getnextID(final String tableName, final String id) {
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT " + id + " FROM " + tableName + " ORDER BY id DESC LIMIT 1;");
			if(resultSet.next()) {
				return resultSet.getInt(1) + 1;
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * Permet de récupérer les donnés d'un joueur dans la base de données grâce à
	 * son id
	 */
	public static OlympaPlayer getPlayer(final int id) {
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE id = '" + id + "';");
			if(resultSet.next()) {
				return getEmeraldPlayer(resultSet);
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Permet de récupérer les donnés d'un joueur dans la base de données grâce à
	 * son pseudo
	 */
	public static OlympaPlayer getPlayer(final String playerName) {
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE name = '" + playerName + "';");
			if(resultSet.next()) {
				return getEmeraldPlayer(resultSet);
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Permet de récupérer les donnés d'un joueur dans la base de données grâce à
	 * son uuid
	 */
	public static OlympaPlayer getPlayer(final UUID playerUUID) {
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE uuid = '" + playerUUID + "';");
			if(resultSet.next()) {
				return getEmeraldPlayer(resultSet);
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Permet de récupérer les donnés d'un joueur dans la base de données grâce à
	 * son ts3databaseid
	 */
	public static OlympaPlayer getPlayerByTS3ID(final int ts3databaseid) {
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE ts3databaseid = '" + ts3databaseid + "';");
			if(resultSet.next()) {

				return getEmeraldPlayer(resultSet);
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getPlayerExactName(final String playerName) {
		final List<Object> list = MySQL.selectTable("SELECT name FROM players WHERE 'name' = " + playerName + ";", "name");
		if(list.isEmpty()) {
			return null;
		} else {
			return (String) list.get(0);
		}
	}

	public static List<OlympaPlayer> getPlayersByGroup(final EmeraldGroup group) {
		final List<OlympaPlayer> emeraldPlayers = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM server.players WHERE `group` LIKE '%" + group.getID() + "%';");
			while(resultSet.next()) {
				final OlympaPlayer emeraldPlayer = getEmeraldPlayer(resultSet);
				if(emeraldPlayer.getGroup().getID() == group.getID()) {
					emeraldPlayers.add(emeraldPlayer);
				}
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return emeraldPlayers;
	}

	public static List<OlympaPlayer> getPlayersByIp(final String ip) {
		final List<OlympaPlayer> emeraldPlayers = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE ip = '" + ip + "';");
			while(resultSet.next()) {
				emeraldPlayers.add(getEmeraldPlayer(resultSet));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return emeraldPlayers;
	}

	public static List<OlympaPlayer> getPlayersByIpHistory(final String ipHistory) {
		final List<OlympaPlayer> emeraldPlayers = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM server.players WHERE `ip_history` LIKE '%" + ipHistory + "%';");
			while(resultSet.next()) {
				emeraldPlayers.add(getEmeraldPlayer(resultSet));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return emeraldPlayers;
	}

	public static List<OlympaPlayer> getPlayersByNameHistory(final String nameHistory) {
		final List<OlympaPlayer> emeraldPlayers = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE `name_history` LIKE '%" + nameHistory + "%';");
			while(resultSet.next()) {
				emeraldPlayers.add(getEmeraldPlayer(resultSet));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return emeraldPlayers;
	}

	public static List<OlympaPlayer> getPlayersByRegex(final String regex) {
		final List<OlympaPlayer> emeraldPlayers = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE `name` REGEXP '" + regex + "';");
			while(resultSet.next()) {
				emeraldPlayers.add(getEmeraldPlayer(resultSet));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return emeraldPlayers;
	}

	public static List<OlympaPlayer> getPlayersBySimilarName(String name) {
		name = Utils.insertChar(name, "%");
		final List<OlympaPlayer> emeraldPlayers = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE `name` LIKE '" + name + "';");
			while(resultSet.next()) {
				emeraldPlayers.add(getEmeraldPlayer(resultSet));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return emeraldPlayers;
	}

	/**
	 * Récupère l'uuid d'un joueur dans la base de données à l'aide de son pseudo
	 */
	public static UUID getPlayerUniqueId(final String playerName) {
		final List<Object> list = MySQL.selectTable("SELECT uuid FROM players WHERE 'name' = " + playerName + ";", "uuid");
		if(list.isEmpty()) {
			return null;
		} else {
			return (UUID) list.get(0);
		}
	}

	/**
	 * @param ts3databaseid
	 *            ID de la base de donnés teamspeak
	 */
	public static UUID getUUIDfromTS3DatabaseID(final int ts3databaseid) {
		final List<Object> players = MySQL.selectTable("SELECT uuid FROM players WHERE ts3databaseid = '" + ts3databaseid + ";", "uuid");
		if(!players.isEmpty()) {
			return (UUID) players.get(0);
		}
		return null;
	}

	/*
	 * Permet de récupérer les donnés d'un joueur dans la base de données et de crée
	 * une ligne si il n'est pas dans la base de données
	 */
	public static OlympaPlayer initPlayer(final String playerName, final UUID playerUuid, final String playerIp, final boolean createIfNotExist) {
		final long time = Utils.getCurrentTimeinSeconds();
		OlympaPlayer emeraldPlayer = null;
		emeraldPlayer = MySQL.getPlayer(playerUuid);
		if(emeraldPlayer != null) {
			emeraldPlayer.setName(playerName);
			emeraldPlayer.setIP(playerIp);
			emeraldPlayer.setLastCo(time);
			MySQL.savePlayer(emeraldPlayer);
		} else if(createIfNotExist) {
			emeraldPlayer = new OlympaPlayer(playerName, playerUuid, playerIp, time, time);
			MySQL.createPlayer(emeraldPlayer);
		}
		return emeraldPlayer;
	}

	/**
	 * Insérer une/des valeur(s) dans une table
	 */
	public static void insertTable(final String paramString) {
		if(!paramString.contains("INSERT")) {
			throw new IllegalArgumentException("\"" + paramString + "\" n'est pas le bon argument pour lire une/des valeur(s).");
		}
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			state.executeUpdate(paramString);
		} catch(final SQLException e) {
			e.printStackTrace();
		}
	}

	public static boolean playerExist(final UUID playerUuid) {
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM players WHERE uuid = '" + playerUuid + "';");
			if(resultSet.next()) {
				return true;
			} else {
				return false;
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Sauvegarde les infos du emeraldPlayer
	 */
	public static void savePlayer(final OlympaPlayer emeraldPlayer) {
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection()
					.prepareStatement("UPDATE players SET name = ?, ip = ?, `group` = ?, last_connection = ?, pierres = ?, minerais = ?, ts3databaseid = ?, `ip_history` = ? WHERE uuid = ?;");
			int i = 1;
			pstate.setString(i++, emeraldPlayer.getName());
			pstate.setString(i++, emeraldPlayer.getIp());
			pstate.setString(i++, emeraldPlayer.getGroupsToString());
			pstate.setLong(i++, emeraldPlayer.getLastCo());
			pstate.setInt(i++, emeraldPlayer.getPierres());
			pstate.setInt(i++, emeraldPlayer.getMinerais());
			pstate.setInt(i++, emeraldPlayer.getTS3ID());
			pstate.setString(i++, String.join(";", emeraldPlayer.getIpHistory()));
			pstate.setString(i++, emeraldPlayer.getUniqueId().toString());
			pstate.executeUpdate();
		} catch(final SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Lire une/des valeur(s) dans une table
	 */
	public static List<Object> selectTable(final String paramString, final String paramString2) {

		if(!paramString.contains("SELECT")) {
			throw new IllegalArgumentException("\"" + paramString + "\" n'est pas le bon argument pour lire une/des valeur(s).");
		}
		final List<Object> result = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery(paramString);
			while(resultSet.next()) {
				result.add(resultSet.getObject(paramString2));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void updateIp(final String newIp, final OlympaPlayer emeraldPlayer) {
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection().prepareStatement("UPDATE players SET `ip` = ?, `ip_history` = ? WHERE `id` = ?;");

			int i = 1;
			pstate.setString(i++, newIp);
			pstate.setString(i++, String.join(";", emeraldPlayer.getIpHistory()));
			pstate.setInt(i++, emeraldPlayer.getId());
			pstate.executeUpdate();
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		emeraldPlayer.setIP(newIp);
	}

	/**
	 * Update une/des valeur(s) dans une table
	 */
	public static void updateTable(final String paramString) {
		if(!paramString.contains("UPDATE")) {
			throw new IllegalArgumentException("\"" + paramString + "\" n'est pas le bon argument pour lire une/des valeur(s).");
		}
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			state.executeUpdate(paramString);
		} catch(final SQLException e) {
			e.printStackTrace();
		}
	}

	public static void updateUsername(final String newName, final OlympaPlayer emeraldPlayer) {
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection().prepareStatement("UPDATE players SET `name` = ?, `name_history` = CONCAT_WS(';', ?, name_history) WHERE `id` = ?;");

			int i = 1;
			pstate.setString(i++, newName);
			pstate.setString(i++, emeraldPlayer.getName());
			pstate.setInt(i++, emeraldPlayer.getId());
			pstate.executeUpdate();
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		emeraldPlayer.setName(newName);
	}
}
