package fr.tristiisch.emeraldmc.api.spigot.core.teleport;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.Utils;

public class Teleport {

	public static boolean strictMode = false;

	public static Location argsToLoc(final Player player, final String[] args, int start) {
		if(args.length < start + 3) {
			return null;
		}
		for(int i = start; start + 3 > i; i++) {
			if(!Matcher.isDouble(args[i])) {
				return null;
			}
		}
		start += 3;
		if(args.length < start + 2) {
			int i2 = 0;
			return new Location(player.getWorld(), Double.parseDouble(args[i2++]), Double.parseDouble(args[i2++]), Double.parseDouble(args[i2++]));
		}
		for(int i = start; start + 2 > i; i++) {
			if(!Matcher.isDouble(args[i])) {
				int i2 = 0;
				return new Location(player.getWorld(), Double.parseDouble(args[i2++]), Double.parseDouble(args[i2++]), Double.parseDouble(args[i2++]));
			}
		}
		int i2 = 0;
		return new Location(
			player.getWorld(),
			Double.parseDouble(args[i2++]),
			Double.parseDouble(args[i2++]),
			Double.parseDouble(args[i2++]),
			Float.parseFloat(args[i2++]),
			Float.parseFloat(args[i2++]));
	}

	public static boolean getStrictMode() {
		return strictMode;
	}

	public static void setStrictMode(final boolean b) {
		strictMode = b;
	}

	public static void teleportPlayerToPlayer(final Player player, final Player target1, final Player target2) {
		teleportToPlayer(target1, target2);
		player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous avez téléporté &4" + target1.getName() + "&c à &4" + target2.getName() + "&c."));
	}

	public static void teleportToPlayer(final Player player, final Location location) {
		player.teleport(location);

		/*
		 * double cord = location.getX(); long inte; double decimal; inte = (long) cord;
		 * decimal = cord - inte;
		 */
		player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous avez été téléporté en &4" + location.getX() + "&c,&4" + location.getY() + "&c,&4" + location.getZ() + "&c."));
	}

	public static void teleportToPlayer(final Player player, final Player target) {
		final Location targetLocation = target.getLocation();
		player.teleport(targetLocation);
		player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous avez été téléporté à &4" + target.getName() + "&c."));
	}

}
