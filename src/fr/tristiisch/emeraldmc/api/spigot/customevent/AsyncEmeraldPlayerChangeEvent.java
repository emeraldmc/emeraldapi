package fr.tristiisch.emeraldmc.api.spigot.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;

public class AsyncEmeraldPlayerChangeEvent extends PlayerEvent implements Cancellable {

	public static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	private boolean cancel;

	private final OlympaPlayer emeraldPlayer;

	public AsyncEmeraldPlayerChangeEvent(final Player who, final OlympaPlayer emeraldplayer) {
		super(who);
		this.emeraldPlayer = emeraldplayer;
	}

	public OlympaPlayer getEmeraldPlayer() {
		return this.emeraldPlayer;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(final boolean cancel) {
		this.cancel = cancel;
	}

}
