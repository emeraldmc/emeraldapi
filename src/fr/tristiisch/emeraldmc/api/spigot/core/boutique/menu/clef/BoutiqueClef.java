package fr.tristiisch.emeraldmc.api.spigot.core.boutique.menu.clef;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.boutique.BoutiqueItem;
import fr.tristiisch.emeraldmc.api.spigot.core.boutique.BoutiqueItemInfo;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerChangeEvent;
import fr.tristiisch.emeraldmc.api.spigot.customevent.PlayerBuyKeyEvent;
import fr.tristiisch.emeraldmc.api.spigot.customevent.PlayerScoreboardReloadEvent;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;

public class BoutiqueClef {

	public static int PRICE = 3000;
	public static int PRICEx10 = 2500 * 10;

	public static void buy(final Player player, final InventoryClickEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {

			final ItemStack currentItem = event.getCurrentItem();
			int price;
			int amount;
			if(currentItem.isSimilar(BoutiqueItem.CLEF.getItemStack())) {
				price = PRICE;
				amount = 1;
			} else if(currentItem.isSimilar(BoutiqueItem.CLEFx10.getItemStack())) {
				price = PRICEx10;
				amount = 10;
			} else {
				return;
			}

			final AccountProvider accountProvider = new AccountProvider(player.getUniqueId());
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();

			if(!emeraldPlayer.hasMinerais(price)) {
				GuiTools.cancelItem(event, "&cVous n'avez pas assez de minerais.");
				return;
			}

			final PlayerBuyKeyEvent playerBuyKeyEvent = new PlayerBuyKeyEvent(player, amount);
			Bukkit.getPluginManager().callEvent(playerBuyKeyEvent);
			if(!playerBuyKeyEvent.isCancelled()) {
				GuiTools.cancelItem(event, "Vous devez acheter la clef sur le serveur &2&nSkyFight");
				return;
			}

			emeraldPlayer.removeMinerais(price);
			final AsyncEmeraldPlayerChangeEvent event3 = new AsyncEmeraldPlayerChangeEvent(player, emeraldPlayer);
			Bukkit.getPluginManager().callEvent(event3);

			String deter;
			if(amount == 1) {
				deter = "une";
			} else {
				deter = String.valueOf(amount);
			}
			player.sendMessage(Utils.color("&2EmeraldMC &7» &aVous venez d'acheter " + deter + " &2Clef de la box SkyFight&a avec " + price + " minerais !"));

			player.closeInventory();
			PlayerScoreboardReloadEvent.askReload(player);
			accountProvider.sendAccountToRedis(emeraldPlayer);
		});
	}

	public static void openClef(final Player player) {
		final GuiTools guiTools = new GuiTools("&2Boutique Minerais &7- &2Clef", "clef", 9);

		for(final BoutiqueItem boutiqueItem : BoutiqueItem.getItems(BoutiqueItemInfo.CLEF)) {
			guiTools.setItem(guiTools.getMiddleSlot() + boutiqueItem.getSlot(), boutiqueItem.getItemStack());
		}

		guiTools.openInventory(player);
	}
}
