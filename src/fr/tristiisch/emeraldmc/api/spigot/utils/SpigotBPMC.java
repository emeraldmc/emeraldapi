package fr.tristiisch.emeraldmc.api.spigot.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerChangeEvent;
import fr.tristiisch.emeraldmc.api.spigot.customevent.EmeraldServerInfoEvent;

public class SpigotBPMC implements PluginMessageListener {

	public static String DefaultChannel = "BungeeCord";
	public static String EmeraldChannel = EmeraldSpigot.getServerName();

	public static void askEmeraldPlayer(final Player player) {
		System.out.println("AskEmeraldPlayer > " + player.getName());
		sendToBungee(EmeraldChannel, "AskEmeraldPlayer", Bukkit.getServer().getServerName() + "," + player.getUniqueId());
	}

	public static void askServersStatus() {
		sendToBungee(EmeraldChannel, "AskServersStatus", Bukkit.getServer().getServerName());
	}

	public static void connect(final Player player, final String servername) {
		sendToBungee(player, DefaultChannel, "Connect", servername);
	}

	/**
	 * @deprecated NOW: not needed with redis
	 */
	@Deprecated
	public static void sendEmeraldPlayers(final List<OlympaPlayer> players) {
		sendToBungee(EmeraldChannel, "EmeraldPlayers", new Gson().toJson(players));
	}

	/**
	 * @deprecated NOW: not needed with redis
	 */
	@Deprecated
	public static void sendEmeralPlayer(final OlympaPlayer emeraldPlayer) {
		sendToBungee(EmeraldChannel, "EmeraldPlayer", emeraldPlayer.toJson());
		/* System.out.println("sendEmeraldPlayer > " + emeraldPlayer.getName()); */
	}

	public static void sendToBungee(final Player player, final String channel, final String subchannel, final String msg) {
		final ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(subchannel);
		out.writeUTF(msg);
		player.sendPluginMessage(EmeraldSpigot.getInstance(), channel, out.toByteArray());
	}

	public static void sendToBungee(final String subchannel, final String msg) {
		sendToBungee(EmeraldChannel, subchannel, msg);
	}

	public static void sendToBungee(final String channel, final String subchannel, final String msg) {
		final ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(subchannel);
		out.writeUTF(msg);
		Bukkit.getServer().sendPluginMessage(EmeraldSpigot.getInstance(), channel, out.toByteArray());
	}

	@Override
	public void onPluginMessageReceived(final String channel, final Player nul, final byte[] message) {
		if(channel.equalsIgnoreCase(EmeraldChannel)) {
			final ByteArrayDataInput in = ByteStreams.newDataInput(message);
			final String subchannel = in.readUTF();
			String response;

			switch(subchannel) {

			/*
			 * case "EmeraldPlayer": response = in.readUTF(); final EmeraldPlayer
			 * emeraldPlayer = EmeraldPlayer.fromJson(response);
			 * if(!ConnectionsUtils.isWaiting(emeraldPlayer.getUniqueId())) { return; }
			 * ConnectionsUtils.removeWaiting(Bukkit.getPlayer(emeraldPlayer.getUniqueId()))
			 * ; System.out.println("EmeraldPlayer < " + emeraldPlayer.getName());
			 * EmeraldPlayers.addPlayer(emeraldPlayer); final EmeraldPlayerLoad event2 = new
			 * EmeraldPlayerLoad(Bukkit.getPlayer(emeraldPlayer.getUniqueId()),
			 * emeraldPlayer);
			 * EmeraldSpigot.getInstance().getServer().getPluginManager().callEvent(event2);
			 * break;
			 */

			case "EmeraldPlayerChange":
				response = in.readUTF();
				final UUID uuid = UUID.fromString(response);
				Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
					final OlympaPlayer before = new AccountProvider(uuid).getEmeraldPlayer();
					final AsyncEmeraldPlayerChangeEvent event = new AsyncEmeraldPlayerChangeEvent(Bukkit.getPlayer(uuid), before);
					Bukkit.getPluginManager().callEvent(event);
				});
				break;

			case "ServersStatus":
				response = in.readUTF();
				final List<EmeraldServer> emeraldServers = new Gson().fromJson(response, new TypeToken<ArrayList<EmeraldServer>>() {
				}.getType());
				EmeraldServers.setServers(emeraldServers);
				final EmeraldServerInfoEvent event3 = new EmeraldServerInfoEvent(emeraldServers);
				Bukkit.getPluginManager().callEvent(event3);
				break;

			case "test":
				response = in.readUTF();
				System.out.println("Test reçu: " + response);
				break;

			default:
				response = in.readUTF();
				System.out.println("§cMessage BPMC perdu: " + subchannel + " > " + response);
				break;
			}
		} else if(channel.equalsIgnoreCase(DefaultChannel)) {

		}
	}

}
