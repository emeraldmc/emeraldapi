package fr.tristiisch.emeraldmc.api.bungee.group;

import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeBPMC;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeeGroup {

	/**
	 * @params field Calendar.MONTH
	 *
	 */
	@SuppressWarnings("deprecation")
	public static void addGroup(final ProxiedPlayer proxiedPlayer, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer, final EmeraldGroup newGroup, final int field, final int amount) {
		emeraldPlayer.addGroup(newGroup, Utils.addTimeToCurrentTime(field, amount));
		if(proxiedPlayer != null) {
			proxiedPlayer.sendMessage(BungeeConfigUtils.getString("bungee.group.messages.playerchangegrouptoplayer").replace("%group%", newGroup.getName()));
		}
		updateEmeraldPlayer(proxiedPlayer, accountProvider, emeraldPlayer);
	}

	@SuppressWarnings("deprecation")
	public static void addGroup(final ProxiedPlayer proxiedPlayer, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer, final EmeraldGroup newGroup, final long timestamp) {
		emeraldPlayer.addGroup(newGroup, timestamp);

		if(proxiedPlayer != null) {
			proxiedPlayer.sendMessage(BungeeConfigUtils.getString("bungee.group.messages.playerchangegrouptoplayer").replace("%group%", newGroup.getName()));
		}

		updateEmeraldPlayer(proxiedPlayer, accountProvider, emeraldPlayer);
	}

	public static void removeGroup(final ProxiedPlayer proxiedPlayer, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer, final EmeraldGroup newGroup) {
		emeraldPlayer.removeGroup(newGroup);
		if(emeraldPlayer.getGroups().size() == 0) {
			emeraldPlayer.setGroups(EmeraldGroup.JOUEUR, 0);
		}
		updateEmeraldPlayer(proxiedPlayer, accountProvider, emeraldPlayer);
	}

	public static void setGroup(final ProxiedPlayer proxiedPlayer, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer, final EmeraldGroup newGroup, final long timestamp) {
		emeraldPlayer.setGroups(newGroup, timestamp);
		updateEmeraldPlayer(proxiedPlayer, accountProvider, emeraldPlayer);
	}

	private static void updateEmeraldPlayer(final ProxiedPlayer proxiedPlayer, final AccountProvider accountProvider, final OlympaPlayer emeraldPlayer) {
		if(proxiedPlayer != null) {
			for(final String group : proxiedPlayer.getGroups()) {
				proxiedPlayer.removeGroups(group);
			}
			proxiedPlayer.addGroups(emeraldPlayer.getGroup().getName());
			accountProvider.sendAccountToRedis(emeraldPlayer);
			BungeeBPMC.sendEmeraldPlayerChange(proxiedPlayer);
		} else {
			MySQL.savePlayer(emeraldPlayer);
		}
	}
}
