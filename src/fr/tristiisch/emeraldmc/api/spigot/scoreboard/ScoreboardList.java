package fr.tristiisch.emeraldmc.api.spigot.scoreboard;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public class ScoreboardList {

	private String prefix;
	private String suffix;
	private Team team;
	public static Scoreboard scoreboard;

	public ScoreboardList(final String name, final String prefix, final String suffix, final Scoreboard current) throws Exception {
		this.prefix = prefix;
		this.suffix = suffix;
		this.team = current.getTeam(name);
		/*
		 * this.team = FakeTeam.getRegisterTeams().stream().filter(t ->
		 * t.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
		 */
		if(this.team == null){
			/*
			 * this.team = new FakeTeam(name); this.team.create();
			 */
			this.team = current.registerNewTeam(name);
		}
		scoreboard = current;
		this.team.setCanSeeFriendlyInvisibles(false);

		/* VERIFICATION */
		int prefixLength = 0;
		int suffixLength = 0;
		if (prefix != null) {
			prefixLength = prefix.length();
		}
		if (suffix != null) {
			suffixLength = suffix.length();
		}

		if(prefixLength + suffixLength >= 32) {
			throw new Exception("prefix and suffix lenghts are greater than 16");
		}

		if (suffix != null) {
			this.team.setSuffix(Utils.color(suffix));
		}
		if (prefix != null) {
			this.team.setPrefix(Utils.color(prefix));
		}
	}

	public ScoreboardList(final String name, final String prefix, final String suffix) throws Exception {
		this(name, prefix, suffix, Bukkit.getScoreboardManager().getMainScoreboard());
	}

	@SuppressWarnings("deprecation")
	public void set(final Player player){
		this.team.addPlayer(player);
		player.setScoreboard(scoreboard);
	}

	@SuppressWarnings("deprecation")
	public void remove(final Player player){
		this.team.removePlayer(player);
	}

	public void resetTagUtils(final UUID uuid) {
		this.remove(Bukkit.getPlayer(uuid));
	}

	public void setAll(final Collection<Player> players) {
		for (final Player player : players) {
			this.set(player);
		}
	}

	public void setAll(final Player[] players) {
		Player[] arrayOfPlayer;
		final int j = (arrayOfPlayer = players).length;
		for (int i = 0; i < j; i++) {
			final Player player = arrayOfPlayer[i];
			this.set(player);
		}
	}

	public void setPrefix(final String prefix) {
		this.prefix = ChatColor.translateAlternateColorCodes('&', prefix);
		this.team.setPrefix(this.prefix);
	}

	public void setSuffix(final String suffix) {
		this.suffix = ChatColor.translateAlternateColorCodes('&', suffix);
		this.team.setSuffix(this.suffix);
	}

	public String getPrefix() {
		return this.prefix;
	}

	public String getSuffix() {
		return this.suffix;
	}

	public void removeTeam() {
		/* this.team.delete(); */
		this.team.unregister();
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}

	public static void setNameTag(final Player player, String name, final String prefix) {
		try{
			name += player.getName().toLowerCase().substring(1, 3);
			final ScoreboardList ttm = new ScoreboardList(name, prefix, null);
			ttm.set(player);
		}catch (final Exception e){
			e.printStackTrace();
		}
	}

	public static void setNameTag(final Player player, String name, final String prefix, final String suffix) {
		try {
			name += player.getName().toLowerCase().substring(1, 3);
			final ScoreboardList ttm = new ScoreboardList(name, prefix, suffix);
			ttm.set(player);
		} catch(final Exception e) {
			e.printStackTrace();
		}
	}


}