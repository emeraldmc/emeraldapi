package fr.tristiisch.emeraldmc.api.commons.usernamehistory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Usernames {

	public static Username getLastUsername(final UUID uuid) {
		return loadHistory(uuid).get(0);
	}

	public static List<Username> loadHistory(final OlympaPlayer emeraldPlayer) {
		try {
			final String url = "https://api.mojang.com/user/profiles/" + emeraldPlayer.getUniqueId().toString().replace("-", "") + "/names";
			final URL url2 = new URL(url);
			final InputStreamReader reader = new InputStreamReader(url2.openStream());

			final Type listType = new TypeToken<ArrayList<Username>>() {
			}.getType();
			final List<Username> usernames = new Gson().fromJson(reader, listType);
			updateUsername(usernames.get(0), emeraldPlayer);
			return usernames;

		} catch(final IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	public static List<Username> loadHistory(final UUID uuid) {
		try {
			final String url = "https://api.mojang.com/user/profiles/" + uuid.toString().replace("-", "") + "/names";
			final URL url2 = new URL(url);
			final InputStreamReader reader = new InputStreamReader(url2.openStream());

			final Type listType = new TypeToken<ArrayList<Username>>() {
			}.getType();
			final List<Username> usernames = new Gson().fromJson(reader, listType);
			updateUsername(usernames.get(0), uuid);
			return usernames;

		} catch(final IOException e) {
			e.printStackTrace();
			return null;
		}

		//https://api.mojang.com/user/profiles/ca0a166316964d62b93f281965522a76/names
		/*	final ArrayList<Username> history = new ArrayList<>();
				final JsonElement response = Utils.getJsonObject(url);
				if(response == null) {
					return null;
				}
				for(final JsonElement obj : response.getAsJsonArray()) {
					if(obj.containsKey("error")){
						return null;
					}
					final String name = (String) obj.get("name");
					long changedToAt = 0;
					if(obj.containsKey("changedToAt")){
						changedToAt = (Long) obj.get("changedToAt");
					}
					final Username usrn = new Username(name, changedToAt);
					history.add(usrn);

				}
				return Usernames;*/
	}

	private static void updateUsername(final Username username, final OlympaPlayer emeraldPlayer) {
		if(emeraldPlayer.getName().equals(username.getName())) {
			return;
		}
		emeraldPlayer.setName(username.getName());
	}

	public static String updateUsername(final Username username, final UUID uuid) {
		final ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(uuid);
		if(proxiedPlayer != null) {
			return null;
		}

		final OlympaPlayer emeraldPlayer = MySQL.getPlayer(uuid);

		if(emeraldPlayer.getName().equals(username.getName())) {
			return null;
		}
		emeraldPlayer.setName(username.getName());
		MySQL.savePlayer(emeraldPlayer);
		return username.getName();
	}
}
