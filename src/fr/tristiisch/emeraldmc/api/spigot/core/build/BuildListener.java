package fr.tristiisch.emeraldmc.api.spigot.core.build;

import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class BuildListener implements Listener {

	private static boolean canceldrop = true;

	public static void disableCancelDrop() {
		canceldrop = false;
	}

	@EventHandler
	private void PlayerPickupItemEvent(final PlayerPickupItemEvent event) {
		if(event.isCancelled() || !canceldrop) {
			return;
		}
		if(!this.canI(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	private void PlayerDropItemEvent(final PlayerDropItemEvent event) {
		if(event.isCancelled() || !canceldrop) {
			return;
		}
		if(!this.canI(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	private void BlockBreakEvent(final BlockBreakEvent event) {
		if(event.isCancelled()) {
			return;
		}
		final Player player = event.getPlayer();
		if(!this.canI(event.getPlayer())) {
			player.playSound(player.getLocation(), Sound.VILLAGER_NO, 1, 1);
			event.setCancelled(true);
		}
	}

	@EventHandler
	private void BlockPlaceEvent(final BlockPlaceEvent event) {
		if(event.isCancelled()) {
			return;
		}
		final Player player = event.getPlayer();
		if(!this.canI(event.getPlayer())) {
			player.playSound(player.getLocation(), Sound.VILLAGER_NO, 1, 1);
			event.setCancelled(true);
		}
	}

	@EventHandler
	private void PlayerInteractEntityEvent(final PlayerInteractEntityEvent event) {
		if(event.isCancelled()) {
			return;
		}
		if(!this.canI(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	private void EntityDamageByEntityEvent(final EntityDamageByEntityEvent event) {
		if(event.isCancelled()) {
			return;
		}
		if(event.getDamager() instanceof Player) {
			if(!(event.getEntity() instanceof Player)) {
				if(!this.canI((Player) event.getDamager())) {
					((Player) event.getDamager()).updateInventory();
					event.setCancelled(true);
				}
			}
		} else if(!(event.getEntity() instanceof Player)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void HangingPlaceEvent(final HangingPlaceEvent event) {
		if(event.isCancelled()) {
			return;
		}
		if(!this.canI(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void HangingBreakEvent(final HangingBreakByEntityEvent event) {
		if(event.isCancelled()) {
			return;
		}
		if(!(event.getEntity() instanceof Player) || !this.canI((Player) event.getEntity())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void PlayerEggThrowEvent(final PlayerEggThrowEvent event) {
		if(!event.isHatching()) {
			return;
		}
		if(!this.canI(event.getPlayer())) {
			event.setHatching(false);
		}
	}

	private boolean canI(final Player player) {
		return player.getGameMode() == GameMode.CREATIVE && Build.isBuildModeEnable(player);
	}
}
