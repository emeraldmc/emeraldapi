package fr.tristiisch.emeraldmc.api.spigot.anticheat.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public class FlyListener implements Listener{

	@EventHandler
	public void PlayerMoveEvent(final PlayerMoveEvent event) {
		final Location to = event.getTo();
		final Location from = event.getFrom();

		final Vector vec = new Vector(to.getX(), to.getY(), to.getZ());

		final double i = vec.distance(new Vector(from.getX(), from.getY(), from.getZ()));
		final Player player = event.getPlayer();
		if(!player.isSprinting()) {
			if(i > .23) {
				/* event.setCancelled(true); */
				final String msg = Utils.color("&c✦ &2BetaFly &7» &4%pseudo% " + String.valueOf(i) + "").replace("%pseudo%", player.getName());
				final Player staff = Bukkit.getPlayer("Tristiisch");
				if(staff == null) {
					return;
				}
				staff.sendMessage(msg);
			}

		} else {
			if(i > .48) {
				/* event.setCancelled(true); */
				final String msg = Utils.color("&c✦ &2BetaFly &7» &4%pseudo% (sprint) " + String.valueOf(i) + "").replace("%pseudo%", player.getName());
				final Player staff = Bukkit.getPlayer("Tristiisch");
				if(staff == null) {
					return;
				}
				staff.sendMessage(msg);
			}
		}
	}
}
