package fr.tristiisch.emeraldmc.api.spigot.core.invsee;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class InvseeCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	public static void openInv(final Player player, String targetName) {
		final OpenInventoryCommandEvent openInventoryCommandEvent = new OpenInventoryCommandEvent(player, targetName);
		Bukkit.getPluginManager().callEvent(openInventoryCommandEvent);
		if(openInventoryCommandEvent.isCancelled()) {
			return;
		}
		if(!openInventoryCommandEvent.isTargetConnected()) {

			final OfflinePlayer targetOffline = Bukkit.getOfflinePlayer(openInventoryCommandEvent.getTargetName());

			targetName = targetOffline != null ? targetOffline.getName() : targetName;
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.".replaceAll("%player%", targetOffline.getName())));
			return;
		}
		player.openInventory(openInventoryCommandEvent.getTarget().getInventory());
	}

	private final int power;

	public InvseeCommand(final EmeraldGroup group) {
		this.power = group.getPower();
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {

			final Player player = (Player) sender;
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(!emeraldPlayer.hasPower(EmeraldGroup.MODERATEURP) && emeraldPlayer.hasPowerLessThan(this.power)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}

			if(args.length == 0) {
				player.sendMessage(Utils.color("&cUsage &7» &c/" + label.toLowerCase() + " <joueur>"));
				return;
			}

			if(player.getName().equalsIgnoreCase(args[0])) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous ne pouvez pas utiliser cette commande sur vous-même."));
				return;
			}
			openInv(player, args[0]);
		});
		return true;
	}

}
