package fr.tristiisch.emeraldmc.api.spigot.core.verif;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.anticheat.PlayerCheat;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;

public class VerifCommand implements CommandExecutor {

	private final int power;

	public VerifCommand(final int power) {
		this.power = power;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String arg, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final Player player = (Player) sender;
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerLessThan(this.power)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}

			if(args.length == 0) {
				player.sendMessage(Utils.color("&cUsage &7» &c/verif <joueur>"));
				return;
			}

			final Player target = Bukkit.getPlayer(args[0]);
			if(target == null) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.".replaceAll("%player%", args[0])));
				return;
			}

			if(SpigotUtils.isSamePlayer(player, target) && emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous ne peuvez pas vous vérifier vous-même."));
				return;
			}
			openVerifGUi(player, target);
		});
		return true;
	}

	public static void openVerifGUi(final Player player, final Player target) {
		final Inventory i = Bukkit.createInventory(null, PlayerCheat.guiSize, Utils.color(PlayerCheat.InventoryName + target.getName()));
		PlayerCheat.verifier.put(player, PlayerCheat.getByPlayer(target));
		player.openInventory(i);
	}
}
