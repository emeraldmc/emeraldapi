package fr.tristiisch.emeraldmc.api.spigot.motd;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer.EmeraldServerStatus;
import fr.tristiisch.emeraldmc.api.spigot.utils.TPS;

public class MotdListener implements Listener {
	
	@EventHandler
	public void ServerListPingEvent(ServerListPingEvent event) {
		event.setMotd(new EmeraldServer(TPS.getTPS(), EmeraldServerStatus.OPEN).toJson());
	}
}
