package fr.tristiisch.emeraldmc.api.spigot.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldConsole;
import net.minecraft.server.v1_8_R3.MobEffect;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityEffect;
import net.minecraft.server.v1_8_R3.PacketPlayOutRemoveEntityEffect;

public class SpigotUtils {

	public static void addNightVision(final Player player) {
		final PacketPlayOutEntityEffect packet = new PacketPlayOutEntityEffect(player.getEntityId(), new MobEffect(16, 0, 0, false, false));
		Reflection.sendPacket(player, packet);
	}

	public static Location addYToLocation(final Location location, final float y) {
		return new Location(location.getWorld(), location.getX(), location.getY() + 1, location.getZ(), location.getYaw(), location.getPitch());
	}

	public static List<Location> getBlockAround(final Location location, final int raduis) {
		final List<Location> locations = new ArrayList<>();
		for(int x = raduis; x >= -raduis; x--) {
			for(int y = raduis; y >= -raduis; y--) {
				for(int z = raduis; z >= -raduis; z--) {
					locations.add(location.getBlock().getRelative(x, y, z).getLocation());
				}
			}
		}
		return locations;
	}

	public static short getDataValueByChatColor(final ChatColor color) {
		switch(color) {

		case BLACK:
			return 15;
		case DARK_BLUE:
			return 11;
		case DARK_GREEN:
			return 13;
		case DARK_AQUA:
			return 9;
		case DARK_RED:
			return 14;
		case DARK_PURPLE:
			return 10;
		case GOLD:
			return 1;
		case GRAY:
			return 8;
		case DARK_GRAY:
			return 9;
		case BLUE:
			return 11;
		case GREEN:
			return 5;
		case AQUA:
			return 3;
		case RED:
			return 14;
		case LIGHT_PURPLE:
			return 2;
		case YELLOW:
			return 4;
		default:
			return 0;
		}
	}

	public static short getDataValueByChatColor(final net.md_5.bungee.api.ChatColor color) {
		switch(color) {
		case BLACK:
			return 15;
		case DARK_BLUE:
			return 11;
		case DARK_GREEN:
			return 13;
		case DARK_AQUA:
			return 9;
		case DARK_RED:
			return 14;
		case DARK_PURPLE:
			return 10;
		case GOLD:
			return 1;
		case GRAY:
			return 8;
		case DARK_GRAY:
			return 9;
		case BLUE:
			return 11;
		case GREEN:
			return 5;
		case AQUA:
			return 3;
		case RED:
			return 14;
		case LIGHT_PURPLE:
			return 2;
		case YELLOW:
			return 4;
		default:
			return 0;
		}
	}

	public static String getExactName(final String name) {
		if(name.equalsIgnoreCase(EmeraldConsole.getName())) {
			return EmeraldConsole.getName();
		}
		final Player player = Bukkit.getPlayer(name);
		if(player != null) {
			return player.getName();
		}
		return MySQL.getPlayerExactName(name);
	}

	public static Location getFirstBlockUnderPlayer(final Player player) {
		Location location = player.getLocation();
		do {
			if(location.getBlockY() == 0) {
				return null;
			}
			location = new Location(location.getWorld(), location.getBlockX(), location.getBlockY() - 1, location.getBlockZ());
		} while(location.getBlock().getType() == Material.AIR);
		return location;
	}

	public static String getName(final UUID uuid) {
		if(uuid.equals(EmeraldConsole.getUniqueId())) {
			return EmeraldConsole.getName();
		}
		final Player player = Bukkit.getPlayer(uuid);
		if(player != null) {
			return player.getName();
		}
		return MySQL.getNameFromUUID(uuid);
	}

	public static boolean hasEnoughPlace(final Inventory inventory, final ItemStack... items) {
		final Inventory inventory2 = Bukkit.createInventory(null, inventory.getSize());
		inventory2.setContents(inventory.getContents());
		int amount1 = 0;
		for(final ItemStack item : inventory2.getContents()) {
			if(item != null) {
				amount1 += item.getAmount();
			}
		}

		int amount2 = 0;
		for(final ItemStack item : items) {
			amount2 += item.getAmount();
		}

		final int amount3 = amount1 + amount2;
		inventory2.addItem(items);

		int amount4 = 0;
		for(final ItemStack item : inventory2.getContents()) {
			if(item != null) {
				amount4 += item.getAmount();
			}
		}

		if(amount4 == amount3) {
			return true;
		}

		return false;
	}

	/*
	 * public static EmeraldPlayer getPlayer(final Player player) { final
	 * EmeraldPlayer emeraldPlayer = EmeraldPlayers.getPlayer(player.getUniqueId());
	 * if(emeraldPlayer != null) { return emeraldPlayer; } return
	 * MySQL.getPlayer(player.getUniqueId()); }
	 */

	public static boolean isOnGround(final Player player) {
		Location location = player.getLocation();
		location = new Location(location.getWorld(), location.getBlockX(), location.getBlockY() - 1, location.getBlockZ());
		return location.getBlock().getType() == Material.AIR;
	}

	public static boolean isSamePlayer(final Player player, final Player target) {
		return player.getUniqueId().equals(target.getUniqueId());
	}
	/*
	 * public static EmeraldPlayer getPlayer(final UUID playerUuid) {
	 * if(playerUuid.equals(EmeraldConsole.getUniqueId())) { return null; } final
	 * EmeraldPlayer emeraldPlayer = EmeraldPlayers.getPlayer(playerUuid);
	 * if(emeraldPlayer != null) { return emeraldPlayer; } return
	 * MySQL.getPlayer(playerUuid); } public static EmeraldPlayer getPlayer(final
	 * String playerName) { if(playerName.equals(EmeraldConsole.getName())) { return
	 * null; } final EmeraldPlayer emeraldPlayer = ProxyServer.getInstance()
	 * .getPlayers() .stream() .filter(p -> p.getName().equals(playerName)) .map(p
	 * -> EmeraldPlayers.getPlayer(p.getUniqueId())) .findFirst() .orElse(null);
	 * if(emeraldPlayer != null) { return emeraldPlayer; } return
	 * MySQL.getPlayer(playerName); }
	 */

	public static Color translateChatColorToColor(final ChatColor chatColor) {
		switch(chatColor) {
		case AQUA:
			return Color.AQUA;
		case BLACK:
			return Color.BLACK;
		case BLUE:
			return Color.BLUE;
		case DARK_AQUA:
			return Color.BLUE;
		case DARK_BLUE:
			return Color.BLUE;
		case DARK_GRAY:
			return Color.GRAY;
		case DARK_GREEN:
			return Color.GREEN;
		case DARK_PURPLE:
			return Color.PURPLE;
		case DARK_RED:
			return Color.RED;
		case GOLD:
			return Color.YELLOW;
		case GRAY:
			return Color.GRAY;
		case GREEN:
			return Color.GREEN;
		case LIGHT_PURPLE:
			return Color.PURPLE;
		case RED:
			return Color.RED;
		case WHITE:
			return Color.WHITE;
		case YELLOW:
			return Color.YELLOW;
		default:
			break;
		}
		return null;
	}

	public static Color translateChatColorToColor(final net.md_5.bungee.api.ChatColor chatColor) {
		switch(chatColor) {
		case AQUA:
			return Color.AQUA;
		case BLACK:
			return Color.BLACK;
		case BLUE:
			return Color.BLUE;
		case DARK_AQUA:
			return Color.BLUE;
		case DARK_BLUE:
			return Color.BLUE;
		case DARK_GRAY:
			return Color.GRAY;
		case DARK_GREEN:
			return Color.GREEN;
		case DARK_PURPLE:
			return Color.PURPLE;
		case DARK_RED:
			return Color.RED;
		case GOLD:
			return Color.YELLOW;
		case GRAY:
			return Color.GRAY;
		case GREEN:
			return Color.GREEN;
		case LIGHT_PURPLE:
			return Color.PURPLE;
		case RED:
			return Color.RED;
		case WHITE:
			return Color.WHITE;
		case YELLOW:
			return Color.YELLOW;
		default:
			break;
		}
		return null;
	}

	public static String translateEffects(String s) {
		s = WordUtils.capitalize(s.toLowerCase()).replaceAll("_", " ");
		switch(s) {
		case "Absorption":
			return "Absorption";
		case "Blindness":
			return "aveuglement";
		case "Confusion":
			return "Nausée";
		case "Damage resistance":
			return "Résistance";
		case "Speed":
			return "Vitesse";
		case "Slowness":
			return "Lenteur";
		case "Strength":
			return "Force";
		case "Instant health":
			return "Soin instantané";
		case "Instant damage":
			return "Dégats instatanée";
		case "Regeneration":
			return "Régénération";
		case "Water breathing":
			return "Apnée";
		case "Invisibility":
			return "Invisibilité";
		case "Weakness":
			return "Faiblesse";
		case "Fire resistance":
			return "Résistance au feu";
		case "Night vision":
			return "Vision nocturne";
		case "Jump":
			return "Sauts améliorés";
		default:
			return s;
		}
	}

	public void removeNightVision(final Player player) {
		final PacketPlayOutRemoveEntityEffect packet = new PacketPlayOutRemoveEntityEffect(player.getEntityId(), new MobEffect(16, 0, 1, false, false));
		Reflection.sendPacket(player, packet);
	}

}
