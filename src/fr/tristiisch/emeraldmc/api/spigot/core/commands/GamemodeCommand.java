package fr.tristiisch.emeraldmc.api.spigot.core.commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class GamemodeCommand implements CommandExecutor {

	final private HashMap<GameMode, List<String>> gamemodes = new HashMap<>();
	private static int[] power;

	public GamemodeCommand(final EmeraldGroup... groups) {
		this.gamemodes.put(GameMode.SURVIVAL, Arrays.asList("survival", "survie", "0", "s"));
		this.gamemodes.put(GameMode.CREATIVE, Arrays.asList("creative", "creatif", "1", "c"));
		this.gamemodes.put(GameMode.ADVENTURE, Arrays.asList("adventure", "aventure", "2", "a"));
		this.gamemodes.put(GameMode.SPECTATOR, Arrays.asList("spectator", "spectateur", "3", "sp", "spec"));
		setPower(groups);
	}

	public static void setPower(final EmeraldGroup... groups) {
		power = Arrays.stream(groups).mapToInt(EmeraldGroup::getPower).toArray();
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			Player player = null;
			OlympaPlayer emeraldPlayer = null;
			GameMode gamemode = this.getGamemodeLabel(label);
			GameMode gamemodeOld;
			if(sender instanceof Player) {
				player = (Player) sender;
				emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(!emeraldPlayer.hasPower(power)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}

				if(gamemode != null) {
					gamemodeOld = player.getGameMode();
					if(gamemode == gamemodeOld) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous êtes déjà en &4%gamemode%&c.".replaceAll("%gamemode%", this.getGamemodeName(gamemode))));
						return;
					}
					final Player player2 = (Player) sender;
					final GameMode gamemode2 = gamemode;
					Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> player2.setGameMode(gamemode2));
					player.sendMessage(Utils.color("&2EmeraldMC &7» &aGamemode &2%oldgamemode% &achangé en &2%newgamemode%&a.".replaceAll("%oldgamemode%", this.getGamemodeName(gamemodeOld))
							.replaceAll("%newgamemode%", this.getGamemodeName(gamemode))));
					return;
				}
			}

			Player target;
			if(args.length >= 2) {

				if(emeraldPlayer != null && !emeraldPlayer.hasPower(EmeraldGroup.ADMIN)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}

				target = Bukkit.getPlayer(args[1]);
				if(target == null) {
					sender.sendMessage(Utils.color("&2EmeraldMC &7» &cJoueur &4%player% &cintrouvable.".replaceAll("%player%", args[1])));
					return;
				}
				gamemodeOld = target.getGameMode();
				gamemode = this.getGamemode(args[0]);
				if(gamemode == null) {
					sender.sendMessage(Utils.color("&2EmeraldMC &7» &cGamemode %gamemode% inconnu.".replaceAll("%gamemode%", args[0])));
					return;
				} else if(gamemode == gamemodeOld) {
					sender.sendMessage(Utils
							.color("&2EmeraldMC &7» &cJoueur &4%player%&c est déjà en &4%gamemode%&c.".replaceAll("%player%", target.getName()).replaceAll("%gamemode%", this.getGamemodeName(gamemode))));
					return;
				}

				final Player target2 = target;
				final GameMode gamemode2 = gamemode;
				Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> target2.setGameMode(gamemode2));
				target.sendMessage(Utils.color("&2EmeraldMC &7» &aGamemode &2%oldgamemode% &achangé en &2%newgamemode%&a par &2%player%.".replaceAll("%player%", sender.getName())
						.replaceAll("%oldgamemode%", this.getGamemodeName(gamemodeOld))
						.replaceAll("%newgamemode%", this.getGamemodeName(gamemode))));
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &aGamemode de &2%player% &a(&2%oldgamemode%&a) changé en &2%newgamemode%&a.".replaceAll("%player%", target.getName())
						.replaceAll("%oldgamemode%", this.getGamemodeName(gamemodeOld))
						.replaceAll("%newgamemode%", this.getGamemodeName(gamemode))));

			} else if(args.length >= 1) {
				if(player == null) {
					sender.sendMessage(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole"));
					return;
				}
				gamemodeOld = player.getGameMode();
				gamemode = this.getGamemode(args[0]);
				if(gamemode == null) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cGamemode &4%gamemode% &cinconnu.".replaceAll("%gamemode%", args[0])));
					return;
				} else if(gamemode == gamemodeOld) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous êtes déjà en &4%gamemode%&c.".replaceAll("%gamemode%", this.getGamemodeName(gamemode))));
					return;
				}
				final Player player2 = player;
				final GameMode gamemode2 = gamemode;
				Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> player2.setGameMode(gamemode2));
				player.sendMessage(Utils.color("&2EmeraldMC &7» &aGamemode &2%oldgamemode% &achangé en &2%newgamemode%&a.".replaceAll("%oldgamemode%", this.getGamemodeName(gamemodeOld)).replaceAll("%newgamemode%", this.getGamemodeName(gamemode))));
			} else {
				sender.sendMessage(Utils.color("&cUsage &7» &c/gm <survival|creative|adventure|spectator>"));
			}
		});
		return true;
	}

	private GameMode getGamemode(final String s) {
		return this.gamemodes.entrySet().stream().filter(gamemode -> gamemode.getValue().contains(s.toLowerCase())).findFirst().map(gamemode -> gamemode.getKey()).orElse(null);
	}

	private GameMode getGamemodeLabel(final String label) {
		return this.gamemodes.values().stream().filter(gm -> gm.get(3).equals(label.substring(2).toLowerCase())).map(gm -> this.getGamemode(gm.get(1))).findFirst().orElse(null);
	}

	private String getGamemodeName(final GameMode gamemode) {
		return this.gamemodes.values().stream().filter(gm -> gm.get(0).equals(gamemode.toString().toLowerCase())).findFirst().map(gm -> gm.get(1)).orElse(null);
	}
}
