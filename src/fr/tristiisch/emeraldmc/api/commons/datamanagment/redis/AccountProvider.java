package fr.tristiisch.emeraldmc.api.commons.datamanagment.redis;

import java.util.UUID;

import com.google.gson.Gson;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import redis.clients.jedis.Jedis;

public class AccountProvider {

	public static String REDIS_KEY = "ep:";
	public static final OlympaPlayer DEFAULT_ACCOUNT = new OlympaPlayer("username", UUID.randomUUID(), "ip", 0L, 0L);

	RedisAccess redisAccesss;
	UUID uuid;

	public AccountProvider(final UUID uuid) {
		this.uuid = uuid;
		this.redisAccesss = RedisAccess.INSTANCE;
	}

	public OlympaPlayer createNewAccount(final UUID uuid, final String name, final String ip) {
		final OlympaPlayer emeraldPlayer = DEFAULT_ACCOUNT.clone();
		final long time = Utils.getCurrentTimeinSeconds();

		emeraldPlayer.setUUID(uuid);
		emeraldPlayer.setName(name);
		emeraldPlayer.setIP(ip);
		emeraldPlayer.setFirstCo(time);
		emeraldPlayer.setLastCo(time);

		MySQL.createPlayer(emeraldPlayer);

		return emeraldPlayer;
	}

	public OlympaPlayer getAccountFromDatbase() {
		return MySQL.getPlayer(this.uuid);
	}

	public OlympaPlayer getAccountFromRedis() {
		String json = null;
		try(Jedis jedis = this.redisAccesss.connect()) {
			json = jedis.get(this.getKey());
		}
		this.redisAccesss.closeResource();

		if(json == null) {
			return null;
		}
		return new Gson().fromJson(json, OlympaPlayer.class);
	}

	public OlympaPlayer getEmeraldPlayer() {
		OlympaPlayer emeraldPlayer = this.getAccountFromRedis();
		if(emeraldPlayer == null) {
			emeraldPlayer = this.getAccountFromDatbase();
			if(emeraldPlayer != null) {
				this.sendAccountToRedis(emeraldPlayer);
			}
		}
		return emeraldPlayer;
	}

	public String getKey() {
		return REDIS_KEY + this.uuid.toString();
	}

	public void removeAccountFromRedis() {
		try(Jedis jedis = this.redisAccesss.connect()) {
			jedis.del(this.getKey());
		}
		this.redisAccesss.closeResource();
	}

	public void sendAccountToRedis(final OlympaPlayer emeraldPlayer) {
		try(Jedis jedis = this.redisAccesss.connect()) {
			jedis.set(this.getKey(), new Gson().toJson(emeraldPlayer));
		}
		this.redisAccesss.closeResource();
	}
}
