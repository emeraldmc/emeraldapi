package fr.tristiisch.emeraldmc.api.spigot.anticheat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerCheatListener implements Listener {

	public PlayerCheatListener() {
		for(final Player player : Bukkit.getOnlinePlayers()) {
			new PlayerCheat(player);
		}
	}

	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent e) {
		new PlayerCheat(e.getPlayer());
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		PlayerCheat.removePlayer(player);
	}
}
