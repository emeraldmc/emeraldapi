package fr.tristiisch.emeraldmc.api.bungee.privatemessage;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public final class PrivateMessageToggleCommand extends Command {

	public static List<UUID> players = new ArrayList<>();

	public PrivateMessageToggleCommand(final String command, final String... aliases) {
		super(command, null, aliases);
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
			return;
		}

		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			final ProxiedPlayer player = (ProxiedPlayer) sender;
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.RESPMODO)) {
				player.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
				return;
			}

			if(players.contains(player.getUniqueId())) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous messages privés sont désormais &2&lactivés"));
				players.remove(player.getUniqueId());
			} else {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous messages privés sont désormais &4&ldésactivés"));
				players.add(player.getUniqueId());
			}
		});
	}

}
