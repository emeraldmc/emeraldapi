package fr.tristiisch.emeraldmc.api.commons.datamanagment.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisAccess {

	public static RedisAccess INSTANCE;
	private final RedisCredentials redisCredentials;
	private JedisPool pool;

	public RedisAccess(final RedisCredentials redisCredentials) {
		INSTANCE = this;
		this.redisCredentials = redisCredentials;
		this.initJedis();
	}

	public JedisPool getJedisPool() {
		return this.pool;
	}

	public Jedis connect() {
		final Jedis jedis = this.getJedisPool().getResource();
		jedis.auth(this.getCredentials().getPassword());
		jedis.clientSetname(this.getCredentials().getClientName());
		jedis.select(1);
		return jedis;
	}

	public static void init() {
		new RedisAccess(new RedisCredentials("127.0.0.1", "Qfr0HgyhGqX9T94BOMNLG3PI7o65JKyh", 6379));
	}

	public RedisCredentials getCredentials() {
		return this.redisCredentials;
	}

	public void closeResource() {
		this.pool.getResource().close();
	}

	public static void close() {
		INSTANCE.getJedisPool().close();
	}

	public void initJedis() {

		final ClassLoader previous = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(Jedis.class.getClassLoader());
		this.pool = new JedisPool(this.redisCredentials.getIp(), this.redisCredentials.getPort());
		Thread.currentThread().setContextClassLoader(previous);
	}
}
