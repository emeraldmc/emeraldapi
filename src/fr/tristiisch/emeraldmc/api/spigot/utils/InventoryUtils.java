package fr.tristiisch.emeraldmc.api.spigot.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

public class InventoryUtils {

	/**
	 * Converts the player inventory to a String array of Base64 strings. First
	 * string is the content and second string is the armor.
	 *
	 * @param playerInventory
	 *            to turn into an array of strings.
	 * @return Array of strings: [ main content, armor content ]
	 * @throws IllegalStateException
	 */
	public static String[] playerInventoryToBase64(final PlayerInventory playerInventory) throws IllegalStateException {
		//get the main content part, this doesn't return the armor
		final String content = itemStackArrayToBase64(playerInventory.getContents());
		final String armor = itemStackArrayToBase64(playerInventory.getArmorContents());

		return new String[] { content, armor };
	}

	public static void setPlayerInventory(final Player player, final String[] inventoryBase64) throws IllegalStateException {
		final PlayerInventory inventory = player.getInventory();
		try {
			inventory.setContents(itemStackArrayFromBase64(inventoryBase64[0]));
			inventory.setArmorContents(itemStackArrayFromBase64(inventoryBase64[1]));
		} catch(IllegalArgumentException | IOException e1) {
			e1.printStackTrace();
		}

	}

	/**
	 *
	 * A method to serialize an {@link ItemStack} array to Base64 String.
	 *
	 * <p />
	 *
	 * Based off of {@link #toBase64(Inventory)}.
	 *
	 * @param items to turn into a Base64 String.
	 * @return Base64 string of the items.
	 * @throws IllegalStateException
	 */
	public static String itemStackArrayToBase64(final ItemStack[] items) throws IllegalStateException {
		try {
			final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			final BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

			// Write the size of the inventory
			dataOutput.writeInt(items.length);

			// Save every element in the list
			for(final ItemStack item : items) {
				dataOutput.writeObject(item);
			}

			// Serialize that array
			dataOutput.close();
			return Base64Coder.encodeLines(outputStream.toByteArray());
		} catch (final Exception e) {
			throw new IllegalStateException("Unable to save item stacks.", e);
		}
	}

	/**
	 * A method to serialize an inventory to Base64 string.
	 *
	 * <p />
	 *
	 * Special thanks to Comphenix in the Bukkit forums or also known
	 * as aadnk on GitHub.
	 *
	 * <a href="https://gist.github.com/aadnk/8138186">Original Source</a>
	 *
	 * @param inventory to serialize
	 * @return Base64 string of the provided inventory
	 * @throws IllegalStateException
	 */
	public static String toBase64(final Inventory inventory) throws IllegalStateException {
		try {
			final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			final BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

			// Write the size of the inventory
			dataOutput.writeInt(inventory.getSize());

			// Save every element in the list
			for (int i = 0; i < inventory.getSize(); i++) {
				dataOutput.writeObject(inventory.getItem(i));
			}

			// Serialize that array
			dataOutput.close();
			return Base64Coder.encodeLines(outputStream.toByteArray());
		} catch (final Exception e) {
			throw new IllegalStateException("Unable to save item stacks.", e);
		}
	}

	/**
	 *
	 * A method to get an {@link Inventory} from an encoded, Base64, string.
	 *
	 * <p />
	 *
	 * Special thanks to Comphenix in the Bukkit forums or also known
	 * as aadnk on GitHub.
	 *
	 * <a href="https://gist.github.com/aadnk/8138186">Original Source</a>
	 *
	 * @param data Base64 string of data containing an inventory.
	 * @return Inventory created from the Base64 string.
	 * @throws IOException
	 */
	public static Inventory fromBase64(final String data) throws IOException {
		try {
			final ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
			final BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
			final Inventory inventory = Bukkit.getServer().createInventory(null, dataInput.readInt());

			// Read the serialized inventory
			for (int i = 0; i < inventory.getSize(); i++) {
				inventory.setItem(i, (ItemStack) dataInput.readObject());
			}

			dataInput.close();
			return inventory;
		} catch (final ClassNotFoundException e) {
			throw new IOException("Unable to decode class type.", e);
		}
	}

	/**
	 * Gets an array of ItemStacks from Base64 string.
	 *
	 * <p />
	 *
	 * Base off of {@link #fromBase64(String)}.
	 *
	 * @param data Base64 string to convert to ItemStack array.
	 * @return ItemStack array created from the Base64 string.
	 * @throws IOException
	 */
	public static ItemStack[] itemStackArrayFromBase64(final String data) throws IOException {
		try {
			final ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
			final BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
			final ItemStack[] items = new ItemStack[dataInput.readInt()];

			// Read the serialized inventory
			for (int i = 0; i < items.length; i++) {
				items[i] = (ItemStack) dataInput.readObject();
			}

			dataInput.close();
			return items;
		} catch (final ClassNotFoundException e) {
			throw new IOException("Unable to decode class type.", e);
		}


	}

	public static void removePlayerDat(final UUID uuid) {
		final File file = new File(Bukkit.getServer().getWorlds().get(0).getWorldFolder(), "playerdata/" + uuid.toString() + ".dat");
		file.delete();
	}


	public long getOfflinePlayerStatistic(final OfflinePlayer player, final Statistic statistic) {
		final File worldFolder = new File(Bukkit.getServer().getWorlds().get(0).getWorldFolder(), "stats");
		final File playerStatistics = new File(worldFolder, player.getUniqueId().toString() + ".json");
		if(playerStatistics.exists()){
			final JSONParser parser = new JSONParser();
			JSONObject jsonObject = null;
			try {
				jsonObject = (JSONObject) parser.parse(new FileReader(playerStatistics));
			} catch (IOException | ParseException e) {
				e.printStackTrace();
			}
			final StringBuilder statisticNmsName = new StringBuilder("stat.");
			for(final char character : statistic.name().toCharArray()) {
				if(statisticNmsName.charAt(statisticNmsName.length() - 1) == '_') {
					statisticNmsName.setCharAt(statisticNmsName.length() - 1,Character.toUpperCase(character));
				}else {

					statisticNmsName.append(Character.toLowerCase(character));
				}
			}
			if(jsonObject.containsKey(statisticNmsName.toString())) {
				return (long) jsonObject.get(statisticNmsName.toString());
			} else {
				return 0;
			}
		}
		return 0;
	}
}
