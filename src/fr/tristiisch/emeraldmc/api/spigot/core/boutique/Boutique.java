package fr.tristiisch.emeraldmc.api.spigot.core.boutique;

import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;

public class Boutique {

	public static void openMenu(final Player player) {
		final GuiTools guiTools = new GuiTools("&2Boutique Minerais", "shop", 9);

		for(final BoutiqueItem boutiqueItem : BoutiqueItem.getItems(BoutiqueItemInfo.MENU)) {
			guiTools.setItem(guiTools.getMiddleSlot() + boutiqueItem.getSlot(), boutiqueItem.getItemStack());
		}

		guiTools.openInventory(player);
	}
}
