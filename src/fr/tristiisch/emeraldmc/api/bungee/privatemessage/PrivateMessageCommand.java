package fr.tristiisch.emeraldmc.api.bungee.privatemessage;

import java.util.Arrays;

import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public final class PrivateMessageCommand extends Command {

	public PrivateMessageCommand(final String command, final String... aliases) {
		super(command, null, aliases);

		PrivateMessage.privateMessageCommand.add(command);
		PrivateMessage.privateMessageCommand.addAll(Arrays.asList(aliases));
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
			return;
		}
		final ProxiedPlayer proxiedPlayer = (ProxiedPlayer) sender;
		if(args.length > 1) {
			final ProxiedPlayer targetPlayer = ProxyServer.getInstance().getPlayer(args[0]);
			if(targetPlayer == null || PrivateMessageToggleCommand.players.contains(targetPlayer.getUniqueId())) {
				proxiedPlayer.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &2" + args[0] + " &cest introuvable".replaceAll("%target%", args[0])));
				return;
			}

			if(targetPlayer == proxiedPlayer) {
				proxiedPlayer.sendMessage(Utils.color("&2EmeraldMC &7» &cTu ne peux pas envoyé des messages à toi-même"));
				return;
			}

			final StringBuilder stringBuilder = new StringBuilder();
			for(int i = 1; i < args.length; i++) {
				stringBuilder.append(args[i] + " ");
			}
			final String message = stringBuilder.toString();

			PrivateMessage.send(proxiedPlayer, targetPlayer, message);

			PrivateMessage.setReply(proxiedPlayer, targetPlayer);
			PrivateMessage.setReply(targetPlayer, proxiedPlayer);
		} else {
			proxiedPlayer.sendMessage(Utils.color("&6Usage &7» &c/msg <joueur> <message>"));
		}
	}

}
