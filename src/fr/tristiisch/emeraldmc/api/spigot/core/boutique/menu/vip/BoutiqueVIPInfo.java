package fr.tristiisch.emeraldmc.api.spigot.core.boutique.menu.vip;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemGlow;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import net.md_5.bungee.api.ChatColor;

public enum BoutiqueVIPInfo {

	DIAMANT(EmeraldGroup.DIAMANT, ChatColor.AQUA, 36000, 11000,
			ItemTools.create(Material.DIAMOND_BLOCK, 1, "&bDiamant", Arrays.asList("", "&7Avantage sur notre site: ", "&a&nemeraldmc.fr/boutique"))),
	EMERAUDE(EmeraldGroup.EMERAUDE, ChatColor.GOLD, 46000, 14000,
			ItemTools.create(Material.EMERALD_BLOCK, 1, "&6Emeraude", Arrays.asList("", "&7Avantage sur notre site: ", "&a&nemeraldmc.fr/boutique"), ItemGlow.getItemGlowEnchant())),
	RUBIS(EmeraldGroup.RUBIS, ChatColor.YELLOW, 16000, 5000, ItemTools.create(Material.NETHERRACK, 1, "&eRubis", Arrays.asList("", "&7Avantage sur notre site: ", "&a&nemeraldmc.fr/boutique"))),
	SAPHIR(EmeraldGroup.SAPHIR, ChatColor.DARK_AQUA, 26000, 8000, ItemTools.create(Material.LAPIS_BLOCK, 1, "&3Saphir", Arrays.asList("", "&7Avantage sur notre site: ", "&a&nemeraldmc.fr/boutique")));

	public static BoutiqueVIPInfo get(final ItemStack item) {
		return Arrays.stream(BoutiqueVIPInfo.values()).filter(vip -> item.isSimilar(vip.getItem())).findFirst().orElse(null);
	}

	public static BoutiqueVIPInfo get(final String name) {
		return Arrays.stream(BoutiqueVIPInfo.values()).filter(vip -> vip.toString().equalsIgnoreCase(name)).findFirst().orElse(null);
	}

	final private ChatColor color;
	final private EmeraldGroup group;
	final private ItemStack item;

	final private int priceLife;

	final private int priceMonth;

	private BoutiqueVIPInfo(final EmeraldGroup group, final ChatColor color, final int priceLife, final int priceMonth, final ItemStack item) {
		this.group = group;
		this.color = color;
		this.priceLife = priceLife;
		this.priceMonth = priceMonth;
		this.item = item;
	}

	public ChatColor getColor() {
		return this.color;
	}

	public EmeraldGroup getGroup() {
		return this.group;
	}

	public ItemStack getItem() {
		return this.item;
	}

	public String getName() {
		return this.group.getName();
	}

	public String getNameColored() {
		return this.color + this.group.getName();
	}

	public int getPriceLife() {
		return this.priceLife;
	}

	public int getPriceMonth() {
		return this.priceMonth;
	}
}
