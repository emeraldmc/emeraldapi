package fr.tristiisch.emeraldmc.api.commons;

public enum Prefix {

	DEFAULT("&2%serverName% &7%symbole% "),
	DEFAULT_BAD("&2%serverName% &7%symbole% &c"),
	DEFAULT_GOOD("&2%serverName% &7%symbole% &a"),
	USAGE("&cUsage &7%symbole% &c");

	static {
		final String symbole = "»";
		for(final Prefix prefix : Prefix.values()) {
			prefix.setPrefix(prefix.toStringWithoutFormat().replaceAll("%serverName%", "EmeraldMC").replaceAll("%symbole%", symbole));
		}
	}

	String prefix;

	private Prefix(final String prefix) {
		this.prefix = prefix;
	}

	private void setPrefix(final String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String toString() {
		return Utils.color(this.prefix);
	}

	public String toStringWithoutFormat() {
		return this.prefix;
	}
}
