package fr.tristiisch.emeraldmc.api.bungee.ban;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.ban.objects.EmeraldBan;
import fr.tristiisch.emeraldmc.api.bungee.ban.objects.EmeraldBanType;
import fr.tristiisch.emeraldmc.api.commons.Utils;

public class MuteUtils {

	static private List<EmeraldBan> mutes = new ArrayList<>();

	public static List<EmeraldBan> getMute() {
		return mutes;
	}

	public static void addMute(final EmeraldBan mute) {
		mutes.add(mute);
	}

	public static EmeraldBan getMute(final UUID uuid) {
		return mutes.stream().filter(emeraldMute -> String.valueOf(emeraldMute.getPlayer()).equals(String.valueOf(uuid))).findFirst().orElse(null);
	}

	public static void removeMute(final UUID uuid) {
		removeMute(getMute(uuid));
	}

	public static void removeMute(final EmeraldBan mute) {
		mutes.remove(mute);
	}

	public static EmeraldBan getMuteFromMySQL(final UUID uuid) {
		return BanMySQL.getActiveSanction(uuid, EmeraldBanType.MUTE);
	}

	public static boolean chechExpireBan(final EmeraldBan mute) {
		if(Utils.getCurrentTimeinSeconds() > mute.getExpires()) {
			removeMute(mute);
			BanMySQL.expireBan(mute);
			return true;
		}
		return false;
	}



}
