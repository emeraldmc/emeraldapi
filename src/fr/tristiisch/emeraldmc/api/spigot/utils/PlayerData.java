package fr.tristiisch.emeraldmc.api.spigot.utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class PlayerData {

	public PlayerData(final ItemStack[] inventoryItemContents, final ItemStack[] inventoryArmorContents) {
		this.inventoryItemContents = inventoryItemContents;
		this.inventoryArmorContents = inventoryArmorContents;
	}

	private ItemStack[] inventoryItemContents;
	private ItemStack[] inventoryArmorContents;

	/*	private List<PotionEffect> potionEffects;
	private double health;*/

	public ItemStack[] getInventoryItemContents() {
		return this.inventoryItemContents;
	}
	public void setInventoryItemContents(final ItemStack[] inventoryItemContents) {
		this.inventoryItemContents = inventoryItemContents;
	}

	public ItemStack[] getInventoryArmorContents() {
		return this.inventoryArmorContents;
	}
	public void setInventoryArmorContents(final ItemStack[] inventoryArmorContents) {
		this.inventoryArmorContents = inventoryArmorContents;
	}


	public static void clearInventory(final Player player) {
		final PlayerInventory inventory = player.getInventory();
		inventory.clear();
		inventory.setArmorContents(new ItemStack[inventory.getArmorContents().length]);
	}
}
