package fr.tristiisch.emeraldmc.api.spigot.customevent.listener;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.customevent.ArrowHitBlockEvent;
import fr.tristiisch.emeraldmc.api.spigot.customevent.FirstPlayerJoinEvent;
import fr.tristiisch.emeraldmc.api.spigot.customevent.PlayerMoveBlockEvent;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import net.minecraft.server.v1_8_R3.EntityArrow;

public class CustomEventsListener implements Listener {

	@EventHandler
	public void PlayerMoveEvent(final PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		if(!LocationUtils.isIn(event.getFrom(), event.getTo())) {
			final PlayerMoveBlockEvent event2 = new PlayerMoveBlockEvent(player, event);
			EmeraldSpigot.getInstance().getServer().getPluginManager().callEvent(event2);
		}

	}


	@EventHandler
	public void PlayerJoinEvent(final PlayerJoinEvent event){
		final Player player = event.getPlayer();
		if(Bukkit.getOnlinePlayers().size() == 1) {
			final FirstPlayerJoinEvent event2 = new FirstPlayerJoinEvent(player, event);
			EmeraldSpigot.getInstance().getServer().getPluginManager().callEvent(event2);
		}
	}

	@EventHandler
	private void onProjectileHit(final ProjectileHitEvent e) {
		if(e.getEntityType() == EntityType.ARROW) {
			// Must be run in a delayed task otherwise it won't be able to find the block
			Bukkit.getScheduler().scheduleSyncDelayedTask(EmeraldSpigot.getInstance(), () -> {
				try {

					final EntityArrow entityArrow = ((CraftArrow) e.getEntity()).getHandle();

					final Field fieldX = EntityArrow.class.getDeclaredField("d");
					final Field fieldY = EntityArrow.class.getDeclaredField("e");
					final Field fieldZ = EntityArrow.class.getDeclaredField("f");

					fieldX.setAccessible(true);
					fieldY.setAccessible(true);
					fieldZ.setAccessible(true);

					final int x = fieldX.getInt(entityArrow);
					final int y = fieldY.getInt(entityArrow);
					final int z = fieldZ.getInt(entityArrow);

					if(CustomEventsListener.this.isValidBlock(y)) {
						final Block block = e.getEntity().getWorld().getBlockAt(x, y, z);
						Bukkit.getServer().getPluginManager().callEvent(new ArrowHitBlockEvent((Arrow) e.getEntity(), block));
					}

				} catch(final NoSuchFieldException e11) {
					e11.printStackTrace();
				} catch(final SecurityException e12) {
					e12.printStackTrace();
				} catch(final IllegalArgumentException e13) {
					e13.printStackTrace();
				} catch(final IllegalAccessException e14) {
					e14.printStackTrace();
				}
			});

		}
	}

	// If the arrow hits a mob or player the y coord will be -1
	private boolean isValidBlock(final int y) {
		return y != -1;
	}
}
