package fr.tristiisch.emeraldmc.api.bungee.staffchat;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

@SuppressWarnings("deprecation")
public class StaffChatListener implements Listener {

	@EventHandler
	public void ChatEvent(final ChatEvent event) {
		if(event.isCancelled()) {
			return;
		}
		if(event.getMessage().startsWith(";")) {
			final ProxiedPlayer player = (ProxiedPlayer) event.getSender();
			event.setCancelled(true);
			ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
				final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(!emeraldPlayer.getGroup().isStaffMember()) {
					player.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
					return;
				}
				final String message = event.getMessage();

				if(message.length() < 2) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &aMerci de mettre un message"));
					return;
				}

				final String msg = message.substring(1);
				if(msg.startsWith(" ")) {
					msg.substring(1);
				}
				StaffChat.sendMessage(emeraldPlayer, msg);

			});
		}
	}
}
