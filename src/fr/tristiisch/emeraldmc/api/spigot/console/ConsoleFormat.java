package fr.tristiisch.emeraldmc.api.spigot.console;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.SimpleMessage;
import org.bukkit.Bukkit;

import com.google.common.collect.Maps;

import fr.tristiisch.emeraldmc.api.commons.console.ColorAppender;
import fr.tristiisch.emeraldmc.api.commons.console.CommonLogInstaller;

public class ConsoleFormat extends ColorAppender {
	
	private static String TERMINAL_NAME = "TerminalConsole";

	private static Layout<? extends Serializable> oldLayout;
	private static List<String> hidemessages = new ArrayList<>();
	
	
	public ConsoleFormat(Appender oldAppender, Map<String, String> levelColors) {
		super(oldAppender
				, hidemessages
				, false
				, false
                , levelColors);
	}

	@Override
	public LogEvent onAppend(LogEvent logEvent) {
		String oldMessage = logEvent.getMessage().getFormattedMessage();
		Message newMessage = new SimpleMessage(formatter.colorizePluginTag(oldMessage, logEvent.getLevel().name()));
		return clone(logEvent, logEvent.getLoggerName(), newMessage);
	}

	@Override
	protected Collection<String> loadPluginNames() {
		return Stream.of(Bukkit.getPluginManager().getPlugins())
				.map(plugin -> plugin.getName())
				.collect(Collectors.toSet());
	}
	
	private static void installLogFormat(Map<String, String> levelColors) {
		Appender terminalAppender = CommonLogInstaller.getTerminalAppender(TERMINAL_NAME);

		oldLayout = terminalAppender.getLayout();
		String logFormat = "%d{HH:mm:ss} [%level]: %msg%n";
		if (oldLayout.toString().contains("minecraftFormatting")) {
			logFormat = logFormat.replace("%msg", "%minecraftFormatting{%msg}");
		}

		logFormat = logFormat.replace("%level",  "%highlight{%level}{"
					+ "FATAL=red, "
					+ "ERROR=red, "
					+ "WARN=yellow, "
					+ "INFO=green, "
					+ "DEBUG=green, "
					+ "TRACE=blue}");

		String dateStyle = "cyan";
		logFormat = logFormat.replaceFirst("(%d)\\{.{1,}\\}", "%style{$0}{" + dateStyle + "}");
		try {
			PatternLayout layout = CommonLogInstaller.createLayout(logFormat);
			CommonLogInstaller.setLayout(layout, terminalAppender);
		} catch (ReflectiveOperationException ex) {
			Bukkit.getLogger().log(Level.WARNING, "Cannot install log format", ex);
		}
	}

	public static void enable() {
		Map<String, String> levelColors = Maps.newHashMap();
		levelColors.put("FATAL", "red");
		levelColors.put("ERROR", "red");
		levelColors.put("WARN", "yellow");
		levelColors.put("DEBUG", "green");
		levelColors.put("TRACE", "green");

		//try to run it as early as possible
		installLogFormat(levelColors);
		
	}

	public static void disable() {
        Appender terminalAppender = CommonLogInstaller.getTerminalAppender(TERMINAL_NAME);
        Logger rootLogger = ((Logger) LogManager.getRootLogger());

        ConsoleFormat colorPluginAppender = null;
        for (Appender value : rootLogger.getAppenders().values()) {
            if (value instanceof ConsoleFormat) {
                colorPluginAppender = (ConsoleFormat) value;
                break;
            }
        }

        if (colorPluginAppender != null) {
            rootLogger.removeAppender(terminalAppender);
            rootLogger.addAppender(colorPluginAppender.getOldAppender());
        }

        try {
            CommonLogInstaller.setLayout(oldLayout, terminalAppender);
        } catch (ReflectiveOperationException ex) {
            Bukkit.getLogger().log(Level.WARNING, "Cannot revert log format", ex);
        }
	}
}
