package fr.tristiisch.emeraldmc.api.bungee.utils;

import com.google.common.collect.Maps;

import fr.tristiisch.emeraldmc.api.commons.console.CommonFormatter;
import net.md_5.bungee.api.ProxyServer;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ConsoleFormat extends Formatter {

	private final Formatter oldFormatter;

	private final DateFormat date = new SimpleDateFormat("HH:mm:ss");

	private final CommonFormatter formatter;

	private final Map<String, String> levelColors = Maps.newHashMap();
	
    public ConsoleFormat(Formatter oldFormatter) {
    	
		this.oldFormatter = oldFormatter;

		List<String> ignoreMessages = new ArrayList<>();
		boolean colorizeTag = true;
		boolean truncateColor = false;

		levelColors.put("FATAL", "red");
		levelColors.put("ERROR", "red");
		levelColors.put("WARN", "yellow");
		levelColors.put("INFO", "green");
		levelColors.put("DEBUG", "green");
		levelColors.put("TRACE", "blue");

		this.formatter = new CommonFormatter(ignoreMessages, colorizeTag, truncateColor, levelColors);
	}

	@Override
	public String format(LogRecord record) {
		if (formatter.shouldIgnore(record.getMessage())) {
			return "";
		}

		StringBuilder formatted = new StringBuilder();
		String message = oldFormatter.formatMessage(record);

		String levelColor = "";
		String log4JName = translateToLog4JName(record.getLevel());
		levelColor = formatter.format(levelColors.get(log4JName));

		formatted.append(levelColor);

		formatted.append(this.date.format(record.getMillis()));
		formatted.append(" [");
		formatted.append(record.getLevel().getName());
		formatted.append("] ");

		formatted.append(formatter.getReset());

		formatted.append(formatter.colorizePluginTag(message, translateToLog4JName(record.getLevel())));

		formatted.append('\n');
		if (record.getThrown() != null) {
			StringWriter writer = new StringWriter();
			record.getThrown().printStackTrace(new PrintWriter(writer));
			formatted.append(writer);
		}

		return formatted.toString();
	}

	public Formatter getOldFormatter() {
		return oldFormatter;
	}

	private String translateToLog4JName(Level level) {
		if (level == Level.SEVERE) {
			return "ERROR";
		} else if (level == Level.WARNING) {
			return "WARN";
		} else if (level == Level.INFO) {
			return "INFO";
		} else if (level == Level.CONFIG) {
			return "DEBUG";
		} else {
			return "TRACE";
		}
	}
    private Set<String> loadPluginNames() {
        return ProxyServer.getInstance().getPluginManager().getPlugins().stream()
                .map(plugin -> plugin.getDescription().getName())
                .collect(Collectors.toSet());
    }
    
    public void initPluginColors(String def) {
        Set<String> plugins = loadPluginNames();
        Map<String, String> pluginColors = new HashMap<>();
        formatter.initPluginColors(plugins, pluginColors, def);
    }

	public static void enable() {
        ProxyServer bungee = ProxyServer.getInstance();
        Logger bungeeLogger = bungee.getLogger();

        Handler[] handlers = bungeeLogger.getHandlers();
        for (Handler handler : handlers) {
            Formatter oldFormatter = handler.getFormatter();

            ConsoleFormat newFormatter = new ConsoleFormat(oldFormatter);
            newFormatter.initPluginColors("blue");
            handler.setFormatter(newFormatter);
        }
	}

	public static void disable() {
        ProxyServer bungee = ProxyServer.getInstance();
        Logger bungeeLogger = bungee.getLogger();

        Handler[] handlers = bungeeLogger.getHandlers();
        for (Handler handler : handlers) {
            Formatter formatter = handler.getFormatter();
            if (formatter instanceof ConsoleFormat) {
                handler.setFormatter(((ConsoleFormat) formatter).getOldFormatter());
            }
        }
	}
}
