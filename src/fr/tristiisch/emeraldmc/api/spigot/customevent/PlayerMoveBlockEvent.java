package fr.tristiisch.emeraldmc.api.spigot.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerMoveEvent;
/**
 * Lorsqu'un joueur se déplace & change de block (PlayerMoveEvent sans prendre en compte le yaw/pitch)
 * 
 */
public class PlayerMoveBlockEvent extends PlayerEvent {
	
	public static final HandlerList handlers = new HandlerList();

	final private PlayerMoveEvent playerMoveEvent;
	
	public PlayerMoveBlockEvent(Player player, PlayerMoveEvent playerMoveEvent) {
		super(player);
		this.playerMoveEvent = playerMoveEvent;
	}

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }
	
    public static HandlerList getHandlerList(){
    	return handlers; 
    }

	public PlayerMoveEvent getPlayerMoveEvent() {
		return playerMoveEvent;
	}
}
