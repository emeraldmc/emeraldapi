package fr.tristiisch.emeraldmc.api.commons.usernamehistory;

public class Username {

	String name;
	long changedToAt;

	public Username(final String name, final long changedToAt) {
		this.name = name;
		this.changedToAt = changedToAt;
	}

	public String getName() {
		return this.name;
	}

	public long getTime() {
		return this.changedToAt / 1000;
	}
}