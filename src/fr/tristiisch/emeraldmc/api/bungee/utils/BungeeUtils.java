package fr.tristiisch.emeraldmc.api.bungee.utils;

import java.util.UUID;
import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldConsole;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldServer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeeUtils {

	public static String connectScreen(final String s) {
		return Utils.color(BungeeConfigUtils.getString("commun.messages.connectscreenprefix") + s + BungeeConfigUtils.getString("commun.messages.connectscreensuffix"));
	}

	public static TextComponent formatStringToJSON(final String s) {
		final TextComponent textcomponent = new TextComponent();
		final BaseComponent[] msgs = TextComponent.fromLegacyText(Utils.color(s));
		for(final BaseComponent msg : msgs) {
			textcomponent.addExtra(msg);
		}
		return textcomponent;
	}

	public static String getExactName(final String name) {
		if(name.equalsIgnoreCase(EmeraldConsole.getName())) {
			return EmeraldConsole.getName();
		}
		final ProxiedPlayer player = ProxyServer.getInstance().getPlayer(name);
		if(player != null) {
			return player.getName();
		}
		return MySQL.getPlayerExactName(name);
	}

	public static String getName(final UUID uuid) {
		if(uuid.equals(EmeraldConsole.getUniqueId())) {
			return EmeraldConsole.getName();
		}
		final ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
		if(player != null) {
			return player.getName();
		}
		return MySQL.getNameFromUUID(uuid);
	}

	public static String getPlayersNamesByIp(final String ip) {
		return MySQL.getPlayersByIp(ip).stream().map(em -> em.getName()).collect(Collectors.joining(", "));
	}

	public static EmeraldServer getServer(final ServerInfo server) {
		return EmeraldServers.getServers().stream().filter(Sname -> Sname.getName().equals(server.getName())).findFirst().orElse(null);
	}

	public static ServerInfo getServerInfo(final EmeraldServer server) {
		return ProxyServer.getInstance().getServers().values().stream().filter(Sname -> Sname.getName().equals(server.getName())).findFirst().orElse(null);
	}

	public static void sendMessageToStaff(final TextComponent text) {
		for(final ProxiedPlayer player : ProxyServer.getInstance()
				.getPlayers()
				.stream()
				.filter(p -> new AccountProvider(p.getUniqueId()).getEmeraldPlayer().getGroup().isStaffMember())
				.collect(Collectors.toList())) {
			player.sendMessage(text);
		}
	}
}
