package fr.tristiisch.emeraldmc.api.spigot.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class ForceKickSpigotCommand implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			if(sender instanceof Player) {
				final Player player = (Player) sender;
				final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}
			}
			if(args.length > 0) {

				if(Matcher.isUsername(args[0])) {
					Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
						final Player target = Bukkit.getPlayer(args[0]);
						target.kickPlayer("");
						target.remove();
						sender.sendMessage(Utils.color("&aLe joueur " + target.getName() + " a été kick."));
					});

				} else {
					sender.sendMessage(Utils.color("&2EmeraldMC &7» &c" + args[0] + " n'est pas un pseudo valide."));
					return;
				}
			} else {
				sender.sendMessage(Utils.color("&cUsage &7» &c/forcekickspigot <joueur>"));
			}
		});

		return true;
	}
}
