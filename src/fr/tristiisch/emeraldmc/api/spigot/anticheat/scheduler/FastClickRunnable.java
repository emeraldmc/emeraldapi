package fr.tristiisch.emeraldmc.api.spigot.anticheat.scheduler;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.anticheat.PlayerCheat;
import fr.tristiisch.emeraldmc.api.spigot.utils.TPS;

public class FastClickRunnable extends BukkitRunnable {

	private final int AlertCPS = 18;
	// en sec
	private final int TimeBetwenAlert = 30;
	public static int sizeHistoryCps = 10;

	@Override
	public void run() {
		for(final PlayerCheat pc : PlayerCheat.players.values()) {
			if(pc.clicks.size() >= sizeHistoryCps + 1) {
				pc.clicks.remove(1);
			}
			pc.clicks.add(pc.click);
			if(pc.click >= this.AlertCPS) {
				final double tps = TPS.getTPS();
				int LagAlertCPS = (int) ((20 - tps) * 2);
				LagAlertCPS += pc.getPing() / 50;
				if(pc.click >= this.AlertCPS + LagAlertCPS) {
					pc.nbAlerts++;
					if(pc.lastAlert + this.TimeBetwenAlert * 1000L < System.currentTimeMillis()) {
						pc.lastAlert = System.currentTimeMillis();
						final String msg = Utils.color("&c✦ &2FastClick &7» &4%pseudo% &c%cps% cps, %ping% ms, %tps% tps.".replace("%pseudo%", pc.name)
								.replace("%cps%", String.valueOf(pc.click))
								.replace("%ping%", String.valueOf(pc.getPing()))
								.replace("%tps%", String.valueOf(tps)));
						Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
							for(final Player player : Bukkit.getOnlinePlayers()) {
								final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
								if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.MODERATEUR)) {
									player.sendMessage(msg);
								}
							}
						});
					}
				}
			}
			pc.click = 0;
		}
	}
}
