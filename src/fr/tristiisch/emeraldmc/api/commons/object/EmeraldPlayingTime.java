package fr.tristiisch.emeraldmc.api.commons.object;

import java.util.HashMap;
import java.util.stream.Collectors;

import net.md_5.bungee.api.config.ServerInfo;

public class EmeraldPlayingTime {

	
	private HashMap<String, Long> time = new HashMap<>();
	
	public Long getPlayingTime(ServerInfo serverInfo) {
		return getPlayingTime(serverInfo.getName());
	}
	
	public Long getPlayingTime(String serverName) {
		return time.get(serverName);
	}
	
	public String getPlayingTimeToString() {
		return this.time.entrySet().stream().map(entry -> entry.getKey() + ":" + entry.getValue()).collect(Collectors.joining(","));
	}
	
	public static HashMap<String, Integer> getPlayingTimeFromString(String s) {
		HashMap<String, Integer> members = new HashMap<>();
		for(String member : s.split(",")) {
			String[] memberRole = member.split(":");
			String serverName = memberRole[0];
			int seconds = Integer.parseInt(memberRole[1]);
			members.put(serverName, seconds);
		}
		return members;
	}
	
	public void setPlayingTime(ServerInfo serverInfo, Long i) {
		setPlayingTime(serverInfo.getName(), i);
	}
	
	public void setPlayingTime(String serverName, Long i) {
		time.put(serverName, i);
	}
}
