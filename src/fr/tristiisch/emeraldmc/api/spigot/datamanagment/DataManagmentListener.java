package fr.tristiisch.emeraldmc.api.spigot.datamanagment;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerLoadEvent;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotBPMC;

public class DataManagmentListener implements Listener {

	@EventHandler
	public void onPlayerPreLogin(final AsyncPlayerPreLoginEvent event) {
		final UUID uuid = event.getUniqueId();
		final Player player = Bukkit.getPlayer(uuid);

		if(Bukkit.getOnlinePlayers().size() == 1) {
			SpigotBPMC.askServersStatus();
		}

		Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
			if(player != null) {
				player.kickPlayer("&cUne autre personne s'est connecté sur ce serveur avec votre compte.");
				player.remove();
			}
		});
	}

	@EventHandler
	public void PlayerJoinEvent(final PlayerJoinEvent event) {
		event.setJoinMessage(null);
		final Player player = event.getPlayer();
		for(final Player players : Bukkit.getOnlinePlayers()) {
			players.hidePlayer(player);
		}
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final AccountProvider account = new AccountProvider(player.getUniqueId());
			final OlympaPlayer emeraldPlayer = account.getEmeraldPlayer();

			if(emeraldPlayer == null) {

				Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
					player.kickPlayer("&cUne erreur est survenu lors du chargement de vos données. #Spigot");
					try {
						throw new Exception("Une erreur est survenu lors du chargement des données de " + player.getName() + " (" + player.getUniqueId() + ")");
					} catch(final Exception e) {
						e.printStackTrace();
					}
					player.remove();
				});
				return;

			}
			Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
				for(final Player players : Bukkit.getOnlinePlayers()) {
					players.showPlayer(player);
				}
			});
			final AsyncEmeraldPlayerLoadEvent event2 = new AsyncEmeraldPlayerLoadEvent(player, emeraldPlayer);
			Bukkit.getPluginManager().callEvent(event2);
			if(event2.getJoinMessage() != null && !event2.getJoinMessage().isEmpty()) {
				Bukkit.broadcastMessage(event2.getJoinMessage());
			}
		});
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {

	}
}
