package fr.tristiisch.emeraldmc.api.spigot.core.clear;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class ClearCommand implements CommandExecutor {

	private static int power;

	public ClearCommand(final EmeraldGroup group) {
		setGroup(group);
	}

	public static void setGroup(final EmeraldGroup group) {
		power = group.getPower();
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String arg, final String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			return true;
		}
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {

			final Player player = (Player) sender;
			final OlympaPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerLessThan(power)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}

			if(args.length >= 1) {
				if(!emeraldPlayer.hasPower(EmeraldGroup.MODERATEURP) && emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}
				final Player target = Bukkit.getPlayer(args[0]);
				if(target == null) {
					player.sendMessage("&2EmeraldMC &7» &cLe joueur &4%player% &cn'est pas connecté.".replaceAll("%player%", args[0]));
					return;
				}
				final int size = Clear.player(target);
				target.sendMessage(Utils.color("&2EmeraldMC &7» Votre inventaire a été nettoyer de " + size + " objets."));
				player.sendMessage(Utils.color("&2EmeraldMC &7» Vous avez clear l'inventaire de " + target.getName() + " (" + size + " objets)."));


			} else {
				final int size = Clear.player(player);
				player.sendMessage(Utils.color("&2EmeraldMC &7» Votre inventaire a été nettoyer de " + size + " objets."));
			}
		});
		return true;
	}
}
