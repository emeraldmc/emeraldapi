package fr.tristiisch.emeraldmc.api.spigot.core.verif;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.anticheat.PlayerCheat;
import fr.tristiisch.emeraldmc.api.spigot.anticheat.scheduler.FastClickRunnable;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.api.spigot.utils.TPS;

public class VerifRunnable extends BukkitRunnable {

	private ChatColor getIntervalChatColor(final int i, final int min, final int max) {
		if(i == 0) {
			return ChatColor.GRAY;
		} else if(i < min) {
			return ChatColor.GREEN;
		} else if(i < max) {
			return ChatColor.GOLD;
		} else {
			return ChatColor.RED;
		}
	}

	private int getIntervalGlassPaneColor(final int i, final int min, final int max) {
		if(i == 0) {
			return 8;
		} else if(i < min) {
			return 5;
		} else if(i < max) {
			return 1;
		} else {
			return 14;
		}
	}

	@Override
	public void run() {
		for(final PlayerCheat target : PlayerCheat.verifier.values()) {
			final double tps = TPS.getTPS();

			final Player targetPlayer = target.getPlayer();
			if(targetPlayer == null) {
				return;
			}
			if(!targetPlayer.isOnline()) {
				PlayerCheat.removePlayer(targetPlayer);
				return;
			}
			final HashMap<Integer, ItemStack> items = new HashMap<>();
			ItemStack item;
			final List<String> lore = new ArrayList<>();

			// Tête de target
			item = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
			final SkullMeta skullmeta = (SkullMeta) item.getItemMeta();
			skullmeta.setOwner(targetPlayer.getName());
			skullmeta.setDisplayName("§6" + targetPlayer.getName());
			final OlympaPlayer emeraldTarget = new AccountProvider(targetPlayer.getUniqueId()).getEmeraldPlayer();
			skullmeta.setLore(new ArrayList<>(
				Arrays.asList(" ",
					// getAbsorptionHearts todo: reflection
					Utils.color("&7Vie: &e" + Math.round(targetPlayer.getHealth()) + " &c❤"),
					"",
					Utils.color("&7Grade" + Utils.withOrWithoutS(emeraldTarget.getGroups().size()) + ": &e" + String.join(", ", emeraldTarget.getGroupsName())),
					Utils.color("&7Première connexion: &e" + Utils.timestampToDate(emeraldTarget.getFirstCo())),
					Utils.color("&7Connecté depuis: &e" + Utils.timestampToDuration(emeraldTarget.getLastCo())),
					Utils.color("&7Minerais: &e" + emeraldTarget.getMinerais()),
					Utils.color("&7Pierre" + Utils.withOrWithoutS(emeraldTarget.getPierres()) + ": &e" + emeraldTarget.getPierres()),
					" ")));
			item.setItemMeta(skullmeta);

			items.put(0, item);

			// Effects
			int effect = 0;
			lore.add("");
			for(final PotionEffect effects : targetPlayer.getActivePotionEffects()) {
				lore.add(Utils.color(
					"&7" + SpigotUtils.translateEffects(effects.getType().getName()) + " " + (effects.getAmplifier() + 1) + (effects.hasParticles() ? "" : " &c(&l✖&c particules)&7 ") + ": &e" + Utils
							.secondsToCalendar(effects.getDuration() / 20)));
				effect++;
			}
			String name = "§6Effets";
			Material material = Material.POTION;
			if(effect == 0) {
				lore.add(Utils.color("&7Aucun effet."));
				lore.add("");
				material = Material.GLASS_BOTTLE;
				name = "§6Effet";
			}
			item = ItemTools.create(material, effect, (byte) effect, name, lore);
			lore.clear();

			items.put(1, item);

			// CPS
			final Integer currentClick = target.clicks.get(target.clicks.size() - 1);
			int glassPaneColor = this.getIntervalGlassPaneColor(currentClick, 10, 18);
			lore.add("");
			lore.add(Utils.color(("&7Nombre d'alerte%s%: &e" + target.nbAlerts).replaceAll("%s%", Utils.withOrWithoutS(target.nbAlerts))));
			lore.add(Utils.color("&7CPS maximum: &e" + target.maxCPS));
			lore.add("");
			for(final int click : Lists.reverse(target.clicks)) {
				final ChatColor chatcolor = this.getIntervalChatColor(click, 10, 18);
				lore.add(Utils.color(chatcolor + String.valueOf(click) + " &7cps"));
			}
			do {
				lore.add("");
			} while(FastClickRunnable.sizeHistoryCps + 4 >= lore.size());
			item = ItemTools.create(Material.STAINED_GLASS_PANE, currentClick, (byte) glassPaneColor, Utils.color("&6CPS"), lore);
			lore.clear();

			items.put(2, item);

			// PING
			glassPaneColor = this.getIntervalGlassPaneColor(target.getPing(), 100, 200);
			ChatColor chatcolor = this.getIntervalChatColor(target.getPing(), 100, 200);

			lore.add("");
			lore.add(Utils.color(chatcolor + String.valueOf(target.getPing()) + " &7ms"));
			lore.add("");
			item = ItemTools.create(Material.STAINED_GLASS_PANE, target.getPing(), (byte) glassPaneColor, Utils.color("&6Ping"), lore);
			// TODO: REFLECTION
			/* item = ItemTools.setStackSize(item, 128); */
			lore.clear();

			items.put(3, item);

			// TPS
			final int tpsint = (int) Math.round(tps);
			if(tpsint >= 18) {
				chatcolor = ChatColor.GREEN;
				glassPaneColor = 5;
			} else if(tpsint >= 16) {
				chatcolor = ChatColor.GOLD;
				glassPaneColor = 1;
			} else {
				chatcolor = ChatColor.RED;
				glassPaneColor = 14;
			}

			lore.add("");
			lore.add(Utils.color(chatcolor + String.valueOf(Utils.round(tps, 2)) + " &7"));
			lore.add("");
			item = ItemTools.create(Material.STAINED_GLASS_PANE, tpsint, (byte) glassPaneColor, Utils.color("&6TPS du serveur"), lore);
			lore.clear();
			items.put(4, item);
			// Armor
			int i = PlayerCheat.slotArmor;
			items.put(i++, targetPlayer.getInventory().getHelmet());
			items.put(i++, targetPlayer.getInventory().getChestplate());
			items.put(i++, targetPlayer.getInventory().getLeggings());
			items.put(i++, targetPlayer.getInventory().getBoots());

			// Inventaire
			int slot = PlayerCheat.slotInv;
			int slot2 = PlayerCheat.slotHotBar;
			for(final ItemStack itemStack : targetPlayer.getInventory().getContents()) {
				if(slot2 < PlayerCheat.slotHotBar + 9) {
					items.put(slot2, itemStack);
					slot2++;
				} else {
					items.put(slot, itemStack);
					++slot;
				}

			}
			for(final Player verifier : PlayerCheat.verifier.keySet()) {
				if(PlayerCheat.verifier.get(verifier) == target) {
					if(verifier.getOpenInventory().getTopInventory() != null && verifier.getOpenInventory().getTopInventory().getTitle().startsWith(Utils.color(PlayerCheat.InventoryName))) {
						final InventoryView inventory = verifier.getOpenInventory();
						for(final Entry<Integer, ItemStack> entry : items.entrySet()) {
							if(entry.getValue() == null || !entry.getValue().equals(inventory.getItem(entry.getKey()))) {
								inventory.setItem(entry.getKey(), entry.getValue());
							}
						}
					} else {
						PlayerCheat.verifier.remove(verifier);
						return;
					}
				}
			}
		}
	}
}
