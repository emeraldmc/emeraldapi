package fr.tristiisch.emeraldmc.api.bungee.session;

import java.util.ArrayList;
import java.util.List;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class SessionShow {

	public static void showSessions(final SessionCommand sessionCommand, final List<Session> sessions) {
		final List<TextComponent> targetAccountsTexts = new ArrayList<>();
		final TextComponent msg = new TextComponent(Utils.color("&3=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n"));
		msg.addExtra(new TextComponent(Utils.colorFix("&6" + targetAccountsTexts.size() + " comptes:")));

		for(final Session session : sessions) {
			ChatColor color;
			final OlympaPlayer emeraldPlayer = MySQL.getPlayer(session.getEmeraldPlayerId());
			if(ProxyServer.getInstance().getPlayer(emeraldPlayer.getUniqueId()) != null) {
				color = ChatColor.GREEN;
			} else {
				color = ChatColor.GRAY;
			}
			final TextComponent targetMsg = new TextComponent(
				Utils.color(color + emeraldPlayer.getName() + " " + session.getStartDate() + " " + session.getStopDate() + " " + session.getDifference()));

			if(sessionCommand.hasPermission(EmeraldGroup.ADMIN)) {
				targetMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Obtenir des infos sur &e" + session.getIpId())).create()));
				targetMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/info " + emeraldPlayer.getIp()));
			} else {
				targetMsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&6Obtenir des infos sur &e" + emeraldPlayer.getName())).create()));
				targetMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/info " + emeraldPlayer.getUniqueId()));
			}
			targetAccountsTexts.add(targetMsg);
		}

		Utils.toTextComponent(msg, targetAccountsTexts, ChatColor.YELLOW + ", ", ChatColor.YELLOW + "\n");
	}
}
