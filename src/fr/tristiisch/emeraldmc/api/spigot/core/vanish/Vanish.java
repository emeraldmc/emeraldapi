package fr.tristiisch.emeraldmc.api.spigot.core.vanish;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.OlympaPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.tablist.TabList;

public class Vanish {

	private static List<Player> vanish = new ArrayList<>();

	public static void enable(final Player player, final boolean showMessage){

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			for(final Player target : Bukkit.getOnlinePlayers()) {
				final OlympaPlayer emeraldTarget = new AccountProvider(target.getUniqueId()).getEmeraldPlayer();
				if(emeraldTarget == null || emeraldTarget.hasPowerLessThan(EmeraldGroup.ADMIN)) {
					Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> target.hidePlayer(player));
				}
			}
			Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
				player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 999999, 0, false, false), true);
				player.spigot().setCollidesWithEntities(false);
				/* GhostUtils.ghostFactory.addPlayer(player); */
			});

			addVanish(player);
			if(showMessage) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &aMode invisible activé."));
			}
			TabList.setToAll();
		});
	}

	public static void disable(final Player player, final boolean showMessage){
		for(final Player players : Bukkit.getOnlinePlayers()) {
			players.showPlayer(player);
		}
		Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
			player.removePotionEffect(PotionEffectType.INVISIBILITY);
			/* GhostUtils.ghostFactory.removePlayer(player); */
			player.spigot().setCollidesWithEntities(true);
		});

		removeVanish(player);
		if(showMessage) {
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cMode invisible désactivé."));
		}
		TabList.setToAll();
	}

	public static boolean isVanish(final Player player){
		return vanish.contains(player);
	}

	public static List<Player> getVanish() {
		return vanish;
	}

	public static void addVanish(final Player player) {
		vanish.add(player);
	}

	public static void removeVanish(final Player player) {
		vanish.remove(player);
	}
}
