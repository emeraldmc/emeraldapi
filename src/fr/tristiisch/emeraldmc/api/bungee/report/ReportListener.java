package fr.tristiisch.emeraldmc.api.bungee.report;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ReportListener implements Listener {

	@EventHandler
	public void PlayerQuitEvent(final PlayerDisconnectEvent event) {
		final ProxiedPlayer player = event.getPlayer();
		Report.remove(player);
	}
}
