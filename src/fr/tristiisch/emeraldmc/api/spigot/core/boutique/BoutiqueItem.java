package fr.tristiisch.emeraldmc.api.spigot.core.boutique;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.core.boutique.menu.clef.BoutiqueClef;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemGlow;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public enum BoutiqueItem {

	CANCEL(0, ItemTools.create(Material.STAINED_GLASS_PANE, 1, (short) 14, "&cAnnuler", "", "&6En cours de développement !", ""), BoutiqueItemInfo.UTILS),
	CLEF(-1, ItemTools.create(Material.TRIPWIRE_HOOK, 1, "&2Clef de la Box", Arrays.asList("", "&bPrix: &3" + BoutiqueClef.PRICE + " minerais"), ItemGlow.getItemGlowEnchant()), BoutiqueItemInfo.CLEF),
	CLEF_MENU(-2, ItemTools.create(Material.CHEST, 1, "&7Clef", "", "&4[&cNOUVEAU&4] &bAchats de clef"), BoutiqueItemInfo.MENU),
	CLEFx10(+1, ItemTools.create(Material.TRIPWIRE_HOOK, 10, "&2Clefs de la Box &c&lx10", Arrays.asList("", "&bPrix: &3" + BoutiqueClef.PRICEx10 + " minerais"), ItemGlow.getItemGlowEnchant()),
			BoutiqueItemInfo.CLEF),
	COMFIRM(0, ItemTools.create(Material.STAINED_GLASS_PANE, 1, (short) 5, "&aConfirmer", "", "&6En cours de développement !"), BoutiqueItemInfo.UTILS),

	COSMETIC(2, ItemTools.create(Material.FIREBALL, 1, "&7Cosmétique", "", "&6En cours de développement !"), BoutiqueItemInfo.MENU),
	VIP(0, ItemTools.create(Material.EMERALD, 1, "&7VIP", "", "&bAchats de grades"), BoutiqueItemInfo.MENU);

	public static List<BoutiqueItem> getItems(final BoutiqueItemInfo boutiqueItemInfo) {
		return Arrays.stream(BoutiqueItem.values()).filter(boutiqueItem -> boutiqueItem.getBoutiqueItemInfo().equals(boutiqueItemInfo)).collect(Collectors.toList());
	}

	private final BoutiqueItemInfo boutiqueItemInfo;
	private final ItemStack itemStack;
	private int slot;

	private BoutiqueItem(final int slot, final ItemStack itemStack, final BoutiqueItemInfo boutiqueItemInfo) {
		this.slot = slot;
		this.itemStack = itemStack;
		this.boutiqueItemInfo = boutiqueItemInfo;
	}

	public BoutiqueItemInfo getBoutiqueItemInfo() {
		return this.boutiqueItemInfo;
	}

	public ItemStack getItemStack() {
		return this.itemStack;
	}

	public int getSlot() {
		return this.slot;
	}
}
